# H@H-AUTO #

A light weighted BDD framework built using java spring to perform  WEB, iOS Native, Android Native and REST API automation.

**Libraries used:**
1. TestNG
2. Cucumber
3. Spring
4. Selenium
5. Appium
6. Rest Assured

**Build Tool:** Maven

### Getting Started ###

### Prerequisites

What you need to install before importing the project.
```
1. Java 11
2. Maven
3. Node
4. npm
5. Appium Desktop and Appium (npm package)
6. Android
7. Xcode
```
### Plugins to be installed in Intellij
```
1. Cucumber for Java
2. Lombok
```
### To run your project
1. Through maven `mvn clean install`
2. Alternatively, you can also run by executing the `testNG.xml`

### Some handy commands;

1. To get device udid: `instruments -s devices`
2. To get the bundle id: `ideviceinstaller -u <udid> -l`
3. Check the device connected and its status: `adb devices`

### Connect your repo to upstream repo:

1. `git remote -v` this will give the current origin.
2. `git remote add upstream https://<git_username>@bitbucket.org/biofourmis/bio-auto.git`, this will add the upstream branch.
3. `git remote -v` to check if the upstream is added.

### To pull from the upstream repo:

##### Make sure you connect to the upstream repo
```
git checkout master
git fetch upstream
git merge upstream/master 
```

Resolve the merge conflicts if any, commit and push.

### Biofourmis libraries
* **bio-xcel** - Inspired by Spring JPA, we have built a very simple library using which one can perfrom CRUD operation on an Excel sheet. Where the excel document is trated as a database and each sheet in the document is treated as a table.
* **bio-graph** - A library to validate if a graph is present in a given area or not. This library can also be used to compare expected and actual screenshots.
* **bio-simulator** - A library to sync Everion & VItalPatch events and vitals.

### Biofourmis Plugins
* **biovitals-maven-plugin**
  * **zephyr-scale-integration:** A plugin that runs on the post integration phase, this plugin sync your results from cucumber to the Zephy scale. It creates it own test cycle and updates the results based on the automation execution.

### References
* Cucumber: https://cucumber.io/docs/guides/overview/
* Spring-test: https://docs.spring.io/spring-framework/docs/current/reference/html/testing.html
* Lombok: https://www.baeldung.com/intro-to-project-lombok
* Java-faker: https://www.baeldung.com/java-faker
* Jackson-databind: https://www.baeldung.com/jackson-object-mapper-tutorial
* Json-path: https://www.baeldung.com/guide-to-jayway-jsonpath
* Web-driver-manager: https://github.com/bonigarcia/webdrivermanager
* Testinium: https://github.com/Testinium/MobileDeviceInfo
* Fillo: https://codoid.com/fillo/
