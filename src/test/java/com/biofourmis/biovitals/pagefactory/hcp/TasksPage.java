package com.biofourmis.biovitals.pagefactory.hcp;

import com.biofourmis.biovitals.utilities.CommonUtils;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.touch.offset.PointOption;
import io.cucumber.java.bs.A;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;
import java.util.List;
import java.util.Map;

public class TasksPage extends CommonUtils {

    private WebDriver driver;

    public TasksPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver, Duration.ofSeconds(5)), this);
    }

    @AndroidFindBy(xpath = "//*[@content-desc=\"Tasks\"]")
    private WebElement tasks;
    @AndroidFindBy(xpath = "//*[@content-desc=\"Patients\"]")
    private WebElement patients;
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/etTaskDesc")
    private WebElement taskDesc;
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/text_input_end_icon")
    private WebElement taskCalendar;
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/etAssignTo")
    private WebElement assignToIcon;
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/btnCreateTask")
    private WebElement create;

    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/etDateTime")
    private WebElement dueDateTimeInout;

    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/confirm_button")
    private WebElement calOk;
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/material_timepicker_ok_button")
    private WebElement timeOk;
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/material_timepicker_cancel_button")
    private WebElement timeCancel;

    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/textview_profile_name")
    private WebElement careTeamMembersList;
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/searchBtn")
    private WebElement searchCare;
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/searchEditText")
    private WebElement searchCareName;
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/etLeaveComment")
    private WebElement inputComment;
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/text_input_end_icon")
    private WebElement sendComment;
    @AndroidFindBy(xpath = "//*[@content-desc=\"Edit\"]")
    private WebElement editTask;
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/btnCreateTask")
    private WebElement updateBtn;
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/btnUpdateTaskStatus")
    private WebElement completeBtn;
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/month_navigation_next")
    private WebElement nextMonth;
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/btnAssign")
    private WebElement assign;
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/tvAssignedTo")
    private WebElement assignedName;
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/tvDueDate")
    private WebElement dueDatValue;

    private By expendIcon(String mrn) {
        return By.xpath("//*[contains(@text,'" + mrn + "')]//ancestor::androidx.cardview.widget.CardView//*[@resource-id='com.biofourmis.careathomehcp:id/ivExpandTask']");
    }

    private By openTasks(String mrn, int count) {
        return By.xpath("//*[contains(@text,'" + mrn + "')]//ancestor::androidx.cardview.widget.CardView//*[contains(@text,'Open (" + count + ")')]");
    }

    private By openTasksValue(String mrn) {
        return By.xpath("//*[contains(@text,'" + mrn + "')]//ancestor::androidx.cardview.widget.CardView//*[contains(@text,'Open')]");
    }

    private By completedTasksValue(String mrn) {
        return By.xpath("//*[contains(@text,'" + mrn + "')]//ancestor::androidx.cardview.widget.CardView//*[contains(@text,'Completed')]");
    }

    private By completedTasks(String mrn, int count) {
        return By.xpath("//*[contains(@text,'" + mrn + "')]//ancestor::androidx.cardview.widget.CardView//*[contains(@text,'Completed (" + count + ")')]");
    }

    private By taskExpendIcon(String mrn) {
        return By.xpath("//*[contains(@text,'" + mrn + "')]//ancestor::androidx.cardview.widget.CardView//*[@resource-id='com.biofourmis.careathomehcp:id/ivAddTask']");
    }

    private By mrn = By.id("com.biofourmis.careathomehcp:id/clPatient");
    private By mrnValue = By.id("com.biofourmis.careathomehcp:id/textview_profile_mrn");

    public void clickOnTasks() throws Exception {
        _waitForElementVisible(tasks);
        _click(tasks);
    }

    public void scrollTillPatient(String mrnValue) throws Exception {
        int loop = 0;
        _sleep(10);
        while (loop++ < 30) {
            _sleep(2);
            List<WebElement> ids = driver.findElements(mrn);

            try {
                boolean flag = driver.findElement(By
                        .xpath("//*[contains(@text,'" + mrnValue + "')]"))
                        .isDisplayed();
                if (flag) {
                    break;
                }
            } catch (Exception e) {
                System.out.println("---");
            }

            new TouchAction((PerformsTouchActions) driver)
                    .press(PointOption.point(ids.get(3).getLocation()))
                    .moveTo(PointOption.point(ids.get(0).getLocation()))
                    .perform()
                    .release();
        }
        if (!_isElementVisible(openTasksValue(mrnValue))) {
            Dimension dimensions = driver.manage().window().getSize();
            int screenWidth = dimensions.getWidth();
            int screenHeight = dimensions.getHeight();


            new TouchAction((PerformsTouchActions) driver)
                    .press(PointOption.point(new Point(screenWidth / 2, screenHeight / 2)))
                    .moveTo(PointOption.point(new Point(screenWidth / 2, 300)))
                    .perform()
                    .release();
        }
    }

    public void addNewTaskDetails(Map<String, String> taskValue, boolean flag) throws Exception {

        if (!flag)
            _sendKeys(taskDesc, taskValue.get("Description"));
        else {
            // Add details for all
        }
    }

    public boolean verifyTaskAddedSuccessFully(String primaryPatientMRN, Map<String, String> details) throws Exception {

        if (!_isElementVisible(patients))
            _pressBack();

        _click(patients);
        _sleep(3);
        _click(tasks);
        _sleep(5);

        scrollTillPatient(primaryPatientMRN);

        _waitForElementVisible(expendIcon(primaryPatientMRN));
        _click(expendIcon(primaryPatientMRN));
        return _isTextContainsPresent(details.get("Description"));
    }

    public void clickOnCreate() throws Exception {
        _waitForElementVisible(create);
        _click(create);
        _waitForElementInvisible(create);
    }

    public boolean verifyCreateButtonEnable() throws Exception {
        try {
            _sleep(3);
            _hideKeyBoard();
            return _isElementEnable(create);
        } catch (Exception e) {
            _hideKeyBoard();
            return _isElementEnable(create);
        }
    }

    public void clickOnAddNewTaskIcon(String mrn) throws Exception {
        _click(taskExpendIcon(mrn));
    }

    public String getOpenTasksValue(String mrn) throws Exception {
        if (!_isElementVisible(openTasksValue(mrn)))
            scrollComments();
        return _getText(openTasksValue(mrn));
    }

    public void viewTask(String description) throws Exception {
        _clickOnTextContains(description);
    }

    public void addComment(String comment) throws Exception {
        _waitForElementVisible(inputComment);
        _sendKeys(inputComment, comment);
        _click(sendComment);
        _sleep(2);
    }

    public boolean verifyCommentAddSuccessfully(String comment) {
        _sleep(2);
        return _isTextContainsPresent(comment);
    }

    public void clickOnEditTask() throws Exception {
        _click(editTask);
    }

    public void updateTaskDescription(String description) throws Exception {
        _waitForElementVisible(taskDesc);
        _sendKeys(taskDesc, description);
        _hideKeyBoard();
    }

    public void updateTask() throws Exception {
        _click(updateBtn);
        //_clickOnTextContains("Update");
        _waitForElementVisible(editTask);
    }

    public boolean verifyUpdatedTaskDescription(String description) {
        _sleep(2);
        return _isTextContainsPresent(description);
    }

    public void completeATask() throws Exception {
        _click(completeBtn);
        // _clickOnTextContains("Complete");
    }

    public void clickOnCompletedTask(String mrn) throws Exception {
        _click(completedTasksValue(mrn));
    }

    public boolean verifyTaskDisplayedUnderCompleted(String description) {
        return _isTextContainsPresent(description);
    }

    public void clickOnOpen(String mrn) throws Exception {
        _click(openTasksValue(mrn));
    }

    public boolean verifyReOpenTask(String description) {
        return _isTextContainsPresent(description);
    }

    public void scrollComments() {
        _sleep(2);

        Dimension dimensions = driver.manage().window().getSize();
        int screenWidth = dimensions.getWidth();
        int screenHeight = dimensions.getHeight();


        new TouchAction((PerformsTouchActions) driver)
                .press(PointOption.point(new Point(screenWidth / 2, screenHeight / 2)))
                .moveTo(PointOption.point(new Point(screenWidth / 2, 300)))
                .perform()
                .release();
    }

    public Map<String, String> addDueDateTime(Map<String, String> details) throws Exception {
        _click(taskCalendar);
        _click(nextMonth);
        _click(calOk);
        _click(timeOk);
        details.put("Due", _getText(dueDateTimeInout));
        return details;
    }

    public void addAssignee(String assignee) throws Exception {
        _click(assignToIcon);
        _waitForElementVisible(searchCare);
        _click(searchCare);
        _waitForElementVisible(searchCareName);
        _sleep(5);
        Actions a = new Actions(driver);
        a.sendKeys(assignee.split(" ")[0]);
        a.perform();
        _sleep(5);
        _clickOnTextContains(assignee);
        _hideKeyBoard();
        _click(assign);
        _waitForElementInvisible(searchCare);
        _sleep(2);
    }

    public String verifyUpdatedAssignee() throws Exception {
        _waitForElementVisible(assignedName);
        return _getText(assignedName);
    }

    public String verifyUpdatedDueDate() throws Exception {
        _waitForElementVisible(dueDatValue);
        return _getText(dueDatValue);
    }
}
