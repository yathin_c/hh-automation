package com.biofourmis.biovitals.pagefactory.hcp;

import com.biofourmis.biovitals.utilities.CommonUtils;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

@Log4j
public class HandOffPage extends CommonUtils {

    private WebDriver driver;

    public HandOffPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/handOffBtn")
    private WebElement handOffBtn;
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/patientSummaryET")
    private WebElement summary;
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/tilPatientSummary")
    private WebElement summartTitle;
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/etOtherInput")
    private WebElement customSituation;
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/synthesisHeading")
    private WebElement synthesisHeading;
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/tilNotes")
    private WebElement synthesisTitile;
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/notesET")
    private WebElement note;
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/noChangesCB")
    private WebElement noChange;
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/btnYes")
    private WebElement btnProceed;

    public void waitForhandOffPageLoad() {
        _waitForProgressBarDisappearHCP();
        _waitForElementVisible(handOffBtn);
    }

    public void selectSeverity(String s) throws Exception {
        _clickOnTextContains(s);
    }

    public void enterSummary(String s) throws Exception {
        //com.biofourmis.careathomehcp:id/tilPatientSummary
        _click(summartTitle);
        _sendKeys(summary,s);
    }

    public void selectSituationalOptions(String s, boolean isPredefined) throws Exception {
        if(isPredefined)
            _clickOnTextContains(s);
        else {
            _swipeUp(1);
            _click(customSituation);
            _sendKeys(customSituation, s);
            _hideKeyBoard();
        }
    }

    public void enetrSynthesis(String s) throws Exception {
       _swipeUp(1);
       _sleep(2);
        _click(synthesisTitile);
        _sendKeys(note,s);
    }

    public void clickOnHandOffbtn() throws Exception {
        _click(handOffBtn);
        _click(btnProceed);
        _waitForElementInvisible(handOffBtn);
    }

    public boolean ishandOffBtnEnable() throws Exception {
        return _isElementEnable(handOffBtn);
    }

    public void selectNoChanges() throws Exception {
        _click(noChange);
        _click(btnProceed);
        _waitForElementInvisible(handOffBtn);
    }

}
