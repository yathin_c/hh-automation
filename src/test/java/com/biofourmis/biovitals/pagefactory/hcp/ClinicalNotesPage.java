package com.biofourmis.biovitals.pagefactory.hcp;

import com.biofourmis.biovitals.utilities.CommonUtils;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class ClinicalNotesPage extends CommonUtils {

    private WebDriver driver;

    public ClinicalNotesPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/tvClinicalNotesTitle")
    private WebElement clinicalNotesTitle;
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/expandable_text")
    private WebElement expandableText;
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/expand_collapse")
    private WebElement expandCollapse;
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/searchIcon")
    private WebElement search;

    public boolean verifyNoteTitleDisplayed(String title) throws Exception {
        _waitForProgressBarDisappearHCP();
        return _isTextContainsPresent(title);
    }

    public boolean verifyNoteDescriptionDisplayed(String description) throws Exception {
        _waitForProgressBarDisappearHCP();
        if (_isTextContainsPresent(description))
            return true;
        else
            _click(expandCollapse);
        return _isTextContainsPresent(description);
    }

    public void navigateBackToPatient() {
        try {
            while (!_isElementVisible(search)) {
                _waitForProgressBarDisappearHCP();
                _pressBack();
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public boolean verifyNoteDeleted(String title) {
        _waitForProgressBarDisappearHCP();
        return !_isTextContainsPresent(title);
    }
}
