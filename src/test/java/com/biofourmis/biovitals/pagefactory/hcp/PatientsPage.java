package com.biofourmis.biovitals.pagefactory.hcp;

import com.biofourmis.biovitals.utilities.CommonUtils;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;

public class PatientsPage extends CommonUtils {

    private WebDriver driver;

    public PatientsPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/searchIcon")
    private WebElement search;
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/searchEditText")
    private WebElement searchInput;
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/menuIcon")
    private WebElement threeDots;
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/tvClinicalNotes")
    private WebElement clinicalNotesOption;
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/tvPatientTasks")
    private WebElement viewTaskOption;
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/tv_handoff_BS")
    private WebElement handOffOption;
    @AndroidFindBy(id = "com.biofourmis.careathomehcp:id/tvAddRemovePatient")
    private WebElement AddRemovePatientOption;

    public void searchPatient(String mrn) throws Exception {
        _waitForProgressBarDisappearHCP();
         _waitForElementVisible(search);
        _sleep(5);
        _click(search);
        _waitForElementVisible(searchInput);
        _sendKeys(searchInput,mrn);
        _waitForProgressBarDisappearHCP();
        // 1442 E WifiVendorHal: getWifiLinkLayerStats(l.947) failed {.code = ERROR_NOT_SUPPORTED, .description = }
    }

    public void openPatientMenu() throws Exception {
        _waitForProgressBarDisappearHCP();
        _click(threeDots);
    }

    public void clickOnMenu(String menu) throws Exception {
        _waitForProgressBarDisappearHCP();
        _clickOnText(menu);
        _waitForProgressBarDisappearHCP();
    }

}
