package com.biofourmis.biovitals.pagefactory.patient;

import com.biofourmis.biovitals.utilities.CommonUtils;
import io.appium.java_client.android.Activity;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.List;

@Log4j
public class TodaysPlanPage extends CommonUtils {
    private WebDriver driver;

    public TodaysPlanPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(xpath = "//*[@resource-id='com.biofourmis.careathomerpm:id/recyclerview_todays_calendar']//android.view.ViewGroup")
    List<WebElement> todayCalendar;
    @AndroidFindBy(xpath = "//*[@content-desc=\"Today's plan\"]")
    private WebElement todayPlan;
    @AndroidFindBy(xpath = "//*[@content-desc=\"My health\"]")
    private WebElement myHealth;

    public boolean verifyCarePlanDisplayed(String carePlan) throws Exception {
        _waitForLoading();
        _waitForElementVisible(myHealth);
        _click(myHealth);
        _waitForLoading();
        _waitForElementVisible(todayPlan);
        _click(todayPlan);
        _waitForLoading();
        _sleep(2);
        return _isTextContainsPresent(carePlan);
    }

    public boolean isActivityDisplayed(String arg0) throws Exception {
        _waitForLoading();
        _click(myHealth);
        _waitForLoading();
        _click(todayPlan);
        _waitForLoading();
        _sleep(2);
        return _isTextContainsPresent(arg0);
    }

    public String getActivityDetails(String arg0) {
        for (WebElement element : todayCalendar) {
            String text = element.findElement(By.xpath("//*[@resource-id='com.biofourmis.careathomerpm:id/textview_calendar_action_name']")).getText();
            if (text.contains(arg0))
                return element.findElement(By.xpath("//*[@resource-id='com.biofourmis.careathomerpm:id/textview_plan_time']")).getText().strip();
        }
        Assert.fail("Activity not find in calendar");
        return null;
    }

    public void navigateToTodays() throws Exception {
        try {
            while (!_isElementVisible(todayPlan)) {
                _pressBack();
                _sleep(2);
            }
            _click(todayPlan);
        }catch (Exception e)
        {
            AndroidDriver androidDriver = (AndroidDriver<WebElement>)driver;
            androidDriver.startActivity(new Activity(
                    androidDriver.getCapabilities().getCapability("appPackage").toString(),
                    androidDriver.getCapabilities().getCapability("appActivity").toString()));
            _sleep(10);
        }
}
}
