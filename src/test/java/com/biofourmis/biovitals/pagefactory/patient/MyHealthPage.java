package com.biofourmis.biovitals.pagefactory.patient;

import com.biofourmis.biovitals.utilities.CommonUtils;
import com.google.common.collect.ImmutableMap;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidBy;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.time.Duration;
import java.util.List;

public class MyHealthPage extends CommonUtils {

    private WebDriver driver;

    public MyHealthPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/button_enter_vitals")
    private WebElement enterVitalsBtn;

    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/image_close_popup")
    private WebElement closePopUpIcon;

    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/text_title")
    private WebElement popUpTitle;

    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/textview_input_unit")
    private WebElement vitalUnit;

    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/textview_positive")
    private WebElement submit;

    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/textview_negative")
    private WebElement cancel;

    private By vitalUnits = By.id("com.biofourmis.careathomerpm:id/textview_input_unit");

    private By sendValueIn(String name) {
        return By.xpath("//*[contains(@text,'" + name + "')]/..//*[contains(@resource-id,'com.biofourmis.careathomerpm:id/edittext_input_value')]");
    }

    public void navigateToMyHealthTab() throws Exception {
        _clickOnTextContains(_getString("text.my.health"));
    }

    public boolean verifyEnterVitalButtonDisplayed() throws MalformedURLException {
        return _isTextContainsPresent(_getString("enter.vitals"));
    }

    public void clickOnEnterVitalsBtn() throws Exception {
        _clickOnTextContains(_getString("enter.vitals"));
    }

    public boolean isOptionDisplayedOnPopUp(String getString) {
        return _isTextContainsPresent(getString);
    }

    public void clickOnVital(String getString) throws Exception {
        _clickOnTextContains(getString);
    }

    public boolean isClosePopUpIconDisplayed() throws Exception {
        return _isElementVisible(closePopUpIcon);
    }

    public void clickOnClosePopUp() throws Exception {
        _click(closePopUpIcon);
    }

    public String verifyToastMessage() {
        return _getToastMessage(driver);
    }

    public void sendValueIn(String name, String value) throws Exception {
        _sendKeys(sendValueIn(name),value);
    }

    public void clickOnSubmit() throws Exception {
        _click(submit);
    }

    public void clickOnCancel() throws Exception {
        _click(cancel);
    }

    //Check oxygen level
    //Please input your oxygen level in the box below
    //%
    //com.biofourmis.careathomerpm:id/textview_input_unit
    //com.biofourmis.careathomerpm:id/edittext_input_value

    //Check temperature
    //Please input your temperature in the box below
    //°F

    //Check blood glucose
    //Please input your blood glucose in the box below
    //mg/dL
}
