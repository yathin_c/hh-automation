package com.biofourmis.biovitals.pagefactory.patient;

import com.biofourmis.biovitals.utilities.CommonUtils;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.util.Map;

@Log4j
public class LoginPage extends CommonUtils {

    private WebDriver driver;
    public LoginPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/textview_scan_button")
    private WebElement scanBtn;
    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/textview_title_scan_qr_description")
    private WebElement biovitalIcon;
    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/btnProceed")
    private WebElement proceed;
    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/etPatientId")
    private WebElement patientId;
    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/etRole")
    private WebElement role;
    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/etQRHash")
    private WebElement qr;
    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/etAppId")
    private WebElement appId;
    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/textview_message_loading")
    private WebElement loading;
    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/ok_btn")
    private WebElement okBtn;
    @AndroidFindBy(id = "com.android.permissioncontroller:id/permission_allow_foreground_only_button")
    private WebElement allowLocation;
    @AndroidFindBy(id = "com.android.permissioncontroller:id/allow_always_radio_button")
    private WebElement alwaysAllow;
    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/textview_done")
    private WebElement done;
    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/textview_header_dialog")
    private WebElement confirmLogout;
    @AndroidFindBy(xpath = "//*[contains(@text,'While using the app')]")
    private WebElement whileUsingTheApp;
    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/textview_skip_photo")
    private WebElement skipProfilePic;

    public void login(Map<String, String> qrHashDetails) throws Exception {
        _hideKeyBoard();
        _waitForElementVisible(biovitalIcon);
        clickOnBiovitalIcon();
        _waitForElementVisible(patientId);
        _waitForElementVisible(proceed);
        _sendKeys(patientId, qrHashDetails.get("id"));
        _sendKeys(role, qrHashDetails.get("role"));
        _sendKeys(qr, qrHashDetails.get("qrHash"));
        _sendKeys(appId, qrHashDetails.get("appId"));
        _click(proceed);
        _waitForElementInvisible(loading);
        _waitForLoading();
    }

    public void clickOnBiovitalIcon() {
        try {
            biovitalIcon.click();
            biovitalIcon.click();
            biovitalIcon.click();
            biovitalIcon.click();
            biovitalIcon.click();
            biovitalIcon.click();
        } catch (Exception e) {
            _sleep(2);
            if (_waitForElementVisible(scanBtn)) {
                biovitalIcon.click();
                biovitalIcon.click();
                biovitalIcon.click();
                biovitalIcon.click();
                biovitalIcon.click();
                biovitalIcon.click();
            }
        }
    }

    public boolean verifyLoginSuccessfully() {
        return _isElementVisible(scanBtn);
    }

    public void acceptLoginPermissions() throws Exception {
        if (_isElementVisible(whileUsingTheApp))
            _click(whileUsingTheApp);
        if (_isElementVisible(skipProfilePic))
            _click(skipProfilePic);
        if (_isElementVisible(okBtn))
            _click(okBtn);
        if (_isElementVisible(allowLocation))
            _click(allowLocation);
        if (_isElementVisible(allowLocation))
            _click(allowLocation);
        if (_isElementVisible(okBtn))
            _click(okBtn);
        if (_isElementVisible(alwaysAllow))
            _click(alwaysAllow);
        if (!_isElementVisible(done))
            _pressBack();
    }

    public void acceptPermissions() throws Exception {
        if (_isElementVisible(whileUsingTheApp))
            _click(whileUsingTheApp);
        if (_isElementVisible(okBtn))
            _click(okBtn);
        if (_isElementVisible(allowLocation))
            _click(allowLocation);
        if (_isElementVisible(allowLocation))
            _click(allowLocation);
        if (_isElementVisible(okBtn))
            _click(okBtn);
        if (_isElementVisible(alwaysAllow))
            _click(alwaysAllow);
        if (!_isElementVisible(done))
            _pressBack();
    }

    public void acceptCallPermissions() throws Exception {
        if (_isElementVisible(whileUsingTheApp))
            _click(whileUsingTheApp);
    }

    public void clickOnDone() throws Exception {
        _click(done);
    }

    public boolean isTodayPlanDisplayed() {
        return _isTextContainsPresent("Today's plan");
    }

    public boolean logOut() throws Exception {
        _clickOnTextContains("Log out");
        _waitForElementVisible(confirmLogout);
        _clickOnTextContains("Yes, log out");
        _waitForElementInvisible(loading);
        _waitForLoading();
        return _waitForElementVisible(scanBtn);
    }
}
