package com.biofourmis.biovitals.pagefactory.patient;

import com.biofourmis.biovitals.utilities.CommonUtils;
import com.biofourmis.biovitals.utilities.CustomUtils;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

@Log4j
public class SettingsPage extends CommonUtils {
    private WebDriver driver;

    public SettingsPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(id = "com.biofourmis.careathomerpm:id/cbLanguageSelector")
    private List<WebElement> languages;

    public void navigateToHiddenSettings() throws Exception {
        CustomUtils customUtils = new CustomUtils(driver);
        _sleep(5);
        customUtils.clickOnHeaderToOpenHiddenTab(driver);
    }

    public boolean verifySettingsOptions(String value) {
        return _isTextContainsPresent(value);
    }

    public boolean verifyAssignedDevice() throws Exception {
        _clickOnTextContains("Assigned devices");
        return _isTextContainsPresent("No devices connected yet, please connect devices to view them.");
    }

    public boolean verifyNewDevices() throws Exception {
        _clickOnTextContains("New devices");
        return _isTextContainsPresent("Make sure your device is turned on in range and ready to connect")
                && _isTextContainsPresent("Connect");
    }

    public boolean verifyVitalPatchCalibration() throws Exception {
        _clickOnTextContains("VitalPatch® calibration");
        return _isTextContainsPresent("VitalPatch® not found") &&
                _isTextContainsPresent("Please check device connection and try to calibrate again.");
    }

    public boolean verifyLanguage() throws Exception {
        _clickOnTextContains("Language");
        if (languages.size() == 0)
            return false;

        return _isTextContainsPresent("Set language");
    }
}
