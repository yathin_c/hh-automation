package com.biofourmis.biovitals.pagefactory.web;

import com.biofourmis.biovitals.utilities.CommonUtils;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.Map;

@Log4j
public class TasksPage extends CommonUtils {

    private WebDriver driver;

    public TasksPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//*[@type=\"task-ic\"]")
    private WebElement taskIcon;
    @FindBy(xpath = "//*[@type=\"ic-close\"]")
    private WebElement closeIcon;
    @FindBy(xpath = "//*[contains(@class,\"patient-task\")]//*[contains(text(),'New task')]")
    private WebElement newTask;
    @FindBy(id = "description")
    private WebElement description;
    @FindBy(xpath = "//*[@placeholder=\"Leave a comment\"]")
    private WebElement commentInput;
    @FindBy(xpath = "//*[contains(@class,\"comment-input\")]//*[@type='send']")
    private WebElement sendComment;
    @FindBy(xpath = "//*[@type=\"ic-date\"]")
    private WebElement calendarIcon;

    @FindBy(xpath = "//*[@aria-label=\"Add a minute\"]")
    private WebElement addOneMinute;
    @FindBy(xpath = "//*[@aria-label=\"Minus a minute\"]")
    private WebElement minusOneMinute;
    @FindBy(xpath = "//*[@formcontrolname=\"assignedTo\"]")
    private WebElement assigneeTo;
    @FindBy(xpath = "//*[@formcontrolname=\"assignedTo\"]//input")
    private WebElement assigneeToInput;
    @FindBy(xpath = "//*[@title=\"Clear all\"]")
    private WebElement clearAssignee;
    @FindBy(xpath = "//*[@type=\"ic-close-dark\"]")
    private WebElement clearDate;
    @FindBy(xpath = "//*[@formcontrolname='dueDate']/..//div")
    private WebElement dueDate;
    @FindBy(xpath = "//*[contains(@class,\"completed-tasks\")]//*[contains(text(),'Completed')]")
    private WebElement completed;

    public void clickOnTaskIcon() throws Exception {
        _waitForPageLoad();
        _pageWait(taskIcon);
        _click(taskIcon);
        Assert.assertTrue(_waitForElementVisible(newTask), "Task container not open.");
    }

    public void clickOnNewTask() throws Exception {
        _click(newTask);
    }

    public void inputTaskDescription(String string) throws Exception {
        _waitForElementVisible(description);
        _sendKeys(description, string);
    }

    public void createTask() throws Exception {
        _clickOnTextContains("Create");
    }

    public boolean verifyCreatedTaskSuccessfully(String description) {
        return _isTextContainsPresent(description);
    }

    public boolean verifyCloseIcon() throws Exception {
        _click(closeIcon);
        return _waitForElementInvisible(closeIcon);
    }

    public boolean verifyCloseIcon(String description) throws Exception {
        _click(closeIcon);
        if (_waitForElementInvisible(closeIcon)) {
            return !_isTextContainsPresent(description);
        } else
            return false;
    }

    public boolean verifyCancelButton(String description) throws Exception {
        _clickOnTextContains("Cancel");
        _waitForPageLoad();
        _pageWait(newTask);
        return _isTextContainsPresent(description);
    }

    public void addComment(String comment) throws Exception {
        _sendKeys(commentInput, comment);
        _click(sendComment);
    }

    public boolean verifyAddedComment(String comment) {
        return _isTextContainsPresent(comment);
    }

    public void viewTask(String description) throws Exception {
        _waitForPageLoad();
        _sleep(10);

        _scrollToElement(driver.findElement(By.xpath("//*[contains(text(),'" + description + "')]")));
        _sleep(20);
        Assert.assertTrue(_isTextContainsPresent(description), "Created task with description [" + description + "] not found.");
        _clickOnTextContains(description);
    }

    public void clickOnEditTask() throws Exception {
        _clickOnTextContains("Edit");
    }

    public void updateDescription(String description) throws Exception {
        _waitForElementVisible(this.description);
        _sendKeys(this.description, description);
    }

    public void clickOnUpdate() throws Exception {
        _clickOnText("Update");
    }

    public boolean verifyUpdatedTaskDescription(String description) {
        return _isTextContainsPresent(description);
    }

    public boolean VerifyClickOnBack() throws Exception {
        _clickOnTextContains("Back");
        return _waitForElementVisible(newTask);
    }

    public void completeTask() throws Exception {
        _sleep(5);
        _clickOnText("Complete");
    }

    public void clickOnCompletedTasks() throws Exception {
        _waitForPageLoad();
        _waitForElementVisible(completed);
        _click(completed);
    }

    public void clickOnReOpen() throws Exception {
        _sleep(5);
        _clickOnText("Reopen");
    }

    public boolean verifyTaskReopened() {
        return _isTextPresent("Complete");
    }

    public Map<String, String> inputOptionalFields(Map<String, String> taskValue) throws Exception {
        _click(calendarIcon);
        _waitForElementVisible(addOneMinute);
        _click(addOneMinute);
        _click(minusOneMinute);
        _click(addOneMinute);
        _click(addOneMinute);
        _click(addOneMinute);
        _clickOnText("Set");
        _sleep(5);
        if (_isTextPresent("Set")) {
            _clickOnText("Set");
        }
        _sleep(5);
        String due = _getText(dueDate);
        taskValue.put("DueDate", due);

        _waitForElementVisible(assigneeTo);
        _click(assigneeTo);
        _sendKeys(assigneeToInput, taskValue.get("Assignee"));
        _sendKeys(assigneeToInput, Keys.ENTER);
        return taskValue;
    }

    public void removeOptionalFields() throws Exception {
        _waitForElementVisible(clearDate);
        _click(clearDate);
        _click(clearAssignee);
    }

    private By time(String desc, String value) {
        return By.xpath("//*[text()='" + desc + "']/..//*[contains(text(),'" + value + "')]");
    }

    public boolean verifyOptionalFieldsValue(Map<String, String> taskValue) {
        String desc = taskValue.get("Description");
        String dueDate = taskValue.get("DueDate");
        String assignee = taskValue.get("Assignee");
        String time = dueDate.split(",")[1];

        Assert.assertTrue(_isElementVisible(time(desc, assignee)),
                "Assignee name not matched.");
        Assert.assertTrue(_isElementVisible(time(desc, "Today," + time)),
                "Created time not matched.");
        return true;
    }

    public void searchForPatientInTasks(String mrn) {
        _waitForPageLoad();
        _scrollToElement(driver.findElement(By.xpath("//*[contains(text(),'" + mrn + "')]")));
        _waitForPageLoad();
    }

    private By newTask(String mrn) {
        return By.xpath("//*[contains(text(),'" + mrn + "')]//ancestor::div[contains(@class,'patient-task') and contains(@class,'pb-o')]//*[contains(text(),'New task')]");
    }

    private By completeTask(String mrn) {
        return By.xpath("//*[contains(text(),'" + mrn + "')]//ancestor::div[contains(@class,'patient-task') and contains(@class,'pb-o')]//*[contains(text(),'Completed')]");
    }

    public void clickOnNewTaskOnTasks(String mrn) throws Exception {
        _smartClick(newTask(mrn));
    }

    public void clickOnCompletedTasksTab(String mrn) throws Exception {
        _waitForPageLoad();
        _smartClick(completeTask(mrn));
    }
}
