package com.biofourmis.biovitals.pagefactory.web;

import com.biofourmis.biovitals.utilities.CommonUtils;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

@Log4j
public class HomePage extends CommonUtils {

    private WebDriver driver;

    public HomePage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//*[@type='ic-hamburger']")
    private WebElement hamburger;

    @FindBy(xpath = "//*[@class='z-10 open']")
    private WebElement openHamburger;

    @FindBy(xpath = "//*[@class='z-10 open']//ul//li")
    private List<WebElement> navMenu;

    @FindBy(xpath = "//*[@class='patients-list-title ml-24']")
    private WebElement patientList;

    @FindBy(xpath = "//*[contains(@class,'h-title')]")
    private WebElement otherTitle;

    @FindBy(xpath = "//*[contains(@class,'register-patient')]")
    private WebElement appPatientPage;

    @FindBy(id = "add-patient")
    private WebElement addPatient;

    @FindBy(xpath = "//*[contains(@class,'main-header-bg')]//*[contains(@src,'logo_biofourmis.svg')]")
    private WebElement homeIcon;

    @FindBy(id = "filterPatient")
    private WebElement searchPatient;

    @FindBy(xpath = "//*[@type='chevron-down']//img")
    private WebElement userDetailsIcon;

    @FindBy(xpath = "//*[@class='qr-code-image']")
    private WebElement qrCode;

    private By lhnMenu(String value) {
        return By.xpath("//*[@class='z-10 open']//ul//li//*[contains(text(),'" + value + "')]");
    }

    private By selectPatient(String value) {
        return By.xpath("//*[contains(@class,'patient-details-container')]//*[contains(text(),\"" + value + "\")]");
    }

    public void clickOnHamburger() throws Exception {
        if (!_isElementPresent(openHamburger))
            _click(hamburger);
    }

    public void verifyLhnMenu(List<String> siteMenu) {
        List<String> availableMenu = new ArrayList<>();
        _waitForPageLoad();
        for (WebElement element : navMenu)
            availableMenu.add(element.getText());
        Assert.assertEquals(siteMenu, availableMenu, "LHN menus not matched.");
    }

    public void navigateTo(String menu) {
        _waitForPageLoad();
        for (WebElement element : navMenu)
            if (element.getText().strip().equalsIgnoreCase(menu)) {
                element.click();
                return;
            }
    }

    public boolean verifyRespectivePage(String menu) throws Exception {
        clickOnHamburger();
        _click(lhnMenu(menu));
        _waitForPageLoad();
        if (menu.toLowerCase(Locale.ROOT).trim().contains("patient")
                || menu.toLowerCase(Locale.ROOT).trim().contains("discharged"))
            return Pattern.matches(menu + " \\([0-9\\(\\)]+\\)", _getText(patientList));
        else if (menu.toLowerCase(Locale.ROOT).trim().contains("task")
                || menu.toLowerCase(Locale.ROOT).trim().contains("inventory"))
            return _getText(otherTitle).equals(menu);
        else
            throw new RuntimeException("This menu [" + menu + "] support is not added yet.");
    }

    public void goToHomePage() throws Exception {
        driver.navigate().refresh();
        _sleep(5);
        _waitForPageLoad();
        _waitForElementVisible(homeIcon);
        _sleep(5);
        try
        {
            _click(homeIcon);
        }
        catch (Exception e)
        {
            _sleep(20);
            _click(homeIcon);
        }
        _waitForPageLoad();
    }

    public boolean verifyAddPatientButton() throws Exception {
        return _isElementVisible(addPatient) && _isElementEnable(addPatient);
    }

    public boolean clickOnAddPatient() throws Exception {
        try {
            _waitForPageLoad();
            _click(addPatient);
            _waitForPageLoad();
            return _isElementPresent(appPatientPage);
        } catch (Exception e) {
            return false;
        }
    }

    public void searchPatient(String arg0) throws Exception {
        _waitForPageLoad();
        _waitForLoading();
        _waitForElementVisible(searchPatient);
        _sendKeys(searchPatient, arg0);
    }

    public void searchPatientWithoutWait(String arg0) throws Exception {
        _sendKeys(searchPatient, arg0);
    }

    public boolean isPatientPresent(String mrn) throws Exception {
        _waitForPageLoad();
        return _isTextPresent(" There's no exact match for your search ");
    }

    public void clickOnPatient(String arg0) throws Exception {
        _click(selectPatient(arg0));
    }

    public String getQRHash() throws Exception {
        //_waitForPageLoad();
        _pageWait(userDetailsIcon);
        _smartClick(userDetailsIcon);
        //_waitForPageLoad();
        _waitForElementInvisible(userDetailsIcon);
        _clickOnTextContains("View QR code");
        return readQRUrl();
    }

    public String readQRUrl() throws Exception {
        _waitForPageLoad();
        _pageWait(qrCode);
        return _getAttributeValue(qrCode, "src");
    }

    public void selectLHNMenu(String menu) throws Exception {
        _clickOnTextContains(menu);
    }

    public boolean isAnyPatientAdded() throws Exception {
        return !Pattern.matches("My patients" + " \\([0\\(\\)]+\\)", _getText(patientList));
    }
}
