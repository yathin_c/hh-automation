package com.biofourmis.biovitals.pagefactory.web;

import com.biofourmis.biovitals.utilities.CommonUtils;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.List;
import java.util.Map;

@Log4j
public class ClinicalNotesPage extends CommonUtils {
    private WebDriver driver;

    public ClinicalNotesPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    private By newNoteBtn = By.xpath("//*[contains(text(),'+ New clinical note')]//parent::button");

    @FindBy(xpath = "//*[contains(@class,'no-notes')]//*[contains(text(),' + New clinical note')]")
    private WebElement newNoteBtnWhenNoNotes;
    @FindBy(xpath = "//*[@formcontrolname=\"title\"]")
    private WebElement title;
    @FindBy(xpath = "//*[@formcontrolname=\"description\"]")
    private WebElement description;
    private By create = By.xpath("//*[text()='Create']//parent::button");
    @FindBy(xpath = "//*[@placeholder=\"Search\"]")
    private WebElement search;
    @FindBy(xpath = "//*[contains(@src,'edit')]/..//*[contains(@class,'cursor-pointer')]")
    private WebElement editNote;
    @FindBy(xpath = "//*[contains(@src,'trash')]/..//*[contains(@class,'cursor-pointer')]")
    private WebElement deleteNote;
    @FindBy(id = "cancel")
    private WebElement cancelPop;
    @FindBy(id = "confirm")
    private WebElement confirmPop;
    @FindBy(xpath = "//*[contains(text(),'Select care team members')]/..//*[@placeholder=\"Search\"]")
    private WebElement clinicianSearch;
    @FindBy(xpath = "//*[contains(@class,'search-input')]//*[@placeholder=\"Search\"]")
    private WebElement searchPlaceHolderImput;
    @FindBy(xpath = "//*[contains(@class,\"close-search\")]")
    private WebElement crossSearch;
    @FindBy(xpath = "//*[contains(@class,'search-input')]//*[@placeholder=\"Search\"]")
    private WebElement searchPlaceHolderFilter;
    @FindBy(id = "calendarBtn")
    private WebElement calendarFilter;

    private By applyFilter = By.xpath("//*[contains(text(),'Apply filter')]//parent::button");
    private By resetFilter = By.xpath("//*[contains(text(),'Reset filter')]//parent::button");

    private By crNameInNote(String name) {
        return By.xpath("//*[contains(@class,'note-card')]//*[contains(text(),'" + name + "')]");
    }

    private By careGiver(String care) {
        return By.xpath("//*[contains(text(),'" + care + "')]//ancestor::*[contains(@class,'user-card')]");
    }

    public void clickOnClinicalNotes() throws Exception {
        _sleep(5);
        if (!_isTextContainsPresent("Clinical notes"))
            _sleep(10);
        _clickOnTextContains("Clinical notes");
    }

    public boolean verifyNewClinicalNotButtonWhenNoNote() {
        return _isElementVisible(newNoteBtnWhenNoNotes);
    }

    public void clickOnAddNewNote(){
        _waitForPageLoad();
        List<WebElement> button = driver.findElements(newNoteBtn);

        for (WebElement el : button) {
            try {
                el.click();
                return;
            } catch (Exception e) {
                //Just Chill
            }
        }
    }

    public boolean verifyNewClinicalNoteHeading() {
        return _isElementVisible(By.xpath("//*[contains(@class,'heading truncate')]//*[contains(text(),'New clinical note')]"));
    }

    public boolean verifyErrorMessage(String titleMsg, String descMsg) throws Exception {
        _click(title);
        _click(description);
        _click(title);
        return _isTextContainsPresent(titleMsg)
                && _isTextContainsPresent(descMsg);
    }

    public boolean createButtonDisable() throws Exception {
        return _isElementEnable(create);
    }

    public void addTitle(String details) throws Exception {
        _sendKeys(title, details);
    }

    public void addDescription(String details) throws Exception {
        _sendKeys(description, details);
    }

    public boolean verifyCreatedNote(String details) {
        return _isTextContainsPresent(details);
    }

    public void searchForNote(String value) throws Exception {
        _waitForElementVisible(search);
        _clear(search);
        _sendKeys(search, value);
    }

    public void updateNoteTitle(Map<String, String> updateNoteTitle) throws Exception {
        _click(title);
        _clear(title);
        _sendKeys(title, updateNoteTitle.get("Title"));
    }

    public void updateNoteDescription(Map<String, String> defaultClinicalNotesDetails) throws Exception {
        _click(description);
        _clear(description);
        _sendKeys(description, defaultClinicalNotesDetails.get("Description"));
    }

    public void deleteNote() throws Exception {
        _smartClick(deleteNote);
    }

    public boolean verifyDeletePopUP() {
        return _isTextContainsPresent("Delete this note?") &&
                _isTextContainsPresent("Yes, delete") &&
                _isTextContainsPresent("No, do not delete");
    }

    public void performCancelPopUp(boolean isAccept) throws Exception {
        if (isAccept)
            _click(confirmPop);
        else
            _click(cancelPop);
    }

    public boolean verifyNewClinicalNoteText(String value) {
        return _isTextContainsPresent(value);
    }

    public boolean isCreateBtnEnable() throws Exception {
        return _isElementEnable(create);
    }

    public void clickOnCreateBtn() throws Exception {
        _click(create);
    }

    public boolean verifyToastMessage(String clinical_note_created_successfully) {
        return _isTextContainsPresent(clinical_note_created_successfully, true);
    }

    public void clickOnSave() throws Exception {
        _clickOnTextContains("Save");
        Assert.assertTrue(verifyToastMessage("Clinical note edited successfully"),
                "[Clinical note edited successfully] toast message should be displayed.");
    }

    public void clickOnEditIcon() throws Exception {
        _click(editNote);
    }

    public boolean verifyCareGiver(String clinicianName) {
        return _isTextContainsPresent(clinicianName);
    }

    public boolean verifySearchInputOptions() throws Exception {
        _clear(search);
        return _isElementVisible(searchPlaceHolderImput);
    }

    public void inputValueInSearchField(String title) throws Exception {
        _sendKeys(searchPlaceHolderImput, title);
    }

    public boolean verifyCrossIconAfterEnterValueInField() {
        return _isElementVisible(crossSearch);
    }

    public boolean verifySearchedNoteDisplayed(String title) {
        return _isTextContainsPresent(title);
    }

    public void clickOnCrossBtn() throws Exception {
        _click(crossSearch);
    }

    public boolean verifyRandomSearchmessage(String value) {
        return _isTextContainsPresent(value);
    }

    public void clickOnCalendarFilter() throws Exception {
        _click(calendarFilter);
    }

    public boolean isOptionDisplayed(String current_day) {
        return _isTextContainsPresent(current_day);
    }

    public void clickOnOption(String current_day) throws Exception {
        _clickOnTextContains(current_day);
    }

    public boolean isNoteDisplayed(String title) {
        return _isTextContainsPresent(title);
    }

    public void clickOnFilter() throws Exception {
        _clickOnTextContains("Filter");
    }

    public void clickOnFilterSearch() throws Exception {
        _click(clinicianSearch);
    }

    public void searchClinician(String clinicianName) throws Exception {
        _sendKeys(clinicianSearch, clinicianName);
    }

    public void selectClinician(String clinicianName) throws Exception {
        _click(careGiver(clinicianName));
    }

    public void clickOnApplyFilter() throws Exception {
        _click(applyFilter);
        _waitForLoading();
    }

    public void clickOnResetFilter() throws Exception {
        _click(resetFilter);
        _waitForLoading();
    }

    public void clickOnClearAll() throws Exception {
        _clickOnTextContains("Clear all");
        _waitForLoading();
    }

    public boolean isClinicianNameDisplayed(String name) {
        return _isElementVisible(crNameInNote(name));
    }

    public boolean isResetFilterDisplayed() {
        return _isElementVisible(resetFilter);
    }

    public boolean verifyEditIconDisplayed() {
        return _isElementVisible(editNote);
    }

    public boolean verifyDeleteIconDisplayed() {
        return _isElementVisible(deleteNote);
    }
}
