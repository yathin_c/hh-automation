package com.biofourmis.biovitals.pagefactory.web;

import com.biofourmis.biovitals.utilities.CommonUtils;
import com.github.javafaker.Faker;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.time.LocalDateTime;
import java.util.*;

@Log4j
public class AddPatientPage extends CommonUtils {
    private WebDriver driver;
    private Faker faker;

    public AddPatientPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        faker = new Faker();
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//*[text()=' Patient information ']")
    private WebElement addPatientTitle;
    @FindBy(id = "mrnid")
    private WebElement mrn;
    @FindBy(id = "main-next-finish")
    private WebElement next;
    @FindBy(id = "firstname")
    private WebElement fName;
    @FindBy(id = "lastname")
    private WebElement lName;
    @FindBy(xpath = "//*[@id='Mgenderid']//parent::label")
    private WebElement male;
    @FindBy(xpath = "//*[@id='Fgenderid']//parent::label")
    private WebElement female;
    @FindBy(xpath = "//*[@id='Othergenderid']//parent::label")
    private WebElement otherGender;
    @FindBy(id = "dobid")
    private WebElement dob;
    @FindBy(id = "addr1id")
    private WebElement addr1id;
    @FindBy(id = "addr2id")
    private WebElement addr2id;
    @FindBy(id = "zipid")
    private WebElement zip;
    @FindBy(id = "cityid")
    private WebElement city;
    @FindBy(id = "stateid")
    private WebElement state;
    @FindBy(id = "mobileid")
    private WebElement mobile;
    @FindBy(id = "landlineid")
    private WebElement landline;
    @FindBy(id = "languageid")
    private WebElement language;
    @FindBy(xpath = "//*[@formcontrolname='phoneNumber']")
    private WebElement familyPhone;
    @FindBy(xpath = "//*[@formcontrolname='familyRelationship']")
    private WebElement familyRelation;
    @FindBy(id = "basic-finish")
    private WebElement cancel;
    @FindBy(id = "mrnPatternInfo")
    private WebElement mrnPatternInfo;
    @FindBy(id = "mrnRequiredInfo")
    private WebElement mrnRequired;
    @FindBy(xpath = "//*[@formcontrolname='kitContents']")
    private WebElement selectDevices;
    @FindBy(xpath = "//*[@formcontrolname=\"addressLine1\"]")
    private WebElement equipmentAddressLine1;
    @FindBy(xpath = "//*[@formcontrolname=\"addressLine2\"]")
    private WebElement equipmentAddressLine2;
    @FindBy(xpath = "//*[@formcontrolname=\"zipCode\"]")
    private WebElement equipmentZipCode;
    @FindBy(xpath = "//*[@formcontrolname=\"city\"]")
    private WebElement equipmentCity;
    @FindBy(xpath = "//*[@formcontrolname=\"state\"]")
    private WebElement equipmentState;
    @FindBy(xpath = "//*[@formcontrolname=\"enrollmentType\"]")
    private WebElement enrollmentType;
    @FindBy(xpath = "//*[@formcontrolname=\"sendEmail\"]")
    private WebElement sendEmail;
    @FindBy(xpath = "//*[@formcontrolname=\"firstName\"]")
    private WebElement point_F_Name;
    @FindBy(xpath = "//*[@formcontrolname=\"lastName\"]")
    private WebElement point_L_Name;
    @FindBy(xpath = "//*[@formcontrolname=\"mobileNumber\"]")
    private WebElement point_number;
    @FindBy(id = "cancel")
    private WebElement cancelAddPatientPop;
    @FindBy(id = "confirm")
    private WebElement confirmAddPatientPop;
    @FindBy(id = "add-patient")
    private WebElement addPatient;
    @FindBy(id = "filterPatient")
    private WebElement searchPatient;
    @FindBy(id = "contactprecautionsid")
    private WebElement contactprecautionsid;
    @FindBy(id = "admittingdiagnosisid")
    private WebElement admittingdiagnosisid;
    @FindBy(id = "clinicalsummaryid")
    private WebElement clinicalsummary;
    @FindBy(xpath = "//*[text()='Phone number']")
    private WebElement personalPhone;
    @FindBy(xpath = "//*[@class='qr-code-image']")
    private WebElement qrCode;

    private By selectCodeAndAlergies(String value) {
        return By.xpath("//*[@value='" + value + "']//parent::label");
    }

    public By verifyReviewPage(String header, String value) {
        return By.xpath("//*[contains(text(),\"" + header + "\")]/..//*[contains(text(),\"" + value + "\")]");
    }

    public By verifyReviewPage(String header) {
        return By.xpath("//*[contains(text(),\"" + header + "\")]/..//*[contains(@class,'frm-value')]");
    }

    private By isChecked(String menu) {
        return By.xpath("//*[contains(text(),'" + menu + "')]/..//*[@type='check-mark']");
    }

    private By selectPhysician(String name) {
        return By.xpath("//*[contains(text(),'Physicians')]/..//*[contains(text(),'" + name + "')]");
    }

    private By selectNursesParamedics(String name) {
        return By.xpath("//*[contains(text(),'Nurses and Paramedics')]/..//*[contains(text(),'" + name + "')]");
    }

    private By selectLanguage(String value) {
        return By.xpath("//*[@id='languageid']//*[contains(@class,'options')]//*[contains(text(),'" + value + "')]");
    }

    public void clickOnNext() throws Exception {
        log.info("Click on Next.");
        _click(next);
    }

    public boolean verifyAppPatientWizardDisplayed() {
        return _isElementVisible(addPatientTitle);
    }

    public void isAllTextPresent(List<String> text) {
        String errorMessage = "";
        for (String value : text)
            if (!_isTextPresent(value))
                errorMessage += "\n" + "Text [" + value + "] not found on Personal Information tab.";
        Assert.assertTrue(errorMessage.strip().length() < 2, errorMessage);
    }

    public void verifyPersonalInfoPageRequiredFields(Map<String, String> values, String fields) throws Exception {

        String[] fieldsArr = fields.split(":");

        log.info("Verify required files of clinic.");
        verifyElements(fieldsArr);

        Assert.assertEquals(_getText(language).strip(), values.get("Preferred_Lang").strip(),
                "");

        String mrnDigit = faker.number().digits(4);
        log.info("Enter MRN number [" + mrnDigit + "].");
        _sendKeys(mrn, mrnDigit);

        Assert.assertTrue(_isElementEnable(next), "Next button should be enable after fill MRN* field.");

        //Verify MRN* with spaces values. TODO

        log.info("Clear MRN field.");
        _clear(mrn);

        mrnDigit = faker.number().digits(2);
        log.info("Enter MRN value as [" + mrnDigit + "]");
        _sendKeys(mrn, mrnDigit);

        log.info("Verify Next button should be disable.");
        Assert.assertFalse(_isElementEnable(next), "Next button should be disable until MRN* field has valid value.");

        log.info("Verify error message [MRN needs to be between 4-16 characters, please check again]");
        Assert.assertEquals(_getText(mrnPatternInfo).strip(), "MRN needs to be between 4-16 characters, please check again");

        log.info("Clear MRN field.");
        _clear(mrn);

        mrnDigit = "------";
        log.info("Enter MRN value as [" + mrnDigit + "]");
        _sendKeys(mrn, mrnDigit);

        log.info("Verify Next button should be disable.");
        Assert.assertFalse(_isElementEnable(next), "Next button should be disable until MRN* field has valid value.");

        log.info("Verify error message [Only alphanumerics are allowed for this field]");
        Assert.assertEquals(_getText(mrnPatternInfo).strip(), "Only alphanumerics are allowed for this field");

        log.info("Clear MRN field.");
        _clear(mrn);

        mrnDigit = "random";
        log.info("Enter MRN value as [" + mrnDigit + "]");
        _sendKeys(mrn, mrnDigit);

        while (mrn.getAttribute("value").length() != 0)
            driver.switchTo().activeElement().sendKeys(Keys.BACK_SPACE);

        log.info("Verify Next button should be disable.");
        Assert.assertFalse(_isElementEnable(next), "Next button should be disable until MRN* field has valid value.");

        log.info("Verify error message [Please specify patient's MRN].");
        Assert.assertEquals(_getText(mrnRequired).strip(), "Please specify patient's MRN");

        log.info("Clear MRN field.");
        _clear(mrn);

        mrnDigit = faker.number().digits(19);
        log.info("Enter MRN value as [" + mrnDigit + "]");
        _sendKeys(mrn, mrnDigit);

        log.info("Verify Next button should be disable.");
        Assert.assertFalse(_isElementEnable(next), "Next button should be disable until MRN* field has valid value.");

        log.info("Verify error message [MRN needs to be between 4-16 characters, please check again].");
        Assert.assertEquals(_getText(mrnPatternInfo).strip(), "MRN needs to be between 4-16 characters, please check again");

        log.info("Clear MRN field.");
        _clear(mrn);

        mrnDigit = values.get("MRN");
        log.info("Enter MRN value as [" + mrnDigit + "]");
        _sendKeys(mrn, mrnDigit);

        log.info("Verify Next button should be disable.");
        Assert.assertTrue(_isElementEnable(next), "Next button should be disable until MRN* field has valid value.");

        log.info("MRN invalid pattern message should not be displayed.");
        Assert.assertFalse(_isElementVisible(mrnPatternInfo), "MRN invalid pattern message displayed.");

        log.info("MRN required message should not be displayed.");
        Assert.assertFalse(_isElementVisible(mrnRequired), "MRN required message displayed.");

        _scrollToElement(personalPhone);

        _smartClick(language);

        _waitForPageLoad();
        _pressDown(5);
        _smartClick(selectLanguage(values.get("Preferred_Lang")));

        log.info("Verify preferred language. [" + values.get("Preferred_Lang") + "]");
        String expected = values.get("Preferred_Lang").toUpperCase(Locale.ROOT);
        String actual = _getText(language).toUpperCase(Locale.ROOT);
        Assert.assertEquals(actual, expected,
                "Selected preferred language not matched. Expected : " + expected + ", Actual : " + actual);
    }

    private void verifyElements(String[] fieldsArr) {

        Assert.assertTrue(verifyMRN(), "Failed while verify MRN/Reference Id.");
    }

    private boolean verifyMRN() {
        return true;
    }

    public void verifyCareTeamInfoPageRequiredFields(List<String> physician, List<String> nurse) throws Exception {

        log.info("Verify Next button is disable.");
        Assert.assertFalse(_isElementEnable(next),
                "Next button should be disable until any physician or nurse not selected.");
        _waitForPageLoad();
        for (String name : physician)
            if (name.length() > 1) {
                _scrollToElement(driver.findElement(selectPhysician(name)));
                _waitForPageLoad();
                _click(selectPhysician(name));
            }
        for (String name : nurse)
            if (name.length() > 1) {
                _scrollToElement(driver.findElement(selectNursesParamedics(name)));
                _waitForPageLoad();
                _click(selectNursesParamedics(name));
            }

        log.info("Verify Next button is enable.");
        Assert.assertTrue(_isElementEnable(next), "Next button should be enable until MRN* field is empty.");

    }

    @FindBy(xpath = "//*[@formcontrolname=\"gender\"]")
    private WebElement gender;

    public void fillPersonalInformation(Map<String, String> values, boolean requiredOnly) throws Exception {
        _waitForPageLoad();
        _sendKeys(mrn, values.get("MRN"));
        if (!requiredOnly) {
            _sendKeys(fName, values.get("F_Name"));
            _sendKeys(lName, values.get("L_Name"));
            //Gender
            _click(gender);
            if (values.get("Gender").equals("Male"))
                _clickOnText("Male ");
            else if (values.get("Gender").equals("Female"))
                _clickOnText("Female ");
            else if (values.get("Gender").equals("Other"))
                _clickOnText("Other ");
            _click(dob);
            String[] dob = values.get("DOB").split("/");
            _click(By.xpath("//*[text()='" + dob[2].strip() + "']"));

            int currentMonth = LocalDateTime.now().getMonth().getValue();
            if (currentMonth > Integer.parseInt(dob[0].strip())) {
                for (int i = 0; i < currentMonth - Integer.parseInt(dob[0].strip()); i++) {
                    _click(By.className("myDpPrevBtn"));
                }
            } else {
                for (int i = 0; i < Integer.parseInt(dob[0].strip()) - currentMonth; i++) {
                    _click(By.className("myDpNextBtn"));
                }
            }

            _click(By.xpath("//*[text()='" + dob[1].strip() + "']"));

            _sendKeys(addr1id, values.get("Address_line_1"));
            _sendKeys(addr2id, values.get("Address_line_2"));
            _sendKeys(zip, values.get("Zip_code"));
            _sendKeys(city, values.get("City"));
            _sendKeys(state, values.get("State"));
            _sendKeys(mobile, values.get("Patient_Mob"));
            _sendKeys(landline, values.get("Patient_Landline"));
            _sendKeys(familyPhone, values.get("Family_Pho_Num"));
            _sendKeys(familyRelation, values.get("Familial_Relation"));
        }
        _scrollToElement(language);
        _smartClick(language);
        _waitForPageLoad();
        _smartClick(selectLanguage(values.get("Preferred_Lang")));
    }

    public void fillMedicalInformation(Map<String, String> defaultPatientDetails, boolean requiredOnly) throws Exception {
        _waitForPageLoad();
        _click(selectCodeAndAlergies(defaultPatientDetails.get("Code_Status")));
        _click(selectCodeAndAlergies(defaultPatientDetails.get("Allergies")));
        _smartClick(contactprecautionsid);
        _waitForPageLoad();
        driver.findElement(By.xpath("//*[text()='" + defaultPatientDetails.get("Contact_precautions") + "']")).click();
        _smartClick(contactprecautionsid);

        _smartClick(admittingdiagnosisid);
        _waitForPageLoad();
        _clickOnTextContains(defaultPatientDetails.get("Admitting_diagnosis"));
        _smartClick(admittingdiagnosisid);
        _sendKeys(clinicalsummary, defaultPatientDetails.get("Clinical_summary"));
    }

    public void fillCareTeamInformation(Map<String, String> defaultPatientDetails, boolean requiredOnly) throws Exception {
        String[] physician = defaultPatientDetails.get("Physicians").split(",");
        String[] nurse = defaultPatientDetails.get("Nurses_Paramedics").split(",");

        _waitForPageLoad();
        for (String name : physician)
            if (!name.strip().isBlank() || !name.strip().isEmpty()) {
                _scrollToElement(driver.findElement(selectPhysician(name)));
                _waitForPageLoad();
                _click(selectPhysician(name));
            }
        for (String name : nurse) {
            if (!name.strip().isBlank() || !name.strip().isEmpty()) {
                _scrollToElement(driver.findElement(selectNursesParamedics(name)));
                _waitForPageLoad();
                _click(selectNursesParamedics(name));
            }
        }
    }

    public void fillEquipmentInformation(Map<String, String> defaultPatientDetails, boolean requiredOnly) throws Exception {

        _waitForPageLoad();
        _clickOnTextContains(defaultPatientDetails.get("Length_of_stay"));

        _click(selectDevices);
        _click(selectDevices);

        Assert.assertTrue(_isTextContainsPresent("Please select at least one device"),
                "Failed while verify ");

        _click(selectDevices);
        _clickOnTextContains(defaultPatientDetails.get("Devices"));
        _click(selectDevices);

        _sendKeys(equipmentAddressLine1, defaultPatientDetails.get("Ship_Address_1"));
        _scrollToElement(sendEmail);
        _sendKeys(equipmentZipCode, defaultPatientDetails.get("Ship_ZipCode"));
        _sendKeys(equipmentCity, defaultPatientDetails.get("Ship_City"));
        _sendKeys(equipmentState, defaultPatientDetails.get("Ship_State"));

        _waitForPageLoad();
        _scrollToElement(sendEmail);
        _waitForPageLoad();
        _smartClick(enrollmentType);
        _clickOnTextContains(defaultPatientDetails.get("Enrollment_type"));
        _clickOnTextContains(defaultPatientDetails.get("Point_of_contact"));

        _click(By.xpath("//*[text()='" + defaultPatientDetails.get("Email_equipment") + "']"));

        if (!requiredOnly) {
            _sendKeys(equipmentAddressLine2, defaultPatientDetails.get("Ship_Address_2"));
            _sendKeys(point_F_Name, defaultPatientDetails.get("Point_F_Name"));
            _sendKeys(point_L_Name, defaultPatientDetails.get("Point_L_Name"));
            _sendKeys(point_number, defaultPatientDetails.get("Point_number"));
        }
    }

    public boolean verifyCheckedSign(String menu) {
        return _isElementVisible(isChecked(menu));
    }

    public void verifyAdvanceReviewPage(List<String> advanceReviewPageContent) {
        for (String value : advanceReviewPageContent)
            Assert.assertTrue(driver.findElements(By.xpath("//*[contains(text(),'" + value + "')]")).size() == 2,
                    value + " should be present 2 times in page.");
    }

    public void cancelAddPatientWizard() throws Exception {
        log.info("Click on cancel.");
        _smartClick(cancel);
    }

    public boolean verifyAddPatientCancelPopUp(String message) throws Exception {
        Assert.assertTrue(_isElementVisible(cancelAddPatientPop), "[Cancel] is not visible on cancel Add Patient pop-up.");
        Assert.assertTrue(_isElementVisible(confirmAddPatientPop), "[Confirm] is not visible on cancel Add Patient pop-up.");
        Assert.assertEquals(_getText(cancelAddPatientPop), "No", "[No] is not displayed on cancel pop-up.");
        Assert.assertEquals(_getText(confirmAddPatientPop), "Yes", "[Yes] is not displayed on cancel pop-up.");
        Assert.assertTrue(_isTextContainsPresent(message), "[" + message + "] is not displayed on cancel pop-up.");

        log.info("Click on confirm option.");
        _click(confirmAddPatientPop);
        _waitForPageLoad();
        return _waitForElementVisible(addPatient);
    }

    public void savePatient() throws Exception {
        _waitForElementInvisible(next);
        _smartClick(next);
        _waitForPageLoad();
        _waitForLoading();
        _waitForElementInvisible(next);
        _waitForPageLoad();
        _waitForLoading();
    }

    public void verifyPatientDetailsOnReviewTab(Map<String, String> defaultPatientDetails, boolean allDetails) throws Exception {
        String expected = "";
        if (allDetails) {
            //Excluding mandatory fields.
            expected = defaultPatientDetails.get("F_Name") + " " + defaultPatientDetails.get("L_Name");
            Assert.assertTrue(_getText(By.xpath("//*[contains(text(),'Patient information')]/..//*[text()=' Full name ']/..//*[contains(@class,\"frm-value\")]")).equalsIgnoreCase(expected),
                    "");
            //   , "Full name not matched. Expected : " + expected);

            expected = defaultPatientDetails.get("Gender");
            Assert.assertTrue(_isElementVisible(verifyReviewPage("Gender", expected)),
                    "Gender not matched. Expected : " + expected);

            expected = defaultPatientDetails.get("DOB");
            Assert.assertTrue(_isElementVisible(verifyReviewPage("DOB", expected)),
                    "DOB not matched. Expected : " + expected);

            expected = defaultPatientDetails.get("Address_line_1") + " " + defaultPatientDetails.get("Address_line_2");
            Assert.assertTrue(_isElementVisible(verifyReviewPage("Address", expected)),
                    "Address not matched. Expected : " + expected);

            expected = defaultPatientDetails.get("Patient_Mob");
            Assert.assertTrue(_isElementVisible(verifyReviewPage("Patient's mobile number", expected)),
                    "Patient mobile number not matched. Expected : " + expected);

            expected = defaultPatientDetails.get("Patient_Landline");
            Assert.assertTrue(_isElementVisible(verifyReviewPage("Patient's landline number", expected)),
                    "Patient Landline number not matched. Expected : " + expected);

            expected = defaultPatientDetails.get("Family_Pho_Num");
            Assert.assertTrue(_isElementVisible(verifyReviewPage("Family contact information", expected)),
                    "Family phone number not matched. Expected : " + expected);

            expected = defaultPatientDetails.get("Familial_Relation");
            Assert.assertTrue(_isElementVisible(verifyReviewPage("Familial relationship", expected)),
                    "Familial relationship not matched. Expected : " + expected);

            expected = defaultPatientDetails.get("Code_Status");
            Assert.assertTrue(_isElementVisible(verifyReviewPage("Code status", expected)),
                    "Code status not matched. Expected : " + expected);

            expected = defaultPatientDetails.get("Allergies");
            Assert.assertTrue(_isElementVisible(verifyReviewPage("Allergies", expected)),
                    "Allergies value not matched. Expected : " + expected);

            expected = defaultPatientDetails.get("Contact_precautions");
            Assert.assertTrue(_isElementVisible(verifyReviewPage("Contact precautions", expected)),
                    "Contact precautions not matched. Expected : " + expected);

            expected = defaultPatientDetails.get("Admitting_diagnosis");
            Assert.assertTrue(_isElementVisible(verifyReviewPage("Admitting diagnosis", expected)),
                    "Admitting diagnosis value not matched. Expected : " + expected);

            expected = defaultPatientDetails.get("Clinical_summary");
            Assert.assertTrue(_isElementVisible(verifyReviewPage("Clinical summary", expected)),
                    "Clinical summary not matched. Expected : " + expected);

            expected = defaultPatientDetails.get("Point_of_contact");
            Assert.assertTrue(_isElementVisible(verifyReviewPage("Point of contact", expected)),
                    "Point of contact value not matched. Expected : " + expected);

            expected = defaultPatientDetails.get("Point_F_Name") + " " + defaultPatientDetails.get("Point_L_Name");
            Assert.assertEquals(_getText(By.xpath("//*[contains(text(),'Equipment information')]/..//*[text()=' Full name ']/..//*[contains(@class,\"frm-value\")]"))
                    , expected);

            expected = defaultPatientDetails.get("Point_number");
            Assert.assertTrue(_isElementVisible(By.xpath("//*[contains(text(),'Equipment information')]/..//*[contains(text(),'Phone number')]/..//*[contains(text(),'" + expected + "')]")),
                    "Phone number under Equipment information not matched. Expected : " + expected);
        }

        expected = defaultPatientDetails.get("MRN");
        Assert.assertTrue(_isElementVisible(verifyReviewPage("MRN", expected)),
                "MRN number not matched. Expected : " + expected);

        expected = defaultPatientDetails.get("Preferred_Lang");
        Assert.assertTrue(_isElementVisible(verifyReviewPage("Patient's preferred language", expected)),
                "Preferred language not matched. Expected : " + expected);

        _scrollToElement(driver.findElement(verifyReviewPage("Enrollment type")));

        expected = defaultPatientDetails.get("Physicians");
        Assert.assertTrue(_isElementVisible(verifyReviewPage("Care team", expected)),
                "Physicians value not matched. Expected : " + expected);

        expected = defaultPatientDetails.get("Nurses_Paramedics");
        Assert.assertTrue(_isElementVisible(verifyReviewPage("Care team", expected)),
                "Nurses Paramedics value not matched. Expected : " + expected);

        expected = defaultPatientDetails.get("Length_of_stay");
        Assert.assertTrue(_isElementVisible(verifyReviewPage("Length of stay", expected)),
                "Length of stay not matched. Expected :" + expected);

        expected = defaultPatientDetails.get("Devices");
        Assert.assertTrue(_getText(By.xpath("//*[contains(text(),'Devices')]/..//*[contains(@class,'block')]"))
                        .contains(expected),
                "Devices not matched. Expected : " + expected);

        Assert.assertTrue(_isElementVisible(verifyReviewPage(" Documents ", "Privacy Policy")),
                "Privacy Policy not found under documents list.");
        Assert.assertTrue(_isElementVisible(verifyReviewPage(" Documents ", "Terms & Conditions")),
                "Terms & Conditions not found under documents list.");
        Assert.assertTrue(_isElementVisible(verifyReviewPage(" Documents ", "Consent")),
                "Consent not found under documents list.");
        Assert.assertTrue(_isElementVisible(verifyReviewPage(" Documents ", "Addressed / stamped return envelope")),
                "Addressed / stamped return envelope not found under documents list.");
        Assert.assertTrue(_isElementVisible(verifyReviewPage(" Documents ", "Quickstart guide")),
                "Quickstart guide not found under documents list.");

        expected = defaultPatientDetails.get("Ship_Address_1");
        Assert.assertTrue(_isElementVisible(verifyReviewPage(" Shipping address ", expected)),
                "Shipping Address not matched. Expected : " + expected);

        expected = defaultPatientDetails.get("Ship_ZipCode");
        Assert.assertTrue(_isElementVisible(By.xpath("//*[contains(text(),'Equipment information')]/..//*[contains(text(),'Zip code')]/..//*[contains(text(),'" + expected + "')]")),
                "Ship Zip code not matched. Expected : " + expected);

        expected = defaultPatientDetails.get("Ship_City");
        Assert.assertTrue(_isElementVisible(By.xpath("//*[contains(text(),'Equipment information')]/..//*[contains(text(),'City')]/..//*[contains(text(),'" + expected + "')]")),
                "Ship city not matched. Expected : " + expected);

        expected = defaultPatientDetails.get("Ship_State");
        Assert.assertTrue(_isElementVisible(By.xpath("//*[contains(text(),'Equipment information')]/..//*[contains(text(),'State')]/..//*[contains(text(),'" + expected + "')]")),
                "Ship state not matched. Expected : " + expected);

        expected = defaultPatientDetails.get("Enrollment_type");
        Assert.assertTrue(_getText(verifyReviewPage("Enrollment type")).strip().equals(expected),
                "Enrollment type not matched. Expected : " + expected);

        expected = defaultPatientDetails.get("Email_equipment");
        Assert.assertTrue(_isElementVisible(verifyReviewPage(" Email equipment information to the Biofourmis logistics team after patient’s admission? ", expected)),
                "Email equipment information option not matched. Expected : " + expected);
    }

    public void declineCancelPatientPopUp() throws Exception {
        _click(cancelAddPatientPop);
    }

    public boolean verifyPatientCreatedPopUp(Map<String, String> defaultPatientDetails) {
        _waitForPageLoad();
        String fName = defaultPatientDetails.get("F_Name").strip();
        String lName = defaultPatientDetails.get("L_Name").strip();
        try {
            Assert.assertTrue(_isTextContainsPresent(defaultPatientDetails.get("MRN") + " has been admitted."),
                    defaultPatientDetails.get("MRN") + " had been admitted success message not displayed.");
            return true;
        } catch (AssertionError e) {
            if (_isTextContainsPresent(defaultPatientDetails.get("F_Name") + " " + defaultPatientDetails.get("L_Name") + " has been admitted."))
                return true;
            else {
                if (_isTextContainsPresent(fName.charAt(0) + fName.substring(1, fName.length()).toLowerCase(Locale.ROOT) + "  " + lName.charAt(0) + lName.substring(1, lName.length()).toLowerCase(Locale.ROOT) + " has been admitted."))
                    return true;
                else if (_isTextContainsPresent(fName.charAt(0) + fName.substring(1, fName.length()).toLowerCase(Locale.ROOT) + " " + lName.charAt(0) + lName.substring(1, lName.length()).toLowerCase(Locale.ROOT) + " has been admitted."))
                    return true;
                else
                    return _isTextContainsPresent(defaultPatientDetails.get("F_Name") + "  " + defaultPatientDetails.get("L_Name") + " has been admitted.");
            }
        }
    }

    public boolean verifyPatientCreatedQRPage(String start, String end, boolean isLoggedIn) {
        _waitForPageLoad();
//        if (isLoggedIn)
//            Assert.assertTrue(_isTextContainsPresent("Already logged in to the patient app with this QR code"),
//                    "Already logged in to the patient app with this QR code message not displayed.");
//        else
//            Assert.assertTrue(_isTextContainsPresent("Open the patient app and scan this QR code to sync devices."),
//                    "Open the patient app and scan this QR code to sync devices message not displayed.");

        Assert.assertTrue(_isTextContainsPresent("QR code generated at"),
                "QR code generated at option not visible.");
        Assert.assertTrue(_isTextContainsPresent("QR code expires at"),
                "QR code expires at option not visible.");
        Assert.assertTrue(_isTextContainsPresent("QR code not working?"),
                "QR code not working? option not visible.");
        Assert.assertTrue(_isTextContainsPresent("Generate a new code"),
                "Generate a new code option not visible.");
        Assert.assertTrue(_isTextContainsPresent("Download QR code"),
                "Download QR code option not visible.");
//        Assert.assertTrue(_isTextContainsPresent(start),
//                "QR code generated at date should be [" + start + "].");
//        Assert.assertTrue(_isTextContainsPresent(end),
//                "QR code expires at date should be [" + end + "].");

        return _isElementVisible(qrCode) && _isTextContainsPresent("View patient details");
    }

    public void clickOnViewPatientDetails() throws Exception {
        _waitForPageLoad();
        _sleep(2);
        _clickOnTextContains("View patient details");
    }

    public boolean verifyNewPatientTutorial(String patientName) throws Exception {
        Assert.assertTrue(_isTextContainsPresent("Set activities for patient"),
                "[Set activities for patient] tutorial option not displayed.");
        Assert.assertTrue(_isTextContainsPresent("It will guide patient to adhere to the plan set by the care team."),
                "[It will guide patient to adhere to the plan set by the care team.] tutorial option not displayed.");
        Assert.assertTrue(_isTextContainsPresent("1 of 2"),
                "[1 of 2] tutorial option not displayed.");
        Assert.assertTrue(_isTextContainsPresent("Skip"),
                "[Skip] tutorial option not displayed.");

        log.info("Click on Next button.");
        _sleep(2);
        driver.findElement(By.xpath("//*[contains(text(),'Next')]//parent::button")).click();
        _waitForPageLoad();

        Assert.assertTrue(_isTextContainsPresent("Set vitals threshold"),
                "[Set vitals threshold] tutorial option not displayed.");
        Assert.assertTrue(_isTextContainsPresent("Alert will be triggered when vitals exceeded the threshold."),
                "[Alert will be triggered when vitals exceeded the threshold.] tutorial option not displayed.");
        Assert.assertTrue(_isTextContainsPresent("2 of 2"),
                "[2 of 2] tutorial option not displayed.");
        Assert.assertTrue(_isTextContainsPresent("Skip"),
                "[Skip] tutorial option not displayed.");

        log.info("Click on Next button.");
        _sleep(2);
        driver.findElement(By.xpath("//*[contains(text(),'Done')]//parent::button")).click();
        _waitForPageLoad();

        return _isTextContainsPresent(patientName);
    }

    public boolean isNextEnable() throws Exception {
        return _isElementEnable(next);
    }

    public void enterMRN(String mrnDigit) throws Exception {
        _sendKeys(mrn, mrnDigit);
    }

    public void clearMRN() throws Exception {
        _clear(mrn);
    }

    public String getMRNInfo() throws Exception {
        return _getText(mrnPatternInfo).strip();
    }

    public void clearMRNWithBackSpace() {
        while (mrn.getAttribute("value").length() != 0)
            driver.switchTo().activeElement().sendKeys(Keys.BACK_SPACE);

        while (mrn.getAttribute("value").length() != 0)
            driver.switchTo().activeElement().sendKeys(Keys.BACK_SPACE);
    }

    public String getMRNReq() throws Exception {
        return _getText(mrnRequired).strip();
    }

    public void enterFirstName(String firstName) throws Exception {
        _sendKeys(fName, firstName);
    }

    public void enterLastName(String lastName) throws Exception {
        _sendKeys(lName, lastName);
    }

    public void enterGender(String s) throws Exception {
        _click(gender);
        _sleep(2);
        _clickOnTextContains(s);
    }

    public void enterDOB(String dobValue) throws Exception {
        _click(dob);
        String[] dob = dobValue.split("/");
        _click(By.xpath("//*[text()='" + dob[2].strip() + "']"));

        int currentMonth = LocalDateTime.now().getMonth().getValue();
        if (currentMonth > Integer.parseInt(dob[0].strip())) {
            for (int i = 0; i < currentMonth - Integer.parseInt(dob[0].strip()); i++) {
                _click(By.className("myDpPrevBtn"));
            }
        } else {
            for (int i = 0; i < Integer.parseInt(dob[0].strip()) - currentMonth; i++) {
                _click(By.className("myDpNextBtn"));
            }
        }
        _click(By.xpath("//*[text()='" + dob[1].strip() + "']"));
    }

    public void enterCity(String replaceAll) throws Exception {
        _sendKeys(city, replaceAll);
    }

    public void enterState(String replaceAll) throws Exception {
        _sendKeys(state, replaceAll);
    }

    public void enterLang(String english) throws Exception {
        _scrollToElement(language);
        _smartClick(language);
        _waitForPageLoad();
        _smartClick(selectLanguage(english));
    }

    public void enterZip(String digits) throws Exception {
        _sendKeys(zip, digits);
    }

    public void enterAddressline1(String country) throws Exception {
        _sendKeys(addr1id, country);
    }
}
