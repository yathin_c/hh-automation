package com.biofourmis.biovitals.pagefactory.web;

import com.biofourmis.biovitals.utilities.CommonUtils;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.Locale;
import java.util.Map;

@Log4j
public class HandOffPage extends CommonUtils {

    private WebDriver driver;

    public HandOffPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//*[@type=\"ic-overflow\"]")
    private WebElement threeDots;
    @FindBy(xpath = "//*[@type=\"ic-handoff\"]")
    private WebElement iconHandoff;
    @FindBy(xpath = "//*[@class='handoff-sidenav-content']//*[@type=\"ic-close\"]")
    private WebElement close;
    @FindBy(id = "patientsummaryid")
    private WebElement summary;
    @FindBy(id = "notes")
    private WebElement notes;
    @FindBy(id = "handoff")
    private WebElement handoff;

    @FindBy(id = "stableseverity")
    private WebElement stable;
    @FindBy(id = "“watcher”severity")
    private WebElement watcher;
    @FindBy(id = "unstableseverity")
    private WebElement unstable;
    @FindBy(id = "priorvisit")
    private WebElement priorvisit;

    @FindBy(xpath = "//*[@type='ic-close']")
    private WebElement ic_close;
    @FindBy(id = "OtherssacpId")
    private WebElement customInput;
    @FindBy(xpath = "//*[@formcontrolname=\"otherSacp\"]")
    private WebElement customValue;
    @FindBy(xpath = "//*[@type='ic-edit']")
    private WebElement customEditIcon;
    @FindBy(xpath = "//*[@type='ic-edit-selected']")
    private WebElement customEditIconSelected;


    public void clickOnPatientOptions() throws Exception {
        _waitForPageLoad();
        _waitForElementVisible(threeDots);
        try {
            _click(threeDots);
        } catch (Exception e) {
            _waitForLoading();
            _waitForPageLoad();
            _click(threeDots);
        }
        _sleep(10);
    }

    public boolean clickOnOption(String hand_off) throws Exception {
        _clickOnTextContains(hand_off);
        return _waitForElementVisible(close);
    }

    public boolean closeHandOffWizard() throws Exception {
        _click(close);
        return _waitForElementInvisible(close);
    }

    public void enterHandOffDetails(Map<String, String> details, boolean isAdd) throws Exception {
        _clickOnTextContains(details.get("severity"));
        if (isAdd) {
            _clear(summary);
            _sendKeys(summary, details.get("summary"));
            _clickOnTextContains(details.get("awareness"));
            _clear(notes);
            _sendKeys(notes, details.get("synthesis"));
        }
        _click(handoff);
        _waitForPageLoad();
        if (_isTextContainsPresent("Proceed"))
            _clickOnTextContains("Proceed");
        _waitForPageLoad();
        if (_isTextContainsPresent("Reload"))
            _click(By.id("confirm"));
        _waitForPageLoad();
    }

    public boolean verifyDetails(Map<String, String> details) throws Exception {
        _pageWait(close);
        Assert.assertTrue(driver.findElement(By.xpath("//*[contains(@id,'" + details.get("severity").toLowerCase(Locale.ROOT) + "')]//input")).isSelected(), "");
        Assert.assertEquals(_getAttributeValue(summary, "value"), details.get("summary"), "");
        Assert.assertEquals(_getAttributeValue(notes, "value"), details.get("synthesis"), "");
        Assert.assertTrue(driver.findElement(By.xpath("//*[contains(@id,'" + details.get("awareness") + "')]//input")).isSelected(), "");
        Assert.assertTrue(driver.findElement(By.id("priorvisit")).isEnabled(), "");
        Assert.assertFalse(driver.findElement(By.id("priorvisit")).isSelected(), "");
        _click(close);
        return _waitForElementInvisible(close);
    }

    public boolean isHandOffBtnEnable() throws Exception {
        return _isElementEnable(handoff);
    }

    private By handoffOptions(String name) {
        return By.xpath("//*[contains(@class,'patient-handoff-container')]//*[contains(text(),'" + name + "')]");
    }

    public boolean isOptionDisplayed(String s) {
        return _isElementVisible(handoffOptions(s));
    }

    public void selectSeverity(String severity) throws Exception {
        _clickOnTextContains(severity);
    }

    public void enterSummary(String text) throws Exception {
        _sendKeys(summary, text);
    }

    public void selectAwareness(String awareness, boolean isCustom) throws Exception {
        if (isCustom) {
            //Custom
        } else
            _clickOnTextContains(awareness);
    }

    public void enterNotes(String text) throws Exception {
        _sendKeys(notes, text);
    }

    public void clickOnHandOff() throws Exception {
        _click(handoff);
        _waitForLoading();
    }

    public boolean isPoPOptionDisplayed(String s) {
        return _isTextContainsPresent(s);
    }

    public void clickOnCancel() throws Exception {
        if (_isTextContainsPresent("Cancel"))
            _clickOnTextContains("Cancel");
    }

    public void clickOnProceed() throws Exception {
        if (_isTextContainsPresent("Proceed"))
            _clickOnTextContains("Proceed");
        _waitForLoading();
    }

    public void clickOnHandOffIcon() throws Exception {
        _waitForLoading();
        _waitForElementVisible(iconHandoff);
        _sleep(2);
        if (!_isElementVisible(ic_close))
            _click(iconHandoff);

        _waitForElementVisible(ic_close);
        _sleep(3);
    }

    public String getSeverityStyle(String stable) {
        _sleep(3);
        if (stable.equalsIgnoreCase("Stable"))
            return _getAttributeValue(this.stable, "style");
        else if (stable.equalsIgnoreCase("Watcher"))
            return _getAttributeValue(this.watcher, "style");
        else
            return _getAttributeValue(this.unstable, "style");
    }

    public boolean isAwarenessSelected(String name) {
        return driver.findElement(By.id(name + "sacpId")).isSelected();
    }

    public String getSummary() {
        return _getAttributeValue(summary, "value");
    }

    public String getNote() {
        return _getAttributeValue(notes, "value");
    }

    public boolean isPriorvisitEnable() {
        return driver.findElement(By.id("priorvisit")).isEnabled();
    }

    public boolean isPriorvisitSelected() {
        return driver.findElement(By.id("priorvisit")).isSelected();
    }

    public boolean isSeveritySelected(String severity) {
        return driver.findElement(By.xpath("//*[contains(@id,'" + severity.toLowerCase(Locale.ROOT) + "')]//input")).isSelected();
    }

    public void clickOnNoPrioriChanges() throws Exception {
        _clickOnTextContains("No changes from prior visit");
    }

    public void clickOnClose() throws Exception {
        _waitForElementVisible(ic_close);
        try {
            _click(ic_close);
        } catch (Exception e) {
            System.out.println();
        }
    }

    public String verifyContactCR() {
        return driver.findElement(By.xpath("//*[contains(text(),'Contact')]")).getText();
    }

    public void clickOnEditCustomizeAwareness() throws Exception {
        _click(customEditIcon);
    }

    public void enterCustomAwareness(String s) throws Exception {
        _sendKeys(customValue, s);
    }

    public String getCustomAwarenessValue() throws Exception {
        return _getAttributeValue(customValue, "value");
    }

    public void clearCustomAwareness() throws Exception {
        _clear(customValue);
        _click(customValue);
        _click(customEditIconSelected);
    }

    public boolean isCustomValueClear() {
        return _isTextContainsPresent("Customize your own option");
    }
}
