package com.biofourmis.biovitals.utilities;

import com.biofourmis.biovital.GraphImpl;
import com.biofourmis.biovital.GraphLib;
import com.biofourmis.biovitals.configs.AppConfig;
import com.biofourmis.biovitals.configs.CustomConfig;
import com.biofourmis.biovitals.configs.LocalisationConfig;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.*;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.Coordinates;
import org.openqa.selenium.interactions.Locatable;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.util.StringUtils;
import org.testng.Assert;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * This class is read only do not modify anything here
 */
@Log4j
public class CommonUtils extends SwipeUtils {

    private final WebDriver driver;
    private final GraphLib graphLib;
    AppConfig appConfig;
    CustomConfig customConfig;

    LocalisationConfig localisationConfig;

    public CommonUtils(WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.appConfig = AppConfig.getBean(AppConfig.class);
        this.localisationConfig = LocalisationConfig.getBean(LocalisationConfig.class);
        this.customConfig = CustomConfig.getBean(CustomConfig.class);
        graphLib = new GraphImpl();
    }

    /**
     * @return Device OS version
     */
    public Double _getDeviceVersion() {
        return Double.valueOf(((AppiumDriver<MobileElement>) this.driver)
                .getCapabilities()
                .getCapability(MobileCapabilityType.PLATFORM_VERSION)
                .toString());
    }

    /**
     * @return Platform name
     */
    public String _getPlatformName() {
        return ((AppiumDriver<MobileElement>) this.driver)
                .getPlatformName();
    }

    /**
     * Waits for a Element by with explicit wait and returns true if found
     *
     * @param by By value of element
     * @return boolean
     */
    public boolean _isElementVisible(By by) {
        try {
            new FluentWait<WebDriver>(driver)
                    .withTimeout(Duration.ofSeconds(appConfig.getSeleniumTimeout()))
                    .pollingEvery(Duration.ofMillis(10))
                    .ignoring(NoSuchElementException.class)
                    .ignoring(StaleElementReferenceException.class)
                    .ignoring(ElementNotInteractableException.class)
                    .ignoring(ElementNotVisibleException.class)
                    .ignoring(ElementNotSelectableException.class)
                    .ignoring(ElementClickInterceptedException.class)
                    .until(ExpectedConditions.visibilityOfElementLocated(by));
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    /**
     * Waits for a Element element with explicit wait and returns true if found
     *
     * @param element WebElement value of element
     * @return boolean
     */
    public boolean _isElementVisible(WebElement element) {
        try {
            By eID = _convertElemToBy(element);
            new FluentWait<WebDriver>(driver)
                    .withTimeout(Duration.ofSeconds(appConfig.getSeleniumTimeout()))
                    .pollingEvery(Duration.ofMillis(10))
                    .ignoring(NoSuchElementException.class)
                    .ignoring(StaleElementReferenceException.class)
                    .ignoring(ElementNotInteractableException.class)
                    .ignoring(ElementNotVisibleException.class)
                    .ignoring(ElementNotSelectableException.class)
                    .ignoring(ElementClickInterceptedException.class)
                    .until(ExpectedConditions.visibilityOfElementLocated(eID));
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    /**
     * Waits for a By element with explicit wait and returns true if found
     *
     * @param element WebElement value of element
     * @return boolean
     */
    public boolean _isElementPresent(WebElement element) {
        try {
            By eID = _convertElemToBy(element);
            new FluentWait<WebDriver>(driver)
                    .withTimeout(Duration.ofSeconds(appConfig.getSeleniumTimeout()))
                    .pollingEvery(Duration.ofMillis(10))
                    .ignoring(NoSuchElementException.class)
                    .ignoring(StaleElementReferenceException.class)
                    .ignoring(ElementNotInteractableException.class)
                    .ignoring(ElementNotVisibleException.class)
                    .ignoring(ElementNotSelectableException.class)
                    .ignoring(ElementClickInterceptedException.class)
                    .until(ExpectedConditions.presenceOfElementLocated(eID));
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    /**
     * Waits for a By element with explicit wait and returns true if found
     *
     * @param by WebElement value of element
     * @return boolean
     */
    public boolean _isElementPresent(By by) {
        try {
            new FluentWait<WebDriver>(driver)
                    .withTimeout(Duration.ofSeconds(appConfig.getSeleniumTimeout()))
                    .pollingEvery(Duration.ofMillis(10))
                    .ignoring(NoSuchElementException.class)
                    .ignoring(StaleElementReferenceException.class)
                    .ignoring(ElementNotInteractableException.class)
                    .ignoring(ElementNotVisibleException.class)
                    .ignoring(ElementNotSelectableException.class)
                    .ignoring(ElementClickInterceptedException.class)
                    .until(ExpectedConditions.presenceOfElementLocated(by));
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean _isElementEnable(WebElement element) throws Exception {
        By eID = _convertElemToBy(element);
        return driver.findElement(eID).isEnabled();
    }

    public boolean _isElementEnable(By by) throws Exception {
        return driver.findElement(by).isEnabled();
    }

    public void _clear(WebElement element) throws Exception {
        By eID = _convertElemToBy(element);
        driver.findElement(eID).clear();
    }

    public boolean _isTextContainsPresent(String textValue) {
        try {
            By eID = _getByOfText(textValue, true, false);
            return _isElementPresent(eID);
        } catch (Exception exception) {
            return false;
        }
    }

    public boolean _isTextContainsPresent(String textValue, boolean wildCard) {
        try {
            By eID = _getByOfText(textValue, true, wildCard);
            return _isElementPresent(eID);
        } catch (Exception exception) {
            return false;
        }
    }

    private By _getByOfText(String textValue, boolean isContains, boolean wildCard) {
        if (driver instanceof AndroidDriver)
            if (isContains)
                return By.xpath("//*[contains(@text,\"" + textValue + "\")]");
            else
                return By.xpath("//*[@text=\"" + textValue + "\"]");
        else if (driver instanceof IOSDriver) {
            System.out.println("Add your logic here.");
            return null;
        } else {
            if (isContains) {
                if (wildCard)
                    return By.xpath("//*[contains(.,\"" + textValue + "\")]");
                else
                    return By.xpath("//*[contains(text(),\"" + textValue + "\")]");
            } else
                return By.xpath("//*[text()=\"" + textValue + "\"]");
        }
    }

    public boolean _isTextPresent(String textValue) {
        try {
            By eID = _getByOfText(textValue, false, false);
            return _isElementPresent(eID);
        } catch (Exception exception) {
            return false;
        }
    }

    /**
     * This will explicitly wait for 30 seconds till the page is loaded
     */
    public void _waitForPageLoad() {
        if (driver != null) {
            long timeout = appConfig.getSeleniumTimeout();
            try {
                new FluentWait<WebDriver>(driver)
                        .withTimeout(Duration.ofSeconds(timeout))
                        .pollingEvery(Duration.ofMillis(10))
                        .ignoring(NoSuchElementException.class)
                        .ignoring(StaleElementReferenceException.class)
                        .ignoring(ElementNotInteractableException.class)
                        .ignoring(ElementNotVisibleException.class)
                        .ignoring(ElementNotSelectableException.class)
                        .ignoring(ElementClickInterceptedException.class)
                        .until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@class,'cdk-overlay-backdrop-showing')]")));
            } catch (Exception ex) {
            }

            try {
                new FluentWait<WebDriver>(driver)
                        .withTimeout(Duration.ofSeconds(timeout))
                        .pollingEvery(Duration.ofMillis(10))
                        .ignoring(NoSuchElementException.class)
                        .ignoring(StaleElementReferenceException.class)
                        .ignoring(ElementNotInteractableException.class)
                        .ignoring(ElementNotVisibleException.class)
                        .ignoring(ElementNotSelectableException.class)
                        .ignoring(ElementClickInterceptedException.class)
                        .until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@class,'spinner')]")));
            } catch (Exception ex) {
            }
            try {
                new FluentWait<WebDriver>(driver)
                        .withTimeout(Duration.ofSeconds(timeout))
                        .pollingEvery(Duration.ofMillis(10))
                        .ignoring(NoSuchElementException.class)
                        .ignoring(StaleElementReferenceException.class)
                        .ignoring(ElementNotInteractableException.class)
                        .ignoring(ElementNotVisibleException.class)
                        .ignoring(ElementNotSelectableException.class)
                        .ignoring(ElementClickInterceptedException.class)
                        .until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@class,'cdk-visually-hidden')]")));
            } catch (Exception ex) {
            }

            ExpectedCondition<Boolean> expectation = driver -> ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete");
            try {
                Thread.sleep(3000);
                new FluentWait<WebDriver>(driver)
                        .withTimeout(Duration.ofSeconds(timeout))
                        .pollingEvery(Duration.ofMillis(10))
                        .ignoring(NoSuchElementException.class)
                        .ignoring(StaleElementReferenceException.class)
                        .ignoring(ElementNotInteractableException.class)
                        .ignoring(ElementNotVisibleException.class)
                        .ignoring(ElementNotSelectableException.class)
                        .ignoring(ElementClickInterceptedException.class)
                        .until(expectation);
            } catch (Throwable error) {
                Assert.fail("Timeout waiting for Page Load Request to complete.");
            }
        }
    }

    /**
     * Performs a click operation on a WebElement with explicit wait
     *
     * @param element WebElement
     */
    public void _click(WebElement element) throws Exception {
        By eID = _convertElemToBy(element);
        if (!_isElementVisible(eID))
            throw new RuntimeException("Unable to locate the element even after waiting for " + appConfig.getSeleniumTimeout() + " seconds");
        driver.findElement(eID).click();
    }

    /**
     * Performs a click operation on a WebElement with explicit wait
     *
     * @param element WebElement
     */
    public void _smartClick(WebElement element) throws Exception {
        try {
            _click(element);
        } catch (Exception e) {
            _click(element);
        }
    }

    /**
     * Performs a click operation on a WebElement with explicit wait
     *
     * @param element WebElement
     */
    public void _smartClick(By element) throws Exception {
        try {
            _click(element);
        } catch (Exception e) {
            _click(element);
        }
    }

    /**
     * Performs a click operation on a WebElement with explicit wait
     *
     * @param element WebElement
     */
    public void _clickIfDisplayed(WebElement element) throws Exception {
        By eID = _convertElemToBy(element);
        if (!_isElementVisible(eID))
            throw new RuntimeException("Unable to locate the element even after waiting for " + appConfig.getSeleniumTimeout() + " seconds");
        if (element.isDisplayed()) {
            driver.findElement(eID).click();
        }
    }

    /**
     * Performs a click operation on a By element with explicit wait
     *
     * @param element By
     */
    public void _click(By element) throws Exception {
        if (!_isElementVisible(element))
            throw new RuntimeException("Unable to locate the element even after waiting for " + appConfig.getSeleniumTimeout() + " seconds");
        driver.findElement(element).click();
    }

    /**
     * Performs a click operation on a By element with explicit wait
     *
     * @param text String
     */
    public void _clickOnTextContains(String text) throws Exception {
        By element = _getByOfText(text, true, false);
        if (!_isElementVisible(element))
            throw new RuntimeException("Unable to locate the element even after waiting for " + appConfig.getSeleniumTimeout() + " seconds");
        driver.findElement(element).click();
    }

    /**
     * Performs a click operation on a By element with explicit wait
     *
     * @param text String
     */
    public void _clickOnText(String text) throws Exception {
        By element = _getByOfText(text, false, false);
        if (!_isElementVisible(element))
            throw new RuntimeException("Unable to locate the element even after waiting for " + appConfig.getSeleniumTimeout() + " seconds");
        driver.findElement(element).click();
    }

    /**
     * Explicitly waits for the element to be present and return the text
     *
     * @param element WebElement
     * @return String
     */
    public String _getText(WebElement element) throws Exception {
        By eID = _convertElemToBy(element);
        if (!_isElementVisible(eID))
            throw new RuntimeException("Unable to locate the element even after waiting for " + appConfig.getSeleniumTimeout() + " seconds");
        return driver.findElement(eID).getText();
    }

    /**
     * Explicitly waits for the element to be present and return the text
     *
     * @param element By
     * @return String
     */
    public String _getText(By element) throws Exception {
        if (!_isElementVisible(element))
            throw new RuntimeException("Unable to locate the element even after waiting for " + appConfig.getSeleniumTimeout() + " seconds");
        return driver.findElement(element).getText();
    }

    /**
     * Explicitly waits for the element to be present and returns the text using Javascript
     *
     * @param element WebElement
     * @return String
     */
    public String _getTextJS(WebElement element) throws Exception {
        By eID = _convertElemToBy(element);
        if (!_isElementVisible(eID))
            throw new RuntimeException("Unable to locate the element even after waiting for " + appConfig.getSeleniumTimeout() + " seconds");
        return (String) ((JavascriptExecutor) this.driver).executeScript("return arguments[0].value;", element);
    }

    /**
     * Performs sendKeys in an element with explicit wait
     *
     * @param element      WebElement
     * @param keysSequence Text to type-in
     */
    public void _sendKeys(WebElement element, String keysSequence) throws Exception {
        By eId = _convertElemToBy(element);
        if (!_isElementVisible(eId))
            throw new RuntimeException("Unable to locate the element even after waiting for " + appConfig.getSeleniumTimeout() + " seconds");
        driver.findElement(eId).clear();
        driver.findElement(eId).sendKeys(keysSequence);
        _hideKeyBoard();
    }

    /**
     * Performs sendKeys in an element with explicit wait
     *
     * @param element WebElement
     * @param keys    Text to type-in
     */
    public void _sendKeys(WebElement element, Keys keys) throws Exception {
        By eId = _convertElemToBy(element);
        if (!_isElementVisible(eId))
            throw new RuntimeException("Unable to locate the element even after waiting for " + appConfig.getSeleniumTimeout() + " seconds");
        driver.findElement(eId).clear();
        driver.findElement(eId).sendKeys(keys);
        _hideKeyBoard();
    }

    /**
     * Performs sendKeys in an element with explicit wait
     *
     * @param element      By
     * @param keysSequence Text to type-in
     */
    public void _sendKeys(By element, String keysSequence) throws Exception {
        if (!_isElementVisible(element))
            throw new RuntimeException("Unable to locate the element even after waiting for " + appConfig.getSeleniumTimeout() + " seconds");
        driver.findElement(element).clear();
        driver.findElement(element).sendKeys(keysSequence);
        _hideKeyBoard();
    }

    /**
     * Set a value in the dropdown
     *
     * @param element WebElement
     * @param locator xpath of the dropdown value
     */
    public void _setValue(WebElement element, String locator) throws Exception {
        _click(element);
        _click(By.xpath(locator));
    }

    /**
     * Set a value in the dropdown
     *
     * @param element By
     * @param locator xpath of the dropdown value
     */
    public void _setValue(By element, String locator) throws Exception {
        _click(element);
        _click(By.xpath(locator));
    }

    /**
     * Performs a scroll on a web page based on the web element co-ordinates
     *
     * @param element WebElement
     */
    public void _scrollToElement(WebElement element) {
        Coordinates cor = ((Locatable) element).getCoordinates();
        cor.inViewPort();
    }

    /**
     * Performs a scroll within the provided web element
     *
     * @param webElement WebElement
     */
    public void _scrollWithinElement(WebElement webElement) {
        ((JavascriptExecutor) this.driver).executeScript("arguments[0].scrollTop=500;", webElement);
    }


    /**
     * Perform a browser back actions
     */
    public void _goBack() {
        this.driver.navigate().back();
    }

    /**
     * Perform a browser forward actions
     */
    public void _goForward() {
        this.driver.navigate().forward();
    }

    /**
     * Redirects to the provide URL
     *
     * @param URI URL
     */
    public void _goTo(String URI) {
        this.driver.get(URI);
        _waitForPageLoad();
    }

    /**
     * Refreshes the page and wait for 5 seconds
     */
    public void _refreshPage() {
        this.driver.navigate().refresh();
        _waitForPageLoad();
        _sleep(5);
        _waitForPageLoad();
    }

    /**
     * Introduces a Thread.sleep for specified duration. Use this only when no other wait solves your problem
     *
     * @param sleepTime in seconds
     */
    public void _sleep(int sleepTime) {
        try {
            Thread.sleep(sleepTime * 1000L);
        } catch (Throwable error) {
            error.printStackTrace();
            Assert.fail("Timeout waiting for Page Load Request to complete.");
        }
    }

    /**
     * @param element
     * @return the count of elements found
     * @throws Exception
     */
    public int _getElementSize(WebElement element) throws Exception {
        By eID = _convertElemToBy(element);
        if (this.driver.findElements(eID).size() > 0) {
            return this.driver.findElements(eID).size();
        }
        return 0;
    }

    /**
     * @param by
     * @return the count of elements found
     * @throws Exception
     */
    public int _getElementSize(By by) throws Exception {
        if (this.driver.findElements(by).size() > 0) {
            return this.driver.findElements(by).size();
        }
        return 0;
    }

    /**
     * Performs a mouseover on the given element
     *
     * @param element WebElement
     */
    public void _mouseOverAndClick(WebElement element) {
        Actions action = new Actions(this.driver);
        action.moveToElement(element).click().build().perform();
    }

    /**
     * Performs a mouseover on the given element
     *
     * @param by By
     */
    public void _mouseOverAndClick(By by) {
        Actions action = new Actions(this.driver);
        action.moveToElement(driver.findElement(by)).click().build().perform();
    }

    /**
     * Performs a mouseover on the given element
     *
     * @param by By
     */
    public void _mouseOver(By by) {
        Actions action = new Actions(this.driver);
        action.moveToElement(driver.findElement(by)).build().perform();
    }

    /**
     * Performs a double click on the given element
     *
     * @param element WebElement
     */
    public void _doubleClick(WebElement element) {
        Actions action = new Actions(this.driver).doubleClick(element);
        action.build().perform();
    }

    /**
     * Performs a right click on the given element
     *
     * @param element WebElement
     */
    public void _rightClick(WebElement element) {
        Actions action = new Actions(this.driver).contextClick(element);
        action.build().perform();
    }

    /**
     * Performs a drag and drop operation from the element A to B
     *
     * @param locatorFrom from WebElement
     * @param locatorTo   to WebElement
     */
    public void _dragAndDrop(WebElement locatorFrom, WebElement locatorTo) {
        Actions builder = new Actions(this.driver);
        Action dragAndDrop = builder.clickAndHold(locatorFrom).moveToElement(locatorFrom).release(locatorTo).build();
        dragAndDrop.perform();
    }

    public void _dismissAlert() {
        this.driver.switchTo().alert().dismiss();
    }

    public void _acceptAlert() {
        this.driver.switchTo().alert().accept();
    }

    public String _getTextFromAlertPopup() {
        return (this.driver.switchTo().alert().getText());
    }

    public void _sendKeysToAlertPopup(String stringToSend) {
        this.driver.switchTo().alert().sendKeys(stringToSend);
    }

    public void _turnOffImplicitWaits() {
        this.driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
    }

    public void _turnOnImplicitWaits() {
        this.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public void _setImplicitWaits(int secValue) {
        this.driver.manage().timeouts().implicitlyWait(secValue, TimeUnit.SECONDS);
    }

    public void _deleteCookie() {
        this.driver.manage().deleteAllCookies();
    }

    public void _getCookie() {
        Set<Cookie> cookie = this.driver.manage().getCookies();
        for (Cookie c : cookie) {
            log.info(" Cookie Data : " + c);
        }
    }

    public void _addCookie() {
        Cookie name = new Cookie("This is my cookie.", "123456");
        this.driver.manage().addCookie(name);
        Set<Cookie> cookie = this.driver.manage().getCookies();
        for (Cookie c : cookie) {
            log.info(" Cookie Data : " + c);
        }
    }

    public void _scrollUpJS() {
        JavascriptExecutor jse1 = (JavascriptExecutor) this.driver;
        jse1.executeScript("window.scrollBy(0,-250)", "");
    }

    public void _scrollDownJS() {
        JavascriptExecutor jse1 = (JavascriptExecutor) this.driver;
        jse1.executeScript("window.scrollBy(0,250)", "");
    }

    public void _windowResize() throws Exception {
        this.driver.manage().window().setSize(new Dimension(1440, 900));
        Thread.sleep(2000);
    }

    public void _pageWait(WebElement nextPageElement) throws Exception {
        _waitForPageLoad();
        By newElem = _convertElemToBy(nextPageElement);
        boolean holdPage = true;
        int count = 0;
        while (_isElementVisible(newElem) && holdPage) {
            count++;
            holdPage = false;
        }
    }

    /**
     * This method is only for RPM (or) Angular default date picker
     *
     * @param dateString in dd/MM/yyy format
     */
    public void _setDate(String dateString) {
        try {
            Date expectedDate = new SimpleDateFormat("dd/MM/yyyy").parse(dateString);
            Calendar expectedDateCal = Calendar.getInstance();
            expectedDateCal.setTime(expectedDate);
            boolean isYearVisible = false;
            _click(By.xpath("//button[@aria-label='Choose month and year']"));
            while (!isYearVisible) {
                String dateRange = driver.findElement(By.xpath("//button[@aria-label='Choose date']")).getText();
                String[] range = dateRange.split(" ");
                int start = Integer.parseInt(range[0].trim());
                int end = Integer.parseInt(range[2].trim());
                if (start <= expectedDateCal.get(Calendar.YEAR) && end >= expectedDateCal.get(Calendar.YEAR)) {
                    _click(By.xpath("//div[contains(text(),'" + expectedDateCal.get(Calendar.YEAR) + "')]"));
                    isYearVisible = true;
                } else if (expectedDateCal.get(Calendar.YEAR) > end) {
                    _click(By.xpath("//button[@aria-label='Next 24 years']"));
                } else {
                    _click(By.xpath("//button[@aria-label='Previous 24 years']"));
                }
            }
            _click(By.xpath("//div[contains(text(),'" + new SimpleDateFormat("MMM").format(expectedDateCal.getTime()).toUpperCase(Locale.ROOT) + "')]"));
            _click(By.xpath("//div[contains(text(),'" + expectedDateCal.get(Calendar.DAY_OF_MONTH) + "')]"));
        } catch (ParseException e) {
            log.info("Invalid date format, please specify the date in dd/MM/yyyy format ");
        } catch (Exception e) {
            log.info("Element not found");
        }
    }

    /**
     * This method is only for HF-DTx
     *
     * @param dateString in dd/MM/yyy format
     */
    public void _setDtxDate(String dateString) {
        try {
            Date expectedDate = new SimpleDateFormat("dd/MM/yyyy").parse(dateString);
            Calendar expectedDateCal = Calendar.getInstance();
            expectedDateCal.setTime(expectedDate);

            boolean isYearVisible = false;
            driver.findElement(By.xpath("//div[@class='myDpMonthYearText']/button[2]")).click();
            while (!isYearVisible) {
                String dateRange = driver.findElement(By.xpath("//div[@class='myDpMonthYearText']/button")).getText();
                String[] range = dateRange.split(" ");
                int start = Integer.parseInt(range[0].trim());
                int end = Integer.parseInt(range[2].trim());

                if (start <= expectedDateCal.get(Calendar.YEAR) && end >= expectedDateCal.get(Calendar.YEAR)) {
                    driver.findElement(By.xpath("//span[contains(text(),'" + expectedDateCal.get(Calendar.YEAR) + "')]")).click();
                    isYearVisible = true;
                } else if (expectedDateCal.get(Calendar.YEAR) > end) {
                    driver.findElement(By.xpath("//div[@class='myDpNextBtn']/button")).click();
                } else {
                    driver.findElement(By.xpath("//div[@class='myDpPrevBtn']/button")).click();
                }
            }

            driver.findElement(By.xpath("//div[@class='myDpMonthYearText']/button[1]")).click();
            String month = new SimpleDateFormat("MMM").format(expectedDateCal.getTime()).toLowerCase(Locale.ROOT);
            driver.findElement(By.xpath("//span[contains(text(),'" + StringUtils.capitalize(month) + "')]")).click();

            int weekOfMonth = expectedDateCal.get(Calendar.WEEK_OF_MONTH);
            int dayOfWeek = expectedDateCal.get(Calendar.DAY_OF_WEEK);

            if (dayOfWeek == 7) {
                dayOfWeek = 0;
            } else {
                weekOfMonth -= 1;
                dayOfWeek -= 1;
            }
            driver.findElement(By.id("d_" + weekOfMonth + "_" + dayOfWeek)).click();

        } catch (ParseException e) {
            log.info("Invalid date format, please specify the date in dd/MM/yyyy format ");
        }
    }

    /**
     * @param data
     * @param element
     * @throws Exception
     */
    public void _clickTheOption(String data, WebElement element) throws Exception {
        _click(element);
        _click(By.xpath("//span[contains(text(),'" + data + "')]"));
    }

    /**
     * Take screenshot of given element
     *
     * @param element
     * @return BufferedImage of element
     * @throws IOException
     */
    public BufferedImage _getElementScreenShot(WebElement element) throws IOException {
        return ImageIO.read(element.getScreenshotAs(OutputType.FILE));
    }

    /**
     * Take screen shot of given element and verify is data points present
     *
     * @param element
     * @return true if data points present else false
     * @throws IOException
     */
    public boolean _isDataAvailableInGraph(WebElement element) throws IOException {
        return graphLib.isDataAvailable(ImageIO.read(element.getScreenshotAs(OutputType.FILE)));
    }

    /**
     * Take screen shot of given element and verify is given color code present
     *
     * @param element
     * @param colourCode
     * @return true if color code present else false
     * @throws IOException
     */
    public boolean _isDataAvailableInGraph(WebElement element, String colourCode) throws IOException {
        return graphLib.isDataAvailable(ImageIO.read(element.getScreenshotAs(OutputType.FILE)), colourCode);
    }

    /**
     * Verify data points present in provided bufferedImage
     *
     * @param bufferedImage
     * @return true if data points present else false
     * @throws IOException
     */
    public boolean _isDataAvailableInGraph(BufferedImage bufferedImage) throws IOException {
        return graphLib.isDataAvailable(bufferedImage);
    }

    /**
     * Verify given color code present in provided bufferedImage
     *
     * @param bufferedImage
     * @param colourCode
     * @return true if color code present else false
     * @throws IOException
     */
    public boolean _isDataAvailableInGraph(BufferedImage bufferedImage, String colourCode) throws IOException {
        return graphLib.isDataAvailable(bufferedImage, colourCode);
    }

    /**
     * Verify data points present in provided file
     *
     * @param file
     * @return true if data points present else false
     * @throws IOException
     */
    public boolean _isDataAvailableInGraph(File file) throws IOException {
        return graphLib.isDataAvailable(ImageIO.read(file));
    }

    /**
     * Verify given color code present in provided file
     *
     * @param file
     * @param colourCode
     * @return true if color code present else false
     * @throws IOException
     */
    public boolean _isDataAvailableInGraph(File file, String colourCode) throws IOException {
        return graphLib.isDataAvailable(ImageIO.read(file), colourCode);
    }

    /**
     * Take screenshot of given actual and expected and compare
     *
     * @param actual
     * @param expected
     * @return true if both images matched else false
     * @throws IOException
     */
    public boolean _isImagesMatched(WebElement actual, WebElement expected) throws IOException {
        return graphLib.isExactMatch(
                ImageIO.read(actual.getScreenshotAs(OutputType.FILE)),
                ImageIO.read(expected.getScreenshotAs(OutputType.FILE)));
    }

    /**
     * Compare actual and expected bufferedImages
     *
     * @param actual
     * @param expected
     * @return true if both images matched else false
     * @throws IOException
     */
    public boolean _isImagesMatched(BufferedImage actual, BufferedImage expected) throws IOException {
        return graphLib.isExactMatch(actual, expected);
    }

    /**
     * Compare actual and expected files
     *
     * @param actualImage
     * @param expectedImage
     * @return true if both images matched else false
     * @throws IOException
     */
    public boolean _isImagesMatched(File actualImage, File expectedImage) throws IOException {
        return graphLib.isExactMatch(ImageIO.read(actualImage), ImageIO.read(expectedImage));
    }

    /**
     * Compare actual and expected files
     *
     * @param actualImageElement
     * @param expectedImage
     * @return true if both images matched else false
     * @throws IOException
     */
    public boolean _isImagesMatched(WebElement actualImageElement, File expectedImage) throws IOException {
        return graphLib.isExactMatch(ImageIO.read(actualImageElement.getScreenshotAs(OutputType.FILE)),
                ImageIO.read(expectedImage));
    }

    public void log(Object message) {
        if (customConfig.getScenario() != null)
            customConfig.getScenario().log(message.toString());
        else
            System.err.println("Scenario not found for logging");
        log.info(message);
    }

    public String _getAttributeValue(WebElement element, String placeholder) {
        return element.getAttribute(placeholder);
    }

    public boolean _waitForElementInvisible(WebElement element) {
        try {
            By eID = _convertElemToBy(element);
            new FluentWait<WebDriver>(driver)
                    .withTimeout(Duration.ofSeconds(appConfig.getSeleniumTimeout()))
                    .pollingEvery(Duration.ofMillis(10))
                    .ignoring(NoSuchElementException.class)
                    .ignoring(StaleElementReferenceException.class)
                    .ignoring(ElementNotInteractableException.class)
                    .ignoring(ElementNotVisibleException.class)
                    .ignoring(ElementNotSelectableException.class)
                    .ignoring(ElementClickInterceptedException.class)
                    .until(ExpectedConditions.invisibilityOfElementLocated(eID));
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean _waitForElementInvisible(By by) {
        try {
            new FluentWait<WebDriver>(driver)
                    .withTimeout(Duration.ofSeconds(appConfig.getSeleniumTimeout()))
                    .pollingEvery(Duration.ofMillis(10))
                    .ignoring(NoSuchElementException.class)
                    .ignoring(StaleElementReferenceException.class)
                    .ignoring(ElementNotInteractableException.class)
                    .ignoring(ElementNotVisibleException.class)
                    .ignoring(ElementNotSelectableException.class)
                    .ignoring(ElementClickInterceptedException.class)
                    .until(ExpectedConditions.invisibilityOfElementLocated(by));
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean _waitForElementVisible(WebElement element) {
        try {
            By eID = _convertElemToBy(element);
            new FluentWait<WebDriver>(driver)
                    .withTimeout(Duration.ofSeconds(appConfig.getSeleniumTimeout()))
                    .pollingEvery(Duration.ofMillis(10))
                    .ignoring(NoSuchElementException.class)
                    .ignoring(StaleElementReferenceException.class)
                    .ignoring(ElementNotInteractableException.class)
                    .ignoring(ElementNotVisibleException.class)
                    .ignoring(ElementNotSelectableException.class)
                    .ignoring(ElementClickInterceptedException.class)
                    .until(ExpectedConditions.visibilityOfElementLocated(eID));
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean _waitForElementVisible(WebElement element, int seconds) {
        try {
            By eID = _convertElemToBy(element);
            new FluentWait<WebDriver>(driver)
                    .withTimeout(Duration.ofSeconds(seconds))
                    .pollingEvery(Duration.ofMillis(10))
                    .ignoring(NoSuchElementException.class)
                    .ignoring(StaleElementReferenceException.class)
                    .ignoring(ElementNotInteractableException.class)
                    .ignoring(ElementNotVisibleException.class)
                    .ignoring(ElementNotSelectableException.class)
                    .ignoring(ElementClickInterceptedException.class)
                    .until(ExpectedConditions.visibilityOfElementLocated(eID));
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean _waitForElementVisible(By by) {
        try {
            new FluentWait<WebDriver>(driver)
                    .withTimeout(Duration.ofSeconds(appConfig.getSeleniumTimeout()))
                    .pollingEvery(Duration.ofMillis(10))
                    .ignoring(NoSuchElementException.class)
                    .ignoring(StaleElementReferenceException.class)
                    .ignoring(ElementNotInteractableException.class)
                    .ignoring(ElementNotVisibleException.class)
                    .ignoring(ElementNotSelectableException.class)
                    .ignoring(ElementClickInterceptedException.class)
                    .until(ExpectedConditions.visibilityOfElementLocated(by));
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    /**
     * @param pathElement
     * @param textIdentifier
     * @param startPoint     Only accept 0 or -1
     *                       0 -> Start from Mid of the graph
     *                       -1 -> Start from left side of the graph
     * @return Collection of Tool-tips values
     * @throws InterruptedException Note: BI tooltips is not supported yet. Only for Web tool-tips
     */
    public Set<String> collectValuesFromGraph(WebElement pathElement, String textIdentifier, Integer startPoint) throws InterruptedException {

        if (driver instanceof AndroidDriver || driver instanceof IOSDriver)
            throw new RuntimeException("Only Support for Web graphs");

        By valuePoint = By.xpath("//*[contains(text(),'textIdentifier')]".replace("textIdentifier", textIdentifier)),
                timeValPoint = By.xpath("//*[contains(text(),'textIdentifier')]/..//span[contains(text(),'Time:')]".replace("textIdentifier", textIdentifier));

        int endPoint = 0;
        Set<String> values = new LinkedHashSet<>();

        Actions action = new Actions(driver);
        _scrollToElement(pathElement);
        _sleep(1);
        action.click(pathElement);
        _sleep(3);
        System.out.println("Rectangle width : " + pathElement.getSize().width);

        startPoint = startPoint * (pathElement.getSize().width / 2);
        endPoint = pathElement.getSize().width / 2;

        // ======= Left to Right path ============ //
        for (int i = startPoint; i < endPoint; i += 2) {
            try {
                action.moveToElement(pathElement, i, -10).click().build().perform();
            } catch (Exception e) {
                System.out.println("Right collect stopped at " + i);
                break;
            }
            if (driver.getPageSource().contains(textIdentifier)) {
                String value = null;
                try {
                    value = driver.findElement(timeValPoint).getText() + " " +
                            driver.findElement(valuePoint).getText();
                } catch (Exception e) {
                    System.err.println("Element not found");
                    Thread.sleep(1000);
                }
                values.add(value);
            }
        }
        System.out.println("Collected values " + values);
        return values;
    }


    /**
     * Use this function to press the back button in device/web
     */
    public void _pressBack() {
        if (driver instanceof AndroidDriver) {
            AndroidDriver<WebElement> android = (AndroidDriver<WebElement>) driver;
            android.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
        } else if (driver instanceof IOSDriver) {
            IOSDriver<WebElement> ios = (IOSDriver<WebElement>) driver;
            ios.navigate().back();
        } else
            driver.navigate().back();
    }

    /**
     * Use this function to press the back button in device/web by given number of times.
     *
     * @param times
     */
    public void _pressBack(int times) {
        for (int i = 0; i < times; i++) {
            if (driver instanceof AndroidDriver) {
                AndroidDriver<WebElement> android = (AndroidDriver<WebElement>) driver;
                android.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
            } else if (driver instanceof IOSDriver) {
                IOSDriver<WebElement> ios = (IOSDriver<WebElement>) driver;
                ios.navigate().back();
            } else
                driver.navigate().back();
        }
    }

    public void _waitForLoading() {
        _waitForElementInvisible(By.id("com.biofourmis.careathomerpm:id/textview_message_loading"));
        _waitForElementInvisible(By.className("android.widget.ProgressBar"));
    }

    public void _hideKeyBoard() {
        if (driver instanceof AndroidDriver) {
            AndroidDriver<WebElement> android = (AndroidDriver<WebElement>) driver;
            android.hideKeyboard();
        } else if (driver instanceof IOSDriver) {
            // IOSDriver<WebElement> ios = (IOSDriver<WebElement>) driver;
            // ios.hideKeyboard();
        }
        _sleep(5);
    }

    protected void _pressDown(int count) {
        Actions actionProvider = new Actions(driver);
        for (int i = 0; i < count; i++)
            actionProvider.sendKeys(Keys.ARROW_DOWN).build().perform();
    }


    public boolean _waitForProgressBarDisappearHCP() {
        try {
            WebElement element = driver.findElement(By.xpath("//*[@id=com.biofourmis.careathomehcp:id/progress_bar]"));
            _sleep(10);
            //TODO remove hardcode wait
            if (_waitForElementInvisible(element, 60))
                return true;
            else
                return false;
        } catch (Exception e) {
            return true;
        }
    }

    private boolean _waitForElementInvisible(WebElement element, int i) {
        return _waitForElementInvisible(element, i);
    }

    public String _getString(String key) throws MalformedURLException {
        return localisationConfig.resourceBundle().getString(key);
    }

    /**
     * Use this method to switch between windows
     * 0 to switch to parent window and 1 to n for child windows
     */
    public void _switchToWindow(int window) {
        ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(window));
    }

    /**
     * Use this method to open new window with url
     */
    public void _openNewWindow(String url) {
        ((JavascriptExecutor) driver).executeScript("window.open('url')");
    }

    /**
     * Read toast message data and return as string
     *
     * @param driver
     * @return
     */
    public String _getToastMessage(WebDriver driver) {
        boolean isAndroid = false;
        if (driver instanceof AndroidDriver)
            isAndroid = true;
        else if (driver instanceof IOSDriver)
            isAndroid = false;
        else {
            log.info("Can't use this for Web. Only Android and iOS supported toast messages.");
        }

        if (isAndroid) {
            try {
                new FluentWait<WebDriver>(driver)
                        .withTimeout(Duration.ofSeconds(20))
                        .pollingEvery(Duration.ofMillis(10))
                        .ignoring(NoSuchElementException.class)
                        .ignoring(StaleElementReferenceException.class)
                        .ignoring(ElementNotInteractableException.class)
                        .ignoring(ElementNotVisibleException.class)
                        .ignoring(ElementNotSelectableException.class)
                        .ignoring(ElementClickInterceptedException.class)
                        .until(ExpectedConditions.presenceOfElementLocated(By.xpath("/hierarchy/android.widget.Toast")));
            } catch (Exception ex) {
                //DO Nothing
            }
            return driver.findElement(By.xpath("//android.widget.Toast")).getText();
        } else {
            //TODO for iOS Test
            return "Write you logic to get iOS toast.";
        }
    }
}
