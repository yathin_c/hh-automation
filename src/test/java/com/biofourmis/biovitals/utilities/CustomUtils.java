package com.biofourmis.biovitals.utilities;

import com.github.javafaker.Faker;
import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import lombok.extern.log4j.Log4j;
import org.apache.hc.client5.http.classic.HttpClient;
import org.apache.hc.client5.http.classic.methods.HttpPost;
import org.apache.hc.client5.http.entity.mime.MultipartEntityBuilder;
import org.apache.hc.client5.http.impl.classic.HttpClientBuilder;
import org.apache.hc.core5.http.HttpResponse;
import org.openqa.selenium.WebDriver;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.function.Function;
import java.util.zip.ZipOutputStream;

/**
 * Add custom functions related to your project here
 */
@Log4j
public class CustomUtils {

    WebDriver driver;
    Faker faker = new Faker();
    private static Map<String, String> userInput;
    private static Map<String, ArrayList<String>> chat;
    private static ArrayList<String> patientChat;
    private static ArrayList<String> clinicianChat;
    private static Map<String, String> clinicalNote;
    private static Map<String, String> diaryNote;
    private static Map<String, String> patientDetails;
    private static String carePlanMessage;
    private static List<HashMap<String, String>> activityDetails;
    private static String qrHashUrl = "";
    private static boolean flag = true;
    public static String mnr;

    public String getQrHashUrl() {
        return qrHashUrl;
    }

    public void setQrHashUrl(String qrHashUrl) {
        this.qrHashUrl = qrHashUrl;
    }

    public CustomUtils(WebDriver driver) {
        if (flag) {
            userInput = new LinkedHashMap<>();
            chat = new LinkedHashMap<>();
            patientChat = new ArrayList<>();
            clinicianChat = new ArrayList<>();
            chat.put("Patient", patientChat);
            chat.put("Clinician", clinicianChat);
            clinicalNote = new LinkedHashMap<>();
            diaryNote = new LinkedHashMap<>();
            patientDetails = new LinkedHashMap<>();
            carePlanMessage = getRandomString();
            activityDetails = new ArrayList<>();
            flag = false;
        }
        this.driver = driver;
    }

    public String getRandomString() {
        int leftLimit = 48; // numeral '0'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();
        String generatedString = random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
        if (generatedString.length() == 0)
            getRandomString();
        return generatedString;
    }


    public void setPatientDetails(String mrn, String name) {
        patientDetails.put("MRN", mrn);
        patientDetails.put("Name", name);
    }

    public Map<String, String> getPatientDetails() {
        return patientDetails;
    }

    public String getCarePlan() {
        return carePlanMessage;
    }

    public void setCarePlanMessage(String str) {
        carePlanMessage = str;
    }

    public List<String> verifyWebLoginText() {
        return List.of(
                " Powered by Biovitals™ ",
                " Hospital@Home™ ",
                " Email address ",
                " Password ",
                " Forgot password?",
                //" Log in with Single Sign-On",
                " Log in ",
                "your@email.com"
        );
    }

    private Function<String, String> defaultDataForAddPatient = (String value) -> {
        Map<String, String> data = new LinkedHashMap<>();
        data.put("MRN", String.valueOf(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)));
        data.put("F_Name", String.valueOf(faker.name().firstName()));
        data.put("L_Name", String.valueOf(faker.name().lastName()).replace("'", ""));
        data.put("Gender", "Male");
        data.put("DOB", "09/15/1992");
        data.put("Address_line_1", String.valueOf(faker.address().fullAddress()));
        data.put("Address_line_2", String.valueOf(faker.address().secondaryAddress()));
        data.put("Zip_code", String.valueOf(faker.number().digits(5)));
        data.put("City", String.valueOf(faker.address().city().replaceAll("'", "")));
        data.put("State", String.valueOf(faker.address().state()).replaceAll("'", ""));
        data.put("Patient_Mob", "55555" + String.valueOf(faker.number().randomNumber(5, true)));
        data.put("Patient_Landline", "44444" + String.valueOf(faker.number().randomNumber(5, true)));
        data.put("Preferred_Lang", "English");
        data.put("Family_Pho_Num", "6666" + String.valueOf(faker.number().randomNumber(5, true)));
        data.put("Familial_Relation", "Brother");
        data.put("Code_Status", "Full code");
        data.put("Allergies", "NKDA");
        data.put("Contact_precautions", "Contact");
        data.put("Admitting_diagnosis", "SSTI");
        data.put("Clinical_summary", getRandomString());
        data.put("Physicians", "Physician Automation");
        data.put("Nurses_Paramedics", "Nurse Automation");
        data.put("Length_of_stay", "Less than 3 months");
        data.put("Devices", "VitalPatch (2 patches)");
        data.put("Documents", "All");
        data.put("Ship_Use_patient_address", "false");
        data.put("Ship_Address_1", String.valueOf(faker.address().fullAddress()));
        data.put("Ship_Address_2", String.valueOf(faker.address().fullAddress()));
        data.put("Ship_ZipCode", String.valueOf(faker.number().randomNumber(4, true)));
        data.put("Ship_City", String.valueOf(faker.address().city()).replaceAll("'", ""));
        data.put("Ship_State", String.valueOf(faker.address().state()).replaceAll("'", ""));
        data.put("Enrollment_type", "PACE Home (onboarding call)");
        data.put("Point_of_contact", "Patient");
        data.put("Point_F_Name", String.valueOf(faker.name().firstName()));
        data.put("Point_L_Name", String.valueOf(faker.name().lastName()).replaceAll("'", ""));
        data.put("Point_number", String.valueOf(faker.number().digits(10)));
        data.put("Email_equipment", " No ");
        data.put("QR_Start", "");
        data.put("QR_End", "");
        return data.get(value);
    };

    public void setQRExpireDetails(int days) {
        int time = LocalDateTime.now().getHour();
        String startTimeValue = "";
        String endTimeValue = "";
        String minute = String.valueOf(LocalDateTime.now().getMinute());
        if (minute.length() == 1)
            minute = "0" + "" + minute;

        if (time > 11) {
            startTimeValue = String.valueOf(time - 12) + ":" + minute + " PM, ";
            endTimeValue = String.valueOf(time - 12) + ":" + minute + " PM, ";
        } else if (time == 0) {
            startTimeValue = String.valueOf(12) + ":" + minute + " AM, ";
            endTimeValue = String.valueOf(12) + ":" + minute + " AM, ";
        } else {
            startTimeValue = String.valueOf(time) + ":" + minute + " AM, ";
            endTimeValue = String.valueOf(time) + ":" + minute + " AM, ";
        }

        String monthStartName = LocalDateTime.now().getMonth().name().substring(0, 1).toUpperCase(Locale.ROOT) +
                LocalDateTime.now().getMonth().name().substring(1, 3).toLowerCase(Locale.ROOT);

        String monthEndName = LocalDateTime.now().plusDays(days).getMonth().name().substring(0, 1).toUpperCase(Locale.ROOT) +
                LocalDateTime.now().plusDays(days).getMonth().name().substring(1, 3).toLowerCase(Locale.ROOT);

        startTimeValue += monthStartName + " " + LocalDateTime.now().getDayOfMonth() + ", " + LocalDateTime.now().getYear();

        endTimeValue += monthEndName + " " + LocalDateTime.now().plusDays(days).getDayOfMonth() + ", " + LocalDateTime.now().plusDays(days).getYear();

        Map<String, String> qrTimeLine = new HashMap<>();
        qrTimeLine.put("QR_Start", startTimeValue);
        qrTimeLine.put("QR_End", endTimeValue);
        getDefaultPatientDetails(qrTimeLine);
    }

    public void updateQRExpireDetails(int days) {
        String start_QR = getDefaultPatientDetails().get("QR_Start");
        String end_QR = getDefaultPatientDetails().get("QR_End");
        setQRExpireDetails(days);

        if (getDefaultPatientDetails().get("QR_Start").equalsIgnoreCase(start_QR)
                && getDefaultPatientDetails().get("QR_End").equalsIgnoreCase(end_QR)) {
//TODO
        }
    }

    public Map<String, String> getDefaultPatientDetails(Map<String, String> input) {
        if (userInput.isEmpty())
            userInput = new HashMap<>(input);
        else
            userInput.putAll(input);

        List<String> fields = List.of("MRN", "F_Name", "L_Name", "Gender", "DOB", "Address_line_1",
                "Address_line_2", "Zip_code", "City", "State", "Patient_Mob", "Patient_Landline", "Preferred_Lang",
                "Family_Pho_Num", "Familial_Relation", "Code_Status", "Allergies", "Contact_precautions",
                "Admitting_diagnosis", "Clinical_summary", "Physicians", "Nurses_Paramedics", "Length_of_stay",
                "Devices", "Documents", "Ship_Use_patient_address", "Ship_Address_1", "Ship_Address_2",
                "Enrollment_type", "Point_of_contact", "Point_F_Name", "Point_L_Name",
                "Point_number", "Email_equipment", "Ship_ZipCode", "Ship_City", "Ship_State", "QR_Start", "QR_End"
        );
        fields.parallelStream().forEach(values -> {
                    if (!userInput.containsKey(values))
                        userInput.put(values, defaultDataForAddPatient.apply(values));
                }
        );
        return userInput;
    }

    public Map<String, String> getDefaultPatientDetails() {
        if (!userInput.isEmpty()) {
            return userInput;
        }
        userInput = new HashMap<>();
        List<String> fields = List.of("MRN", "F_Name", "L_Name", "Gender", "DOB", "Address_line_1",
                "Address_line_2", "Zip_code", "City", "State", "Patient_Mob", "Patient_Landline", "Preferred_Lang",
                "Family_Pho_Num", "Familial_Relation", "Code_Status", "Allergies", "Contact_precautions",
                "Admitting_diagnosis", "Clinical_summary", "Physicians", "Nurses_Paramedics", "Length_of_stay",
                "Devices", "Documents", "Ship_Use_patient_address", "Ship_Address_1", "Ship_Address_2",
                "Enrollment_type", "Point_of_contact", "Point_F_Name", "Point_L_Name",
                "Point_number", "Email_equipment", "Ship_ZipCode", "Ship_City", "Ship_State", "QR_Start", "QR_End"
        );
        fields.parallelStream().forEach(values -> {
                    if (!userInput.containsKey(values)) {
                        userInput.put(values, defaultDataForAddPatient.apply(values));
                    }
                }
        );
        return userInput;
    }

    public String getRandomLanguage() {
        return "English";
        //  return List.of("English", "Spanish").get(faker.number().numberBetween(0, 1));
    }

    public List<String> patientInfoPage() {
        return List.of(" Patient information ",
                "MRN*",
                "First name",
                "Last name",
                "Gender",
                "DOB",
                "Address line 1",
                "Address line 2",
                "Zip code",
                "City",
                "State",
                "Patient's mobile number",
                "Patient's landline number",
                "Patient's preferred language",
                "English ",
                " Family contact information ",
                "Phone number",
                "Familial relationship",
                " Next ",
                " Cancel ");
    }

    public List<String> medicalInfoPage() {
        return List.of(" Medical information ",
                " Please enter patient's medical history and clinical summary ",
                " Medical history ",
                "Code status",
                " Full code ",
                " DNI/DNR ",
                " CMO ",
                " Other ",
                " Unknown ",
                "Allergies",
                " NKDA ",
                " Allergies per EHR ",
                "Contact precautions",
                "Admitting diagnosis",
                " Clinical summary ",
                " Previous ",
                " Next ",
                " Cancel ");
    }

    public List<String> careTeamInfoPage() {
        return List.of(" Care team ",
                " Physicians ",
                " Nurses and Paramedics ",
                " Previous ",
                " Next ",
                " Cancel "
        );
    }

    public List<String> equipmentInfoPage() {
        return List.of(" Equipment information ",
                "Length of stay*",
                " Less than 3 months ",
                " More than 3 months ",
                " Kit content ",
                "Devices*",
                "Documents*",
                "Privacy Policy",
                "Terms & Conditions",
                "Consent",
                "Addressed / stamped return envelope",
                "Quickstart guide",
                " Shipping address ",
                "Use patient address",
                "Address line 1*",
                "Address line 2",
                "Zip code*",
                "City*",
                "State*",
                "Enrollment type*",
                "Point of contact*",
                " Patient ",
                " Caregiver ",
                "First name",
                "Last name",
                "Phone number",
                "Email equipment information to the Biofourmis logistics team after patient’s admission?*",
                " Yes ",
                " No ",
                " Previous ",
                " Next ",
                " Cancel "
        );
    }

    public List<String> reviewPage() {
        return List.of(" Review ",
                " Please review the filled in information carefully. ",
                " Patient information ",
                " MRN ",
                " Full name ",
                " Gender ",
                " DOB ",
                " Address ",
                " Zip code ",
                " City ",
                " State ",
                " Patient's mobile number ",
                " Patient's landline number ",
                " Patient's preferred language ",
                " Family contact information ",
                " Phone number ",
                " Familial relationship ",
                " Care team ",
                " Equipment information ",
                " Length of stay ",
                " Devices ",
                " Documents ",
                " Shipping address ",
                " Zip code ",
                " City ",
                " State ",
                " Enrollment type ",
                " Point of contact ",
                " Full name ",
                " Phone number ",
                " Email equipment information to the Biofourmis logistics team after patient’s admission? ",
                " Finish ",
                " Previous ",
                " Cancel ");
    }

    private Function<String, String> defaultDiaryNotes = (String value) -> {
        List<String> iconsType = Arrays.asList("Pain", "Fever", "Cough", "Vitals", "Eating", "Drinking", "Wound", "Medicine", "Weight", "Other");
        Map<String, String> data = new LinkedHashMap<>();
        data.put("Title", faker.name().username() + getRandomString() + faker.number().randomNumber());
        data.put("Description", getRandomString());
        data.put("Type", iconsType.get(faker.number().numberBetween(0, 9)));
        data.put("Images", String.valueOf(faker.number().numberBetween(1, 5)));
        return data.get(value);
    };

    public Map<String, String> getDefaultDiaryNoteDetails(Map<String, String> input) {
        if (diaryNote.isEmpty())
            diaryNote = new LinkedHashMap<>(input);
        else
            diaryNote.putAll(input);
        List<String> fields = List.of("Title", "Description", "Type", "Images");
        fields.parallelStream().forEach(values -> {
                    if (!diaryNote.containsKey(values))
                        diaryNote.put(values, defaultDiaryNotes.apply(values));
                }
        );
        return diaryNote;
    }

    public List<String> getIconType() {
        return new ArrayList<>(Arrays.asList("Pain", "Fever", "Cough", "Vitals", "Eating", "Drinking", "Wound", "Medicine", "Weight", "Other"));
    }

    public Map<String, String> getDefaultDiaryNoteDetails() {
        if (!diaryNote.isEmpty()) {
            return diaryNote;
        }
        diaryNote = new LinkedHashMap<>();
        List<String> fields = List.of("Title", "Description", "Type", "Images");
        fields.parallelStream().forEach(values -> {
                    if (!diaryNote.containsKey(values)) {
                        diaryNote.put(values, defaultDiaryNotes.apply(values));
                    }
                }
        );
        return diaryNote;
    }

    private Function<String, String> defaultClinicalNotes = (String value) -> {
        Map<String, String> data = new LinkedHashMap<>();
        data.put("Title", faker.lordOfTheRings().character().toLowerCase(Locale.ROOT) + LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
        data.put("Description", faker.aviation().aircraft().toLowerCase(Locale.ROOT) + LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
        return data.get(value);
    };

    public Map<String, String> getDefaultClinicalNotesDetails(Map<String, String> input) {
        if (clinicalNote.isEmpty())
            clinicalNote = new LinkedHashMap<>(input);
        else
            clinicalNote.putAll(input);
        List<String> fields = List.of("Title", "Description");
        fields.parallelStream().forEach(values -> {
                    if (!clinicalNote.containsKey(values))
                        clinicalNote.put(values, defaultClinicalNotes.apply(values));
                }
        );
        return clinicalNote;
    }

    public Map<String, String> getDefaultClinicalNotesDetails() {
        if (!clinicalNote.isEmpty()) {
            return clinicalNote;
        }
        clinicalNote = new LinkedHashMap<>();
        List<String> fields = List.of("Title", "Description");
        fields.parallelStream().forEach(values -> {
                    if (!clinicalNote.containsKey(values)) {
                        clinicalNote.put(values, defaultClinicalNotes.apply(values));
                    }
                }
        );
        return clinicalNote;
    }

    public ArrayList<String> getTimeline(int minutesGap) {
        ArrayList<String> timeLine = new ArrayList<>();
        LocalTime lt = LocalTime.parse("01:00");
        String tempValue = "";
        for (int i = 0; i < (12 * 60) / minutesGap; i++) {
            String temp = lt.toString();
            if (temp.contains(":00"))
                temp = temp.split(":")[0] + "AM";
            else
                temp = temp + "AM";

            if (temp.startsWith("0")) {
                tempValue = temp.substring(1, temp.length());
                if (!timeLine.contains(tempValue))
                    timeLine.add(tempValue);
            } else {
                if (!timeLine.contains(temp))
                    timeLine.add(temp);
            }

            lt = lt.plusMinutes(minutesGap);
        }

        lt = LocalTime.parse("01:00");

        for (int i = 0; i < (12 * 60) / minutesGap; i++) {
            String temp = lt.toString();
            if (temp.contains(":00"))
                temp = temp.split(":")[0] + "PM";
            else
                temp = temp + "PM";

            if (temp.startsWith("0")) {
                tempValue = temp.substring(1, temp.length());
                if (!timeLine.contains(tempValue))
                    timeLine.add(tempValue);
            } else {
                if (!timeLine.contains(temp))
                    timeLine.add(temp);
            }
            lt = lt.plusMinutes(minutesGap);
        }
        return timeLine;
    }

    public String nextAvailableTimeline(int minGap) {
        Integer minute = 0;
        String nextSlot = "";
        LocalDateTime localDateTime = LocalDateTime.now();

        if (minGap == 60) {
            int currentMin = LocalDateTime.now().getMinute();
            localDateTime = LocalDateTime.now().plusMinutes(minGap);
            if (currentMin > 25)
                localDateTime = localDateTime.plusMinutes(minGap);

            if (localDateTime.getHour() < 11) {
                int hour = 12;
                if (localDateTime.getHour() == 0)
                    hour = 12;
                else
                    hour = localDateTime.getHour();
                nextSlot = hour + "AM";

            } else if (localDateTime.getHour() == 12)
                nextSlot = localDateTime.getHour() + "PM";
            else
                nextSlot = localDateTime.getHour() - 12 + "PM";
        } else if (minGap > 20) {
            localDateTime = LocalDateTime.now().plusMinutes(minGap);
            if (localDateTime.getHour() < 11) {
                int hour = 12;
                if (localDateTime.getHour() == 0)
                    hour = 12;
                else
                    hour = localDateTime.getHour();
                nextSlot = hour + "AM";

            } else if (localDateTime.getHour() == 12)
                nextSlot = localDateTime.getHour() + "PM";
            else
                nextSlot = localDateTime.getHour() - 12 + "PM";
        } else {
            minute = LocalDateTime.now().getMinute();
            if (LocalDateTime.now().getMinute() == 0)
                minute = minGap;
            else if (LocalDateTime.now().getMinute() >= 1 && LocalDateTime.now().getMinute() <= minGap)
                minute = minGap * 2;
            else if (LocalDateTime.now().getMinute() > minGap && LocalDateTime.now().getMinute() < minGap * 2)
                minute = minGap * 3;
            else
                minute = minGap * 4;

            if (minute >= 46 && minute < 50) {
                localDateTime = localDateTime.plusMinutes(minute);
                minute = 0;
            } else if (minute >= 60 && localDateTime.getMinute() > 45) {
                localDateTime = localDateTime.plusHours(1);
                minute = minGap;
            } else if (minute >= 60 && localDateTime.getMinute() < 45) {
                localDateTime = localDateTime.plusHours(1);
                minute = 0;
            }

            if (localDateTime.getHour() < 11) {
                int hour = 12;
                if (localDateTime.getHour() == 0)
                    hour = 12;
                else
                    hour = localDateTime.getHour();
                if (minute == 0)
                    nextSlot = hour + "AM";
                else
                    nextSlot = hour + ":" + minute + "AM";
            } else if (localDateTime.getHour() == 12)
                if (minute == 0)
                    nextSlot = localDateTime.getHour() + "PM";
                else
                    nextSlot = localDateTime.getHour() + ":" + minute + "PM";
            else if (minute == 0)
                nextSlot = localDateTime.getHour() - 12 + "PM";
            else
                nextSlot = localDateTime.getHour() - 12 + ":" + minute + "PM";
        }

        if (getTimeline(minGap).contains(nextSlot))
            return nextSlot;
        else
            throw new RuntimeException("This is not suitable time to add activity in today's timeline. Please try to change execution time.");
    }

    /**
     * @param arg0
     * @param nextTimeLine
     * @param details
     */
    public void setActivityDetails(String arg0, String nextTimeLine, String details) {
        HashMap<String, String> data = new HashMap<>();
        data.put("Name", arg0);
        data.put("Time", nextTimeLine);
        switch (arg0) {
            case "Use inhaler":
                System.out.println("Use inhaler");
                break;
            case "Use nebulizer":
                System.out.println("Use nebulizer");
                break;
            case "Inject insulin":
                System.out.println("Inject insulin");
                break;
            case "Check blood pressur":
                System.out.println("Check blood pressur");
                break;
            case "Drink water":
                System.out.println("Drink water");
                data.put("No_Of_Glasses", details);
                break;
        }
        activityDetails.add(data);
    }

    public void updateActivityDetails(String arg0, String nextTimeLine) {
        for (HashMap<String, String> map : activityDetails)
            if (map.get("Name").equals(arg0))
                activityDetails.get(activityDetails.indexOf(map)).put("Time", nextTimeLine);
    }

    public List<HashMap<String, String>> getActivityDetails() {
        return activityDetails;
    }

    public Map<String, String> getQRHashDetails(String urlHash) throws IOException, NotFoundException {
        log.info("Get QR Hash Details.");
        Map<DecodeHintType, Object> hintsMap;
        hintsMap = new EnumMap<>(DecodeHintType.class);
        hintsMap.put(DecodeHintType.TRY_HARDER, Boolean.TRUE);
        hintsMap.put(DecodeHintType.POSSIBLE_FORMATS, EnumSet.allOf(BarcodeFormat.class));

        URL url = new URL(urlHash);
        BufferedImage image = ImageIO.read(url);

        LuminanceSource luminanceSource = new BufferedImageLuminanceSource(image);
        BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(luminanceSource));
        MultiFormatReader tmpBarcodeReader = new MultiFormatReader();

        Result tmpResult;
        String tmpFinalResult;
        tmpResult = tmpBarcodeReader.decode(binaryBitmap, hintsMap);

        tmpFinalResult = String.valueOf(tmpResult.getText());
        String[] hashData = tmpFinalResult
                .replace("{", "")
                .replace("}", "")
                .split(",");

        Map<String, String> returnHash = new LinkedHashMap<>();
        for (String data : hashData) {
            String[] temp = data.replaceAll("\"", "").split(":");
            returnHash.put(temp[0].strip(), temp[1].strip());
        }
        return returnHash;
    }

    public void clickOnHeaderToOpenHiddenTab(WebDriver driver) {
        Map<String, Object> args = new HashMap<>();
        args.put("x", 140);
        args.put("y", 90);
        if (driver instanceof AndroidDriver) {
            AndroidDriver da = (AndroidDriver) driver;
            da.executeScript("mobile: doubleClickGesture", args);
            da.executeScript("mobile: doubleClickGesture", args);
            da.executeScript("mobile: doubleClickGesture", args);
        } else {
            IOSDriver da = (IOSDriver) driver;
            da.executeScript("mobile: doubleClickGesture", args);
            da.executeScript("mobile: doubleClickGesture", args);
            da.executeScript("mobile: doubleClickGesture", args);
        }
    }


    public Map<String, ArrayList<String>> chitChat(ArrayList<String> message, boolean formPatient) {
        if (formPatient)
            patientChat = new ArrayList<>(message);
        else
            clinicianChat = new ArrayList<>(message);
        return chat;
    }

    public Map<String, ArrayList<String>> addMoreChat(String message, boolean formPatient) {
        if (formPatient)
            patientChat.add(message);
        else
            clinicianChat.add(message);
        return chat;
    }

    public ArrayList<String> getChat(boolean formPatient) {
        if (formPatient)
            return chat.get("Patient");
        else
            return chat.get("Clinician");
    }

    public String getMessageText(boolean formPatient) {
        String message;
        if (formPatient) {
            message = faker.gameOfThrones().character() + "-" + LocalDateTime.now();
            patientChat.add(message);
            chat.put("Patient", patientChat);
        } else {
            message = faker.gameOfThrones().character() + "-" + LocalDateTime.now();
            clinicianChat.add(message);
            chat.put("Clinician", clinicianChat);
        }
        log.info("Text message [" + message + "]");
        return message;
    }

    public void resetChatData() {
        patientChat.clear();
        clinicianChat.clear();
    }

    public void setGlobalMNR(String mnr) {
        CustomUtils.mnr = mnr;
    }

    public Map<String, String> getInventoryDetails(String arg0) {
        Map<String, String> data = new LinkedHashMap<>();
        data.put("deviceName", arg0);
        data.put("assetId", String.valueOf(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)));
        data.put("serialNumber", String.valueOf(LocalDateTime.now().plusSeconds(5).toEpochSecond(ZoneOffset.UTC)));
        data.put("manufacturer", faker.gameOfThrones().character());
        data.put("lastServiceAt", String.valueOf(LocalDateTime.now().getDayOfMonth()));
        return data;
    }

    public void resetPatientData() {
        userInput.clear();
    }

    public String createZipOfReport() throws IOException {

        String inputFolder = System.getProperty("user.dir") + "/test-output/";
        String outputFile = System.getProperty("user.dir") + "/src/test/resources/data/TestReport.zip";

        FileOutputStream fos = new FileOutputStream(outputFile);
        ZipOutputStream zipOut = new ZipOutputStream(fos);
        File fileToZip = new File(inputFolder);
        FileUtils.zipFile(fileToZip, fileToZip.getName(), zipOut);
        zipOut.close();
        fos.close();

        return outputFile;
    }

    public void sendReportToSlack(String channelName, String message, String filePath, String token) {
        try {
            String uploadFileMethodURL = "https://slack.com/api/files.upload";
            HttpClient httpClient = HttpClientBuilder.create().disableContentCompression().build();
            HttpPost httpPost = new HttpPost(uploadFileMethodURL);
            MultipartEntityBuilder reqEntity = MultipartEntityBuilder.create();
            reqEntity.addBinaryBody("file", new File(filePath));
            reqEntity.addTextBody("channels", channelName);
            reqEntity.addTextBody("token", token);
            reqEntity.addTextBody("media", "file");
            reqEntity.addTextBody("initial_comment", message);
            httpPost.setEntity(reqEntity.build());
            HttpResponse execute = httpClient.execute(httpPost);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendReport() throws IOException {
        String channel = reportData.get("Channel");
        if (Objects.isNull(channel))
            return;
        if (channel.equalsIgnoreCase("off"))
            return;

        StringBuilder message = new StringBuilder();
        message = message.append("Automation execution report for clinic [").append(reportData.get("Clinic")).append("] ")
                .append("on environment [" + reportData.get("Environment") + "].")
                .append(System.lineSeparator())
                .append("Execution date time [" + LocalDateTime.now().toLocalDate() +
                        " " + LocalDateTime.now().toLocalTime().getHour() + ":"
                        + LocalDateTime.now().toLocalTime().getMinute() + "].");

        log.info("Create zip file of report.");
        String filePath = createZipOfReport();
        String token = reportData.get("Token");
        log.info("Sending the report.");
        sendReportToSlack(channel, String.valueOf(message), filePath, token);

        log.info("Delete compressed file.");
        FileUtils.forceDelete(new File(filePath));
    }

    public static Map<String, String> reportData = new LinkedHashMap<>();

    private static String lastAcknowledgeNote;

    public void setLastAcknowledgeNote(String title) {
        lastAcknowledgeNote=title;
    }
    public String getLastAcknowledgeNote(){
        return lastAcknowledgeNote;
    }
}
