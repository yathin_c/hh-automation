package com.biofourmis.biovitals.utilities;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Map;

/**
 * This class is read only do not modify anything here
 */
@Log4j
@Component
public class RestUtils {

    public Response call(Map<String, String> inputMap) throws Throwable {
        String requestMethod = inputMap.get("reqMethod").toUpperCase();
        String[] supportedAPIMethod = {"POST", "PUT", "GET"};
        if (!Arrays.asList(supportedAPIMethod).contains(requestMethod)) {
            throw new Exception("Unsupported API Method");
        }

        Response response = null;
        RestAssured.baseURI = inputMap.get("url");
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", ContentType.JSON);

        if (inputMap.containsKey("authToken")) {
            request.header("Authorization", "Bearer " + inputMap.get("authToken"));
        }

        if (inputMap.containsKey("reqBody")) {
            request.body(inputMap.get("reqBody"));
        }

        switch (requestMethod) {
            case "GET": {
                response = request.get();
                break;
            }
            case "PUT": {
                response = request.put();
                break;
            }
            case "POST": {
                response = request.post();
                break;
            }
            case "DELETE": {
                response = request.delete();
                break;
            }
        }

        if (response != null) {
            log.info("----------------------------------------------- API CALL DETAILS -----------------------------------------------");
            log.info(requestMethod + " call to " + inputMap.get("url") + ", completed with status code : " + response.getStatusCode());
            log.info(inputMap.containsKey("reqBody") ? ("Request Body :" + inputMap.get("reqBody")) : "Request body is empty");
            log.info("Response : " + response.body().asString());
            log.info("----------------------------------------------------------------------------------------------------------------\n");
        }

        return response;
    }
}
