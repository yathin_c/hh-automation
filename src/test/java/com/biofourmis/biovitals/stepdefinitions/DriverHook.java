package com.biofourmis.biovitals.stepdefinitions;

import com.biofourmis.biovitals.configs.*;
import com.biofourmis.biovitals.constants.DeviceConstants;
import com.biofourmis.biovitals.drivers.AppiumDriverBuilder;
import com.biofourmis.biovitals.drivers.WebDriverBuilder;
import com.biofourmis.biovitals.exception.DeviceNotFoundException;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.spring.CucumberContextConfiguration;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.springframework.test.context.ContextConfiguration;

import java.io.IOException;

/**
 * This class is read only do not modify anything here
 */
@Log4j
@RequiredArgsConstructor
@CucumberContextConfiguration
@ContextConfiguration(classes = {AppConfig.class})
public class DriverHook {

    private final WebDriverBuilder webDriverBuilder;

    private final AppiumDriverBuilder appiumDriverBuilder;

    private final AppConfig appConfig;

    private final CustomConfig customConfig;

    private final WebDriverConfig webDriverConfig;

    private final DeviceConfig deviceConfig;

    private DeviceConstants deviceConstants;

    private WebDriver web;

    private Scenario scenario;

    private TestContextConfig testContextConfig;

    @Before
    public void setScenario(Scenario scenario) {
        customConfig.setScenario(scenario);
        this.scenario = customConfig.getScenario();
        this.testContextConfig = new TestContextConfig();
    }

    @Given("Launch the browser")
    public void launchTheBrowser() {
        this.web = webDriverBuilder.setupDriver(appConfig.getPlatformName());
        webDriverConfig.setDriver(this.web, "web");
        this.testContextConfig.setScenarioDriver(this.web);
        web.get(customConfig.getUrl());
    }

    @Given("Launch the {string} App")
    public void launchTheHFApp(String appName) throws IOException, DeviceNotFoundException {
        this.deviceConstants = deviceConfig.getFreeDevice(appName);
        this.web = appiumDriverBuilder.getAppiumDriver(this.deviceConstants, appName, true);
        this.testContextConfig.setScenarioDriver(this.web);
        webDriverConfig.setDriver(this.web, appName);
    }

    @Given("^Launch the \"([^\"]*)\" App with (reset|noReset)$")
    public void launchTheHFAppWithResetOption(String appName, String option) throws IOException, DeviceNotFoundException {
        this.deviceConstants = deviceConfig.getFreeDevice(appName);
        this.web = appiumDriverBuilder.getAppiumDriver(this.deviceConstants, appName, getResetOption(option));
        this.testContextConfig.setScenarioDriver(this.web);
        webDriverConfig.setDriver(this.web, appName);
    }

    private boolean getResetOption(String option) {
        return !option.equalsIgnoreCase("reset");
    }

    @After(order = 1)
    public void addScreenshotsToReport(Scenario scenario) {
        if (scenario.isFailed()) {
            log.info(scenario.getName() + " failed!! - Capturing Screenshots ");
            byte[] screenshot = null;

            for (WebDriver driver : this.testContextConfig.getScenarioDrivers()) {
                screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
                scenario.attach(screenshot, "image/png", scenario.getName());
            }
        }
    }

    @After(order = 0)
    public void killDriver() {
        try {
            for (WebDriver driver : this.testContextConfig.getScenarioDrivers()) {
                try {
                    if (driver != null)
                        driver.quit();
                } catch (Exception e) {
                }
            }
        } catch (Exception ec) {
        }

        if (this.deviceConstants != null) {
            deviceConfig.toggleDeviceStatus(this.deviceConstants);
            this.deviceConstants = null;
        }
    }

    @Then("^Close the (browser|app)$")
    public void closeTheBrowser(String appType) {
        if (web != null)
            web.quit();
        if (this.deviceConstants != null) {
            deviceConfig.toggleDeviceStatus(this.deviceConstants);
            this.deviceConstants = null;
        }
    }

}