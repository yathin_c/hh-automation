package com.biofourmis.biovitals.stepdefinitions.hcp;

import com.biofourmis.biovitals.constants.configuration.TestConfig;
import com.biofourmis.biovitals.pagefactory.hcp.PatientsPage;
import com.biofourmis.biovitals.utilities.CustomUtils;
import com.github.javafaker.Faker;
import io.cucumber.java.en.And;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

@Log4j
public class PatientsHook {

    @Autowired(required = false)
    @Qualifier(value = "HCP")
    private WebDriver clinician;

    @Autowired
    private TestConfig testConfig;
    @Autowired
    private Faker faker;

    private CustomUtils customUtils = new CustomUtils(null);
    private PatientsPage patientsPage;

    @And("^Search patient in HCP APP and open (Clinical notes|Handoff patient|View tasks|Remove from My patients)$")
    public void searchPatientInHCPAPPAndOpenClinicalNotes(String arg0) throws Exception {
        patientsPage = new PatientsPage(clinician);
        //CustomUtils.mnr="MR1637254860";
        log.info("Search patient with MNR number.");
        patientsPage.searchPatient(CustomUtils.mnr);
        log.info("Open menu option for searched patient.");
        patientsPage.openPatientMenu();
        log.info("Click on option ["+arg0+"].");
        patientsPage.clickOnMenu(arg0);
    }

}
