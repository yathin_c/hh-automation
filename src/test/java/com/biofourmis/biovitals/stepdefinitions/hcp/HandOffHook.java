package com.biofourmis.biovitals.stepdefinitions.hcp;

import com.biofourmis.biovitals.constants.configuration.TestConfig;
import com.biofourmis.biovitals.pagefactory.hcp.HandOffPage;
import com.biofourmis.biovitals.pagefactory.hcp.PatientsPage;
import com.biofourmis.biovitals.utilities.CustomUtils;
import com.github.javafaker.Faker;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.it.Ma;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.testng.Assert;

import java.util.LinkedHashMap;
import java.util.Map;

@Log4j
public class HandOffHook {

    @Autowired(required = false)
    @Qualifier(value = "HCP")
    private WebDriver clinician;

    @Autowired
    private TestConfig testConfig;
    @Autowired
    private Faker faker;

    private CustomUtils customUtils = new CustomUtils(null);
    private HandOffPage handOffPage;
    private Map<String, String> details = new LinkedHashMap<>();

    @Then("Verify add Hand Off with {string} Illness severity in HCP app")
    public void verifyAddHandOffWithIllnessSeverityInHCPApp(String arg0) throws Exception {
        handOffPage = new HandOffPage(clinician);

        log.info("Set hand Off details.");
        setHandOffDetails(arg0,true);

        log.info("Wait for page load.");
        handOffPage.waitForhandOffPageLoad();

        Assert.assertFalse(handOffPage.ishandOffBtnEnable(),
                "Hand Off button should be disable before entering fields.");

        log.info("Select Severity : "+arg0);
        handOffPage.selectSeverity(arg0);
        Assert.assertFalse(handOffPage.ishandOffBtnEnable(),
                "Hand Off button should be disable before entering fields.");

        log.info("Enter summary as : "+details.get("Summary"));
        handOffPage.enterSummary(details.get("Summary"));
        Assert.assertFalse(handOffPage.ishandOffBtnEnable(),
                "Hand Off button should be disable before entering fields.");

        log.info("Enter Situational option as : " +details.get("Situational"));
        handOffPage.selectSituationalOptions(details.get("Situational"), true);
        Assert.assertFalse(handOffPage.ishandOffBtnEnable(),
                "Hand Off button should be disable before entering fields.");

        log.info("Enter Synthesis value as : "+ details.get("Synthesis"));
        handOffPage.enetrSynthesis(details.get("Synthesis"));
        Assert.assertTrue(handOffPage.ishandOffBtnEnable(),
                "Hand Off button should be enable after entering fields.");

        handOffPage.clickOnHandOffbtn();

        Assert.assertTrue(verifyHandOffDetails(details),
                "Failed while verify hand off details.");
    }

    private void setHandOffDetails(String arg0, boolean isPredefineSituation) {
        details.put("Severity", arg0);
        details.put("Summary", faker.esports().game());
        details.put("Synthesis", faker.chuckNorris().fact());
        if (isPredefineSituation)
            details.put("Situational", "AOx3");
        else
            details.put("Situational", faker.esports().league());
    }

    @And("Verify edit Hand Off with {string} Illness severity in HCP app")
    public void verifyEditHandOffWithIllnessSeverityInHCPApp(String arg0) throws Exception {
        Assert.assertFalse(handOffPage.ishandOffBtnEnable(),
                "Hand Off button should be disable before entering fields.");
        if (arg0.equalsIgnoreCase("NoChange")) {
            log.info("Select No Change option.");
            handOffPage.selectNoChanges();
            log.info("Click on HandOff button.");
            handOffPage.clickOnHandOffbtn();
        } else {
            setHandOffDetails(arg0,false);
            log.info("Select Severity : "+arg0);
            handOffPage.selectSeverity(arg0);
            log.info("Enter summary as : "+details.get("Summary"));
            handOffPage.enterSummary(details.get("Summary"));
            log.info("Enter Situational option as : " +details.get("Situational"));
            handOffPage.selectSituationalOptions(details.get("Situational"), false);
            log.info("Enter Synthesis value as : "+ details.get("Synthesis"));
            handOffPage.enetrSynthesis(details.get("Synthesis"));
        }

        Assert.assertTrue(verifyHandOffDetails(details),
                "Failed while verify hand off details.");
    }

    private boolean verifyHandOffDetails(Map<String, String> details) {
        //TODO Add Verification methods of added HandOff
        return true;
    }

}
