package com.biofourmis.biovitals.stepdefinitions.hcp;

import com.biofourmis.biovitals.constants.configuration.TestConfig;
import com.biofourmis.biovitals.pagefactory.hcp.ClinicalNotesPage;
import com.biofourmis.biovitals.utilities.CustomUtils;
import com.github.javafaker.Faker;
import io.cucumber.java.en.Then;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.testng.Assert;

import java.util.Map;

@Log4j
public class ClinicalNotesHook {

    @Autowired(required = false)
    @Qualifier(value = "HCP")
    private WebDriver clinician;

    @Autowired
    private TestConfig testConfig;
    @Autowired
    private Faker faker;

    private CustomUtils customUtils = new CustomUtils(null);
    private ClinicalNotesPage clinicalNotesPage;

    @Then("^Verify (added|updated) note displayed in HCP App$")
    public void verifyAddedNoteDisplayedInHCPApp(String arg0) throws Exception {
        log.info("Get Default Clinical Notes.");
        Map<String, String> note = customUtils.getDefaultClinicalNotesDetails();
        log.info(note);
        clinicalNotesPage = new ClinicalNotesPage(clinician);

        String expected = note.get("Title");
        Assert.assertTrue(clinicalNotesPage.verifyNoteTitleDisplayed(expected),
                "Title [" + expected + "] not found");
        expected = note.get("Description");
        Assert.assertTrue(clinicalNotesPage.verifyNoteDescriptionDisplayed(expected),
                "Desc [" + expected + "] not found");
    }

    @Then("Verify deleted note not displayed in HCP App")
    public void verifyDeletedNoteNotDisplayedInHCPApp() {
        log.info("Get Default Clinical Notes.");
        Map<String, String> note = customUtils.getDefaultClinicalNotesDetails();
        log.info(note);
        String expected = note.get("Title");
        Assert.assertTrue(clinicalNotesPage.verifyNoteDeleted(expected),
                "Task [" + expected + "] should not be displayed after delete.");
    }
}
