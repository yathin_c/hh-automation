package com.biofourmis.biovitals.stepdefinitions.hcp;

import com.biofourmis.biovitals.constants.configuration.TestConfig;
import com.biofourmis.biovitals.pagefactory.hcp.TasksPage;
import com.biofourmis.biovitals.utilities.CustomUtils;
import com.github.javafaker.Faker;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.testng.Assert;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

@Log4j
public class TasksHook {

    @Autowired(required = false)
    @Qualifier(value = "HCP")
    private WebDriver clinician;

    @Autowired
    private TestConfig testConfig;
    @Autowired
    private Faker faker;

    private Map<String, String> details = new LinkedHashMap<>();
    private TasksPage taskPage;
    private String openTaskValue;
    private String patientMrn;

    @And("Search patient name under tasks")
    public void searchPatientNameUnderTasks() throws Exception {
        taskPage = new TasksPage(clinician);
        if (CustomUtils.mnr != null)
            patientMrn = CustomUtils.mnr;
        else
            patientMrn = testConfig.getPrimaryPatientMRN();

        log.info("CLick on Tasks tab.");
        //Instead of this navigate to task by search patient and click on View task.
        taskPage.clickOnTasks();
        log.info("Scroll till patient given MRN.");
        taskPage.scrollTillPatient(patientMrn);

        log.info("Read open task value.");
        openTaskValue = taskPage.getOpenTasksValue(patientMrn);
        log.info("Click on Add new task icon.");
        taskPage.clickOnAddNewTaskIcon(patientMrn);
    }

    @And("^Verify create new task in HCP app with (Required|All) fields$")
    public void verifyCreateNewTaskInHCPAppWithRequiredFields(String arg0) throws Exception {
        boolean flag = arg0.equals("All");
        log.info("Verify create task button should be disable when all fields are empty.");
        Assert.assertFalse(taskPage.verifyCreateButtonEnable(),
                "Create button should be disable before entering required field details.");

        log.info("Get values to create a task.");
        details = getTaskValue(true);

        log.info("Add task details.");
        taskPage.addNewTaskDetails(details, flag);

        log.info("Add task description.");
        taskPage.updateTaskDescription(details.get("Description"));

        if (flag) {
            log.info("Add due date.");
            details = taskPage.addDueDateTime(details);
            log.info("Add assignee.");
            taskPage.addAssignee(details.get("Assignee"));
        }

        log.info("Verify Created button should be enable after entering required fields.");
        Assert.assertTrue(taskPage.verifyCreateButtonEnable(),
                "Create button should be enable after entering required field details.");

        log.info("Click on create button.");
        taskPage.clickOnCreate();

        log.info("Get updated open task value.");
        String newOpenTaskValue = taskPage.getOpenTasksValue(patientMrn);

        log.info("Verifying old Open task value with latest Open task value.");
        Assert.assertFalse(openTaskValue.equalsIgnoreCase(newOpenTaskValue),
                "Open task count not updated after adding new task. Value before adding [" + openTaskValue + "] after adding [" + newOpenTaskValue + "].");

        log.info("Verify created task displayed under open tasks.");
        Assert.assertTrue(taskPage.verifyTaskAddedSuccessFully(patientMrn, details),
                "Added task with description [" + details.get("Description") + " not displayed.");

        if (flag) {
            log.info("Navigate to created task.");
            taskPage.viewTask(details.get("Description"));
            String expected = details.get("Assignee").strip(), actual = taskPage.verifyUpdatedAssignee().strip();
            Assert.assertTrue(expected.equalsIgnoreCase(actual),
                    "Expected assignee is [" + expected + "] and actual assignee is [" + actual + "].");
            expected = details.get("Due");
            actual = taskPage.verifyUpdatedDueDate();

            Assert.assertTrue(expected.equalsIgnoreCase(actual),
                    "Expected Due date is [" + expected + "] and actual Due date is [" + actual + "].");
        }
    }

    @Then("Verify comment on created task in HCP app")
    public void verifyCommentOnCreatedTaskInHCPApp() throws Exception {
        log.info("Navigate to created task.");
        taskPage.viewTask(details.get("Description"));

        String comment = faker.aquaTeenHungerForce().character() + LocalDateTime.now().toString();
        log.info("Add comment in task. [" + comment + "].");
        taskPage.addComment(comment);

        log.info("Scroll till comment");
        taskPage.scrollComments();
        log.info("Verify commented value should match.");
        Assert.assertTrue(taskPage.verifyCommentAddSuccessfully(comment),
                "Comment [" + comment + "] not displayed.");
    }

    @And("^Verify edit a task in HCP app with (All|Required) fields$")
    public void verifyEditATaskInHCPAppWithRequiredFields(String arg0) throws Exception {
        log.info("Click on Edit task.");
        taskPage.clickOnEditTask();
        boolean flag = arg0.equals("All");
        String desc = faker.aquaTeenHungerForce().character() + LocalDateTime.now().toString();
        details.replace("Description", desc);
        taskPage.updateTaskDescription(desc);
        if (flag) {
            log.info("Update due date.");
            details = taskPage.addDueDateTime(details);
            log.info("Update assignee name.");
            taskPage.addAssignee(details.get("Assignee"));
        }
        log.info("Click on update task.");
        taskPage.updateTask();

        desc = details.get("Description");
        Assert.assertTrue(taskPage.verifyUpdatedTaskDescription(desc),
                "Expected updated task description is [" + desc + "].");
        if (flag) {
            String expected = details.get("Assignee").strip(),
                    actual = taskPage.verifyUpdatedAssignee().strip();
            Assert.assertTrue(expected.equalsIgnoreCase(actual),
                    "Expected assignee is [" + expected + "] and actual assignee is [" + actual + "].");
            expected = details.get("Due");
            actual = taskPage.verifyUpdatedDueDate();

            Assert.assertTrue(expected.equalsIgnoreCase(actual),
                    "Expected Due date is [" + expected + "] and actual Due date is [" + actual + "].");
        }
    }

    @Then("Verify complete a task in HCP app")
    public void verifyCompleteATaskInHCPApp() throws Exception {
        log.info("Click on Complete task.");
        taskPage.completeATask();
        log.info("Click on Completed task section.");
        taskPage.clickOnCompletedTask(patientMrn);

        log.info("Verify task displayed under complete task section.");
        String desc = details.get("Description");
        Assert.assertTrue(taskPage.verifyTaskDisplayedUnderCompleted(desc),
                "Task with desc [" + desc + "] should be displayed.");
    }

    @And("Verify comment on completed task in HCP app")
    public void verifyCommentOnCompletedTaskInHCPApp() throws Exception {
        verifyCommentOnCreatedTaskInHCPApp();
    }

    @Then("Verify re-open completed task in HCP app")
    public void verifyReOpenCompletedTaskInHCPApp() throws Exception {
        log.info("Click on complete a task.");
        taskPage.completeATask();
        log.info("Click on Open task section.");
        taskPage.clickOnOpen(patientMrn);
        log.info("Verify re-open task should be displayed under Open task section.");
        String desc = details.get("Description");
        Assert.assertTrue(taskPage.verifyReOpenTask(desc),
                "Re-opened task should be displayed under Open task section with description [" + desc + "].");
    }

    @And("Verify comment on re-opened task in HCP app")
    public void verifyCommentOnReOpenedTaskInHCPApp() throws Exception {
        log.info("Navigate to task.");
        taskPage.viewTask(details.get("Description"));
        verifyCommentOnCreatedTaskInHCPApp();
    }

    @Then("^Verify edit re-open task in HCP app with (All|Required) fields$")
    public void verifyEditReOpenTaskInHCPAppWithAllFields(String arg0) throws Exception {
        log.info("Click on Edit task.");
        taskPage.clickOnEditTask();
        boolean flag = arg0.equals("All");
        String desc = faker.aquaTeenHungerForce().character() + LocalDateTime.now().toString();
        details.replace("Description", desc);

        log.info("Update task description.");
        taskPage.updateTaskDescription(desc);
        if (flag) {
            log.info("Update task due date.");
            details = taskPage.addDueDateTime(details);
            log.info("Edit assignee.");
            taskPage.addAssignee(details.get("Assignee"));
        }
        log.info("Click on update task.");
        taskPage.updateTask();

        log.info("Verify task description.");
        Assert.assertTrue(taskPage.verifyUpdatedTaskDescription(desc),
                "Task description ["+desc+"] should be displayed after updating.");
        if (flag) {
            String expected = details.get("Assignee").strip(),
                    actual = taskPage.verifyUpdatedAssignee().strip();
            Assert.assertTrue(expected.equalsIgnoreCase(actual),
                    "Expected assignee is [" + expected + "] and actual assignee is [" + actual + "].");
            expected = details.get("Due");
            actual = taskPage.verifyUpdatedDueDate();

            Assert.assertTrue(expected.equalsIgnoreCase(actual),
                    "Expected Due date is [" + expected + "] and actual Due date is [" + actual + "].");
        }

    }

    @Then("Verify complete a re-open task in HCP app")
    public void verifyCompleteAReOpenTaskInHCPApp() throws Exception {
        verifyCompleteATaskInHCPApp();
    }

    private Map<String, String> getTaskValue(boolean isAll) {
        if (!details.isEmpty())
            return details;

        details.put("Description", faker.aviation().aircraft() + LocalDateTime.now());
        details.put("Assignee", testConfig.getClinicianName());
        if (isAll)
            details.put("Type", "all");
        else
            details.put("Type", "required");
        return details;
    }
}
