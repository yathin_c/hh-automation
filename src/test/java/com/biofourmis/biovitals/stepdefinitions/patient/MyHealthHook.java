package com.biofourmis.biovitals.stepdefinitions.patient;

import com.biofourmis.biovitals.configs.LocalisationConfig;
import com.biofourmis.biovitals.pagefactory.patient.MyHealthPage;
import com.biofourmis.biovitals.utilities.CustomUtils;
import io.cucumber.java.en.Then;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.testng.Assert;

@Log4j
public class MyHealthHook {

    @Autowired(required = false)
    @Qualifier(value = "HH")
    private WebDriver patient;

    @Autowired(required = false)
    @Qualifier(value = "web")
    private WebDriver web;

    @Autowired
    private LocalisationConfig localisation;
    private CustomUtils customUtils = new CustomUtils(null);
    private MyHealthPage myHealthPage;

    @Then("verify entering manual vitals from my health screen")
    public void verifyEnteringManualVitalsFromMyHealthScreen() throws Exception {
        myHealthPage = new MyHealthPage(patient);
        myHealthPage.navigateToMyHealthTab();

        myHealthPage.verifyEnterVitalButtonDisplayed();

        myHealthPage.clickOnEnterVitalsBtn();

        myHealthPage.isClosePopUpIconDisplayed();

        myHealthPage.isOptionDisplayedOnPopUp(localisation.getString("select.vital"));

        myHealthPage.isOptionDisplayedOnPopUp(localisation.getString("text.blood.pressure"));

        myHealthPage.isOptionDisplayedOnPopUp("SpO2");

        myHealthPage.isOptionDisplayedOnPopUp("Weight");

        myHealthPage.isOptionDisplayedOnPopUp("Temporal temperature");

        myHealthPage.isOptionDisplayedOnPopUp("Blood glucose");

        verifyBPManualInputScreen();

        myHealthPage.clickOnEnterVitalsBtn();

        myHealthPage.clickOnVital("Weight");

        verifyWeightManualInputScreen();

    }

    private void verifyBPManualInputScreen() throws Exception {
        //Check blood pressure
        //Please input your blood pressure in the box below
        //Systolic
        //Diastolic
        //mmHg
        //Cancel
        //Submit
        myHealthPage.clickOnVital(localisation.getString("text.blood.pressure"));

        myHealthPage.isClosePopUpIconDisplayed();

        myHealthPage.isOptionDisplayedOnPopUp("Check blood pressure");
        myHealthPage.isOptionDisplayedOnPopUp("Please input your blood pressure in the box below");
        myHealthPage.isOptionDisplayedOnPopUp("Systolic");
        myHealthPage.isOptionDisplayedOnPopUp("Diastolic");
        myHealthPage.isOptionDisplayedOnPopUp("mmHg");
        myHealthPage.isOptionDisplayedOnPopUp("Cancel");
        myHealthPage.isOptionDisplayedOnPopUp("Submit");

        myHealthPage.clickOnClosePopUp();

        myHealthPage.clickOnEnterVitalsBtn();
        myHealthPage.clickOnVital(localisation.getString("text.blood.pressure"));

        myHealthPage.clickOnSubmit();

        Assert.assertEquals(myHealthPage.verifyToastMessage().strip(), "Input value cannot be empty",
                "[Systolic value should be in the range of 21 to 250] message not displayed.");

        myHealthPage.sendValueIn("Systolic", "12");

        myHealthPage.clickOnSubmit();

        Assert.assertEquals(myHealthPage.verifyToastMessage().strip(), "Systolic value should be in the range of 21 to 250",
                "[Systolic value should be in the range of 21 to 250] message not displayed.");

        myHealthPage.sendValueIn("Systolic", "21");
        myHealthPage.sendValueIn("Diastolic", "12");
        myHealthPage.clickOnSubmit();

        Assert.assertEquals(myHealthPage.verifyToastMessage().strip(), "Diastolic value should be in the range of 21 to 200",
                "[Diastolic value should be in the range of 21 to 250] message not displayed.");

        System.out.println();
    }

    private void verifyWeightManualInputScreen() throws Exception {
        //Check weight
        //Please input your weight in the box below
        //lb
        myHealthPage.isClosePopUpIconDisplayed();
        myHealthPage.isOptionDisplayedOnPopUp("Check weight");
        myHealthPage.isOptionDisplayedOnPopUp("Please input your weight in the box below");
        myHealthPage.isOptionDisplayedOnPopUp("lb");
        myHealthPage.clickOnClosePopUp();

    }
}
