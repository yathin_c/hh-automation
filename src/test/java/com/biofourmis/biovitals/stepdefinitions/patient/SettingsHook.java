package com.biofourmis.biovitals.stepdefinitions.patient;

import com.biofourmis.biovitals.pagefactory.patient.SettingsPage;
import com.biofourmis.biovitals.utilities.CustomUtils;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.testng.Assert;

import java.util.Arrays;
import java.util.List;

@Log4j
public class SettingsHook {
    @Autowired(required = false)
    @Qualifier(value = "HH")
    private WebDriver patient;

    @Autowired(required = false)
    @Qualifier(value = "web")
    private WebDriver web;

    private CustomUtils customUtils = new CustomUtils(null);
    private SettingsPage settings;

    @Then("Navigate to Hidden settings screen")
    public void navigateToHiddenSettingsScreen() throws Exception {
        settings = new SettingsPage(patient);
        log.info("Navigate to Hidden Setting tab.");
        settings.navigateToHiddenSettings();
    }

    @And("Verify Hidden setting screen elements")
    public void verifyHiddenSettingScreenElements() throws Exception {
        settings = new SettingsPage(patient);

        log.info("Verify Hidden Setting screen menu.");
        List<String> menu = Arrays.asList("Settings", "DEVICES", "Assigned devices",
                "New devices", "VitalPatch® calibration", "Language", "Close", "Log out");
        for (String value : menu) {
            log.info("Verify [" + value + "] should be displayed.");
            Assert.assertTrue(settings.verifySettingsOptions(value),
                    " [" + value + "] should be displayed on setting screen.");
        }

        String mrn = customUtils.getPatientDetails().get("MRN");
        log.info("Verify MRN number of patient [" + mrn + "] should be displayed");
        Assert.assertTrue(settings.verifySettingsOptions(mrn),
                "MRN number of patient [" + mrn + "] should be displayed.");

        log.info("Verify assigned devices screen.");
        settings.verifyAssignedDevice();

        log.info("Verify New devices screen.");
        settings.verifyNewDevices();

        log.info("Verify Vital patch calibration screen.");
        settings.verifyVitalPatchCalibration();

        log.info("Verify language screen,");
        settings.verifyLanguage();
    }
}
