package com.biofourmis.biovitals.stepdefinitions.patient;

import com.biofourmis.biovitals.configs.CustomConfig;
import com.biofourmis.biovitals.constants.configuration.TestConfig;
import com.biofourmis.biovitals.pagefactory.patient.ContactPage;
import com.biofourmis.biovitals.pagefactory.patient.LoginPage;
import com.biofourmis.biovitals.utilities.CustomUtils;
import com.github.javafaker.Faker;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.testng.Assert;

import java.time.LocalDateTime;
import java.util.*;

@Log4j
public class ContactHook {
    @Autowired(required = false)
    @Qualifier(value = "HH")
    private WebDriver patient;

    @Autowired(required = false)
    @Qualifier(value = "web")
    private WebDriver web;

    @Autowired
    private Faker faker;
    @Autowired
    private TestConfig config;

    private CustomUtils customUtils = new CustomUtils(null);
    private ContactPage contactPage;
    private LoginPage loginPage;

    @Then("Verify Contact option visible on all screens")
    public void verifyContactOptionVisibleOnAllScreens() throws Exception {
        contactPage = new ContactPage(patient);
        log.info("Click on Today's Plan.");
        contactPage.clickOnTodaysPlan();
        log.info("Verify Contact list displayed on Today's Plan.");
        Assert.assertTrue(contactPage.isContactHeaderVisible(), "Contact list not displayed on Today's Plan tab.");
        log.info("Click on My Health.");
        contactPage.clickOnMyHealth();
        log.info("Verify Contact list displayed on Today's Plan.");
        Assert.assertTrue(contactPage.isContactHeaderVisible(), "Contact list not displayed on My Health tab.");
        log.info("Click on Diary.");
        contactPage.clickOnDiary();
        Assert.assertTrue(contactPage.isContactHeaderVisible(), "Contact list not displayed on Diary tab.");
    }

    @And("Navigate to clinician contact")
    public void navigateToClinicianContact() throws Exception {
        contactPage = new ContactPage(patient);
        //TODO - Only First name visible on patient app. Fix it if causing issue.
        String arg0 = config.getClinicianName().split(" ")[0].strip();
        log.info("Click on Clinician name " + arg0 + " under contact list.");
        contactPage.clickOnClinicianName(arg0);

        List<String> elements = Arrays.asList("Contact " + arg0, "How do you want to connect with " + arg0 + "?",
                "Audio call", "Video call", "Text message");
        for (String element : elements) {
            log.info("Verify element " + element + " displayed on contact info tab.");
            Assert.assertTrue(contactPage.verifyContactPopUp(element), element + " not displayed.");
        }
    }

    @And("Navigate to clinician contact and click on Text message")
    public void navigateToClinicianContactText() throws Exception {
        contactPage = new ContactPage(patient);
        //TODO - Only First name visible on patient app. Fix it if causing issue.
        String arg0 = config.getClinicianName().split(" ")[0].strip();
        log.info("Click on Clinician name " + arg0 + " under contact list.");
        contactPage.clickOnClinicianName(arg0);
        log.info("Click on Text message option.");
        contactPage.clickOn("Text message");
    }

    @Then("Verify sending text message from patient app")
    public void verifySendingTextMessageFromPatientApp() throws Exception {
        String message = faker.chuckNorris().fact() + LocalDateTime.now();
        log.info("Send text message [" + message + "].");
        contactPage.sendText(message);
    }

    @Then("^Verify start (Audio|Video) call from patient app$")
    public void verifyStartAVoiceCallFromPatientApp(String arg0) throws Exception {
        loginPage = new LoginPage(patient);
        log.info("Click on " + arg0 + " option.");
        contactPage.startCall(arg0);
        log.info("Accept camera and audio permissions.");
        loginPage.acceptPermissions();
    }

    @And("^Start (Audio|Video) call from patient app$")
    public void startAudioCallFromPatientApp(String arg0) throws Exception {
        loginPage = new LoginPage(patient);
        log.info("Click on " + arg0 + " option.");
        contactPage.startCall(arg0);
        log.info("Accept camera and audio permissions.");
        loginPage.acceptCallPermissions();
    }

    @Then("^Verify call (Rejected|Accepted) successfully in patient app$")
    public void verifyCallRejectedSuccessfully(String arg0) {
        contactPage = new ContactPage(patient);
        boolean flag = arg0.equals("Accepted");

        log.info("Verify [" + arg0 + "] call.");
        if (flag)
            Assert.assertTrue(contactPage.verifyCallAccepted(), "Call not accepted.");
        else
            Assert.assertFalse(contactPage.verifyCallRejected(), "Call not rejected.");
    }

    @And("Verify Call ended successfully in patient app")
    public void verifyCallEndedSuccessfullyInPatientApp() {
        contactPage = new ContactPage(patient);
        log.info("Verify end a call.");
        Assert.assertFalse(contactPage.verifyCallEnded(), "Call not ended.");
    }

    @Then("^Receive (Audio|Video) call on patient app$")
    public void receiveAudioCallOnPatientApp(String arg0) throws Exception {
        contactPage = new ContactPage(patient);
        log.info("Verify is call ringing.");
        Assert.assertTrue(contactPage.isCallRinging(), "Tap to accept option not visible to accept call.");

        boolean video = arg0.equals("Video");
        log.info("Verify " + arg0 + " call options.");
        if (video)
            Assert.assertTrue(contactPage.verifyIncomingCallText("Incoming video call"),
                    "Text [Incoming video call] not displayed.");
        else
            Assert.assertTrue(contactPage.verifyIncomingCallText("Incoming audio call"),
                    "Text [Incoming audio call] not displayed.");
    }

    @And("^Verify (Reject|Accept) call from patient app$")
    public void verifyRejectCallFromPatientApp(String arg0) throws Exception {
        contactPage = new ContactPage(patient);
        boolean reject = arg0.equals("Reject");

        log.info(arg0 + " the call.");
        if (reject)
            Assert.assertFalse(contactPage.rejectCall(), "Call not rejected.");
        else
            Assert.assertFalse(contactPage.acceptCall(), "Call not accepted.");
    }

    @Then("Verify End call from patient app")
    public void verifyEndCallFromPatientApp() throws Exception {
        contactPage = new ContactPage(patient);
        log.info("End the call");
        contactPage.endCall();
    }

    @And("^Verify text message pop-up displayed in patient and click (Later|Check now)$")
    public void verifyTextMessagePopUpDisplayed(String arg0) throws Exception {
        contactPage = new ContactPage(patient);
        log.info("Verify Unread Message pop-up.");
        Assert.assertTrue(contactPage.verifyUnreadMessagePopUp(),
                "Unread message pop-up not displayed.");

        int messageCount = customUtils.getChat(false).size();
        Assert.assertTrue(contactPage.verifyUnreadMessagePopUpText(messageCount),
                "Unread message pop-up text not matched.");
        Assert.assertTrue(contactPage.clickOnPopUp(arg0),
                arg0 + " option not displayed on unread message pop-up.");
    }

    @Then("Verify chat count badge in patient app")
    public void verifyChatCountBadgeInPatientApp() throws Exception {
        int messageCount = customUtils.getChat(false).size();
        log.info("Chat badge count should be " + messageCount);
        Assert.assertTrue(contactPage.verifyBadgeCount(messageCount),
                "Badge count not match. Expected count should be [" + messageCount + "].");
    }

    @Then("Verify received text message from HCP clinician")
    public void verifyReceivedTextMessageFromHCPClinician() throws Exception {
        contactPage = new ContactPage(patient);

        String arg0 = config.getClinicianName().split(" ")[0].strip();

        contactPage.clickOnClinicianName(arg0);

        int messageCount = customUtils.getChat(false).size();
        log.info("Verify badge count on contact pop-up.");
        Assert.assertTrue(contactPage.verifyBadgeCount(messageCount),
                "Unread message count not matched. Expected count is [" + messageCount + "].");

        log.info("Click on Text message.");
        contactPage.clickOn("Text message");

        log.info("Verify chat screen options.");
        Assert.assertTrue(contactPage.verifyAlertMessage("Please call, don’t text if this is an urgent problem"),
                "[Please call, don’t text if this is an urgent problem] info not displayed in chat header.");
        Assert.assertTrue(contactPage.verifyInputMessage(),
                "Message input message box not visible.");
        Assert.assertTrue(contactPage.verifyChatSendBtn(),
                "Send message button not displayed.");
        Assert.assertTrue(contactPage.verifyClosePopUp(),
                "Close chat container [X] icon not displayed.");

        ArrayList<String> messages = customUtils.getChat(false);
        Assert.assertEquals(contactPage.verifyChatMessage(messages), messages,
                "Chat messages not matched. Expected message(s) " + messages);

        Assert.assertTrue(contactPage.closeChatPopUp(),
                "Not able to close chat pop-up.");
    }

    @Then("Verify sending {int} text message from Patient app to clinician")
    public void verifySendingTextMessageFromPatientApp(int arg0) throws Exception {
        contactPage = new ContactPage(patient);

        String arg1 = config.getClinicianName().split(" ")[0].strip();

        log.info("Click on Clinician name " + arg0 + " under contact list.");
        contactPage.clickOnClinicianName(arg1);
        log.info("Click on Text message.");
        contactPage.clickOn("Text message");

        log.info("Reset All Chat messages.");
        customUtils.resetChatData();
        for (int i = 0; i < arg0; i++) {
            String textMessage = customUtils.getMessageText(true);
            log.info("Sending... message [" + textMessage + "].");
            contactPage.sendTextMessage(textMessage);
            log.info("Verify sent text message" + textMessage + "].");
            Assert.assertTrue(contactPage.verifySentTextMessage(textMessage),
                    "Message [" + textMessage + "] not sent.");
        }
    }

    @Then("Again send {int} text message from Patient app to clinician")
    public void verifySendingTextMessagePatientApp(int arg0) throws Exception {
        contactPage = new ContactPage(patient);

        log.info("Reset All Chat messages.");
        customUtils.resetChatData();
        for (int i = 0; i < arg0; i++) {
            String textMessage = customUtils.getMessageText(true);
            log.info("Sending... message [" + textMessage + "].");
            contactPage.sendTextMessage(textMessage);
            log.info("Verify sent text message" + textMessage + "].");
            Assert.assertTrue(contactPage.verifySentTextMessage(textMessage),
                    "Message [" + textMessage + "] not sent.");
        }
    }

}
