package com.biofourmis.biovitals.stepdefinitions.web;

import com.biofourmis.biovitals.constants.configuration.TestConfig;
import com.biofourmis.biovitals.pagefactory.web.HomePage;
import com.biofourmis.biovitals.utilities.CustomUtils;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.testng.Assert;

import java.util.List;
import java.util.Objects;

@Log4j
public class HomeHook {

    @Autowired
    @Qualifier(value = "web")
    private WebDriver driver;
    @Autowired
    private TestConfig testConfig;

    private HomePage homePage = null;
    private List<String> lhnMenu;
    private CustomUtils customUtils = new CustomUtils(null);

    @Then("Verify LHN menu options displayed")
    public void verifyLHNMenuOptionsDisplayed(DataTable lhnMenu) throws Exception {
        this.lhnMenu = lhnMenu.asList();
        homePage = new HomePage(driver);

        log.info("Click on Hamburger.");
        homePage.clickOnHamburger();

        log.info("Verify LHN menu.[" + this.lhnMenu + "]");
        homePage.verifyLhnMenu(this.lhnMenu);
    }

    @Then("^Navigate to (Tasks|Inventory|Discharged|My patients|All patients) tab on HCP web dashboard$")
    public void navigateToTasksTabOnHCPWebDashboard(String arg0) throws Exception {
        homePage = new HomePage(driver);
        log.info("Click on Hamburger.");
        homePage.clickOnHamburger();

        log.info("Navigate to " + arg0);
        homePage.navigateTo(arg0);
    }

    @And("Verify each menu is clickable and should navigate to respected page")
    public void verifyEachMenuIsClickableAndShouldNavigateToRespectedPage() throws Exception {
        for (String value : this.lhnMenu) {
            log.info("Verify " + value + " menu page.");
            Assert.assertTrue(homePage.verifyRespectivePage(value),
                    value + " page title not matched.");

            //TODO ADD URL VALIDATION HERE
        }
    }

    @Given("Go to home page")
    public void goToHomePage() throws Exception {
        homePage = new HomePage(driver);

        log.info("Navigate to home page.");
        homePage.goToHomePage();
    }

    @Then("Open QR of Patient {string}")
    public void openQROfPatient(String arg0) throws Exception {
        customUtils = new CustomUtils(driver);
        customUtils.setPatientDetails(arg0, "");
        log.info("Navigate to patient with MRN number [" + arg0 + "].");
        navigateToPatientUnderMyPatients(arg0);

        log.info("Set QR hash.");
        customUtils.setQrHashUrl(homePage.getQRHash());
        homePage._refreshPage();
    }

    @Then("Open QR for Patient")
    public void openQRforPatient() throws Exception {
        openQROfPatient(CustomUtils.mnr);
    }

    @Then("Search patient {string} under my patients")
    public void searchPatientUnderMyPatients(String arg0) throws Exception {
        homePage = new HomePage(driver);

        log.info("Navigate to home.");
        homePage.goToHomePage();

        log.info("Search for patient.[" + arg0 + "].");
        homePage.searchPatient(arg0);

        Assert.assertFalse(homePage.isPatientPresent(arg0),
                "Patient not found, MRN [" + arg0 + "].");
    }

    @Then("Search patient under my patients")
    public void searchDefaultPatientUnderMyPatients() throws Exception {
        if (Objects.isNull(CustomUtils.mnr)) {
            CustomUtils.mnr = testConfig.getPrimaryPatientMRN();
            customUtils.setPatientDetails(testConfig.getPrimaryPatientMRN(), testConfig.getPrimaryPatientName());
        }
        searchPatientUnderMyPatients(CustomUtils.mnr);
    }

    @Then("Search patient {string} without refresh")
    public void searchDefaultPatientMyPatients(String arg0) throws Exception {
        homePage = new HomePage(driver);

        log.info("Search for patient.[" + arg0 + "].");
        homePage.searchPatientWithoutWait(arg0);

        Assert.assertFalse(homePage.isPatientPresent(arg0),
                "Patient not found, MRN [" + arg0 + "].");
    }

    @Then("^Search patient under (all|my) patients without refresh$")
    public void searchPatients(String arg0) throws Exception {
        homePage = new HomePage(driver);
        boolean flag = arg0.equals("all");

        String mrn;

        if (flag)
            mrn = testConfig.getUnassignedPatientMRN();
        else {
            if (Objects.isNull(CustomUtils.mnr)) {
                CustomUtils.mnr = testConfig.getPrimaryPatientMRN();
                customUtils.setPatientDetails(testConfig.getPrimaryPatientMRN(), testConfig.getPrimaryPatientName());
            }
            mrn = CustomUtils.mnr;
        }

        log.info("Search for patient.[" + mrn + "].");
        if (flag) {
            navigateToTasksTabOnHCPWebDashboard("All patients");
            homePage.searchPatient(mrn);
        } else
            homePage.searchPatientWithoutWait(mrn);

        Assert.assertFalse(homePage.isPatientPresent(mrn),
                "Patient not found, MRN [" + mrn + "].");
    }

    @And("Navigate to patient {string} under my patients")
    public void navigateToPatientUnderMyPatients(String arg0) throws Exception {
        homePage = new HomePage(driver);

        log.info("Navigate to home page.");
        homePage.goToHomePage();

        log.info("Search patient with MRN number [" + arg0 + "].");
        homePage.searchPatient(arg0);

        log.info("Verify patient with MRN [" + arg0 + "] should be displayed.");
        Assert.assertFalse(homePage.isPatientPresent(arg0), "Patient not found.");

        log.info("Click on patient.");
        homePage.clickOnPatient(arg0);
    }

    @And("Navigate to patient under my patients")
    public void navigateToDefaultPatientUnderMyPatients() throws Exception {
        if (Objects.isNull(CustomUtils.mnr)) {
            CustomUtils.mnr = testConfig.getPrimaryPatientMRN();
            customUtils.setPatientDetails(testConfig.getPrimaryPatientMRN(), testConfig.getPrimaryPatientName());
        }
        navigateToPatientUnderMyPatients(CustomUtils.mnr);
    }

    @And("Navigate to created patient under my patients")
    public void navigateToPatientUnderMyPatients() throws Exception {
        homePage = new HomePage(driver);

        log.info("Navigate to home page.");
        homePage.goToHomePage();

        String arg0 = customUtils.getDefaultPatientDetails().get("MRN");
        log.info("Search patient with MRN number [" + arg0 + "].");
        homePage.searchPatient(arg0);

        log.info("Verify patient with MRN [" + arg0 + "] should be displayed.");
        Assert.assertFalse(homePage.isPatientPresent(arg0), "Patient not found.");

        log.info("Click on patient.");
        homePage.clickOnPatient(arg0);
    }
}
