package com.biofourmis.biovitals.stepdefinitions.web;

import com.biofourmis.biovitals.configs.AppConfig;
import com.biofourmis.biovitals.configs.CustomConfig;
import com.biofourmis.biovitals.constants.configuration.TestConfig;
import com.biofourmis.biovitals.drivers.WebDriverBuilder;
import com.biofourmis.biovitals.pagefactory.web.LoginPage;
import com.biofourmis.biovitals.utilities.CustomUtils;
import com.github.javafaker.Faker;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.testng.Assert;

import java.util.Locale;
import java.util.Map;

@Log4j
public class LoginHook {

    @Autowired
    @Qualifier(value = "web")
    private WebDriver driver;

    @Autowired
    CustomConfig customConfig;
    @Autowired
    TestConfig testConfig;
    @Autowired(required=false)
    WebDriverBuilder webDriverBuilder;

    @Autowired(required=false)
    AppConfig appConfig;

    private LoginPage loginPage = null;
    private CustomUtils customUtils = new CustomUtils(null);

    @Then("Verify caregiver login successfully")
    public void verifyCaregiverLoginSuccessfully() throws Exception {
        loginPage = new LoginPage(driver);

        log.info("Enter username.");
        loginPage.enterMail(customConfig.getMail());

        log.info("Enter password.");
        loginPage.enterPass(customConfig.getPassword());

        log.info("Click on login button.");
        loginPage.clickOnLogin();

        log.info("Verify Home page displayed successfully.");
        Assert.assertTrue(loginPage.verifyHomePage(),
                "Home page not displayed.");
    }

    @And("Logout user")
    public void logout() throws Exception {
        log.info("Try to logout user.");
        Assert.assertTrue(loginPage.logOut(),
                "Not able to logout.");
    }

    @Then("Verify disabled login button")
    public void verifyDisabledLoginButton() throws Exception {
        loginPage = new LoginPage(driver);
        log.info("Verify disabled login button.");
        Assert.assertFalse(loginPage.isLoginButtonEnable(),
                "Login button is enable, It should be disable until both fields have valid values.");

        log.info("Enter username.");
        loginPage.enterUserName(customConfig.getUser());

        log.info("Verify Login button is disabled.");
        Assert.assertFalse(loginPage.isLoginButtonEnable(),
                "Login button is enable, It should be disable until both fields have valid values.");

        log.info("Clear user name.");
        loginPage.clearUserName();

        log.info("Verify Login button is disabled.");
        Assert.assertFalse(loginPage.isLoginButtonEnable(),
                "Login button is enable, It should be disable until both fields have valid values.");

        log.info("Enter password.");
        loginPage.enterPassword(customConfig.getPassword());

        log.info("Verify Login button is disabled.");
        Assert.assertFalse(loginPage.isLoginButtonEnable(),
                "Login button is enable, It should be disable until both fields have valid values.");

        log.info("Clear password.");
        loginPage.clearPassword();

        log.info("Verify Login button is disabled.");
        Assert.assertFalse(loginPage.isLoginButtonEnable(),
                "Login button is enable, It should be disable until both fields have valid values.");

    }

    @Then("Verify invalid username format")
    public void verifyInvalidUsername(DataTable dataTable) throws Exception {
        loginPage = new LoginPage(driver);
        for (String value : dataTable.asList()) {
            log.info("Verify invalid messages for email for value :" + value);
            Assert.assertTrue(loginPage.verifyUserNameFormat(value),
                    "Failed while verify invalid username format :" + value);
        }
    }

    @Then("Verify invalid password format")
    public void verifyInvalidPassword(DataTable dataTable) throws Exception {
        loginPage = new LoginPage(driver);
        loginPage.enterUserName("my.user@yopmail.com");
        for (String value : dataTable.asList()) {
            log.info("Verify invalid messages for password for value :" + value);
            Assert.assertTrue(loginPage.verifyPasswordFormat(value),
                    "Failed while verify invalid password format :" + value);
        }
    }

    public void verifyEmailAddressRequiredMessage(String message) throws Exception {
        loginPage = new LoginPage(driver);
        log.info("Verify email address required message.");

        log.info("Verify required messages.");
        Assert.assertTrue(loginPage.verifyUserNameRequired(customConfig.getMail(), message),
                "Failed while verify required message.");

        log.info("Enter password.");
        loginPage.enterPass(customConfig.getPassword());

        log.info("Verify email address required message.");
        Assert.assertTrue(loginPage.verifyUserNameRequired(customConfig.getMail(), message),
                "Failed while verify required message.");
    }

    public void verifyPasswordRequired(String message) throws Exception {
        loginPage = new LoginPage(driver);
        log.info("Verify password required message.");
        Assert.assertTrue(loginPage.verifyPasswordRequired(customConfig.getPassword(), message),
                "Failed while verify required message.");

        log.info("Enter email.");
        loginPage.enterMail(customConfig.getMail());

        log.info("Verify password required message.");
        Assert.assertTrue(loginPage.verifyPasswordRequired(customConfig.getPassword(), message),
                "Failed while verify required message.");
    }

    @Then("Verify Invalid email or password")
    public void verifyInvalidEmailOrPassword(Map<String, String> fields) throws Exception {
        loginPage = new LoginPage(driver);
        log.info("Enter username and password.");
        loginPage.login(fields.get("Username"), fields.get("Password"));
    }

    @Then("Verify login screen texts")
    public void verifyLoginScreen() {
        loginPage = new LoginPage(driver);
        for (String value : customUtils.verifyWebLoginText())
            Assert.assertTrue(loginPage.loginPageText(value),
                    "[" + value + "] not displayed on login screen.");
    }

    @And("Login into dashboard")
    public void loginIntoDashboard() throws Exception {
        loginPage = new LoginPage(driver);
        log.info("Enter username and password.");
        loginPage.login(customConfig.getMail().strip(),
                customConfig.getPassword().strip());
        log.info("User " + customConfig.getMail().strip() + " logged in successfully.");
    }

    @And("Login nurse into dashboard")
    public void loginNurseIntoDashboard() throws Exception {
        loginPage = new LoginPage(driver);
        log.info("Enter username and password.");
        loginPage.login(testConfig.getNurseMail().strip(),
                testConfig.getNursePass().strip());
        log.info("User " + testConfig.getNurseMail().strip() + " logged in successfully.");
    }

    @When("Enter the username and password")
    public void enterTheUsernameAndPassword() throws Exception {
        loginPage = new LoginPage(driver);
        log.info("Enter username.");
        loginPage.enterMail(customConfig.getMail());
        log.info("Enter password.");
        loginPage.enterPass(customConfig.getPassword());
    }

    @Then("^Verify the login button should be (enabled|disabled)$")
    public void verifyTheLoginButtonShouldBeEnabled(String arg0) throws Exception {
        log.info("Verify login button " + arg0);
        if (arg0.equalsIgnoreCase("enabled"))
            Assert.assertTrue(loginPage.isLoginButtonEnable(), "Login button should be enabled.");
        else
            Assert.assertFalse(loginPage.isLoginButtonEnable(), "Login button should be disabled.");
    }

    @And("Click on the login button")
    public void clickOnTheLoginButton() throws Exception {
        log.info("Click on Login button.");
        loginPage.clickOnLogin();
    }

    @And("Click on the eye button of password and verify the password")
    public void clickOnTheEyeButtonOfPasswordAndVerifyThePassword() throws Exception {
        loginPage = new LoginPage(driver);
        loginPage.clickPwdEye();
        Assert.assertEquals(loginPage.getPassword(), customConfig.getPassword(),
                "Password not matched something wrong");
    }

    @Then("Verify caregiver login successfully when the username in uppercase")
    public void verifyCaregiverLoginSuccessfullyWhenTheUsernameInUppercase() throws Exception {
        loginPage = new LoginPage(driver);
        log.info("Enter username.");
        loginPage.enterMail(customConfig.getMail().toUpperCase(Locale.ROOT));

        log.info("Enter password.");
        loginPage.enterPass(customConfig.getPassword());

        log.info("Click on login button.");
        loginPage.clickOnLogin();

        log.info("Verify Home page displayed successfully.");
        Assert.assertTrue(loginPage.verifyHomePage(),
                "Home page not displayed.");
    }


    @Then("Verify {string} required i.e. {string}")
    public void verifyRequiredIE(String arg0, String arg1) throws Exception {
        log.info("Verify " + arg0 + " required message.");
        if (arg0.equalsIgnoreCase("email"))
            verifyEmailAddressRequiredMessage(arg1);
        else
            verifyPasswordRequired(arg1);
    }

    @Then("Verify the password should be in masked form")
    public void verifyThePasswordShouldBeInMaskedForm() {
        loginPage = new LoginPage(driver);
        log.info("Verify password is masked.");
        Assert.assertEquals(loginPage.unMaskedPwd(), "password",
                "Password is not masked it is, " + loginPage.unMaskedPwd());
    }

    @When("Enter the username as {string} and password as {string}")
    public void enterTheUsernameAsAndPasswordAs(String arg0, String arg1) throws Exception {
        loginPage = new LoginPage(driver);
        log.info("Enter Username.");
        loginPage.enterMail(arg0);
        log.info("Enter Password.");
        loginPage.enterPass(arg1);
    }

    @And("After enter username and password click {int} time on the login button to see error message")
    public void afterEnterUsernameAndPasswordClickTimeOnTheLoginButtonToSeeErrorMessage(int arg0) throws Exception {
        loginPage = new LoginPage(driver);
        int count = 4;
        for (int i = 0; i < arg0; i++) {
            loginPage.clickOnLogin();
//            Assert.assertEquals(loginPage.loginErrMsg(), "Invalid email or password. Please try again.",
//                    "Error message not matched");
            Assert.assertTrue(loginPage.isLoginAttemptMessage(count), "Remaining Log-in attempt message not matched. [You have " + count + " more password attempts. Once the limit is reached, your account will be locked for 5 minutes]");
        }

        //You have 3 more password attempts. Once the limit is reached, your account will be locked for 5 minutes
    }

    @Then("Verify the error message after invalid email and password")
    public void verifyTheErrorMessageAfterInvalidEmailAndPassword() throws Exception {
        Assert.assertEquals(loginPage.loginErrMsg(), "Invalid email or password. Please try again.",
                "Error message not matched");
    }


    @And("Verify User is not logged out after clicking on Back button")
    public void verifyUserIsNotLoggedOutOnBackClick()throws Exception {
        loginPage._goBack();
        log.info("VerifyUserisnotloggedoutandHomepagedisplayedsuccessfully.");
        Assert.assertTrue(loginPage.verifyHomePage(),
                "Homepagenotdisplayed.");
    }

    @Then("Verify User is logged out from old window after logging into new Window")
    public void verifyUserIsLoggedOutAfterLoggingIntoNewWindow()throws Exception {
        WebDriver tempDriver = webDriverBuilder.setupDriver(appConfig.getPlatformName());
        tempDriver.get(customConfig.getUrl());
        loginPage = new LoginPage(tempDriver);
        log.info("Enterusernameandpassword.");
        loginPage.login(customConfig.getMail().strip(),
                customConfig.getPassword().strip());
        log.info("User" + customConfig.getMail().strip() + "loggedinsuccessfully.");
        tempDriver.close();

        //switchtoolddriverinstanceandcheckforloggedoutmessage.
        loginPage = new LoginPage(driver);
        log.info("VerifyUserisloggedoutfromoldbrowserinstance.");
        Assert.assertTrue(loginPage.isAutoLoggedOutMessage(),
                "Userisnotloggedout.");

    }
}