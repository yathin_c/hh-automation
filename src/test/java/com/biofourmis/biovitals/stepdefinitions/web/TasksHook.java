package com.biofourmis.biovitals.stepdefinitions.web;

import com.biofourmis.biovitals.configs.CustomConfig;
import com.biofourmis.biovitals.constants.configuration.TestConfig;
import com.biofourmis.biovitals.pagefactory.web.TasksPage;
import com.biofourmis.biovitals.utilities.CustomUtils;
import com.github.javafaker.Faker;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.testng.Assert;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

@Log4j
public class TasksHook {
    @Autowired
    @Qualifier(value = "web")
    private WebDriver driver;

    @Autowired
    private Faker faker;

    @Autowired
    private TestConfig testConfig;

    private TasksPage tasks;
    private Map<String, String> details = new LinkedHashMap<>();
    private CustomUtils customUtils = new CustomUtils(null);

    @Given("^Verify create new task with (Required|All) fields$")
    public void verify_create_new_task_with_fields(String string) throws Exception {
        tasks = new TasksPage(driver);
        log.info("Click on Task icon.");
        tasks.clickOnTaskIcon();
        boolean isAll = string.equals("All");
        log.info("Click on New Task.");
        tasks.clickOnNewTask();

        Map<String, String> taskValue = getTaskValue(isAll);
        log.info("Input task description.");
        tasks.inputTaskDescription(taskValue.get("Description"));

        if (isAll) {
            log.info("Input optional fields. [" + taskValue + "]");
            details = tasks.inputOptionalFields(taskValue);
        }
        log.info("Click on Create task.");
        tasks.createTask();

        log.info("Search and click on created task. [" + details.get("Description") + "]");
        tasks.viewTask(details.get("Description"));

        log.info("Verify task created successfully.");
        tasks.verifyCreatedTaskSuccessfully(taskValue.get("Description"));
        if (isAll) {
            log.info("Verify optional fields.");
            tasks.verifyOptionalFieldsValue(taskValue);
        }
    }

    private Map<String, String> getTaskValue(boolean isAll) {
        if (!details.isEmpty())
            return details;

        details.put("Description", faker.aviation().aircraft() + LocalDateTime.now());
        details.put("Assignee", testConfig.getClinicianName());
        if (isAll)
            details.put("type", "all");
        else
            details.put("type", "required");
        return details;
    }

    @Then("Verify comment on created task")
    public void verifyCommentOnCreatedTask() throws Exception {
        String desc = details.get("Description");
        log.info("View task [" + desc + "].");
        tasks.viewTask(desc);
        String comment = faker.aquaTeenHungerForce().character() + LocalDateTime.now().toString();

        log.info("Add comment [" + comment + "].");
        tasks.addComment(comment);

        Assert.assertTrue(tasks.verifyAddedComment(comment),
                "Comment not matched. Expected comment [" + comment + "].");

        Assert.assertTrue(tasks.verifyCloseIcon(),
                "Task not closed after clicking on close icon.");

        log.info("Click on task icon.");
        tasks.clickOnTaskIcon();
        log.info("View task [" + desc + "].");
        tasks.viewTask(desc);
        Assert.assertTrue(tasks.verifyAddedComment(comment),
                "Comment not matched. Expected comment [" + comment + "].");
    }

    @And("^Verify edit a task with (Required|All) fields$")
    public void verifyEditATaskWithFields(String arg0) throws Exception {
        boolean isAll = arg0.equals("All");
        log.info("Click on edit task.");
        tasks.clickOnEditTask();

        details.put("Description", faker.gameOfThrones().dragon() + LocalDateTime.now().toString());
        String desc = details.get("Description");
        log.info("Update task description as [" + desc + "].");
        tasks.updateDescription(desc);

        if (isAll) {
            log.info("Update all input fields as [" + details + "].");
            details = tasks.inputOptionalFields(details);
        }

        log.info("Click on update.");
        tasks.clickOnUpdate();

        Assert.assertTrue(tasks.verifyUpdatedTaskDescription(details.get("Description")),
                "Task with updates description npt found. Description [" + desc + "].");

        log.info("Click on Back option.");
        tasks.VerifyClickOnBack();

        log.info("View task [" + desc + "].");
        tasks.viewTask(details.get("Description"));

        if (isAll)
            Assert.assertTrue(tasks.verifyOptionalFieldsValue(details),
                    "Task optional fields not matched after updating. Expected [" + details + "].");
    }

    @Then("Verify complete a task")
    public void verifyCompleteATask() throws Exception {
        log.info("Complete a task.");
        tasks.completeTask();
        log.info("Open Completed task option.");
        tasks.clickOnCompletedTasks();

        log.info("verify task should be present in completed section.");
        tasks.viewTask(details.get("Description"));
    }

    @And("Verify comment on completed task")
    public void verifyCommentOnCompletedTask() throws Exception {
        String comment = faker.aquaTeenHungerForce().character() + LocalDateTime.now().toString();

        log.info("Add comment in task.");
        tasks.addComment(comment);
        Assert.assertTrue(tasks.verifyAddedComment(comment),
                "Verify comment added successfully. Expected [" + comment + "].");

        log.info("Click on close icon.");
        tasks.verifyCloseIcon();
        log.info("Click on task icon.");
        tasks.clickOnTaskIcon();
        log.info("Open Completed task option.");
        tasks.clickOnCompletedTasks();
        log.info("Search task. Description [" + details.get("Description") + "].");
        tasks.viewTask(details.get("Description"));
        Assert.assertTrue(tasks.verifyAddedComment(comment),
                "Comment not matched. Expected comment on task is [" + comment + "].");
    }

    @Then("Verify re-open completed task")
    public void verifyReOpenCompletedTask() throws Exception {
        String desc = details.get("Description");
        log.info("Search task. Description [" + desc + "].");
        tasks.viewTask(desc);
        log.info("Click on reopen task.");
        tasks.clickOnReOpen();
        Assert.assertTrue(tasks.verifyTaskReopened(),
                "Not able to reopen completed task.");
        log.info("Click on close icon.");
        tasks.verifyCloseIcon();
        log.info("Click on task icon.");
        tasks.clickOnTaskIcon();
        log.info("Search task. Description [" + desc + "].");
        tasks.viewTask(desc);
    }

    @And("Verify comment on re-opened task")
    public void verifyCommentOnReOpenedTask() throws Exception {
        String comment = faker.aquaTeenHungerForce().character() + LocalDateTime.now().toString();
        String desc = details.get("Description");
        log.info("Add comment in task. [" + comment + "].");
        tasks.addComment(comment);
        Assert.assertTrue(tasks.verifyAddedComment(comment),
                "Expected comment is [" + comment + "].");
        log.info("Click on close icon.");
        tasks.verifyCloseIcon();
        log.info("Click on task icon.");
        tasks.clickOnTaskIcon();
        log.info("Search task. Description [" + desc + "].");
        tasks.viewTask(desc);
        Assert.assertTrue(tasks.verifyAddedComment(comment),
                "Expected comment is [" + comment + "].");
    }

    @Then("^Verify edit re-open task with (Required|All) fields$")
    public void verifyEditReOpenTaskWithFields(String arg0) throws Exception {
        boolean isAll = arg0.equals("All");
        log.info("Click on reopen task.");
        tasks.clickOnEditTask();
        details.put("Description", faker.gameOfThrones().dragon() + LocalDateTime.now().toString());
        String desc = details.get("Description");
        log.info("Update task description as [" + desc + "].");
        tasks.updateDescription(desc);

        if (isAll) {
            log.info("Update optional fields.");
            details = tasks.inputOptionalFields(details);
        }

        log.info("Click on info.");
        tasks.clickOnUpdate();
        Assert.assertTrue(tasks.verifyUpdatedTaskDescription(details.get("Description")),
                "Task with description [" + desc + "] not found.");
        log.info("Click on back.");
        tasks.VerifyClickOnBack();
        log.info("Search task. Description [" + desc + "].");
        tasks.viewTask(desc);
        if (isAll)
            Assert.assertTrue(tasks.verifyOptionalFieldsValue(details),
                    "Option fields updated value not matched. [" + details + "].");
    }

    @Then("Verify complete a re-open task")
    public void verifyCompleteAReOpenTask() throws Exception {
        String desc = details.get("Description");
        log.info("Click on complete task.");
        tasks.completeTask();

        log.info("Open complete task(s) option.");
        tasks.clickOnCompletedTasks();
        log.info("Search task. Description [" + desc + "].");
        tasks.viewTask(desc);
    }

    @And("Verify closing task container")
    public void verifyClosingTaskContainer() throws Exception {
        tasks = new TasksPage(driver);

        log.info("Click on Task icon.");
        tasks.clickOnTaskIcon();
        Assert.assertTrue(tasks.verifyCloseIcon(),
                "Not able to close Task container after click on close icon..");

        log.info("Click on Task icon.");
        tasks.clickOnTaskIcon();
        String description = faker.aquaTeenHungerForce() + LocalDateTime.now().toString();

        log.info("Click on new Task");
        tasks.clickOnNewTask();

        log.info("Enter task description.");
        tasks.inputTaskDescription(description);

        Assert.assertTrue(tasks.verifyCloseIcon(description),
                "Task should not be created without after click ob cancel icon.");
    }

    @Then("Verify Cancel button functionality")
    public void verifyCancelButtonFunctionality() throws Exception {
        log.info("Click on Task icon.");
        tasks.clickOnTaskIcon();
        String description = faker.aquaTeenHungerForce() + LocalDateTime.now().toString();
        log.info("Click on New Task option.");
        tasks.clickOnNewTask();
        log.info("Enter task description. [" + description + "]");
        tasks.inputTaskDescription(description);
        log.info("Verify after canceling the task. Task should not be created.");
        Assert.assertFalse(tasks.verifyCancelButton(description),
                "Task created with description [" + description + "] by just input description and click on cancel.");
    }

    @And("Verify edit a task and remove optional fields")
    public void verifyEditATaskAndRemoveOptionalFields() throws Exception {
        log.info("Click on Edit.");
        tasks.clickOnEditTask();
        log.info("Remove optional fields values.");
        tasks.removeOptionalFields();
        log.info("Click on update.");
        tasks.clickOnUpdate();
        //TODO Verify removed tasks
    }

    @And("^Verify create new task with (Required|All) fields on tasks tab$")
    public void verifyCreateNewTaskWithRequiredFieldsOnTaskTab(String arg0) throws Exception {
        tasks = new TasksPage(driver);
        boolean isAll = arg0.equals("All");

        log.info("Scroll to patient.");
        tasks.searchForPatientInTasks(customUtils.getDefaultPatientDetails().get("MRN"));

        log.info("Click on new task.");
        tasks.clickOnNewTaskOnTasks(customUtils.getDefaultPatientDetails().get("MRN"));

        Map<String, String> taskValue = getTaskValue(isAll);
        String desc = taskValue.get("Description");
        log.info("Enter task description.");
        tasks.inputTaskDescription(desc);

        if (isAll) {
            log.info("Input optional fields. Values [" + taskValue + "]");
            details = tasks.inputOptionalFields(taskValue);
        }

        log.info("Click on Create.");
        tasks.createTask();
        desc = details.get("Description");
        log.info("Search task. Description [" + desc + "].");
        tasks.viewTask(desc);

        Assert.assertTrue(tasks.verifyCreatedTaskSuccessfully(desc),
                "Task not created with details [" + details + "].");
        if (isAll)
            Assert.assertTrue(tasks.verifyOptionalFieldsValue(taskValue),
                    "Task details not matched. Values [" + details + "]");
    }

    @Then("Verify comment on created task on tasks tab")
    public void verifyCommentOnCreatedTasksTab() throws Exception {
        String comment = faker.aquaTeenHungerForce().character() + LocalDateTime.now().toString();
        log.info("Add comment [" + comment + "].");
        tasks.addComment(comment);
        Assert.assertTrue(tasks.verifyAddedComment(comment),
                "Comment not added as [" + comment + "]");

        log.info("Refresh page.");
        tasks._refreshPage();

        String desc = details.get("Description");
        log.info("Search task. Description [" + desc + "].");
        tasks.viewTask(desc);

        Assert.assertTrue(tasks.verifyAddedComment(comment),
                "Comment not added as [" + comment + "]");
    }

    @And("Verify comment on completed task on tasks tab")
    public void verifyCommentOnCompletedTasksTab() throws Exception {
        String comment = faker.aquaTeenHungerForce().character() + LocalDateTime.now().toString();
        log.info("Add comment [" + comment + "].");
        tasks.addComment(comment);
        Assert.assertTrue(tasks.verifyAddedComment(comment),
                "Comment not added as [" + comment + "]");
        log.info("Refresh page.");
        tasks._refreshPage();
        log.info("Click on complete task.");
        tasks.clickOnCompletedTasksTab(customUtils.getDefaultPatientDetails().get("MRN"));

        String desc = details.get("Description");
        log.info("Search task. Description [" + desc + "].");
        tasks.searchForPatientInTasks(customUtils.getDefaultPatientDetails().get("MRN"));
        tasks.viewTask(desc);

        Assert.assertTrue(tasks.verifyAddedComment(comment),
                "Comment not added as [" + comment + "]");
    }

    @Then("Verify re-open completed task on tasks tab")
    public void verifyReOpenCompletedTasksTab() throws Exception {
        String desc = details.get("Description");
        log.info("Search task. Description [" + desc + "].");
        tasks.viewTask(desc);
        log.info("Click on reopen.");
        tasks.clickOnReOpen();

        Assert.assertTrue(tasks.verifyTaskReopened(), "Task with description [" + desc + "] not re-open.");
        log.info("Refresh page.");
        tasks._refreshPage();

        log.info("Search task. Description [" + desc + "].");
        tasks.viewTask(desc);
    }

    @And("Verify comment on re-opened task on tasks tab")
    public void verifyCommentOnReOpenedTasksTab() throws Exception {
        String comment = faker.aquaTeenHungerForce().character() + LocalDateTime.now().toString();
        log.info("Add comment [" + comment + "].");
        tasks.addComment(comment);
        Assert.assertTrue(tasks.verifyAddedComment(comment),
                "Comment not added as [" + comment + "]");

        log.info("Refresh page.");
        tasks._refreshPage();

        String desc = details.get("Description");
        log.info("Search task. Description [" + desc + "].");
        tasks.viewTask(desc);
        Assert.assertTrue(tasks.verifyAddedComment(comment),
                "Comment not added as [" + comment + "]");
    }

    @And("^Verify edit a task with (Required|All) fields on tasks tab$")
    public void verifyEditATaskWithRequiredFieldsOnTasksTab(String arg0) throws Exception {
        boolean isAll = arg0.equals("All");
        log.info("Click on edit.");
        tasks.clickOnEditTask();

        details.put("Description", faker.gameOfThrones().dragon() + LocalDateTime.now().toString());
        String desc = details.get("Description");
        log.info("Update task description as [" + desc + "].");
        tasks.updateDescription(desc);

        if (isAll) {
            log.info("Update optional fields..");
            details = tasks.inputOptionalFields(details);
            log.info("Optional fields entered as [" + details + "].");
        }

        log.info("Click on update.");
        tasks.clickOnUpdate();

        Assert.assertTrue(tasks.verifyUpdatedTaskDescription(details.get("Description")),
                "Updated task description not found as [" + desc + "].");

        log.info("Search task. Description [" + desc + "].");
        tasks.viewTask(desc);
        if (isAll)
            Assert.assertTrue(tasks.verifyOptionalFieldsValue(details),
                    "Optional fields value not matched. Values [" + details + "].");
    }

    @Then("^Verify edit re-open task with (Required|All) fields on tasks tab$")
    public void verifyEditReOpenTaskWithFieldsTab(String arg0) throws Exception {
        boolean isAll = arg0.equals("All");
        log.info("Click on edit.");
        tasks.clickOnEditTask();
        details.put("Description", faker.gameOfThrones().dragon() + LocalDateTime.now().toString());

        String desc = details.get("Description");
        log.info("Update task description as [" + desc + "].");
        tasks.updateDescription(desc);

        if (isAll) {
            log.info("Update optional fields..");
            details = tasks.inputOptionalFields(details);
            log.info("Optional fields entered as [" + details + "].");
        }

        log.info("Click on update.");
        tasks.clickOnUpdate();

        Assert.assertTrue(tasks.verifyUpdatedTaskDescription(details.get("Description")),
                "Updated task description not found as [" + desc + "].");

        log.info("Click on close icon.");
        tasks.verifyCloseIcon();

        log.info("Search task. Description [" + desc + "].");
        tasks.viewTask(desc);

        if (isAll)
            Assert.assertTrue(tasks.verifyOptionalFieldsValue(details),
                    "Optional fields value not matched. Values [" + details + "].");
    }

    @Then("Verify complete a task on task tab")
    public void verifyCompleteATaskOnTaskTab() throws Exception {
        log.info("Complete a task.");
        tasks.completeTask();

        log.info("Open Completed task option.");
        tasks.clickOnCompletedTasksTab(customUtils.getDefaultPatientDetails().get("MRN"));

        log.info("verify task should be present in completed section.");
        tasks.viewTask(details.get("Description"));
    }

    @Then("Verify complete a re-open task on task tab")
    public void verifyCompleteAReOpenTaskOnTaskTab() throws Exception {
        String desc = details.get("Description");
        log.info("Click on complete task.");
        tasks.completeTask();

        log.info("Open complete task(s) option.");
        tasks.clickOnCompletedTasksTab(customUtils.getDefaultPatientDetails().get("MRN"));

        log.info("Search task. Description [" + desc + "].");
        tasks.viewTask(desc);
    }
}
