package com.biofourmis.biovitals.stepdefinitions.web;

import com.biofourmis.biovitals.constants.configuration.TestConfig;
import com.biofourmis.biovitals.constants.scripts.ClinicInfo;
import com.biofourmis.biovitals.constants.scripts.Patient;
import com.biofourmis.biovitals.pagefactory.web.AddPatientPage;
import com.biofourmis.biovitals.pagefactory.web.HomePage;
import com.biofourmis.biovitals.utilities.CustomUtils;
import com.github.javafaker.Faker;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.testng.Assert;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;

@Log4j
public class AddPatientHook {

    @Autowired
    @Qualifier(value = "web")
    private WebDriver driver;
    @Autowired
    private Faker faker;
    @Autowired
    private Patient patient;
    @Autowired
    private TestConfig config;
    @Autowired
    private ClinicInfo clinicInfo;

    private HomePage homePage;
    private AddPatientPage addPatientPage;
    private final CustomUtils customUtils = new CustomUtils(null);

    @And("Verify Add Patient button")
    public void verifyAddPatientButton() throws Exception {
        homePage = new HomePage(driver);

        log.info("Navigate to home page.");
        homePage.goToHomePage();

        log.info("Verify Add patient button visible.");
        Assert.assertTrue(homePage.verifyAddPatientButton(),
                "Add Patient button is disable or not present.");
    }

    @Then("Verify mandatory fields message")
    public void verifyMandatoryFields() throws Exception {
        homePage = new HomePage(driver);
        addPatientPage = new AddPatientPage(driver);

        //Read Clinic Mandatory Fields
        log.info("Navigate to home page.");
        homePage.goToHomePage();

        log.info("Click on Add Patient.");
        Assert.assertTrue(homePage.clickOnAddPatient(), "Not able to click on Add Patient.");

        String[] patientOptions = clinicInfo.getAddPatientSteps().split(":");
        List<String> steps = new ArrayList<>();

        for (String option : patientOptions)
            if (option.contains("["))
                steps.add(option.substring(0, option.indexOf("[")).replaceAll("\\s", "").toLowerCase(Locale.ROOT));
            else
                steps.add(option.replaceAll("\\s", "").toLowerCase(Locale.ROOT));

        //For Personal Information page
        if (steps.contains("personalinformation")) {
            log.info("verify MRN/Reference Id* error messages.");
            verifyMRNErrorMessages(clinicInfo.getAddPatientSteps());

            String mrnDigit = String.valueOf(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
            log.info("Enter MRN/Reference Id* value as [" + mrnDigit + "]");
            addPatientPage.enterMRN(mrnDigit);

            log.info("Verify other required fields.");
            verifyOtherRequiredFieldsOnPersonalInfoPage(clinicInfo.getAddPatientSteps(),customUtils.getDefaultPatientDetails());

            log.info("Verify Next button should be enable before entering all req fields.");
            Assert.assertTrue(addPatientPage.isNextEnable(), "Next button should be enable.");

            log.info("Verify Patient information option should not be checked before navigate to next tab.");
            Assert.assertFalse(addPatientPage.verifyCheckedSign("Personal information"),
                    "Patient information option should not be checked before navigate to next tab.");

            addPatientPage.clickOnNext();

            log.info("Verify Patient information option should be checked after navigate to next tab.");
            Assert.assertTrue(addPatientPage.verifyCheckedSign("Personal information"),
                    "Patient information option should be checked after navigate to next tab.");
        }

        //For Medical Information page
        if (steps.contains("medicalinformation")) {
            log.info("Verify Medical information option should not be checked before navigate to next tab.");
            Assert.assertFalse(addPatientPage.verifyCheckedSign("Medical information"),
                    "Medical information option should not checked before navigate to next tab.");

            log.info("Click on next.");
            addPatientPage.clickOnNext();
        }

        //For Care team information page
        if (steps.contains("careteam")) {
            log.info("Verify Care team information page fields.[" + customUtils.careTeamInfoPage() + "].");
            addPatientPage.isAllTextPresent(customUtils.careTeamInfoPage());

            log.info("Verify Medical information option should be checked after navigate to next tab.");
            Assert.assertTrue(addPatientPage.verifyCheckedSign("Medical information"),
                    "Medical information option should be checked after navigate to next tab.");

            log.info("Verify Care team information option should not be checked before navigate to next tab.");
            Assert.assertFalse(addPatientPage.verifyCheckedSign("Care team"),
                    "Care team option should not be checked before navigate to next tab.");

            List<String> physician =List.of(config.getClinicianName());
                    //Arrays.asList(customUtils.getDefaultPatientDetails().get("Physicians").split(","));
            List<String> nurse = List.of(config.getNurseName());
                    //Arrays.asList(customUtils.getDefaultPatientDetails().get("Nurses_Paramedics").split(","));

            log.info("Verify Care team information page.");
            addPatientPage.verifyCareTeamInfoPageRequiredFields(physician, nurse);

            log.info("Click on next.");
            addPatientPage.clickOnNext();

            log.info("Verify Care team information option should be checked before navigate to next tab.");
            Assert.assertTrue(addPatientPage.verifyCheckedSign("Care team"),
                    "Care team option should be checked after navigate to next tab.");
        }

        //Equipment information page
        if (steps.contains("equipmentinformation")) {
            log.info("Verify Equipment information page fields.[" + customUtils.equipmentInfoPage() + "].");
            addPatientPage.isAllTextPresent(customUtils.equipmentInfoPage());

            log.info("Verify Equipment information option should not be checked before navigate to next tab.");
            Assert.assertFalse(addPatientPage.verifyCheckedSign("Equipment information"),
                    "Equipment information option should not be checked before navigate to next tab.");

            log.info("Fill Equipment information.");
            addPatientPage.fillEquipmentInformation(customUtils.getDefaultPatientDetails(), true);

            log.info("Click on next.");
            addPatientPage.clickOnNext();

            log.info("Verify Equipment information option should be checked before navigate to next tab.");
            Assert.assertTrue(addPatientPage.verifyCheckedSign("Equipment information"),
                    "Equipment information option should be checked after navigate to next tab.");
        }
    }

    @And("Fill patient mandatory details")
    public void fillPatientMandatoryDetails() throws Exception {
        homePage = new HomePage(driver);
        addPatientPage = new AddPatientPage(driver);

        List<String> physician = List.of(config.getClinicianName());
                //Arrays.asList(customUtils.getDefaultPatientDetails().get("Physicians").split(","));
        List<String> nurse =List.of(config.getNurseName());
                //Arrays.asList(customUtils.getDefaultPatientDetails().get("Nurses_Paramedics").split(","));
        Map<String, String> values = new HashMap<>();
        values.put("MRN", String.valueOf(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)));
        values.put("Patient_Mob", "3" + String.valueOf(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)));
        values.put("Patient_Landline", "4" + String.valueOf(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)));
        values.put("Family_Pho_Num", "1" + String.valueOf(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)));
        values.put("Point_number", String.valueOf(faker.number().digits(10)));
        values.put("Physicians",config.getClinicianName());
        values.put("Nurses_Paramedics",config.getNurseName());
        log.info("Try to create patient with details :" + customUtils.getDefaultPatientDetails(values));

        //Read Clinic Mandatory Fields
        log.info("Navigate to home page.");
        homePage.goToHomePage();

        log.info("Click on Add Patient.");
        Assert.assertTrue(homePage.clickOnAddPatient(), "Not able to click on Add Patient.");

        String[] patientOptions = clinicInfo.getAddPatientSteps().split(":");
        List<String> steps = new ArrayList<>();

        for (String option : patientOptions)
            if (option.contains("["))
                steps.add(option.substring(0, option.indexOf("[")).replaceAll("\\s", "").toLowerCase(Locale.ROOT));
            else
                steps.add(option.replaceAll("\\s", "").toLowerCase(Locale.ROOT));

        //For Personal Information page
        if (steps.contains("personalinformation")) {
            String mrnDigit = values.get("MRN");
            log.info("Enter MRN/Reference Id* value as [" + mrnDigit + "]");
            addPatientPage.enterMRN(mrnDigit);

            log.info("Verify other required fields.");
            verifyOtherRequiredFieldsOnPersonalInfoPage(clinicInfo.getAddPatientSteps(),customUtils.getDefaultPatientDetails());

            addPatientPage.clickOnNext();
        }

        //For Medical Information page
        if (steps.contains("medicalinformation")) {
            log.info("Click on next.");
            addPatientPage.clickOnNext();
        }

        //For Care team information page
        if (steps.contains("careteam")) {
            log.info("Verify Care team information page.");
            addPatientPage.verifyCareTeamInfoPageRequiredFields(physician, nurse);

            log.info("Click on next.");
            addPatientPage.clickOnNext();
        }

        //Equipment information page
        if (steps.contains("equipmentinformation")) {
            log.info("Fill Equipment information.");
            addPatientPage.fillEquipmentInformation(customUtils.getDefaultPatientDetails(), true);

            log.info("Click on next.");
            addPatientPage.clickOnNext();
        }
    }


    @Then("^Add patient details till section (Personal information|Medical information|Care team|Equipment information|Review)$")
    public void addPatientDetailsTillSection(String arg0) throws Exception {
        addPatientPage = new AddPatientPage(driver);
        Map<String, String> values = new HashMap<>();
        values.put("MRN", String.valueOf(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)));
        values.put("Patient_Mob", "3" + String.valueOf(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)));
        values.put("Patient_Landline", "4" + String.valueOf(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)));
        values.put("Family_Pho_Num", "1" + String.valueOf(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)));
        values.put("Point_number", String.valueOf(faker.number().digits(10)));
        values.put("Physicians",config.getClinicianName());
        values.put("Nurses_Paramedics",config.getNurseName());
        customUtils.getDefaultPatientDetails(values);

        log.info("Try to create patient with details :" + customUtils.getPatientDetails());
        if (arg0.equals("Personal information")) {
            log.info("Fill Personal information.");
            addPatientPage.fillPersonalInformation(customUtils.getDefaultPatientDetails(), false);
        } else if (arg0.equals("Medical information")) {
            log.info("Fill Personal information.");
            addPatientPage.fillPersonalInformation(customUtils.getDefaultPatientDetails(), false);
            log.info("Click on next.");
            addPatientPage.clickOnNext();
            log.info("Fill Medical information.");
            addPatientPage.fillMedicalInformation(customUtils.getDefaultPatientDetails(), false);
        } else if (arg0.equals("Care team")) {
            log.info("Fill Personal information.");
            addPatientPage.fillPersonalInformation(customUtils.getDefaultPatientDetails(), false);
            log.info("Click on next.");
            addPatientPage.clickOnNext();
            log.info("Fill Medical information.");
            addPatientPage.fillMedicalInformation(customUtils.getDefaultPatientDetails(), false);
            log.info("Click on next.");
            addPatientPage.clickOnNext();
            log.info("Fill Care team information.");
            addPatientPage.fillCareTeamInformation(customUtils.getDefaultPatientDetails(), false);
        } else if (arg0.equals("Equipment information")) {
            log.info("Fill Personal information.");
            addPatientPage.fillPersonalInformation(customUtils.getDefaultPatientDetails(), false);
            log.info("Click on next.");
            addPatientPage.clickOnNext();
            log.info("Fill Medical information.");
            addPatientPage.fillMedicalInformation(customUtils.getDefaultPatientDetails(), false);
            log.info("Click on next.");
            addPatientPage.clickOnNext();
            log.info("Fill Care team information.");
            addPatientPage.fillCareTeamInformation(customUtils.getDefaultPatientDetails(), false);
            log.info("Click on next.");
            addPatientPage.clickOnNext();
            log.info("Fill Equipment information.");
            addPatientPage.fillEquipmentInformation(customUtils.getDefaultPatientDetails(), false);
        } else if (arg0.equals("Review")) {
            log.info("Fill Personal information.");
            addPatientPage.fillPersonalInformation(customUtils.getDefaultPatientDetails(), false);
            log.info("Click on next.");
            addPatientPage.clickOnNext();
            log.info("Fill Medical information.");
            addPatientPage.fillMedicalInformation(customUtils.getDefaultPatientDetails(), false);
            log.info("Click on next.");
            addPatientPage.clickOnNext();
            log.info("Fill Care team information.");
            addPatientPage.fillCareTeamInformation(customUtils.getDefaultPatientDetails(), false);
            log.info("Click on next.");
            addPatientPage.clickOnNext();
            log.info("Fill Equipment information.");
            addPatientPage.fillEquipmentInformation(customUtils.getDefaultPatientDetails(), false);
            log.info("Click on next.");
            addPatientPage.clickOnNext();
        }
    }

    @Then("^Fill patient details of section (Personal information|Medical information|Care team|Equipment information|Review)$")
    public void addPatientDetailsOfSection(String arg0) throws Exception {
        addPatientPage = new AddPatientPage(driver);
        if (arg0.equals("Personal information")) {
            log.info("Fill Personal information.");
            Map<String, String> mrn = new LinkedHashMap<>();
            mrn.put("MRN", String.valueOf(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)));
            mrn.put("Patient_Mob", "3" + String.valueOf(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)));
            mrn.put("Patient_Landline", "4" + String.valueOf(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)));
            mrn.put("Family_Pho_Num", "1" + String.valueOf(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)));
            mrn.put("Point_number", String.valueOf(faker.number().digits(10)));
            mrn.put("Physicians",config.getClinicianName());
            mrn.put("Nurses_Paramedics",config.getNurseName());
            addPatientPage.fillPersonalInformation(customUtils.getDefaultPatientDetails(mrn), false);
        } else if (arg0.equals("Medical information")) {
            log.info("Click on next.");
            addPatientPage.clickOnNext();
            log.info("Fill Medical information.");
            addPatientPage.fillMedicalInformation(customUtils.getDefaultPatientDetails(), false);
        } else if (arg0.equals("Care team")) {
            log.info("Click on next.");
            addPatientPage.clickOnNext();
            log.info("Fill Care team information.");
            addPatientPage.fillCareTeamInformation(customUtils.getDefaultPatientDetails(), false);
        } else if (arg0.equals("Equipment information")) {
            log.info("Click on next.");
            addPatientPage.clickOnNext();
            log.info("Fill Equipment information.");
            addPatientPage.fillEquipmentInformation(customUtils.getDefaultPatientDetails(), false);
        } else if (arg0.equals("Review")) {
            log.info("Try to create patient with details :" + customUtils.getPatientDetails());
            log.info("Click on next.");
            addPatientPage.clickOnNext();
        }
    }

    @Then("Cancel Add patient flow")
    public void cancelAddPatientFlow() throws Exception {
        addPatientPage = new AddPatientPage(driver);
        log.info("Click Add patient wizard.");
        addPatientPage.cancelAddPatientWizard();
    }

    @Then("Verify flow should terminate with cancel info pop-up")
    public void verifyFlowShouldTerminateWithCancelInfoPopUp() throws Exception {
        homePage = new HomePage(driver);
        addPatientPage = new AddPatientPage(driver);
        log.info("Navigate to home page.");
        homePage.goToHomePage();

        log.info("Verify click on Add patient button.");
        Assert.assertTrue(homePage.clickOnAddPatient(), "Not able to click on Add Patient button.");

        log.info("Click Add patient wizard.");
        addPatientPage.cancelAddPatientWizard();

        String message = "Are you sure you want to leave this page? All unsaved data will be lost.";
        addPatientPage.verifyAddPatientCancelPopUp(message);
    }

    @And("Verify patient should not be created with given MNR Number")
    public void verifyPatientShouldNotBeCreatedWithGivenMNRNumber() throws Exception {
        addPatientPage = new AddPatientPage(driver);

        log.info("Navigate to home page.");
        homePage.goToHomePage();

        log.info("Search patient.");
        homePage.searchPatient(customUtils.getDefaultPatientDetails().get("MRN"));

        log.info("Verify searched patient displayed.");
        Assert.assertTrue(homePage.isPatientPresent(customUtils.getDefaultPatientDetails().get("MRN")));
    }

    @And("Add patient mandatory details")
    public void addPatientMandatoryDetails() throws Exception {
        homePage = new HomePage(driver);
        addPatientPage = new AddPatientPage(driver);

        List<String> physician = List.of(config.getClinicianName());
                //Arrays.asList(customUtils.getDefaultPatientDetails().get("Physicians").split(","));
        List<String> nurse = List.of(config.getNurseName());

        //Arrays.asList(customUtils.getDefaultPatientDetails().get("Physicians").split(","));
        //Arrays.asList(customUtils.getDefaultPatientDetails().get("Nurses_Paramedics").split(","));

        Map<String, String> values = new HashMap<>();
        values.put("MRN", String.valueOf(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)));
        values.put("Patient_Mob", "3" + String.valueOf(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)));
        values.put("Patient_Landline", "4" + String.valueOf(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)));
        values.put("Family_Pho_Num", "1" + String.valueOf(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)));
        values.put("Point_number", String.valueOf(faker.number().digits(10)));
        values.put("Physicians",config.getClinicianName());
        values.put("Nurses_Paramedics",config.getNurseName());

        customUtils.getDefaultPatientDetails(values);
        log.info("Try to create patient with details :" + customUtils.getPatientDetails());

        //Read Clinic Mandatory Fields
        log.info("Navigate to home page.");
        homePage.goToHomePage();

        log.info("Click on Add Patient.");
        Assert.assertTrue(homePage.clickOnAddPatient(), "Not able to click on Add Patient.");

        String[] patientOptions = clinicInfo.getAddPatientSteps().split(":");
        List<String> steps = new ArrayList<>();

        for (String option : patientOptions)
            if (option.contains("["))
                steps.add(option.substring(0, option.indexOf("[")).replaceAll("\\s", "").toLowerCase(Locale.ROOT));
            else
                steps.add(option.replaceAll("\\s", "").toLowerCase(Locale.ROOT));

        //For Personal Information page
        if (steps.contains("personalinformation")) {
            String mrnDigit = values.get("MRN");
            log.info("Enter MRN/Reference Id* value as [" + mrnDigit + "]");
            addPatientPage.enterMRN(mrnDigit);

            log.info("Verify other required fields.");
            verifyOtherRequiredFieldsOnPersonalInfoPage(clinicInfo.getAddPatientSteps(),customUtils.getDefaultPatientDetails());

            addPatientPage.clickOnNext();
        }

        //For Medical Information page
        if (steps.contains("medicalinformation")) {
            log.info("Click on next.");
            addPatientPage.clickOnNext();
        }

        //For Care team information page
        if (steps.contains("careteam")) {
            log.info("Verify Care team information page.");
            addPatientPage.verifyCareTeamInfoPageRequiredFields(physician, nurse);

            log.info("Click on next.");
            addPatientPage.clickOnNext();
        }

        //Equipment information page
        if (steps.contains("equipmentinformation")) {
            log.info("Fill Equipment information.");
            addPatientPage.fillEquipmentInformation(customUtils.getDefaultPatientDetails(), true);

            log.info("Click on next.");
            addPatientPage.clickOnNext();
        }
  }

    @And("^Click on Cancel and (decline|accept) the info pop-up$")
    public void clickOnCancelAndDeclineTheInfoPopUp(String arg0) throws Exception {
        addPatientPage = new AddPatientPage(driver);
        log.info("Verify cancel add patient wizard.");
        addPatientPage.cancelAddPatientWizard();

        log.info(arg0 + " cancel pop-up");
        boolean flag = arg0.equals("decline");
        String message = "Are you sure you want to leave this page? All unsaved data will be lost.";

        if (flag)
            addPatientPage.declineCancelPatientPopUp();
        else
            addPatientPage.verifyAddPatientCancelPopUp(message);
    }

    @Then("^Verify (all|mandatory) field details on review page$")
    public void verifyFieldDetailsOnReviewPage(String arg0) throws Exception {
        log.info("Try to create patient with details :" + customUtils.getPatientDetails());
        boolean flag = arg0.equals("all");
        String expected = "";
        Map<String,String> values = new LinkedHashMap<>();
        values.put("Physicians",config.getClinicianName());
        values.put("Nurses_Paramedics",config.getNurseName());
        Map<String, String> defaultPatientDetails = customUtils.getDefaultPatientDetails(values);

        //Read Clinic Mandatory Fields
        String[] patientOptions = clinicInfo.getAddPatientSteps().split(":");
        List<String> steps = new ArrayList<>();

        for (String option : patientOptions)
            if (option.contains("["))
                steps.add(option.substring(0, option.indexOf("[")).replaceAll("\\s", "").toLowerCase(Locale.ROOT));
            else
                steps.add(option.replaceAll("\\s", "").toLowerCase(Locale.ROOT));

        //For Personal Information page
        if (steps.contains("personalinformation")) {
            String value = clinicInfo.getAddPatientSteps().split(":")[0];
            List<String> listOfFields = new ArrayList<>();
            String[] dataSet = value.substring(value.indexOf("[") + 1, value.indexOf("]")).split(",");

            for (String data : dataSet)
                listOfFields.add(data.replaceAll("\\s+", "").toLowerCase(Locale.ROOT));

            String mrnName;
            if (listOfFields.contains("mrn") || listOfFields.contains("mrn*"))
                mrnName = "MRN";
            else
                mrnName = "Reference ID";
            expected = defaultPatientDetails.get("MRN");
            Assert.assertTrue(addPatientPage._isElementVisible(addPatientPage.verifyReviewPage(mrnName, expected)),
                    mrnName + " number not matched. Expected : " + expected);

            expected = defaultPatientDetails.get("Preferred_Lang");
            Assert.assertTrue(addPatientPage._isElementVisible(addPatientPage.verifyReviewPage("Patient's preferred language", expected)),
                    "Preferred language not matched. Expected : " + expected);

            if (listOfFields.contains("firstname*")) {
                expected = defaultPatientDetails.get("F_Name") + " " + defaultPatientDetails.get("L_Name");
                Assert.assertTrue(addPatientPage._getText(By.xpath("//*[contains(text(),'Patient information')]/..//*[text()=' Full name ']/..//*[contains(@class,\"frm-value\")]")).equalsIgnoreCase(expected),
                        "");
            }
            if (listOfFields.contains("gender*")) {
                expected = defaultPatientDetails.get("Gender");
                Assert.assertTrue(addPatientPage._isElementVisible(addPatientPage.verifyReviewPage("Gender", expected)),
                        "Gender not matched. Expected : " + expected);
            }
            if (listOfFields.contains("dob*")) {
                expected = defaultPatientDetails.get("DOB");
                Assert.assertTrue(addPatientPage._isElementVisible(addPatientPage.verifyReviewPage("DOB", expected)),
                        "DOB not matched. Expected : " + expected);
            }
            if (listOfFields.contains("addressline1*")) {
                expected = defaultPatientDetails.get("Address_line_1");
                Assert.assertTrue(addPatientPage._isElementVisible(addPatientPage.verifyReviewPage("Address", expected)),
                        "Address not matched. Expected : " + expected);
            }
            if (listOfFields.contains("zipcode*")) {
                expected = defaultPatientDetails.get("Zip_code");
                Assert.assertTrue(addPatientPage._getText(By.xpath("//*[contains(text(),'Patient information')]/..//*[text()=' Zip code ']/..//*[contains(@class,\"frm-value\")]")).equalsIgnoreCase(expected),
                        "");
            }
            if (listOfFields.contains("city*")) {
                expected = defaultPatientDetails.get("City");
                Assert.assertTrue(addPatientPage._getText(By.xpath("//*[contains(text(),'Patient information')]/..//*[text()=' City ']/..//*[contains(@class,\"frm-value\")]")).equalsIgnoreCase(expected),
                        "");
            }
            if (listOfFields.contains("state*")) {
                expected = defaultPatientDetails.get("State");
                Assert.assertTrue(addPatientPage._getText(By.xpath("//*[contains(text(),'Patient information')]/..//*[text()=' State ']/..//*[contains(@class,\"frm-value\")]")).equalsIgnoreCase(expected),
                        "");
            }

            if (flag) {
                expected = defaultPatientDetails.get("Address_line_1") + " " + defaultPatientDetails.get("Address_line_2");
                Assert.assertTrue(addPatientPage._isElementVisible(addPatientPage.verifyReviewPage("Address", expected)),
                        "Address not matched. Expected : " + expected);

                expected = defaultPatientDetails.get("Patient_Mob");
                Assert.assertTrue(addPatientPage._isElementVisible(addPatientPage.verifyReviewPage("Patient's mobile number", expected)),
                        "Patient mobile number not matched. Expected : " + expected);

                expected = defaultPatientDetails.get("Patient_Landline");
                Assert.assertTrue(addPatientPage._isElementVisible(addPatientPage.verifyReviewPage("Patient's landline number", expected)),
                        "Patient Landline number not matched. Expected : " + expected);

                expected = defaultPatientDetails.get("Family_Pho_Num");
                Assert.assertTrue(addPatientPage._isElementVisible(addPatientPage.verifyReviewPage("Family contact information", expected)),
                        "Family phone number not matched. Expected : " + expected);

                expected = defaultPatientDetails.get("Familial_Relation");
                Assert.assertTrue(addPatientPage._isElementVisible(addPatientPage.verifyReviewPage("Familial relationship", expected)),
                        "Familial relationship not matched. Expected : " + expected);
            }
        }

        //For Medical Information page
        if (steps.contains("medicalinformation")) {
            if (flag) {
                expected = defaultPatientDetails.get("Code_Status");
                Assert.assertTrue(addPatientPage._isElementVisible(addPatientPage.verifyReviewPage("Code status", expected)),
                        "Code status not matched. Expected : " + expected);

                expected = defaultPatientDetails.get("Allergies");
                Assert.assertTrue(addPatientPage._isElementVisible(addPatientPage.verifyReviewPage("Allergies", expected)),
                        "Allergies value not matched. Expected : " + expected);

                expected = defaultPatientDetails.get("Contact_precautions");
                Assert.assertTrue(addPatientPage._isElementVisible(addPatientPage.verifyReviewPage("Contact precautions", expected)),
                        "Contact precautions not matched. Expected : " + expected);

                expected = defaultPatientDetails.get("Admitting_diagnosis");
                Assert.assertTrue(addPatientPage._isElementVisible(addPatientPage.verifyReviewPage("Admitting diagnosis", expected)),
                        "Admitting diagnosis value not matched. Expected : " + expected);

                expected = defaultPatientDetails.get("Clinical_summary");
                Assert.assertTrue(addPatientPage._isElementVisible(addPatientPage.verifyReviewPage("Clinical summary", expected)),
                        "Clinical summary not matched. Expected : " + expected);
            }
        }

        //For Care team information page
        if (steps.contains("careteam")) {
            try {
                addPatientPage._scrollToElement(driver.findElement(addPatientPage.verifyReviewPage("Enrollment type")));
            } catch (Exception e) {
            }

            expected = defaultPatientDetails.get("Physicians");
            Assert.assertTrue(addPatientPage._isElementVisible(addPatientPage.verifyReviewPage("Care team", expected)),
                    "Physicians value not matched. Expected : " + expected);

            expected = defaultPatientDetails.get("Nurses_Paramedics");
            Assert.assertTrue(addPatientPage._isElementVisible(addPatientPage.verifyReviewPage("Care team", expected)),
                    "Nurses Paramedics value not matched. Expected : " + expected);
        }

        //Equipment information page
        if (steps.contains("equipmentinformation")) {
            if (flag) {
                expected = defaultPatientDetails.get("Point_of_contact");
                Assert.assertTrue(addPatientPage._isElementVisible(addPatientPage.verifyReviewPage("Point of contact", expected)),
                        "Point of contact value not matched. Expected : " + expected);

                expected = defaultPatientDetails.get("Point_F_Name") + " " + defaultPatientDetails.get("Point_L_Name");
                Assert.assertEquals(addPatientPage._getText(By.xpath("//*[contains(text(),'Equipment information')]/..//*[text()=' Full name ']/..//*[contains(@class,\"frm-value\")]"))
                        , expected);

                expected = defaultPatientDetails.get("Point_number");
                Assert.assertTrue(addPatientPage._isElementVisible(By.xpath("//*[contains(text(),'Equipment information')]/..//*[contains(text(),'Phone number')]/..//*[contains(text(),'" + expected + "')]")),
                        "Phone number under Equipment information not matched. Expected : " + expected);
            }

            expected = defaultPatientDetails.get("Length_of_stay");
            Assert.assertTrue(addPatientPage._isElementVisible(addPatientPage.verifyReviewPage("Length of stay", expected)),
                    "Length of stay not matched. Expected :" + expected);

            expected = defaultPatientDetails.get("Devices");
            Assert.assertTrue(addPatientPage._getText(By.xpath("//*[contains(text(),'Devices')]/..//*[contains(@class,'block')]"))
                            .contains(expected),
                    "Devices not matched. Expected : " + expected);

            Assert.assertTrue(addPatientPage._isElementVisible(addPatientPage.verifyReviewPage(" Documents ", "Privacy Policy")),
                    "Privacy Policy not found under documents list.");
            Assert.assertTrue(addPatientPage._isElementVisible(addPatientPage.verifyReviewPage(" Documents ", "Terms & Conditions")),
                    "Terms & Conditions not found under documents list.");
            Assert.assertTrue(addPatientPage._isElementVisible(addPatientPage.verifyReviewPage(" Documents ", "Consent")),
                    "Consent not found under documents list.");
            Assert.assertTrue(addPatientPage._isElementVisible(addPatientPage.verifyReviewPage(" Documents ", "Addressed / stamped return envelope")),
                    "Addressed / stamped return envelope not found under documents list.");
            Assert.assertTrue(addPatientPage._isElementVisible(addPatientPage.verifyReviewPage(" Documents ", "Quickstart guide")),
                    "Quickstart guide not found under documents list.");

            expected = defaultPatientDetails.get("Ship_Address_1");
            Assert.assertTrue(addPatientPage._isElementVisible(addPatientPage.verifyReviewPage(" Shipping address ", expected)),
                    "Shipping Address not matched. Expected : " + expected);

            expected = defaultPatientDetails.get("Ship_ZipCode");
            Assert.assertTrue(addPatientPage._isElementVisible(By.xpath("//*[contains(text(),'Equipment information')]/..//*[contains(text(),'Zip code')]/..//*[contains(text(),'" + expected + "')]")),
                    "Ship Zip code not matched. Expected : " + expected);

            expected = defaultPatientDetails.get("Ship_City");
            Assert.assertTrue(addPatientPage._isElementVisible(By.xpath("//*[contains(text(),'Equipment information')]/..//*[contains(text(),'City')]/..//*[contains(text(),'" + expected + "')]")),
                    "Ship city not matched. Expected : " + expected);

            expected = defaultPatientDetails.get("Ship_State");
            Assert.assertTrue(addPatientPage._isElementVisible(By.xpath("//*[contains(text(),'Equipment information')]/..//*[contains(text(),'State')]/..//*[contains(text(),'" + expected + "')]")),
                    "Ship state not matched. Expected : " + expected);

            expected = defaultPatientDetails.get("Enrollment_type");
            Assert.assertTrue(addPatientPage._getText(addPatientPage.verifyReviewPage("Enrollment type")).strip().equals(expected),
                    "Enrollment type not matched. Expected : " + expected);

            expected = defaultPatientDetails.get("Email_equipment");
            Assert.assertTrue(addPatientPage._isElementVisible(addPatientPage.verifyReviewPage(" Email equipment information to the Biofourmis logistics team after patient’s admission? ", expected)),
                    "Email equipment information option not matched. Expected : " + expected);
        }
    }

    @And("Save patient")
    public void savePatient() throws Exception {
        log.info("Set QR Expire details.");
        customUtils.setQRExpireDetails(8);
        log.info("Try to create patient with details :" + customUtils.getPatientDetails());
        log.info("Save patient.");
        addPatientPage.savePatient();
        log.info("Update QR Expire details if required.");
        customUtils.updateQRExpireDetails(8);
    }

    @Then("Verify patient created successfully")
    public void verifyPatientCreatedSuccessfully() throws Exception {
        Map<String, String> defaultPatientDetails = customUtils.getDefaultPatientDetails();
        log.info("Try to create patient with details :" + customUtils.getPatientDetails());

        log.info("verify Patient created successfully confirmation.");
        Assert.assertTrue(addPatientPage.verifyPatientCreatedPopUp(defaultPatientDetails),
                defaultPatientDetails.get("F_Name") + " " + defaultPatientDetails.get("L_Name") + " has been admitted not displayed.");

        log.info("Set QR Hash details.");
        customUtils.setQrHashUrl(homePage.readQRUrl());

        log.info("Navigate to home page.");
        homePage.goToHomePage();

        log.info("Search patient.");
        homePage.searchPatient(customUtils.getDefaultPatientDetails().get("MRN"));

        log.info("Verify search patient.");
        Assert.assertFalse(homePage.isPatientPresent(customUtils.getDefaultPatientDetails().get("MRN")));

        customUtils.setGlobalMNR(customUtils.getDefaultPatientDetails().get("MRN"));
    }

    @And("Click on Next")
    public void clickOnNext() throws Exception {
        addPatientPage.clickOnNext();
    }

    @Then("Verify Add Patient button and wizard")
    public void verifyAddPatientButtonAndWizard() throws Exception {
        homePage = new HomePage(driver);
        addPatientPage = new AddPatientPage(driver);

        log.info("Navigate to home page.");
        homePage.goToHomePage();

        log.info("Verify add patient button should be present.");
        Assert.assertTrue(homePage.verifyAddPatientButton(),
                "Add Patient button should be visible/enable.");

        log.info("Click on Add patient button.");
        Assert.assertTrue(homePage.clickOnAddPatient(), "Not able to click on Add Patient.");

        log.info("Verify Add patient wizard displayed.");
        Assert.assertTrue(addPatientPage.verifyAppPatientWizardDisplayed(),
                "Add patient wizard should be displayed.");
    }

    @Then("^Verify after accepting cancel patient wizard, patient should not be created when details filled till (Personal information|Medical information|Care team|Equipment information|Review)$")
    public void verifyPatientShouldNotBeCreatedWhenDetailsFilledTillPersonalInformation(String arg0) throws Exception {
        homePage = new HomePage(driver);
        log.info("Navigate to home page.");
        homePage.goToHomePage();
        Assert.assertTrue(homePage.clickOnAddPatient(), "Not able to click on Add Patient.");
        addPatientPage = new AddPatientPage(driver);
        Map<String, String> mrn = new LinkedHashMap<>();
        mrn.put("MRN", String.valueOf(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)));
        mrn.put("Patient_Mob", "3" + String.valueOf(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)));
        mrn.put("Patient_Landline", "4" + String.valueOf(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)));
        mrn.put("Family_Pho_Num", "1" + String.valueOf(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)));
        mrn.put("Point_number", String.valueOf(faker.number().digits(10)));
        mrn.put("Physicians",config.getClinicianName());
        mrn.put("Nurses_Paramedics",config.getNurseName());
        customUtils.getDefaultPatientDetails(mrn);

        if (arg0.equals("Personal information")) {
            log.info("Fill all Personal information.");
            addPatientPage.fillPersonalInformation(customUtils.getDefaultPatientDetails(), false);
        } else if (arg0.equals("Medical information")) {
            log.info("Fill all Personal information.");
            addPatientPage.fillPersonalInformation(customUtils.getDefaultPatientDetails(), false);
            addPatientPage.clickOnNext();
            log.info("Fill all Medical information.");
            addPatientPage.fillMedicalInformation(customUtils.getDefaultPatientDetails(), false);
        } else if (arg0.equals("Care team")) {
            log.info("Fill all Personal information.");
            addPatientPage.fillPersonalInformation(customUtils.getDefaultPatientDetails(), false);
            addPatientPage.clickOnNext();
            log.info("Fill all Medical information.");
            addPatientPage.fillMedicalInformation(customUtils.getDefaultPatientDetails(), false);
            addPatientPage.clickOnNext();
            log.info("Fill all Care team information.");
            addPatientPage.fillCareTeamInformation(customUtils.getDefaultPatientDetails(), false);
        } else if (arg0.equals("Equipment information")) {
            log.info("Fill all Personal information.");
            addPatientPage.fillPersonalInformation(customUtils.getDefaultPatientDetails(), false);
            addPatientPage.clickOnNext();
            log.info("Fill all Medical information.");
            addPatientPage.fillMedicalInformation(customUtils.getDefaultPatientDetails(), false);
            addPatientPage.clickOnNext();
            log.info("Fill all Care team information.");
            addPatientPage.fillCareTeamInformation(customUtils.getDefaultPatientDetails(), false);
            addPatientPage.clickOnNext();
            log.info("Fill all Equipment information.");
            addPatientPage.fillEquipmentInformation(customUtils.getDefaultPatientDetails(), false);
        }

        verifyPatientShouldNotBeCreatedWithFilledOnlyMandatoryDetails();
    }

    @Then("Verify after accepting cancel patient wizard, patient should not be created")
    public void verifyPatientShouldNotBeCreatedWithFilledOnlyMandatoryDetails() throws Exception {
        addPatientPage.cancelAddPatientWizard();
        String message = "Are you sure you want to leave this page? All unsaved data will be lost.";

        log.info("Verify cancel add patient wizard message.");
        addPatientPage.verifyAddPatientCancelPopUp(message);

        String mrn = customUtils.getDefaultPatientDetails().get("MRN");
        if (homePage.isAnyPatientAdded()) {
            log.info("Search patient with MRN number [" + mrn + "].");
            homePage.searchPatient(mrn);

            log.info("Verify searched patient displayed.");
            Assert.assertTrue(homePage.isPatientPresent(mrn),
                    "Patient should not be created with MRN ["+mrn+"]");
        } else {
            log.info("No patients added yet!");
            Assert.assertTrue(homePage._isTextContainsPresent("No patients added yet!"));
        }
    }

    @And("Open Add Patient wizard")
    public void openAddPatientWizard() throws Exception {
        homePage = new HomePage(driver);

        log.info("Navigate to home page.");
        homePage.goToHomePage();

        log.info("Verify Add patient button.");
        Assert.assertTrue(homePage.clickOnAddPatient(), "Not able to click on Add Patient.");
    }

    @Then("^Verify new patient tutorial (after|before) login$")
    public void verifyNewPatientTutorialAfterLogin(String arg0) throws Exception {
        String start = customUtils.getDefaultPatientDetails().get("QR_Start");
        String end = customUtils.getDefaultPatientDetails().get("QR_End");
        boolean flag = arg0.equals("after");

        log.info("Verify patient creation page and QR.");
        Assert.assertTrue(addPatientPage.verifyPatientCreatedQRPage(start, end, flag), "Failed to verify patient created page and QR.");

        log.info("Click on View patient option.");
        addPatientPage.clickOnViewPatientDetails();

        log.info("Verify new patient tutorial.");
        Assert.assertTrue(addPatientPage.verifyNewPatientTutorial(customUtils.getDefaultPatientDetails().get("F_Name")),
                "Failed while verify new patient tutorial.");
    }

    @Then("Add a new patient")
    public void addANewPatient() throws Exception {
        homePage = new HomePage(driver);
        addPatientPage = new AddPatientPage(driver);

        //--TODO Change Here

        List<String> physician = List.of(config.getClinicianName());
                //Arrays.asList(customUtils.getDefaultPatientDetails().get("Physicians").split(","));
        List<String> nurse = List.of(config.getNurseName());
                //Arrays.asList(customUtils.getDefaultPatientDetails().get("Nurses_Paramedics").split(","));

        Map<String, String> values = new HashMap<>();
        values.put("MRN", "MR" + String.valueOf(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)));
        values.put("Patient_Mob", "3" + String.valueOf(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)));
        values.put("Patient_Landline", "4" + String.valueOf(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)));
        values.put("Family_Pho_Num", "1" + String.valueOf(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)));
        values.put("Point_number", String.valueOf(faker.number().digits(10)));
        values.put("Physicians",physician.get(0));
        values.put("Nurses_Paramedics",nurse.get(0));
        log.info("Try to create patient with details :" + customUtils.getDefaultPatientDetails(values));

        //Read Clinic Mandatory Fields
        log.info("Navigate to home page.");
        homePage.goToHomePage();

        log.info("Click on Add Patient.");
        Assert.assertTrue(homePage.clickOnAddPatient(), "Not able to click on Add Patient.");

        String[] patientOptions = clinicInfo.getAddPatientSteps().split(":");
        List<String> steps = new ArrayList<>();

        for (String option : patientOptions)
            if (option.contains("["))
                steps.add(option.substring(0, option.indexOf("[")).replaceAll("\\s", "").toLowerCase(Locale.ROOT));
            else
                steps.add(option.replaceAll("\\s", "").toLowerCase(Locale.ROOT));

        //For Personal Information page
        if (steps.contains("personalinformation")) {
            addPatientPage.fillPersonalInformation(customUtils.getDefaultPatientDetails(values), false);
            addPatientPage.clickOnNext();
        }

        //For Medical Information page
        if (steps.contains("medicalinformation")) {
            log.info("Click on next.");
            addPatientPage.clickOnNext();
        }

        //For Care team information page
        if (steps.contains("careteam")) {
            log.info("Verify Care team information page.");
            addPatientPage.verifyCareTeamInfoPageRequiredFields(physician, nurse);

            log.info("Click on next.");
            addPatientPage.clickOnNext();
        }

        //Equipment information page
        if (steps.contains("equipmentinformation")) {
            log.info("Fill Equipment information.");
            addPatientPage.fillEquipmentInformation(customUtils.getDefaultPatientDetails(), true);

            log.info("Click on next.");
            addPatientPage.clickOnNext();
        }

        log.info("Set QR Expire details.");
        customUtils.setQRExpireDetails(8);

        log.info("Save patient.");
        addPatientPage.savePatient();

        log.info("Update QR Expire details if required.");
        customUtils.updateQRExpireDetails(8);

        log.info("Set global MRN*");
        customUtils.setGlobalMNR(customUtils.getDefaultPatientDetails().get("MRN"));
        customUtils.setPatientDetails(customUtils.getDefaultPatientDetails().get("MRN"),
                customUtils.getDefaultPatientDetails().get("F_Name")+" "+customUtils.getDefaultPatientDetails().get("L_Name"));
        log.info("Set QR Hash details.");
        customUtils.setQrHashUrl(homePage.readQRUrl());

        log.info("Click on View patient option.");
        addPatientPage.clickOnViewPatientDetails();

        log.info("Verify new patient tutorial.");
        Assert.assertTrue(addPatientPage.verifyNewPatientTutorial(customUtils.getDefaultPatientDetails().get("MRN")),
                "Failed while verify new patient tutorial.");
    }


    private void verifyMRNErrorMessages(String fields) throws Exception {
        String value = fields.split(":")[0];
        List<String> listOfFields = Arrays.asList(value.substring(value.indexOf("[") + 1, value.indexOf("]")).split(","));
        String fieldValue;
        if (listOfFields.contains("MRN*") || listOfFields.contains("MRN"))
            fieldValue = "MRN";
        else
            fieldValue = "Reference ID";

        log.info("Verify Next button should be disable before entering all req fields.");
        Assert.assertFalse(addPatientPage.isNextEnable(), "Next button should be disable until " + fieldValue + "* field is empty.");

        log.info("Verify " + fieldValue + " required filed and formats.");
        String mrnDigit = faker.number().digits(4);
        log.info("Enter " + fieldValue + " number [" + mrnDigit + "].");
        addPatientPage.enterMRN(mrnDigit);

        log.info("Clear " + fieldValue + " field.");
        addPatientPage.clearMRN();

        mrnDigit = faker.number().digits(2);
        log.info("Enter " + fieldValue + " value as [" + mrnDigit + "]");
        addPatientPage.enterMRN(mrnDigit);

        log.info("Verify error message [" + fieldValue + " needs to be between 4-16 characters, please check again]");
        Assert.assertEquals(addPatientPage.getMRNInfo(),
                fieldValue + " needs to be between 4-16 characters, please check again"
                , "Verify error message [" + fieldValue + " needs to be between 4-16 characters, please check again] should be displayed.");

        log.info("Clear " + fieldValue + " field.");
        addPatientPage.clearMRN();

        mrnDigit = "------";
        log.info("Enter " + fieldValue + " value as [" + mrnDigit + "]");
        addPatientPage.enterMRN(mrnDigit);

        log.info("Verify error message [Only alphanumerics are allowed for this field]");
        Assert.assertEquals(addPatientPage.getMRNInfo(),
                "Only alphanumerics are allowed for this field",
                "Verify error message [Only alphanumerics are allowed for this field] should be displayed.");

        log.info("Clear " + fieldValue + " field.");
        addPatientPage.clearMRN();

        mrnDigit = "random";
        log.info("Enter " + fieldValue + " value as [" + mrnDigit + "]");
        addPatientPage.enterMRN(mrnDigit);

        log.info("Clear " + fieldValue + " with backspace.");
        addPatientPage.clearMRNWithBackSpace();

        log.info("Verify error message [Please specify patient's " + fieldValue + "].");
        Assert.assertEquals(addPatientPage.getMRNReq().toLowerCase(Locale.ROOT),
                ("Please specify patient's " + fieldValue + "").toLowerCase(Locale.ROOT),
                "Verify error message [Please specify patient's " + fieldValue + "] message should be displayed.");

        log.info("Clear " + fieldValue + " field.");
        addPatientPage.clearMRN();

        mrnDigit = faker.number().digits(19);
        log.info("Enter " + fieldValue + " value as [" + mrnDigit + "]");
        addPatientPage.enterMRN(mrnDigit);

        log.info("Verify error message [" + fieldValue + " needs to be between 4-16 characters, please check again].");
        Assert.assertEquals(addPatientPage.getMRNInfo(),
                fieldValue + " needs to be between 4-16 characters, please check again",
                "Verify error message [" + fieldValue + " needs to be between 4-16 characters, please check again] message should be displayed.");

        log.info("Clear " + fieldValue + " field.");
        addPatientPage.clearMRN();
    }

    private void verifyOtherRequiredFieldsOnPersonalInfoPage(String mandatoryFields,Map<String,String> values) throws Exception {
        String value = mandatoryFields.split(":")[0];
        List<String> listOfFields = new ArrayList<>();
        String[] dataSet = value.substring(value.indexOf("[") + 1, value.indexOf("]")).split(",");

        for (String data : dataSet)
            listOfFields.add(data.replaceAll("\\s+", "").toLowerCase(Locale.ROOT));

        if (listOfFields.contains("firstname*"))
            addPatientPage.enterFirstName(values.get("F_Name"));
        if (listOfFields.contains("lastname*"))
            addPatientPage.enterLastName(values.get("L_Name"));
        if (listOfFields.contains("gender*"))
            addPatientPage.enterGender(values.get("Gender"));
        if (listOfFields.contains("dob*"))
            addPatientPage.enterDOB(values.get("DOB"));
        if (listOfFields.contains("addressline1*"))
            addPatientPage.enterAddressline1(values.get("Address_line_1"));
        if (listOfFields.contains("zipcode*"))
            addPatientPage.enterZip(values.get("Zip_code"));
        if (listOfFields.contains("city*"))
            addPatientPage.enterCity(values.get("City"));
        if (listOfFields.contains("state*"))
            addPatientPage.enterState(values.get("State"));
        if (listOfFields.contains("patient'spreferredlanguage"))
            addPatientPage.enterLang(values.get("Preferred_Lang"));
    }

}
