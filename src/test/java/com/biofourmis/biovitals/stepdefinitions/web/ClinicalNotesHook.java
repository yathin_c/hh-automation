package com.biofourmis.biovitals.stepdefinitions.web;

import com.biofourmis.biovitals.configs.CustomConfig;
import com.biofourmis.biovitals.constants.configuration.TestConfig;
import com.biofourmis.biovitals.pagefactory.web.ClinicalNotesPage;
import com.biofourmis.biovitals.utilities.CustomUtils;
import com.github.javafaker.Faker;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.testng.Assert;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.TextStyle;
import java.util.*;

@Log4j
public class ClinicalNotesHook {
    @Autowired
    @Qualifier(value = "web")
    private WebDriver driver;
    @Autowired
    private CustomConfig customConfig;
    @Autowired
    private TestConfig testConfig;
    @Autowired
    private Faker faker;

    private ClinicalNotesPage clinicalNotesPage;
    private final CustomUtils customUtils = new CustomUtils(null);

    @Then("Verify clinical notes page")
    public void verifyClinicalNotesPage() throws Exception {
        clinicalNotesPage = new ClinicalNotesPage(driver);

        log.info("Click on clinical notes.");
        clinicalNotesPage.clickOnClinicalNotes();

        log.info("Verify New clinical note button should be displayed.");
        Assert.assertTrue(clinicalNotesPage.verifyNewClinicalNotButtonWhenNoNote(),
                "New Clinical note button should be visible.");

        List<String> stringOnScreen = Arrays.asList(
                "No clinical notes yet",
                "You can use this space to keep notes about patient's condition or update.");
        for (String value : stringOnScreen) {
            log.info("Verify [" + value + "] should be displayed.");
            Assert.assertTrue(clinicalNotesPage.verifyNewClinicalNoteText(value),
                    "[" + value + "] should be visible.");
        }
    }

    @And("Verify empty fields error message")
    public void addNewNoteAndVerifyEmptyFieldsErrorMessage() throws Exception {
        log.info("Click on add new clinical note.");
        clinicalNotesPage.clickOnAddNewNote();

        log.info("Verify new clinical note heading.");
        Assert.assertTrue(clinicalNotesPage.verifyNewClinicalNoteHeading(), "Notes heading [New clinical note] should be found.");

        log.info("Verify [Title] heading present.");
        Assert.assertTrue(clinicalNotesPage._isTextContainsPresent("Title"),
                "[Title] should be displayed.");

        log.info("Verify [Description] heading present.");
        Assert.assertTrue(clinicalNotesPage._isTextContainsPresent("Description"),
                "[Description] should be displayed.");

        log.info("Verify input field error message.");
        String titleMsg = "Please specify title for your note";
        String descMsg = "Please specify description for your note";
        Assert.assertTrue(clinicalNotesPage.verifyErrorMessage(titleMsg, descMsg), "Input fields error message not displayed.");

        log.info("Verify Create button disabled.");
        Assert.assertFalse(clinicalNotesPage.createButtonDisable(), "Create button should be disable while input fields are empty.");
    }

    private void createNewNote() throws Exception {
        log.info("Add new note.");
        Map<String, String> data = new LinkedHashMap<>(customUtils.getDefaultClinicalNotesDetails());

        Assert.assertFalse(clinicalNotesPage.isCreateBtnEnable(),
                "Create button should be disable.");

        clinicalNotesPage.addTitle(data.get("Title"));
        Assert.assertFalse(clinicalNotesPage.isCreateBtnEnable(),
                "Create button should be disable.");

        clinicalNotesPage.addDescription(data.get("Description"));
        Assert.assertTrue(clinicalNotesPage.isCreateBtnEnable(),
                "Create button should be enable.");

        clinicalNotesPage.clickOnCreateBtn();

        Assert.assertTrue(clinicalNotesPage.verifyToastMessage("Clinical note created successfully"),
                "Clinical note created successfully toast not displayed.");
    }

    private void verifyCreatedNote() throws Exception {
        log.info("Verify newly created note.");
        Map<String, String> details = customUtils.getDefaultClinicalNotesDetails();

        String title = details.get("Title");
        String description = details.get("Description");

        log.info("Search note [" + title + "].");
        clinicalNotesPage.searchForNote(title);

        Assert.assertTrue(clinicalNotesPage.verifyCreatedNote(title),
                "Note title [" + title + "] should be displayed.");

        Assert.assertTrue(clinicalNotesPage.verifyCreatedNote(description),
                "Note description [" + description + "] should be displayed.");

        Assert.assertTrue(clinicalNotesPage.verifyCareGiver(testConfig.getClinicianName()),
                "Clinician name should be displayed as [" + testConfig.getClinicianName() + "].");
    }

    private void clickOnAddNewNote() throws Exception {
        clinicalNotesPage = new ClinicalNotesPage(driver);

        log.info("Click on Clinical notes.");
        clinicalNotesPage.clickOnClinicalNotes();

        log.info("Click on Add new note.");
        clinicalNotesPage.clickOnAddNewNote();
    }

    private void searchForNoteAndUpdateTheTitle(String arg0) throws Exception {
        String title = customUtils.getDefaultClinicalNotesDetails().get("Title");

        log.info("Search note [" + title + "].");
        clinicalNotesPage.searchForNote(title);

        log.info("Click on Edit icon.");
        clinicalNotesPage.clickOnEditIcon();

        Map<String, String> updateNote = new LinkedHashMap<>();
        if (arg0.equals("title")) {
            updateNote.put("Title", faker.lordOfTheRings().character().toLowerCase(Locale.ROOT) + LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
            updateNote.put("Description", customUtils.getDefaultClinicalNotesDetails().get("Description"));
            log.info("Update note  title as [" + updateNote.get("Title") + "].");
            clinicalNotesPage.updateNoteTitle(customUtils.getDefaultClinicalNotesDetails(updateNote));
        } else {
            updateNote.put("Title", customUtils.getDefaultClinicalNotesDetails().get("Title"));
            updateNote.put("Description", faker.aquaTeenHungerForce().character().toLowerCase(Locale.ROOT) + LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
            log.info("Update note  description as [" + updateNote.get("Description") + "].");
            clinicalNotesPage.updateNoteDescription(customUtils.getDefaultClinicalNotesDetails(updateNote));
        }
    }

    private void searchForNoteAndUpdate() throws Exception {
        String title = customUtils.getDefaultClinicalNotesDetails().get("Title");

        log.info("Search note [" + title + "].");
        clinicalNotesPage.searchForNote(title);

        Map<String, String> updateNote = new LinkedHashMap<>();
        updateNote.put("Title", faker.lordOfTheRings().character().toLowerCase(Locale.ROOT) + LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
        updateNote.put("Description", faker.aquaTeenHungerForce().character().toLowerCase(Locale.ROOT) + LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));

        log.info("Click on Edit icon.");
        clinicalNotesPage.clickOnEditIcon();

        log.info("Update note  title as [" + updateNote.get("Title") + "].");
        clinicalNotesPage.updateNoteTitle(customUtils.getDefaultClinicalNotesDetails(updateNote));

        log.info("Update note  description as [" + updateNote.get("Description") + "].");
        clinicalNotesPage.updateNoteDescription(customUtils.getDefaultClinicalNotesDetails(updateNote));
    }

    private void verifyUpdatedNote() throws Exception {
        log.info("Verify updated created note.");
        Map<String, String> details = customUtils.getDefaultClinicalNotesDetails();

        String title = details.get("Title");
        String description = details.get("Description");

        log.info("Search note [" + title + "].");
        clinicalNotesPage.searchForNote(title);

        Assert.assertTrue(clinicalNotesPage.verifyCreatedNote(title),
                "Note title [" + title + "] should be displayed.");

        Assert.assertTrue(clinicalNotesPage.verifyCreatedNote(description),
                "Note description [" + description + "] should be displayed.");
    }

    public void searchForNote() throws Exception {
        Map<String, String> details = customUtils.getDefaultClinicalNotesDetails();

        String title = details.get("Title");
        log.info("Search note [" + title + "].");
        clinicalNotesPage.searchForNote(title);
    }

    private void verifyConfirmationPopUpAndDecline(String arg0) throws Exception {

        log.info("Verify newly created note.");
        Map<String, String> details = customUtils.getDefaultClinicalNotesDetails();

        String title = details.get("Title");

        log.info("Click on delete pop-up");
        clinicalNotesPage.verifyDeletePopUP();

        if (arg0.equals("accept")) {
            clinicalNotesPage.performCancelPopUp(true);
            clinicalNotesPage.searchForNote(title);

            Assert.assertFalse(clinicalNotesPage.verifyCreatedNote(title),
                    "Note title [" + title + "] should not be displayed.");
        } else {
            clinicalNotesPage.performCancelPopUp(false);
            clinicalNotesPage.searchForNote(title);

            Assert.assertTrue(clinicalNotesPage.verifyCreatedNote(title),
                    "Note title [" + title + "] should be displayed.");
        }
    }

    @Then("Verify update a note")
    public void verifyUpdateANote() throws Exception {
        log.info("Search for note.");
        searchForNoteAndUpdateTheTitle("title");

        log.info("Click on save.");
        clinicalNotesPage.clickOnSave();

        log.info("Verify updated note details.");
        verifyUpdatedNote();

        log.info("Search for note.");
        searchForNoteAndUpdateTheTitle("description");

        log.info("Click on save.");
        clinicalNotesPage.clickOnSave();

        log.info("Update note details.");
        verifyUpdatedNote();

        log.info("Search and update note details.");
        searchForNoteAndUpdate();

        log.info("Click on save.");
        clinicalNotesPage.clickOnSave();

        log.info("Verify updated note details.");
        verifyUpdatedNote();
    }

    @Then("Verify Delete a note")
    public void deleteANote() throws Exception {
        log.info("Search for note.");
        searchForNote();

        log.info("Delete note.");
        clinicalNotesPage.deleteNote();

        log.info("Decline delete pop-up.");
        verifyConfirmationPopUpAndDecline("decline");

        log.info("Search for note.");
        searchForNote();

        log.info("Delete note.");
        clinicalNotesPage.deleteNote();

        log.info("Accept delete pop-up.");
        verifyConfirmationPopUpAndDecline("accept");

        log.info("Verify note deleted toast message.");
        clinicalNotesPage.verifyToastMessage("Clinical note deleted successfully");
    }

    @Then("Verify Add a note")
    public void verifyAddANote() throws Exception {
        clickOnAddNewNote();
        createNewNote();
        verifyCreatedNote();
    }

    @Then("Verify Search filter input field")
    public void verifySearchFilterInputField() throws Exception {
        clinicalNotesPage = new ClinicalNotesPage(driver);

        log.info("Get Clinical note details.");
        Map<String, String> data = customUtils.getDefaultClinicalNotesDetails();

        Assert.assertTrue(clinicalNotesPage.verifySearchInputOptions(),
                "Search input field should be visible.");

        String title = data.get("Title");
        log.info("Enter values [" + title + "] in search field.");
        clinicalNotesPage.inputValueInSearchField(title);

        Assert.assertTrue(clinicalNotesPage.verifySearchedNoteDisplayed(title),
                "Note with title [" + title + "] should be displayed.");

        String randomValue = faker.random().hex() + LocalDateTime.now().toEpochSecond(ZoneOffset.UTC);

        log.info("Enter values [" + randomValue + "] in search field.");
        clinicalNotesPage.inputValueInSearchField(randomValue);

        Assert.assertTrue(clinicalNotesPage.verifyCrossIconAfterEnterValueInField(),
                "Cross icon should be displayed in search filter to clear input value.");

        Assert.assertFalse(clinicalNotesPage.verifySearchedNoteDisplayed(randomValue),
                "Note with random values " + randomValue + " should not be displayed.");

        Assert.assertTrue(clinicalNotesPage.verifyRandomSearchmessage("No clinical notes found"),
                "No clinical notes found message should be displayed.");

        Assert.assertTrue(clinicalNotesPage.verifyRandomSearchmessage("Try to choose different date and filter combination."),
                "Try to choose different date and filter combination. should be displayed.");

        log.info("Verify click on cross button.");
        clinicalNotesPage.clickOnCrossBtn();

        Assert.assertTrue(clinicalNotesPage.verifySearchedNoteDisplayed(data.get("Title")),
                "After clicking on cross button search filter value should be reset." +
                        " And previous created notes should be displayed.");
    }

    @And("Verify Calendar filter option")
    public void verifyCalendarFilterOption() throws Exception {
        log.info("Click on Calendar filter.");
        clinicalNotesPage.clickOnCalendarFilter();

        Assert.assertTrue(clinicalNotesPage.isOptionDisplayed("Current day"),
                "Current day option should be displayed.");

        Assert.assertTrue(clinicalNotesPage.isOptionDisplayed("Previous day"),
                "Previous day option should be displayed.");

        Assert.assertTrue(clinicalNotesPage.isOptionDisplayed("Last 7 days"),
                "CLast 7 days option should be displayed.");

        Assert.assertTrue(clinicalNotesPage.isOptionDisplayed("Last 2 weeks"),
                "Last 2 weeks option should be displayed.");

        String calenderMonth = LocalDateTime.now().getMonth().getDisplayName(TextStyle.FULL, Locale.getDefault()) + "  " +
                LocalDateTime.now().getYear();
        Assert.assertTrue(clinicalNotesPage.isOptionDisplayed(calenderMonth),
                "[" + calenderMonth + "] month and year value should be displayed on the top of calendar.");

        log.info("Get Clinical notes.");
        Map<String, String> details = customUtils.getDefaultClinicalNotesDetails();

        log.info("Click on Current day option.");
        clinicalNotesPage.clickOnOption("Current day");

        log.info("Wait for it.");
        clinicalNotesPage._sleep(3);
        String title = details.get("Title");
        Assert.assertTrue(clinicalNotesPage.isNoteDisplayed(title),
                "Note with title [" + title + "] should be displayed in current day search.");

        log.info("Click on Calendar filter.");
        clinicalNotesPage.clickOnCalendarFilter();

        log.info("Click on Previous day option.");
        clinicalNotesPage.clickOnOption("Previous day");

        log.info("Wait for it.");
        clinicalNotesPage._sleep(3);
        Assert.assertFalse(clinicalNotesPage.isNoteDisplayed(title),
                "Note with title [" + title + "] should not be displayed under past day search..");

        Assert.assertTrue(clinicalNotesPage.isOptionDisplayed("No clinical notes found"),
                "No clinical notes found message should be displayed.");

        log.info("Click on Calendar filter.");
        clinicalNotesPage.clickOnCalendarFilter();

        log.info("Click on Last 7 days option.");
        clinicalNotesPage.clickOnOption("Last 7 days");

        log.info("Wait for it.");
        clinicalNotesPage._sleep(3);
        Assert.assertTrue(clinicalNotesPage.isNoteDisplayed(title),
                "Note with title [" + title + "] should be displayed in  Last 7 days search.");

        log.info("Click on Calendar filter.");
        clinicalNotesPage.clickOnCalendarFilter();

        log.info("Click on Last 2 weeks option.");
        clinicalNotesPage.clickOnOption("Last 2 weeks");

        log.info("Wait for it.");
        clinicalNotesPage._sleep(3);
        Assert.assertTrue(clinicalNotesPage.isNoteDisplayed(title),
                "Note with title [" + title + "] should be displayed in  Last 2 weeks search.");

    }

    @And("Verify Caregiver filter option")
    public void verifyCaregiverFilterOption() throws Exception {
        log.info("Click on clear all filter option.");
        clinicalNotesPage.clickOnClearAll();

        log.info("Click on clinician filter.");
        clinicalNotesPage.clickOnFilter();

        Assert.assertTrue(clinicalNotesPage.isOptionDisplayed("Select care team members"),
                "[Select care team members] message should be displayed under filter search.");

        log.info("Click on inner filter search.");
        clinicalNotesPage.clickOnFilterSearch();

        String clinician = testConfig.getClinicianName();

        log.info("Search clinician [" + clinician + "]");
        clinicalNotesPage.searchClinician(clinician);

        log.info("Select clinician [" + clinician + "]");
        clinicalNotesPage.selectClinician(clinician);

        log.info("Click on Apply filter.");
        clinicalNotesPage.clickOnApplyFilter();

        Assert.assertTrue(clinicalNotesPage.isClinicianNameDisplayed(clinician),
                "Clinician [" + clinician + "] should be displayed in notes details.");

        log.info("Click on clinician filter.");
        clinicalNotesPage.clickOnFilter();

        Assert.assertTrue(clinicalNotesPage.isResetFilterDisplayed(),
                "Reset filter button should be displayed.");

        log.info("Click on Reset filter.");
        clinicalNotesPage.clickOnResetFilter();

        log.info("Click on clinician filter.");
        clinicalNotesPage.clickOnFilter();

        log.info("Click on filter search.");
        clinicalNotesPage.clickOnFilterSearch();

        clinician = testConfig.getNurseName();

        log.info("Search clinician [" + clinician + "]");
        clinicalNotesPage.searchClinician(clinician);

        log.info("Select clinician [" + clinician + "]");
        clinicalNotesPage.selectClinician(clinician);

        log.info("Click on Apply filter.");
        clinicalNotesPage.clickOnApplyFilter();

        Assert.assertTrue(clinicalNotesPage.isOptionDisplayed("No clinical notes found"),
                "No clinical notes found message should be displayed because no note created yet with clinician [" + clinician + "].");

        log.info("Click on clinician filter.");
        clinicalNotesPage.clickOnFilter();

        Assert.assertTrue(clinicalNotesPage.isResetFilterDisplayed(),
                "Reset filter button should be displayed.");

        log.info("Click on reset filter button.");
        clinicalNotesPage.clickOnResetFilter();

        Assert.assertFalse(clinicalNotesPage.isOptionDisplayed("No clinical notes found"),
                "Previously created notes should be displayed.");
    }

    @And("Verify Calendar and Caregiver filter options together and Clear all")
    public void verifyCalendarAndCaregiverFilterOptionsTogether() throws Exception {
        log.info("Click on calendar filter.");
        clinicalNotesPage.clickOnCalendarFilter();

        log.info("Get note details.");
        Map<String, String> details = customUtils.getDefaultClinicalNotesDetails();

        log.info("Click on Current day option.");
        clinicalNotesPage.clickOnOption("Current day");

        log.info("Select clinician filter.");
        clinicalNotesPage.clickOnFilter();

        log.info("Click on search under clinician filter.");
        clinicalNotesPage.clickOnFilterSearch();

        String clinician = testConfig.getClinicianName();

        log.info("Search clinician [" + clinician + "] under clinician filter search.");
        clinicalNotesPage.searchClinician(clinician);

        log.info("Select clinician [" + clinician + "] under clinician filter search.");
        clinicalNotesPage.selectClinician(clinician);

        log.info("Click on Apply filter.");
        clinicalNotesPage.clickOnApplyFilter();

        String title = details.get("Title");

        Assert.assertTrue(clinicalNotesPage.isNoteDisplayed(title),
                "Clinical note ["+title+"] should be displayed while filters selected are date as Current day and clinician as ["+clinician+"].");

        Assert.assertTrue(clinicalNotesPage.isNoteDisplayed(title),
                "Note [" + title + "] should be displayed.");

        log.info("Click on [Clear all] option.");
        clinicalNotesPage.clickOnClearAll();

        log.info("Click on calendar filter.");
        clinicalNotesPage.clickOnCalendarFilter();

        log.info("Click on Previous day option.");
        clinicalNotesPage.clickOnOption("Previous day");

        log.info("Click on clinician filter.");
        clinicalNotesPage.clickOnFilter();

        log.info("Click on search under clinician filter.");
        clinicalNotesPage.clickOnFilterSearch();

        log.info("Search clinician [" + clinician + "] under clinician filter search.");
        clinicalNotesPage.searchClinician(clinician);

        log.info("Select clinician [" + clinician + "] under clinician filter search.");
        clinicalNotesPage.selectClinician(clinician);

        log.info("Click on Apply filter.");
        clinicalNotesPage.clickOnApplyFilter();

        Assert.assertTrue(clinicalNotesPage.isOptionDisplayed("No clinical notes found"),
                "No clinical note should be displayed while filters selected are date as Previous day and clinician as ["+clinician+"].");

        log.info("Click on clear all.");
        clinicalNotesPage.clickOnClearAll();

        Assert.assertTrue(clinicalNotesPage.isNoteDisplayed(details.get("Title")),
                "Clinical notes list should be displayed after clear all filters.");

        log.info("Click on calendar filter.");
        clinicalNotesPage.clickOnCalendarFilter();

        log.info("Click on Current day filter.");
        clinicalNotesPage.clickOnOption("Current day");

        log.info("Click on clinician filter.");
        clinicalNotesPage.clickOnFilter();

        log.info("Click on search under clinician filter.");
        clinicalNotesPage.clickOnFilterSearch();

        clinician = testConfig.getNurseName();

        log.info("Search clinician [" + clinician + "] under clinician filter search.");
        clinicalNotesPage.searchClinician(clinician);

        log.info("Select clinician [" + clinician + "] under clinician filter search.");
        clinicalNotesPage.selectClinician(clinician);

        log.info("Click on Apply filter.");
        clinicalNotesPage.clickOnApplyFilter();

        Assert.assertTrue(clinicalNotesPage.isOptionDisplayed("No clinical notes found"),
                "No clinical note should be displayed while filters selected are date as current day and clinician as ["+clinician+"].");

        log.info("Click on clear all.");
        clinicalNotesPage.clickOnClearAll();

        Assert.assertTrue(clinicalNotesPage.isNoteDisplayed(details.get("Title")),
                "Clinical notes list should be displayed after clear all filters.");
    }

    @And("Verify edit and delete option not visible")
    public void verifyEditAndDeleteOptionNotVisible() throws Exception {
        log.info("Click on Clinical notes.");
        clinicalNotesPage.clickOnClinicalNotes();

        searchForNote();

        Assert.assertFalse(clinicalNotesPage.verifyEditIconDisplayed(),
                "Edit icon should not be displayed.");

        Assert.assertFalse(clinicalNotesPage.verifyDeleteIconDisplayed(),
                "Delete icon should not be displayed.");
    }
}
