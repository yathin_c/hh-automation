package com.biofourmis.biovitals.stepdefinitions.web;

import com.biofourmis.biovitals.constants.configuration.TestConfig;
import com.biofourmis.biovitals.pagefactory.web.HandOffPage;
import com.biofourmis.biovitals.pagefactory.web.HomePage;
import com.biofourmis.biovitals.pagefactory.web.MyPatientAllPatientPage;
import com.biofourmis.biovitals.utilities.CustomUtils;
import com.github.javafaker.Faker;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.testng.Assert;

import java.time.LocalDateTime;
import java.util.*;

@Log4j
public class HandOffHook {
    @Autowired
    @Qualifier(value = "web")
    private WebDriver driver;

    @Autowired
    private Faker faker;
    @Autowired
    private TestConfig testConfig;

    private CustomUtils customUtils = new CustomUtils(null);
    private HandOffPage handOff;
    private HomePage homePage;
    private String mrn;
    private Map<String, String> details = new HashMap<>();
    private Map<String, String> previousData = new HashMap<>();

    private void setHandOffDetails(String arg0) {
        if (details.isEmpty()) {
            details.put("severity", arg0);
            details.put("summary", faker.gameOfThrones().quote() + LocalDateTime.now());
            details.put("awareness", Arrays.asList("AOx3", "Ambulatory", "Altered", "Nonverbal at baseline").get(faker.random().nextInt(0, 3)));
            details.put("synthesis", faker.aquaTeenHungerForce().character() + LocalDateTime.now());
            details.put("customAwareness", faker.dune().character() + LocalDateTime.now());
        }
    }

    private void updateHandOffDetails(String arg0) {
        previousData.putAll(details);
        details.put("severity", arg0);
        details.put("summary", faker.backToTheFuture().character() + LocalDateTime.now());
        List<String> data = new ArrayList<>(Arrays.asList("AOx3", "Ambulatory", "Altered", "Nonverbal at baseline"));
        data.remove(details.get("awareness"));
        details.put("awareness", data.get(faker.random().nextInt(0, 2)));
        details.put("synthesis", faker.lordOfTheRings().character() + LocalDateTime.now());
        details.put("customAwareness", faker.aviation().aircraft() + LocalDateTime.now());
    }

    private void verifyAddedHandOff() throws Exception {
        log.info("Click on Three dots on patients row.");
        handOff.clickOnPatientOptions();
        log.info("Click on Hand off");
        handOff.clickOnOption("Hand off");
        log.info("Verify Hand Off container should be displayed.");
        Assert.assertTrue(handOff.verifyDetails(details), "Hand off container not displayed.");
    }

    @And("Verify add Hand Off with {string} Illness severity")
    public void verifyAddHandOffWithIllnessSeverity(String arg0) throws Exception {
        homePage = new HomePage(driver);
        handOff = new HandOffPage(driver);
        mrn = customUtils.getDefaultPatientDetails().get("MRN");
        log.info("Get MRN value [" + mrn + "].");
        log.info("Navigate to home.");
        homePage.goToHomePage();

        log.info("Search for patient.[" + mrn + "].");
        homePage.searchPatient(mrn);

        Assert.assertFalse(homePage.isPatientPresent(mrn),
                "Patient not found, MRN [" + mrn + "].");

        log.info("Click on Three dots on patients row.");
        handOff.clickOnPatientOptions();
        log.info("Click on Hand off");
        handOff.clickOnOption("Hand off");
        log.info("Close Hand off container.");
        handOff.closeHandOffWizard();

        log.info("Click on Three dots on patients row.");
        handOff.clickOnPatientOptions();
        log.info("Click on Hand off");
        handOff.clickOnOption("Hand off");

        setHandOffDetails(arg0);
        log.info("Enter hand off details. [" + details + "].");
        handOff.enterHandOffDetails(details, true);

        log.info("Verify Added Hand Off");
        verifyAddedHandOff();
    }

    @And("Verify edit Hand Off with {string} Illness severity")
    public void verifyEditHandOffWithIllnessSeverity(String arg0) throws Exception {
        log.info("Click on Three dots on patients row.");
        handOff.clickOnPatientOptions();

        log.info("Click on Hand off");
        handOff.clickOnOption("Hand off");

        this.details.put("severity", arg0);
        log.info("Update hand off details. [" + details + "].");
        handOff.enterHandOffDetails(details, false);

        log.info("Verify Added Hand Off");
        verifyAddedHandOff();
    }

    @And("Verify handoff container details and mandatory sections")
    public void verifyHandoffContainerDetailsAndMandatorySections() throws Exception {
        handOff = new HandOffPage(driver);

        log.info("Click on Three dots on patients row.");
        handOff.clickOnPatientOptions();

        log.info("Click on Hand off");
        handOff.clickOnOption("Hand off");

        Assert.assertFalse(handOff.isHandOffBtnEnable(),
                "Hand Off button should be disable until all details are not filled.");

        String name[] = customUtils.getPatientDetails().get("Name").split(" ");
        String fName = name[0].strip();
        String lName = name[1].strip();

        Assert.assertTrue(handOff.isOptionDisplayed(fName + "  " + lName),
                "Patient name should be displayed as [" + fName + "  " + lName + "].");

        Assert.assertTrue(handOff.isOptionDisplayed(customUtils.getPatientDetails().get("MRN")),
                "Reference Id/MRN should be displayed [" + customUtils.getPatientDetails().get("MRN") + "].");

        Assert.assertTrue(handOff.isOptionDisplayed("Illness severity"),
                "Illness severity header should be displayed.");

        Assert.assertTrue(handOff.isOptionDisplayed("Stable"),
                "Stable illness severity option should be displayed.");

        Assert.assertTrue(handOff.isOptionDisplayed("“Watcher”"),
                "“Watcher” illness severity option should be displayed.");

        Assert.assertTrue(handOff.isOptionDisplayed("Unstable"),
                "Unstable illness severity option should be displayed.");

        Assert.assertTrue(handOff.isOptionDisplayed("Patient summary"),
                "Patient summary header should be displayed.");

        Assert.assertTrue(handOff.isOptionDisplayed("Situational awareness & contingency planning"),
                "Situational awareness & contingency planning header should be displayed.");

        Assert.assertTrue(handOff.isOptionDisplayed("AOx3"),
                "AOx3 Situational awareness option should be displayed.");

        Assert.assertTrue(handOff.isOptionDisplayed("Ambulatory"),
                "Ambulatory Situational awareness option should be displayed.");

        Assert.assertTrue(handOff.isOptionDisplayed("Altered"),
                "Altered Situational awareness option should be displayed.");

        Assert.assertTrue(handOff.isOptionDisplayed("Nonverbal at baseline"),
                "Nonverbal at baseline Situational awareness option should be displayed.");

        Assert.assertTrue(handOff.isOptionDisplayed("Customize your own option"),
                "Customize your own option Situational awareness option should be displayed.");

        Assert.assertTrue(handOff.isOptionDisplayed("Synthesis"),
                "");

        Assert.assertTrue(handOff.isOptionDisplayed("Altered"),
                "Synthesis header should be displayed.");

        log.info("Set Hand off details.");
        setHandOffDetails("Stable");

        log.info("Select severity as " + details.get("severity"));
        handOff.selectSeverity(details.get("severity"));

        Assert.assertFalse(handOff.isHandOffBtnEnable(),
                "Hand Off button should be disable until all details are not filled.");

        log.info("Enter summary as " + details.get("summary"));
        handOff.enterSummary(details.get("summary"));

        Assert.assertFalse(handOff.isHandOffBtnEnable(),
                "Hand Off button should be disable until all details are not filled.");

        log.info("Select awareness as [" + details.get("awareness") + "].");
        handOff.selectAwareness(details.get("awareness"), false);

        Assert.assertFalse(handOff.isHandOffBtnEnable(),
                "Hand Off button should be disable until all details are not filled.");

        log.info("Enter notes as [" + details.get("synthesis") + "].");
        handOff.enterNotes(details.get("synthesis"));

        Assert.assertTrue(handOff.isHandOffBtnEnable(),
                "Hand Off button should be enable after all details are filled.");

        log.info("Close hand off container.");
        handOff.closeHandOffWizard();
    }

    @Then("Verify adding HandOff with {string} Illness severity")
    public void verifyAddingHandOffWithIllnessSeverity(String arg0) throws Exception {
        log.info("Click on Three dots on patients row.");
        handOff.clickOnPatientOptions();

        log.info("Click on Hand off");
        handOff.clickOnOption("Hand off");

        updateHandOffDetails(arg0);

        log.info("Select severity [" + details.get("severity") + "].");
        handOff.selectSeverity(details.get("severity"));

        log.info("Enter summary [" + details.get("summary") + "].");
        handOff.enterSummary(details.get("summary"));

        log.info("Select awareness [" + details.get("awareness") + "].");
        handOff.selectAwareness(details.get("awareness"), false);

        log.info("Enter synthesis [" + details.get("synthesis") + "].");
        handOff.enterNotes(details.get("synthesis"));

        log.info("Click on handOff");
        handOff.clickOnHandOff();

        Assert.assertTrue(handOff.isPoPOptionDisplayed("Hand off patient?"),
                "Hand off patient? option should be displayed as pop-up.");

        Assert.assertTrue(handOff.isPoPOptionDisplayed("Please make sure all details are accurate."),
                "Please make sure all details are accurate. should be displayed on pop-up.");

        Assert.assertTrue(handOff.isPoPOptionDisplayed("Cancel"),
                "Cancel option should be displayed.");

        Assert.assertTrue(handOff.isPoPOptionDisplayed("Proceed"),
                "Proceed option should be displayed.");

        log.info("Click on cancel option.");
        handOff.clickOnCancel();

        Assert.assertTrue(handOff.isHandOffBtnEnable(),
                "After canceling hand-off pop. All all values should not be changes and Hand off button should be enable.");

        log.info("Click on handOff");
        handOff.clickOnHandOff();

        log.info("Click on proceed");
        handOff.clickOnProceed();

        String name[] = customUtils.getPatientDetails().get("Name").split(" ");
        String fName = name[0].strip();
        String lName = name[1].strip();

        Assert.assertTrue(handOff.isPoPOptionDisplayed(fName + " " + lName + " is handed off successfully"),
                "Toast message not displayed. [" + fName + " " + lName + " is handed off successfully].");
    }

    @And("Verify HandOff details after successfully HandOff patient")
    public void verifyHandOffDetailsAfterSuccessfullyHandOffPatient() throws Exception {
        handOff = new HandOffPage(driver);

        log.info("Click on HandOff icon.");
        handOff.clickOnHandOffIcon();

        String severity = details.get("severity");
        String cssStyle = handOff.getSeverityStyle(severity);

        log.info("Get severity color code.");
        String[] rgb = cssStyle.substring(cssStyle.indexOf("(") + 1, cssStyle.indexOf(")")).split(",");

        Assert.assertTrue(handOff.isSeveritySelected(severity),
                severity + " option should be selected.");

        Assert.assertTrue(isSeveritySelected(rgb, severity),
                severity + " should be highlighted.");

        Assert.assertTrue(handOff.isAwarenessSelected(details.get("awareness")),
                details.get("awareness") + " awareness should be selected.");

        Assert.assertEquals(handOff.getSummary(), details.get("summary"),
                "Actual summary [" + handOff.getSummary() + "] and expected is [" + details.get("summary") + "].");

        Assert.assertEquals(handOff.getNote(), details.get("synthesis"),
                "Actual note [" + handOff.getNote() + "] and expected is [" + details.get("synthesis") + "].");

        Assert.assertTrue(handOff.isPriorvisitEnable(),
                "Prior visit option should be enable.");

        Assert.assertFalse(handOff.isPriorvisitSelected(),
                "Prior visit option should not be selected.");

        Assert.assertTrue(handOff.isPoPOptionDisplayed("No changes from prior visit"),
                "No changes from prior visit text should be displayed.");

        Assert.assertEquals(handOff.verifyContactCR(), "Contact " + testConfig.getClinicianName() + " for questions",
                "Actual [" + handOff.verifyContactCR() + "] and expected is [Contact " + testConfig.getClinicianName() + " for questions].");
        // Last updated:  Today at 1:21 PM TODO
    }

    private boolean isSeveritySelected(String[] rgb, String severity) {
        int red = 0, green = 0, blue = 0;
        if (severity.equalsIgnoreCase("Stable")) {
            red = 37;
            green = 179;
            blue = 108;
        } else if (severity.equalsIgnoreCase("Watcher")) {
            red = 255;
            green = 133;
            blue = 3;
        } else {
            red = 230;
            green = 48;
            blue = 51;
        }

        try {
            if (red >= Integer.parseInt(rgb[0].strip()) - 5 || red <= Integer.parseInt(rgb[0].strip()) + 5)
                if (green >= Integer.parseInt(rgb[1].strip()) - 5 || green <= Integer.parseInt(rgb[1].strip()) + 5)
                    return blue >= Integer.parseInt(rgb[2].strip()) - 5 || blue <= Integer.parseInt(rgb[2].strip()) + 5;
        } catch (NumberFormatException nmf) {
            return false;
        }
        return false;
    }

    @Then("Remove this patient from my patients")
    public void removeThisPatientFromMyPatients() throws Exception {
        log.info("Click on patient options.");
        handOff.clickOnPatientOptions();

        log.info("Click on remove from my patients.");
        handOff.clickOnOption("Remove from My patients");

        MyPatientAllPatientPage myPatientAllPatientPage = new MyPatientAllPatientPage(driver);

        log.info("Remove patient.");
        myPatientAllPatientPage.clickOnRemove();
    }

    @Then("Update HandOff details with {string} Illness severity")
    public void updateHandOffDetailsWithIllnessSeverity(String arg0) throws Exception {
        handOff = new HandOffPage(driver);

        log.info("Click on Handoff icon.");
        handOff.clickOnHandOffIcon();
        updateHandOffDetails(arg0);

        log.info("Select severity [" + details.get("severity") + "].");
        handOff.selectSeverity(details.get("severity"));

        log.info("Enter summary [" + details.get("summary") + "].");
        handOff.enterSummary(details.get("summary"));

        log.info("Select awareness [" + details.get("awareness") + "].");
        handOff.selectAwareness(details.get("awareness"), false);

        log.info("Enter synthesis [" + details.get("synthesis") + "].");
        handOff.enterNotes(details.get("synthesis"));

    }

    @And("^Verify (decline|accept) reset No change priory visit$")
    public void verifyDeclineResetNoChangePrioryVisit(String arg0) throws Exception {
        log.info("Click on No priori changes.");
        handOff.clickOnNoPrioriChanges();

        Assert.assertTrue(handOff.isPoPOptionDisplayed("Override the changes?"),
                "Override the changes? text should be displayed on pop-up.");

        Assert.assertTrue(handOff.isPoPOptionDisplayed("Details from prior visit will replace the new changes that you have made"),
                "Details from prior visit will replace the new changes that you have made text should be displayed on pop-up.");

        Assert.assertTrue(handOff.isPoPOptionDisplayed("Cancel"),
                "Cancel button should be displayed.");

        Assert.assertTrue(handOff.isPoPOptionDisplayed("Proceed"),
                "Proceed button should be displayed.");

        if (arg0.equalsIgnoreCase("decline")) {
            log.info("Click on cancel.");
            handOff.clickOnCancel();
            String severity = details.get("severity");
            String cssStyle = handOff.getSeverityStyle(severity);

            log.info("Get color code for severity.");
            String[] rgb = cssStyle.substring(cssStyle.indexOf("(") + 1, cssStyle.indexOf(")")).split(",");

            Assert.assertTrue(handOff.isSeveritySelected(severity),
                    severity + " should be selected.");

            Assert.assertTrue(isSeveritySelected(rgb, severity),
                    severity + " should be highlighted.");

            Assert.assertTrue(handOff.isAwarenessSelected(details.get("awareness")),
                    details.get("awareness") + " should be selected.");

            Assert.assertEquals(handOff.getSummary(), details.get("summary"),
                    "Actual summary [" + handOff.getSummary() + "] and expected is [" + details.get("summary") + "].");

            Assert.assertEquals(handOff.getNote(), details.get("synthesis"),
                    "Actual notes [" + handOff.getNote() + "] and expected is [" + details.get("synthesis") + "].");

            Assert.assertTrue(handOff.isPriorvisitEnable(),
                    "Prior visit option should be enable.");

            Assert.assertFalse(handOff.isPriorvisitSelected(),
                    "Prior visit option should not be selected.");

            Assert.assertTrue(handOff.isPoPOptionDisplayed("No changes from prior visit"),
                    "No changes from prior visit text should be displayed.");

            Assert.assertEquals(handOff.verifyContactCR(), "Contact " + testConfig.getClinicianName() + " for questions",
                    "Actual [" + handOff.verifyContactCR() + "] and expected is [Contact " + testConfig.getClinicianName() + " for questions].");
            // Last updated:  Today at 1:21 PM TODO
        } else {
            log.info("Click on proceed.");
            handOff.clickOnProceed();
            String severity = previousData.get("severity");
            String cssStyle = handOff.getSeverityStyle(severity);

            log.info("Get color code for severity.");
            String[] rgb = cssStyle.substring(cssStyle.indexOf("(") + 1, cssStyle.indexOf(")")).split(",");

            Assert.assertTrue(handOff.isSeveritySelected(severity),
                    severity + " should be selected.");

            Assert.assertTrue(isSeveritySelected(rgb, severity),
                    severity + " should be highlighted.");

            Assert.assertTrue(handOff.isAwarenessSelected(previousData.get("awareness")),
                    previousData.get("awareness") + " should be selected.");

            Assert.assertEquals(handOff.getSummary(), previousData.get("summary"),
                    "Actual summary [" + handOff.getSummary() + "] and expected is [" + previousData.get("summary") + "].");

            Assert.assertEquals(handOff.getNote(), previousData.get("synthesis"),
                    "Actual notes [" + handOff.getNote() + "] and expected is [" + previousData.get("synthesis") + "].");

            Assert.assertTrue(handOff.isPriorvisitEnable(),
                    "Prior visit option should be enable.");

            Assert.assertTrue(handOff.isPriorvisitSelected(),
                    "Prior visit option should not be selected.");

            Assert.assertTrue(handOff.isPoPOptionDisplayed("No changes from prior visit"),
                    "No changes from prior visit text should be displayed.");

            Assert.assertEquals(handOff.verifyContactCR(), "Contact " + testConfig.getClinicianName() + " for questions",
                    "Actual [" + handOff.verifyContactCR() + "] and expected is [Contact " + testConfig.getClinicianName() + " for questions].");
            // Last updated:  Today at 1:21 PM TODO
        }

        log.info("Click on Handoff.");
        handOff.clickOnHandOff();
        log.info("Click on Proceed.");
        handOff.clickOnProceed();
    }

    @Then("Verify Nothing should change after decline reset")
    public void verifyNothingShouldChangeAfterDeclineReset() throws Exception {
        handOff = new HandOffPage(driver);

        log.info("Click on Handoff icon.");
        handOff.clickOnHandOffIcon();

        String severity = previousData.get("severity");
        String cssStyle = handOff.getSeverityStyle(severity);

        log.info("Get color code for severity.");
        String[] rgb = cssStyle.substring(cssStyle.indexOf("(") + 1, cssStyle.indexOf(")")).split(",");

        Assert.assertTrue(handOff.isSeveritySelected(severity),
                severity + " should be selected.");

        Assert.assertTrue(isSeveritySelected(rgb, severity),
                severity + " should be highlighted.");

        Assert.assertTrue(handOff.isAwarenessSelected(previousData.get("awareness")),
                previousData.get("awareness") + " should be selected.");

        Assert.assertEquals(handOff.getSummary(), previousData.get("summary"),
                "Actual summary [" + handOff.getSummary() + "] and expected is [" + previousData.get("summary") + "].");

        Assert.assertEquals(handOff.getNote(), previousData.get("synthesis"),
                "Actual notes [" + handOff.getNote() + "] and expected is [" + previousData.get("synthesis") + "].");

        Assert.assertTrue(handOff.isPriorvisitEnable(),
                "Prior visit option should be enable.");

        Assert.assertFalse(handOff.isPriorvisitSelected(),
                "Prior visit option should not be selected.");

        Assert.assertTrue(handOff.isPoPOptionDisplayed("No changes from prior visit"),
                "No changes from prior visit text should be displayed.");

        Assert.assertEquals(handOff.verifyContactCR(), "Contact " + testConfig.getClinicianName() + " for questions",
                "Actual [" + handOff.verifyContactCR() + "] and expected is [Contact " + testConfig.getClinicianName() + " for questions].");

        // Last updated:  Today at 1:21 PM TODO
        log.info("Click on close.");
        handOff.clickOnClose();
    }

    @And("Navigate to removed patient under all patients and Verify HandOff details should be read only mode")
    public void navigateToRemovedPatientUnderAllPatientsAndVerifyHandOffDetailsShouldBeReadOnlyMode() throws Exception {
        homePage = new HomePage(driver);
        log.info("Click on Hamburger.");
        homePage.clickOnHamburger();

        log.info("Navigate to All patients");
        homePage.navigateTo("All patients");

        String arg0 = CustomUtils.mnr;
        MyPatientAllPatientPage myPatientAllPatientPage = new MyPatientAllPatientPage(driver);

        log.info("Search patient " + arg0);
        myPatientAllPatientPage.searchInput(arg0);

        log.info("Click on patient id.");
        myPatientAllPatientPage.clickPatientMRN(arg0);

        log.info("Click on Handoff icon.");
        handOff.clickOnHandOffIcon();

        try {
            handOff.selectSeverity(details.get("severity"));
            Assert.fail("Handoff options should not be editable for un-assigned clinician.");
        } catch (ElementClickInterceptedException e) {
        }

        try {
            handOff.enterSummary(details.get("summary"));
            Assert.fail("Handoff options should not be editable for un-assigned clinician.");
        } catch (InvalidElementStateException e) {
        }

        try {
            handOff.selectAwareness(details.get("awareness"), false);
            Assert.fail("Handoff options should not be editable for un-assigned clinician.");
        } catch (ElementClickInterceptedException e) {
        }

        try {
            handOff.enterNotes(details.get("synthesis"));
            Assert.fail("Handoff options should not be editable for un-assigned clinician.");
        } catch (InvalidElementStateException e) {
        }

        Assert.assertFalse(handOff.isPriorvisitEnable(),
                "Handoff options should not be editable for un-assigned clinician.");

        Assert.assertFalse(handOff.isHandOffBtnEnable(),
                "Handoff options should not be editable for un-assigned clinician.");

        log.info("Click on close.");
        handOff.clickOnClose();
    }

    @And("Verify all Illness severity are selectable and highlighted after selected")
    public void verifyAllIllnessSeverityAreSelectableAndHighlightedAfterSelected() throws Exception {
        handOff = new HandOffPage(driver);

        log.info("Click on handoff icon.");
        handOff.clickOnHandOffIcon();

        log.info("Select severity.");
        handOff.selectSeverity("Stable");
        String cssStyle = handOff.getSeverityStyle("Stable");

        log.info("Get color code for severity.");
        String[] rgb = cssStyle.substring(cssStyle.indexOf("(") + 1, cssStyle.indexOf(")")).split(",");

        Assert.assertTrue(isSeveritySelected(rgb, "Stable"),
                "Stable severity should be highlighted.");

        log.info("Select severity as Watcher.");
        handOff.selectSeverity("Watcher");

        cssStyle = handOff.getSeverityStyle("Watcher");

        log.info("Get color code for severity.");
        rgb = cssStyle.substring(cssStyle.indexOf("(") + 1, cssStyle.indexOf(")")).split(",");

        Assert.assertTrue(isSeveritySelected(rgb, "Watcher"),
                "Watcher severity should be highlighted.");

        log.info("Select severity as Unstable.");
        handOff.selectSeverity("Unstable");

        cssStyle = handOff.getSeverityStyle("Unstable");

        log.info("Get color code for severity.");
        rgb = cssStyle.substring(cssStyle.indexOf("(") + 1, cssStyle.indexOf(")")).split(",");

        Assert.assertTrue(isSeveritySelected(rgb, "Unstable"),
                "Unstable severity should be highlighted.");
    }

    @Then("Verify multi select awareness options")
    public void verifyMultiSelectAwarenessOptions() throws Exception {

        List<String> situations = List.of("AOx3", "Ambulatory", "Altered", "Nonverbal at baseline");
        Map<String, Boolean> selectedOptions = new LinkedHashMap<>();

        for (String value : situations) {
            boolean flag = handOff.isAwarenessSelected(value);
            selectedOptions.put(value, flag);
        }

        for (String value : situations) {
            if (!selectedOptions.get(value)) {
                handOff.selectAwareness(value, false);
                selectedOptions.put(value, true);
                break;
            }
        }

        log.info("Click on Handoff.");
        handOff.clickOnHandOff();

        Assert.assertTrue(handOff.isPoPOptionDisplayed("Proceed"),
                "Proceed option should be displayed.");

        log.info("Click on proceed.");
        handOff.clickOnProceed();

        log.info("Click on Handoff.");
        handOff.clickOnHandOffIcon();

        Map<String, Boolean> actual = new LinkedHashMap<>();

        for (String value : situations) {
            boolean flag = handOff.isAwarenessSelected(value);
            actual.put(value, flag);
        }

        Assert.assertEquals(selectedOptions, actual,
                "Selected awareness not matched. Actual [" + actual + "] and Expected [" + selectedOptions + "].");
    }

    @And("Verify customize option of awareness")
    public void verifyCustomizeOptionOfAwareness() throws Exception {
        setHandOffDetails("Severity");

        log.info("Click on edit customize awareness.");
        handOff.clickOnEditCustomizeAwareness();

        log.info("Enter custom awareness [" + details.get("customAwareness") + "].");
        handOff.enterCustomAwareness(details.get("customAwareness"));

        log.info("Click on Handoff.");
        handOff.clickOnHandOff();

        Assert.assertTrue(handOff.isPoPOptionDisplayed("Proceed"),
                "Proceed option should be displayed.");

        log.info("Click on proceed.");
        handOff.clickOnProceed();

        log.info("Click on Handoff icon.");
        handOff.clickOnHandOffIcon();

        Assert.assertEquals(handOff.getCustomAwarenessValue(), details.get("customAwareness"),
                "Actual [" + handOff.getCustomAwarenessValue() + "] Expected [" + details.get("customAwareness") + "].");

        log.info("Click on Handoff icon.");
        handOff.clickOnHandOffIcon();

        log.info("Click on custom awareness.");
        handOff.clearCustomAwareness();

        log.info("Click on Handoff.");
        handOff.clickOnHandOff();

        Assert.assertTrue(handOff.isPoPOptionDisplayed("Proceed"),
                "Proceed option should displayed.");

        log.info("Click on proceed.");
        handOff.clickOnProceed();

        log.info("Click on Handoff icon.");
        handOff.clickOnHandOffIcon();

        Assert.assertTrue(handOff.isCustomValueClear(),
                "Custom value should be cleared. [Customize your own option] should be displayed.");
    }

}
