package com.biofourmis.biovitals.stepdefinitions.web;

import com.biofourmis.biovitals.pagefactory.web.ChatPage;
import com.biofourmis.biovitals.utilities.CustomUtils;
import io.cucumber.java.en.*;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.testng.Assert;

import java.util.*;

@Log4j
public class ChatHook {

    @Autowired
    @Qualifier(value = "web")
    private WebDriver driver;

    private ChatPage chatPage;
    private final CustomUtils customUtils = new CustomUtils(null);

    @And("^Verify navigate to chat section by clicking on chat icon on patient (home|details) screen$")
    public void verifyNavigateToChatSectionByClickingOnChatIconOnPatientHomeScreen(String arg0) throws Exception {
        chatPage = new ChatPage(driver);
        boolean flag = arg0.equals("home");

        log.info("Click on chat icon.");
        chatPage.clickOnChatIconOnMyPatients();

        log.info("Verify chat container should be open.");
        Assert.assertTrue(chatPage.verifyChatWindowDisplayed(), "Chat container should be displayed.");

        log.info("Adding call options in list.");
        List<String> chatScreenOptions = Arrays.asList("Video call", "Voice call");

        log.info("Verify all call options are displayed.");
        for (String option : chatScreenOptions)
            Assert.assertTrue(chatPage.verifyChatScreenComponents(option), option + " not displayed.");

        log.info("Verify close button on chat container.");
        Assert.assertTrue(chatPage.isCloseChatButtonVisible(), "Close chat button not displayed.");

        log.info("Verify input box for writing chat message should be displayed.");
        Assert.assertTrue(chatPage.isInputChatBoxVisible(), "Chat input field on chat container not visible.");

        log.info("Verify Send button should be displayed on chat container.");
        Assert.assertTrue(chatPage.isSendChatButtonVisible(), "Send button not visible on chat container.");

        log.info("Verify by clicking on close icon chat container should be closed.");
        Assert.assertFalse(chatPage.closeChatContainer(), "Chat container should be close after clicking on close icon.");
    }

    @Then("Verify sending {int} text message from HCP web dashboard")
    public void verifySendingTextMessageFromHCPWebDashboard(int count) throws Exception {
        log.info("Click on chat icon.");
        chatPage.clickOnChatIconOnMyPatients();

        log.info("Verify chat container should be open.");
        Assert.assertTrue(chatPage.verifyChatWindowDisplayed(), "Chat container should be displayed.");

        log.info("Remove all chat messages.");
        customUtils.resetChatData();

        for (int i = 0; i < count; i++) {
            String textMessage = customUtils.getMessageText(false);

            log.info("Send message [" + textMessage + "]");
            chatPage.sendTextMessage(textMessage);

            log.info("Verify sent message sent successfully.");
            Assert.assertTrue(chatPage.verifySentTextMessage(textMessage), "Last sent message not listed in chat section.");
        }
    }

    @And("^Verify start (video|voice|Audio|Video) call on patient (home|details) screen$")
    public void verifyStartAVideoCallOnPatientHomeScreen(String arg0, String arg1) throws Exception {
        chatPage = new ChatPage(driver);
        boolean isVideo = arg0.equalsIgnoreCase("video");

        if (isVideo) {
            log.info("Click on Video call option.");
            chatPage.clickOnVideoCallIconOnMyPatients();
        } else {
            log.info("Click on Voice call option.");
            chatPage.clickOnVoiceCallIconOnMyPatients();
        }

        log.info("Verify call initiated successfully.");
        Assert.assertTrue(chatPage.isCallInitiated(), arg0 + " call not started.");

        if (arg0.equals("voice") || arg0.equals("video")) {
            log.info("Verify decline call.");
            Assert.assertFalse(chatPage.declineCall(), "Failed to decline a call.");
        }
    }

    @And("^Verify start (video|voice|Audio|Video) call on patient (home|details) screen and cut$")
    public void verifyStartAVideoCallOnPatientScreen(String arg0, String arg1) throws Exception {
        verifyStartAVideoCallOnPatientHomeScreen(arg0, arg1);
    }

    @Then("^Receive (Audio|Video) call on HCP web dashboard$")
    public void receiveAudioCallOnHCPWebDashboard(String arg0) {
        chatPage = new ChatPage(driver);

        log.info("Verify receive " + arg0 + " call.");
        Assert.assertTrue(chatPage.verifyReceiveCall(arg0), "Incoming call container not displayed after waiting 60 seconds.");
        boolean video = arg0.equals("Video");

        log.info("Verify incoming call container elements.");
        if (video)
            Assert.assertTrue(chatPage.verifyReceiveDialogElements("Video call from"), "[Video call from] text not visible.");
        else
            Assert.assertTrue(chatPage.verifyReceiveDialogElements("Audio call from"), "[Audio call from] text not visible.");
        Assert.assertTrue(chatPage.verifyReceiveDialogElements("Accept"), "Accept button not visible.");
        Assert.assertTrue(chatPage.verifyReceiveDialogElements("Reject"), "Reject button not visible.");
    }

    @And("^Verify (Reject|Accept) call from HCP web dashboard$")
    public void verifyDeclineCallFromHCPWebDashboard(String arg0) throws Exception {
        chatPage = new ChatPage(driver);
        boolean isAccept = arg0.equals("Accept");

        log.info(arg0 + " the call.");
        if (isAccept)
            Assert.assertFalse(chatPage.verifyAcceptCall(), "Failed while accept the call.");
        else
            Assert.assertFalse(chatPage.verifyDeclineCall(), "Failed while reject the call.");
    }

    @Then("Verify End call from HCP Web dashboard")
    public void verifyEndCallFromHCPWebDashboard() throws Exception {
        chatPage = new ChatPage(driver);
        log.info("Click on End the call button.");
        Assert.assertFalse(chatPage.endCall(), "Failed while end the call.");
    }

    @Then("^Verify call (Rejected|Accepted) successfully in HCP web dashboard$")
    public void verifyCallRejectedSuccessfullyInHCPWebDashboard(String arg0) {
        chatPage = new ChatPage(driver);
        boolean reject = arg0.equals("Rejected");

        if (reject)
            Assert.assertFalse(chatPage.verifyCallRejected(), "Call not rejected.");
        else
            Assert.assertTrue(chatPage.verifyCallAccepted(), "Call not accepted.");
    }

    @And("Verify Call ended successfully in HCP Web dashboard")
    public void verifyCallEndedSuccessfullyInHCPWebDashboard() {
        chatPage = new ChatPage(driver);
        log.info("verify call ended successfully.");
        Assert.assertFalse(chatPage.verifyCallAccepted(), "Call not ended.");
    }

    @And("Verify badge count and chat on HCP web dashboard")
    public void verifyBadgeCountAndChatOnHCPWebDashboard() throws Exception {
        chatPage = new ChatPage(driver);
        int count = customUtils.getChat(true).size();

        log.info("Verify chat badge count.");
        Assert.assertTrue(chatPage.verifyBadgeCount(count), "Badge count should be " + count);

        log.info("Click on chat badge and navigate to chat container.");
        chatPage.clickOnBadgeAndNavigateToChat();

        ArrayList<String> textMessages = customUtils.getChat(true);
        Assert.assertEquals(chatPage.verifySentTextMessage(textMessages), textMessages, "Chat message not matched.");
    }

    @And("^Start (voice|video) call$")
    public void startVoiceCall(String arg0) throws Exception {
        chatPage = new ChatPage(driver);
        boolean isVideo = arg0.equalsIgnoreCase("video");

        if (isVideo) {
            log.info("Click on Video call option.");
            chatPage.clickOnVideoCallIconOnMyPatients();
        } else {
            log.info("Click on Voice call option.");
            chatPage.clickOnVoiceCallIconOnMyPatients();
        }

        log.info("Verify call initiated successfully.");
        Assert.assertTrue(chatPage.isCallInitiated(), arg0 + " call not started.");
    }

    @Then("Verify minimize and maximize chat container")
    public void verifyMinimizeAndMaximizeChatContainer() throws Exception {
        chatPage = new ChatPage(driver);
        Assert.assertTrue(chatPage.verifyMinimizeContainer(),
                "Call container should be minimized in right bottom corner.");

        Assert.assertTrue(chatPage.verifyMaximizeContainer(),
                "Call container should be maximized.");
    }

    @Then("Verify perform action without minimize the call container")
    public void verifyPerformActionWithoutMinimizeTheCallContainer() throws Exception {
        chatPage = new ChatPage(driver);
        log.info("Performing clicking on Hamburger while call container is maximized.");
        Assert.assertTrue(chatPage.clickOnHamburger(),
                "Hamburger should not be intractable.");
    }

    @And("Minimize the container and drag on other location")
    public void minimizeTheContainerAndDragOnOtherLocation() throws Exception {
        Assert.assertTrue(chatPage.verifyMinimizeContainer(),
                "Call container should be minimized in right bottom corner.");

        log.info("Verify Call container can be draggable on screen when minimize.");
        Assert.assertTrue(chatPage.dragCallContainerToAnotherLoc(),
                "Not able to drag and drop call container ");
    }

    @Then("Perform action after minimize the call container")
    public void performActionAfterMinimizeTheCallContainer() throws Exception {
        chatPage = new ChatPage(driver);
        log.info("Performing clicking on Hamburger while call container is maximized.");
        Assert.assertTrue(chatPage.clickOnHamburger(),
                "Hamburger should be intractable and clicked.");
    }

    @And("Minimize the call container")
    public void minimizeTheCallContainer() throws Exception {
        chatPage = new ChatPage(driver);

        Assert.assertTrue(chatPage.verifyMinimizeContainer(),
                "Call container should be minimized in right bottom corner.");
    }

    @Then("^Verify Video and voice call icons are disable while clinician in call$")
    public void verifyVideoAndVoiceCallIconsAreDisableWhileClinicianInCall() throws Exception {
        chatPage = new ChatPage(driver);

        log.info("Verify Video and voice icons are disable.");
        Assert.assertTrue(chatPage.verifyVideoIconIsDisable(),
                "Video call icon should be disable while clinician is in call.");

        Assert.assertTrue(chatPage.verifyVoiceIconIsDisable(),
                "Voice call icon should be disable while clinician is in call.");

        log.info("Click on message icon.");
        chatPage.clickOnMessageicon();

        Assert.assertTrue(chatPage.verifyVideoCallOptionInChatDisable(),
                "Video call option in chat section should be disable while clinician is in call.");

        Assert.assertTrue(chatPage.verifyVoiceCallOptionInChatDisable(),
                "Voice call option in chat section should be disable while clinician is in call.");

        Assert.assertTrue(chatPage.verifyVideoIconIsDisable(),
                "Video call icon should be disable while clinician is in call.");

        Assert.assertTrue(chatPage.verifyVoiceIconIsDisable(),
                "Voice call icon should be disable while clinician is in call.");
    }

    @Then("^Verify video, voice call and chat icons are (disable|invisible) for unassigned patients while clinician in call$")
    public void verifyVideoAndVoiceCallIconsAreDisableWhileClinician(String arg0) throws Exception {
        chatPage = new ChatPage(driver);
        boolean flag = arg0.equals("disable");
        log.info("Verify video, voice and chat icons should be " + arg0 + " for unassigned patients.");
        if (flag) {
            Assert.assertTrue(chatPage.verifyVideoIconIsDisable(),
                    "Video call icon should be disable while clinician is in call.");
            Assert.assertTrue(chatPage.verifyVoiceIconIsDisable(),
                    "Video call icon should be disable while clinician is in call.");
            Assert.assertTrue(chatPage.verifyChatIconIsDisable(),
                    "Chat icon should be disable while clinician is in call.");
        } else {
            Assert.assertTrue(chatPage.verifyVideoIconIsInvisible(),
                    "Video call icon should not be displayed while clinician is in call.");
            Assert.assertTrue(chatPage.verifyVoiceIconIsInvisible(),
                    "Voice call icon should not be displayed while clinician is in call.");
            Assert.assertTrue(chatPage.verifyChatIconIsInvisible(),
                    "Chat icon should not be displayed while clinician is in call.");
        }
    }

    @And("Verify sending text message from HCP web dashboard")
    public void verifySendingTextMessageFromHCPWebDashboard() throws Exception {
        log.info("Verify chat container should be open.");
        Assert.assertTrue(chatPage.verifyChatWindowDisplayed(), "Chat container should be displayed.");

        log.info("Remove all chat messages.");
        customUtils.resetChatData();
        String textMessage = customUtils.getMessageText(false);

        log.info("Send message [" + textMessage + "]");
        chatPage.sendTextMessage(textMessage);

        log.info("Verify sent message sent successfully.");
        Assert.assertTrue(chatPage.verifySentTextMessage(textMessage), "Last sent message not listed in chat section.");

    }

    @Then("Verify call connectivity more than {int} min")
    public void verifyCallConnectivityMoreThanMin(int arg0) throws Exception {
        chatPage = new ChatPage(driver);
        log.info("Waiting for " + arg0 + " during calling.");
        chatPage.waitForMinutes(arg0);

        Assert.assertTrue(chatPage.verifyOnGoingCall(),
                "Call should be going on till " + arg0 + " minutes.");

        log.info("End the call.");
        chatPage.endCall();
    }
}
