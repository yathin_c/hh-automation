package com.biofourmis.biovitals.stepdefinitions.web;

import com.biofourmis.biovitals.pagefactory.patient.DiaryNotesPage;
import com.biofourmis.biovitals.pagefactory.web.PatientDiaryPage;
import com.biofourmis.biovitals.utilities.CustomUtils;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.testng.Assert;
import org.testng.SkipException;

import java.util.Map;

@Log4j
public class PatientDiaryHook {
    @Autowired
    @Qualifier(value = "web")
    private WebDriver driver;

    private CustomUtils customUtils = new CustomUtils(null);
    private PatientDiaryPage patientDiary = null;

    @And("^Verify added diary note in with (required|all) fields hcp web$")
    public void verifyAddedDiaryNoteInWithRequiredFieldsHcpWeb(String arg0) throws Exception {
        patientDiary = new PatientDiaryPage(driver);
        boolean isRequired = arg0.equals("required");
        Map<String, String> diaryDetails = customUtils.getDefaultDiaryNoteDetails();

        log.info("Navigate to patient diary.");
        patientDiary.navigateToPatientDiary();

        log.info("Verify diary note with " + arg0 + ". Details [" + diaryDetails + "].");
        patientDiary.verifyDiaryNoteDetails(diaryDetails, isRequired);
    }

    @And("^Verify updated diary note in with (required|all) fields hcp web$")
    public void verifyUpdatedDiaryNoteInWithRequiredFieldsHcpWeb(String arg0) throws Exception {
        patientDiary = new PatientDiaryPage(driver);
        boolean isRequired = arg0.equals("required");
        Map<String, String> diaryDetails = customUtils.getDefaultDiaryNoteDetails();

        log.info("Verify updated diary note with " + arg0 + ". Details [" + diaryDetails + "].");
        patientDiary.verifyDiaryNoteDetails(diaryDetails, isRequired);
    }

    @Then("Verify respond diary note from hcp web")
    public void verifyRespondDiaryNoteFromHcpWeb() throws Exception {
        patientDiary = new PatientDiaryPage(driver);

        log.info("Navigate to patient diary.");
        patientDiary.navigateToPatientDiary();

        log.info("Verify diary note response.");
        patientDiary.respondOnDiaryNote(customUtils.getDefaultDiaryNoteDetails().get("Title"));
    }

    @Then("Verify HCP web patient diary content when no note available")
    public void verifyHCPWebPatientDiaryContentWhenNoNoteAvailable() throws Exception {
        patientDiary = new PatientDiaryPage(driver);

        log.info("Navigate to patient diary.");
        patientDiary.navigateToPatientDiary();

        log.info("Verify empty screen in HCP web.");
        patientDiary.verifyPatientDiaryEmptyScreen();
    }

    @When("Unacknowledged only option selected")
    public void unacknowledgedOnlyOptionSelected() throws Exception {
        log.info("Navigate to patient diary.");
        patientDiary.navigateToPatientDiary();

        patientDiary.selectUnacknowledgedToggle();
    }

    @Then("Only Unacknowledged notes should be visible")
    public void onlyUnacknowledgedNotesShouldBeVisible() {
        Assert.assertFalse(patientDiary.isNoteDisplayed(customUtils.getLastAcknowledgeNote()),
                "");

        Assert.assertTrue(patientDiary.isNoteDisplayed(customUtils.getDefaultDiaryNoteDetails().get("Title")),
                "");
    }

    @Then("Verify calendar functionality")
    public void verifyCalendarFunctionality() throws Exception {
        patientDiary = new PatientDiaryPage(driver);

        String title = customUtils.getDefaultDiaryNoteDetails().get("Title");
        log.info("Navigate to patient diary.");
        patientDiary.navigateToPatientDiary();

        patientDiary.clickOnCalendarFilter();

        patientDiary.selectCalenderOption("Current day");

        Assert.assertTrue(patientDiary.isNoteDisplayed(title));

        patientDiary.clickOnCalendarFilter();

        patientDiary.selectCalenderOption("Previous day");

        Assert.assertFalse(patientDiary.isNoteDisplayed(title));

        patientDiary.clickOnCalendarFilter();

        patientDiary.selectCalenderOption("Last 7 day");

        Assert.assertTrue(patientDiary.isNoteDisplayed(title));

        patientDiary.clickOnCalendarFilter();

        patientDiary.selectCalenderOption("Last 2 weeks");

        Assert.assertTrue(patientDiary.isNoteDisplayed(title));

        patientDiary.clickOnCalendarFilter();

        patientDiary.selectCalenderOption("Current day");

        Assert.assertTrue(patientDiary.isNoteDisplayed(title));

        System.out.println(customUtils.getDefaultDiaryNoteDetails().get("Title"));
    }

    @Then("Verify filter functionality")
    public void verifyFilterFunctionality() throws Exception {
        patientDiary = new PatientDiaryPage(driver);

        System.out.println(customUtils.getDefaultDiaryNoteDetails().get("Title"));

        log.info("Navigate to patient diary.");
        patientDiary.navigateToPatientDiary();

        patientDiary.clickOnFilter();

        Assert.assertEquals(patientDiary.getFilterOptions(), customUtils.getIconType(),
                "");

        String icon = customUtils.getDefaultDiaryNoteDetails().get("Type");
        patientDiary.selectFilterOption(icon,false);

        for (String value : customUtils.getIconType()) {
            if (value.equalsIgnoreCase(icon))
                Assert.assertTrue(patientDiary.isFilterOptionSelected(value),
                        "");
            else
                Assert.assertFalse(patientDiary.isFilterOptionSelected(value),
                        "");
        }

        patientDiary.clickOnApplyFilter();

        Assert.assertTrue(patientDiary.isNoteDisplayed(customUtils.getDefaultDiaryNoteDetails().get("Title")),
                "Note not displayed with title, ["+customUtils.getDefaultDiaryNoteDetails().get("Title")+"].");

        patientDiary.clickOnFilter();

        Assert.assertTrue(patientDiary.isResetFilterDisplayed(),
                "");

        Assert.assertTrue(patientDiary.isFilterOptionSelected(icon),
                "");

        patientDiary.selectFilterOption(icon,false);

        Assert.assertFalse(patientDiary.isFilterOptionSelected(icon),
                "");

        for (String value : customUtils.getIconType())
            patientDiary.selectFilterOption(value,true);

        for (String value : customUtils.getIconType())
            Assert.assertTrue(patientDiary.isFilterOptionSelected(value),
                    "");

        patientDiary.clickOnApplyFilter();

        Assert.assertTrue(patientDiary.isNoteDisplayed(customUtils.getDefaultDiaryNoteDetails().get("Title")));

        patientDiary.clickOnFilter();

        for (String value : customUtils.getIconType())
            Assert.assertTrue(patientDiary.isFilterOptionSelected(value),
                    "");

        patientDiary.clickOnResetFilter();

        patientDiary.clickOnFilter();

        for (String value : customUtils.getIconType())
            Assert.assertFalse(patientDiary.isFilterOptionSelected(value),
                    "");

        patientDiary.clickOnApplyFilter();

        Assert.assertTrue(patientDiary.isNoteDisplayed(customUtils.getDefaultDiaryNoteDetails().get("Title")));

        patientDiary.clickOnFilter();

        for (String value : customUtils.getIconType()) {
            if (value.equalsIgnoreCase(icon))
                continue;
            else
              patientDiary.selectFilterOption(value,true);
        }

        patientDiary.clickOnApplyFilter();

        Assert.assertFalse(patientDiary.isNoteDisplayed(customUtils.getDefaultDiaryNoteDetails().get("Title")));

        patientDiary.clickOnFilter();

        patientDiary.clickOnResetFilter();

        Assert.assertTrue(patientDiary.isNoteDisplayed(customUtils.getDefaultDiaryNoteDetails().get("Title")));

    }
}
