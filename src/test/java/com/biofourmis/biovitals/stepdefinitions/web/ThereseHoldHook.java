package com.biofourmis.biovitals.stepdefinitions.web;

import com.biofourmis.biovitals.Simulator;
import com.biofourmis.biovitals.configs.AppConfig;
import com.biofourmis.biovitals.configs.CustomConfig;
import com.biofourmis.biovitals.constants.configuration.TestConfig;
import com.biofourmis.biovitals.constants.scripts.ClinicInfo;
import com.biofourmis.biovitals.dao.EverionModel;
import com.biofourmis.biovitals.dao.VitalPatchModel;
import com.biofourmis.biovitals.impl.BioImpl;
import com.biofourmis.biovitals.pagefactory.web.ThereseHoldPage;
import com.biofourmis.biovitals.repository.ExcelRepo;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.testng.Assert;
import org.testng.SkipException;

import java.util.LinkedHashMap;
import java.util.Map;

@Log4j
public class ThereseHoldHook {

    @Autowired(required = false)
    @Qualifier(value = "web")
    private WebDriver web;

    @Autowired
    private CustomConfig customConfig;
    @Autowired
    private TestConfig testConfig;
    @Autowired
    private AppConfig appConfig;

    private ThereseHoldPage thereseHoldPage;
    private String newValue;
    private String newSetValue;
    private String defaultValueAfterReset;
    private int defaultHRValue;
    private Map<String, String> defaultVItalValues = new LinkedHashMap<>();
    private Map<String, String> previousGeneratedAlert = new LinkedHashMap<>();

    @Then("verify therese hold option and container")
    public void verifyThereseHoldOptionAndContainer() throws Exception {
        thereseHoldPage = new ThereseHoldPage(web);

        log.info("Open Therese hold container.");
        Assert.assertTrue(thereseHoldPage.openThresholdContainer(),
                "Therese hold container not displayed.");

        log.info("Verify [Set the thresholds to trigger alerts when they are passed] message.");
        Assert.assertTrue(thereseHoldPage.verifyContainerTexts("Set the thresholds to trigger alerts when they are passed."),
                "[Set the thresholds to trigger alerts when they are passed] message not displayed.");
    }

    @And("verify default therese hold values")
    public void verifyDefaultThereseHoldValues() throws Exception {
        log.info("Get default vitals.");
        if (defaultVItalValues.isEmpty())
            getDefaultVitalsForClinic();

        Map<String, String> actualVitalsValue = new LinkedHashMap<>();
        actualVitalsValue.putAll(readValues(defaultVItalValues));

        Assert.assertEquals(actualVitalsValue, defaultVItalValues,
                "Actual and expected default vitals values not matched. Expected values " +
                        "[" + defaultVItalValues + "] and actual values are [" + actualVitalsValue + "]");
    }

    private void getDefaultVitalsForClinic() throws Exception {
        log.info("Read therese hold values from excel file.");
        ExcelRepo<ClinicInfo> thereseHold =
                new BioImpl(appConfig.getCurrentWorkingDir() + "/" + customConfig.getDataFile(), ClinicInfo.class);

        Map<String, String> clause = new LinkedHashMap<>();
        clause.put("Name", testConfig.getClinicName());
        clause.put("Server", testConfig.getEnvironment());

        log.info("Search clinic info where Name = " + testConfig.getClinicName() + " and Server = " + testConfig.getEnvironment());
        ClinicInfo clinicInfo = thereseHold.findByClause(clause).stream().findFirst().get();

        String[] values = clinicInfo.getDefaultThreshold().split(":");
        for (String value : values)
            defaultVItalValues.put(value.split("-")[0].strip(), value.split("-")[1].strip());

        log.info("Default therese hold values are [" + defaultVItalValues + "]");
    }

    private Map<String, String> readValues(Map<String, String> vitalsValue) {
        Map<String, String> map = new LinkedHashMap<>();
        for (String value : vitalsValue.keySet()) {
            if (value.equalsIgnoreCase("HHR"))
                map.put(value, thereseHoldPage.getAllDefaultVitals("HR", true));
            if (value.equalsIgnoreCase("LHR"))
                map.put(value, thereseHoldPage.getAllDefaultVitals("HR", false));
            if (value.equalsIgnoreCase("HRR"))
                map.put(value, thereseHoldPage.getAllDefaultVitals("RR", true));
            if (value.equalsIgnoreCase("LRR"))
                map.put(value, thereseHoldPage.getAllDefaultVitals("RR", false));
            if (value.equalsIgnoreCase("HBT"))
                map.put(value, thereseHoldPage.getAllDefaultVitals("Body temperature", true));
            if (value.equalsIgnoreCase("LBT"))
                map.put(value, thereseHoldPage.getAllDefaultVitals("Body temperature", false));
            if (value.equalsIgnoreCase("HTT"))
                map.put(value, thereseHoldPage.getAllDefaultVitals("Temporal temperature", true));
            if (value.equalsIgnoreCase("LTT"))
                map.put(value, thereseHoldPage.getAllDefaultVitals("Temporal temperature", false));
            if (value.equalsIgnoreCase("HD"))
                map.put(value, thereseHoldPage.getAllDefaultVitals("diastolic", true));
            if (value.equalsIgnoreCase("LD"))
                map.put(value, thereseHoldPage.getAllDefaultVitals("diastolic", false));
            if (value.equalsIgnoreCase("HS"))
                map.put(value, thereseHoldPage.getAllDefaultVitals("systolic", true));
            if (value.equalsIgnoreCase("LS"))
                map.put(value, thereseHoldPage.getAllDefaultVitals("systolic", false));
            if (value.equalsIgnoreCase("HSPO2"))
                map.put(value, thereseHoldPage.getAllDefaultVitals("SpO", true));
            if (value.equalsIgnoreCase("LSPO2"))
                map.put(value, thereseHoldPage.getAllDefaultVitals("SpO", false));
        }
        return map;
    }

    @Then("verify closing therese hold container")
    public void verifyClosingThereseHoldContainer() throws Exception {
        log.info("Is close icon displayed.");
        Assert.assertTrue(thereseHoldPage.isCloseBtnDisplayed(),
                "Close icon not displayed for closing container.");

        log.info("Click on close container icon.");
        thereseHoldPage.clickOnCloseContainer();

        Assert.assertTrue(thereseHoldPage.isThereseholdContainerClose(),
                "There hold container should be closed.");
    }

    @And("Verify setting therese hold values")
    public void verifySettingThereseHoldValues() throws Exception {
        thereseHoldPage = new ThereseHoldPage(web);
        log.info("Open Therese hold container.");
        thereseHoldPage.openThresholdContainer();
        log.info("Read HR default value.");
        defaultHRValue = Integer.parseInt(thereseHoldPage.getHRDefaultValue().strip());
        log.info("Default HR value is [" + defaultHRValue + "].");

        newValue = String.valueOf(defaultHRValue + 10);
        log.info("Set HR value as [" + newValue + "].");
        thereseHoldPage.setHrValue(newValue);

        newSetValue = thereseHoldPage.getHRDefaultValue().strip();

        Assert.assertEquals(newValue, newSetValue,
                "Not able to update HR value. Updated HR value should be [" + newValue + "].");

        log.info("Close Therese hold container.");
        thereseHoldPage.closeThresholdContainer();
        log.info("Open Therese hold container.");
        thereseHoldPage.openThresholdContainer();

        log.info("Read HR updated value.");
        newSetValue = thereseHoldPage.getHRDefaultValue().strip();

        Assert.assertEquals(newValue, newSetValue,
                "Not able to update HR value. Updated HR value should be [" + newValue + "].");
    }


    @Then("Verify reset therese hold values")
    public void verifyResetThereseHoldValues() throws Exception {
        log.info("Click on Reset button.");
        thereseHoldPage.clickOnReset();

        Assert.assertTrue(thereseHoldPage.verifyResetPopUp("Reset all thresholds to default settings?"),
                "Reset all thresholds to default settings? Not displayed on reset pop-up.");
        Assert.assertTrue(thereseHoldPage.verifyResetPopUp("Snooze and Smart alerts will remain unchanged."),
                "Snooze and Smart alerts will remain unchanged. Not displayed on reset pop-up.");
        Assert.assertTrue(thereseHoldPage.verifyResetPopUp("No, do not reset"),
                "No, do not reset button not displayed on reset pop-up.");
        Assert.assertTrue(thereseHoldPage.verifyResetPopUp("Yes, reset"),
                "Yes, reset button should be displayed.");

        log.info("No, do not reset option.");
        thereseHoldPage.actionOnResetPop("No, do not reset");

        log.info("read HR value after canceling reset.");
        String hrValueAfterReset = thereseHoldPage.getHRDefaultValue().strip();
        log.info("HR value after canceling reset [" + hrValueAfterReset + "].");

        defaultValueAfterReset = thereseHoldPage.getHRDefaultValue().strip();
        log.info("HR value after reset close and open container again value is [" + hrValueAfterReset + "].");

        Assert.assertNotEquals(defaultHRValue, Integer.parseInt(defaultValueAfterReset.strip()),
                "HR therese hold value should not reset. Changed HR value should be [" + defaultValueAfterReset + "] and found [" + defaultHRValue + "].");

        log.info("Click on Reset button.");
        thereseHoldPage.clickOnReset();

        log.info("Click on Yes, reset option.");
        thereseHoldPage.actionOnResetPop("Yes, reset");

        log.info("read HR value after reset.");
        hrValueAfterReset = thereseHoldPage.getHRDefaultValue().strip();
        log.info("HR value after reset [" + hrValueAfterReset + "].");

        log.info("Close Therese hold container.");
        thereseHoldPage.closeThresholdContainer();
        log.info("Open Therese hold container.");
        thereseHoldPage.openThresholdContainer();
        log.info("Read HR therese hold value again.");
        defaultValueAfterReset = thereseHoldPage.getHRDefaultValue().strip();
        log.info("HR value after reset close and open container again value is [" + hrValueAfterReset + "].");

        Assert.assertEquals(defaultHRValue, Integer.parseInt(defaultValueAfterReset.strip()),
                "HR therese hold value not reset. Default HR value should be [" + defaultHRValue + "] and found [" + defaultValueAfterReset + "].");
    }

    @And("verify badge count when no alert")
    public void verifyBadgeCountWhenNoAlert() {
        Assert.assertFalse(thereseHoldPage.isIndividualBadgeDisplayed(),
                "Badge count should not be displayed on patient individual page.");
    }

    @Then("verify alert option on individual patient page")
    public void verifyAlertOptionOnIndividualPatientPage() throws Exception {
        thereseHoldPage = new ThereseHoldPage(web);

        Assert.assertTrue(thereseHoldPage.isAlertIconDisplayed(),
                "Alert icon should be displayed.");

        log.info("Click on alert filter.");
        thereseHoldPage.clickOnAlertIcon();

        Assert.assertTrue(thereseHoldPage.isAlertPopUPDisplayed(),
                "Alert pop should be displayed.");

        Assert.assertTrue(thereseHoldPage.isOptionDisplayedOnAlertContainer("Active"),
                "Active option should be displayed on alert container.");

        log.info("Click on active alerts option.");
        thereseHoldPage.clickOnActiveAlert();

        Assert.assertTrue(thereseHoldPage.isOptionDisplayedOnAlertContainer("No alerts yet for this patient"),
                "[No alerts yet for this patient] message should be displayed when there is no alert.");

        Assert.assertTrue(thereseHoldPage.isOptionDisplayedOnAlertContainer("Responded"),
                "Responded alerts option should be displayed on alert container.");

        log.info("Click on responded alerts.");
        thereseHoldPage.clickOnRespondedAlert();

        Assert.assertTrue(thereseHoldPage.isOptionDisplayedOnAlertContainer("No resolved alerts yet for this patient"),
                "[No resolved alerts yet for this patient] message should be displayed.");

        log.info("Click on close alerts container.");
        thereseHoldPage.closeThresholdContainer();

        Assert.assertFalse(thereseHoldPage.isAlertPopUPDisplayed(),
                "Alert pop-up should be closed.");
    }

    @Then("verify alert after set therese hold values for vital {string} as {int} and sync for {int} minutes with {string}")
    public void setThereseHoldValuesForVitalAsAndSyncForMinutesWith(String vitalName, int vitalValue, int syncDuration, String device) throws Throwable {
        thereseHoldPage = new ThereseHoldPage(web);
        log.info("Open Therese hold container.");
        thereseHoldPage.openThresholdContainer();
        log.info("Read HR default value.");
        defaultHRValue = Integer.parseInt(thereseHoldPage.getAllDefaultVitals("HR", true).strip());
        log.info("Default HR value is [" + defaultHRValue + "].");

        newValue = String.valueOf(vitalValue);
        log.info("Set HR value as [" + newValue + "].");
        thereseHoldPage.setHrValue(newValue);

        newSetValue = thereseHoldPage.getAllDefaultVitals("HR", true).strip();

        log.info("Get current url.");
        String url = web.getCurrentUrl();
        String patientId = url.substring(url.lastIndexOf("/") + 1);
        log.info("Sync vitals for patient id :" + patientId);
        Assert.assertEquals(newValue, newSetValue,
                "Not able to update HR value. Updated HR value should be [" + newValue + "].");

        if (defaultVItalValues.isEmpty())
            getDefaultVitalsForClinic();

        //Start Syncing
        log.info("Start syncing.");
        Simulator simulator = new Simulator("hh", "staging",
                appConfig.getCurrentWorkingDir() + "/" + "src/test/resources/properties/simulator.properties");

        simulator.forPatientId(patientId);

        if (device.equalsIgnoreCase("Everion")) {
            simulator.syncVitals(EverionModel.builder()
                    .durationInMin(syncDuration)
                    .maxHR(vitalValue + 10)
                    .minHR(vitalValue)
                    .maxRR(Integer.parseInt(defaultVItalValues.get("HRR")))
                    .minRR(Integer.parseInt(defaultVItalValues.get("LRR")))
                    .minSPO2(Integer.parseInt(defaultVItalValues.get("LSPO2")))
                    .minBodyTemp(Integer.parseInt(defaultVItalValues.get("LBT")))
                    .minSkinTemp(Integer.parseInt(defaultVItalValues.get("LTT")))
                    .build()).start();
        } else {
            simulator.syncVitals(VitalPatchModel.builder()
                    .durationInMin(syncDuration)
                    .maxHR(vitalValue + 10)
                    .minHR(vitalValue)
                    .maxRR(Integer.parseInt(defaultVItalValues.get("HRR")))
                    .minRR(Integer.parseInt(defaultVItalValues.get("LRR")))
                    .minBodyTemp(Integer.parseInt(defaultVItalValues.get("LBT")))
                    .minSkinTemp(Integer.parseInt(defaultVItalValues.get("LTT")))
                    .build()).start();
        }

        log.info("Wait for alert generation through simulator.");
        thereseHoldPage.waitForAlertGeneration(syncDuration);

        //Verify alert
        log.info("Click on alert icon.");
        thereseHoldPage.clickOnAlertIcon();

        Assert.assertTrue(thereseHoldPage.isAlertMessageDisplayed("Resting Heart rate is Above " + vitalValue + " bpm"),
                "No alert found for Resting Heart rate is Above " + vitalValue + " bpm");

        previousGeneratedAlert.put("Vital", vitalName);
        previousGeneratedAlert.put("Value", String.valueOf(vitalValue));
        previousGeneratedAlert.put("Device", device);
        previousGeneratedAlert.put("Message", "Resting Heart rate is Above " + vitalValue + " bpm");
    }

    @And("verify resolving an alert and status after resolved")
    public void verifyResolvingAnAlertAndStatusAfterResolved() throws Exception {

        log.info("Click on respond button.");
        thereseHoldPage.clickOnRespond();

        log.info("Click on resolve an alert.");
        thereseHoldPage.clickOnResolveAnAlert();

        log.info("Click on submit an abler response.");
        thereseHoldPage.clickOnSubmitAnAlertResponse();

        log.info("Click on respond alert option in alert container.");
        thereseHoldPage.clickOnRespondedAlert();

        Assert.assertTrue(thereseHoldPage.verifyResolvedAlertMessage("Resolved"),
                "Resolved message should be displayed for responded alert.");

        Assert.assertTrue(thereseHoldPage.verifyResolvedAlertMessage(previousGeneratedAlert.get("Message")),
                "Alert message [" + previousGeneratedAlert.get("Message") + " should be displayed.");

    }

    @Then("verify set therese hold values for un-assigned patient")
    public void verifySetThereseHoldValues() throws Exception {
        thereseHoldPage = new ThereseHoldPage(web);

        log.info("Open Therese hold container.");
        thereseHoldPage.openThresholdContainer();

        Assert.assertTrue(thereseHoldPage._isTextContainsPresent("You can only set thresholds for patients assigned to you."),
                "[You can only set thresholds for patients assigned to you.] message should be displayed for un assigned patient.");
    }

    @And("verify alert for vital {string} with {string}")
    public void verifyAlertForVitalWith(String vital, String device) throws Throwable {
        String url = web.getCurrentUrl();
        String patientId = url.substring(url.lastIndexOf("/") + 1);
        Assert.assertEquals(newValue, newSetValue,
                "Not able to update HR value. Updated HR value should be [" + newValue + "].");

        if (defaultVItalValues.isEmpty())
            getDefaultVitalsForClinic();

        //Start Syncing
        Simulator simulator = new Simulator("hh", "staging",
                appConfig.getCurrentWorkingDir() + "/" + "src/test/resources/properties/simulator.properties");

        simulator.forPatientId(patientId);

        //TODO Use vital HHR
        try {
            log.info("Sync vitals.");
            if (device.equalsIgnoreCase("Everion")) {
                simulator.syncVitals(EverionModel.builder()
                        .durationInMin(30)
                        .maxHR(Integer.parseInt(defaultVItalValues.get("HRR")) + 20)
                        .build()).start();
            } else {
                simulator.syncVitals(VitalPatchModel.builder()
                        .durationInMin(30)
                        .maxHR(Integer.parseInt(defaultVItalValues.get("HRR")) + 20)
                        .build()).start();
            }
        } catch (Exception e) {
            throw new SkipException("Failed while syncing vitals by simulator API's. Exception [" + e.getMessage() + "].");
        }

        log.info("Wait for alert generation.");
        thereseHoldPage.waitForAlertGeneration(30);

        //Verify alert
        log.info("Click on alert icon.");
        thereseHoldPage.clickOnAlertIcon();

        Assert.assertFalse(thereseHoldPage.isRespondBtnVisible(),
                "Respond button should be available.");
    }

    @Then("verify alert badge count and triggered event should be visible in global alert")
    public void verifyAlertBadgeCountAndTriggeredEventShouldVisibleInGlobalAlert() throws Throwable {
        String url = web.getCurrentUrl();
        String patientId = url.substring(url.lastIndexOf("/") + 1);
        log.info("Verify alerts.");
        verifyAlerts(patientId, true, "Everion");
    }

    @Then("verify alert badge count and triggered event should not be visible in global alert")
    public void verifyAlertBadgeCountAndTriggeredEventShouldNotBeVisibleInGlobalAlert() throws Throwable {
        String url = web.getCurrentUrl();
        String patientId = url.substring(url.lastIndexOf("/") + 1);
        log.info("Verify alerts.");
        verifyAlerts(patientId, false, "Everion");
    }

    private void verifyAlerts(String patientId, boolean hasAlert, String device) throws Throwable {
        thereseHoldPage = new ThereseHoldPage(web);

        log.info("Read existing badge count.");
        Integer existingBadgeCount = Integer.parseInt(thereseHoldPage.getGlobalAlertBadgeCount());

        if (defaultVItalValues.isEmpty())
            getDefaultVitalsForClinic();

        Simulator simulator = new Simulator("hh", "staging",
                appConfig.getCurrentWorkingDir() + "/" + "src/test/resources/properties/simulator.properties");

        simulator.forPatientId(patientId);

        //TODO Use vital HHR
        try {
            log.info("Sync for vitals.");
            if (device.equalsIgnoreCase("Everion")) {
                simulator.syncVitals(EverionModel.builder()
                        .durationInMin(30)
                        .maxHR(Integer.parseInt(defaultVItalValues.get("HRR")) + 20)
                        .build()).start();
            } else {
                simulator.syncVitals(VitalPatchModel.builder()
                        .durationInMin(30)
                        .maxHR(Integer.parseInt(defaultVItalValues.get("HRR")) + 20)
                        .build()).start();
            }
        } catch (Exception e) {
            throw new SkipException("Failed while syncing vitals by simulator API's. Exception [" + e.getMessage() + "].");
        }
        log.info("Wait for alert generation.");
        thereseHoldPage.waitForAlertGeneration(30);

        log.info("Read existing badge count.");
        Integer latestBadgeCount = Integer.parseInt(thereseHoldPage.getGlobalAlertBadgeCount());

        if (hasAlert)
            Assert.assertTrue(existingBadgeCount < latestBadgeCount,
                    "Alert badge should be displayed as [" + existingBadgeCount + 1 + "] because alert is generated for assigned patient. Patient Id [" + patientId + "]");
        else
            Assert.assertSame(existingBadgeCount, latestBadgeCount,
                    "Alert badge should be displayed as [" + existingBadgeCount + "] because no alert is generated for un assigned patient. Patient Id [" + patientId + "]");

        log.info("Click on global alert icon.");
        thereseHoldPage.clickOnGlobalAlertIcon();

        if (hasAlert) {
            Assert.assertTrue(thereseHoldPage.isGlobalAlertMessageDisplayed("Resting Heart rate is Above " + defaultVItalValues.get("HRR") + " bpm"),
                    "No alert found for Resting Heart rate is Above " + defaultVItalValues.get("HRR") + " bpm");

            log.info("Click on respond global alert container.");
            thereseHoldPage.clickOnGlobalRespondAnAlert();

            log.info("Click on resolve and alert.");
            thereseHoldPage.clickOnResolveAnAlert();

            log.info("Click on submit an alert response.");
            thereseHoldPage.clickOnSubmitAnAlertResponse();

            log.info("Click on responded alerts.");
            thereseHoldPage.clickOnRespondedAlert();

            Assert.assertTrue(thereseHoldPage.verifyResolvedAlertMessage("Resolved"),
                    "Alert status should be displayed as [Resolved].");

            Assert.assertTrue(thereseHoldPage.verifyResolvedAlertMessage(previousGeneratedAlert.get("Message")),
                    "Alert with message [" + previousGeneratedAlert.get("Message") + "] should be displayed.");

        } else
            Assert.assertFalse(thereseHoldPage.isGlobalAlertMessageDisplayed("Resting Heart rate is Above " + defaultVItalValues.get("HRR") + " bpm"),
                    "Alert found for Resting Heart rate is Above " + defaultVItalValues.get("HRR") + " bpm");
    }
}