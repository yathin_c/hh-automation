package com.biofourmis.biovitals.stepdefinitions.web;

import com.biofourmis.biovitals.configs.CustomConfig;
import com.biofourmis.biovitals.constants.configuration.TestConfig;
import com.biofourmis.biovitals.pagefactory.web.HomePage;
import com.biofourmis.biovitals.pagefactory.web.InventoryPage;
import com.biofourmis.biovitals.utilities.CustomUtils;
import io.cucumber.java.en.And;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.testng.Assert;

import java.util.Map;


@Log4j
public class InventoryHook {

    @Autowired
    @Qualifier(value = "web")
    private WebDriver driver;


    @Autowired
    private TestConfig testConfig;

    private HomePage homePage = null;
    private CustomUtils customUtils = new CustomUtils(null);
    private InventoryPage inventoryPage;
    Map<String, String> reqDetails;

    @And("Add new device {string} in inventory")
    public void addNewDeviceInInventory(String arg0) throws Exception {
        inventoryPage = new InventoryPage(driver);
        log.info("Click on Add device.");
        inventoryPage.clickOnAddDevice();
        reqDetails = customUtils.getInventoryDetails(arg0);
        log.info("Device details : "+ reqDetails);
        log.info("Fill required fields.");
        inventoryPage.fillRequiredFields(reqDetails);
        log.info("Click on save.");
        inventoryPage.clickOnSave();
        log.info("Verify device added successfully.");
        Assert.assertTrue(inventoryPage.verifyDeviceAdded(reqDetails.get("assetId")),
                "No device added with details: "+reqDetails);
    }

    @And("Assign device to caregiver")
    public void assignDeviceToCaregiver() throws Exception {
        log.info("Click on device type. On device name "+ reqDetails.get("deviceName"));
        inventoryPage.selectDeviceSection(reqDetails.get("deviceName"));
        log.info("Click on Assign Icon.");
        inventoryPage.clickOnAssignIcon(reqDetails.get("assetId"));
        log.info("Select care team option.");
        inventoryPage.selectCareTeamOptionForAssign();
        //Search Clinician
        log.info("Search for clinician: "+testConfig.getClinicianName());
        Assert.assertTrue(inventoryPage.searchClinician(testConfig.getClinicianName()),
                "Clinician "+testConfig.getClinicianName()+" not found.");

        log.info("Select searched care team member.");
        inventoryPage.selectCareTeam(testConfig.getClinicianName());
        log.info("Click on Assign.");
        inventoryPage.clickOnAssign();
        log.info("Navigate to assigned devices table(tab).");
        inventoryPage.moveToAssignedTab();
        Assert.assertTrue(inventoryPage.verifyDeviceAdded(reqDetails.get("assetId")),
                "No added device found with asset ID "+ reqDetails.get("assetId"));
    }

    @And("Delete device")
    public void deleteDevice() throws Exception {
        log.info("Click on delete device.");
        inventoryPage.deleteDevice(reqDetails.get("assetId"));
        Assert.assertTrue(inventoryPage.verifyDeviceDeleted(reqDetails.get("assetId")),
                "Deleted device found under assigned devicestab. Asset Id: "+reqDetails.get("assetId"));
        log.info("Move to available devices tab.");
        inventoryPage.moveToAvailableTab();
        Assert.assertTrue(inventoryPage.verifyDeviceDeleted(reqDetails.get("assetId")),
                "Deleted device found under available devices tab. Asset Id: "+reqDetails.get("assetId"));

    }
}
