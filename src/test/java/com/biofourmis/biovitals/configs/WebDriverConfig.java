package com.biofourmis.biovitals.configs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * This class is read only do not modify anything here
 */
@Configuration
public class WebDriverConfig {

    public Map<String, ThreadLocal<WebDriver>> drivers = new HashMap<>();
    private ThreadLocal<WebDriver> webDriver = new ThreadLocal<>();
    private ThreadLocal<WebDriver> HHDriver = new ThreadLocal<>();
    private ThreadLocal<WebDriver> HCPDriver = new ThreadLocal<>();

    /**
     * Add new ThreadLocal instance for you app
     * <p>
     * Example : private ThreadLocal<WebDriver> app_driver = new ThreadLocal<>();
     */


    public void setDriver(WebDriver driver, String appName) {
        if (appName.equalsIgnoreCase("web")) {
            webDriver.set(driver);
            drivers.put(appName, webDriver);
        } else if (appName.toUpperCase(Locale.ROOT).equalsIgnoreCase("HH")) {
            HHDriver.set(driver);
            drivers.put(appName, HHDriver);
        }
        else if (appName.toUpperCase(Locale.ROOT).equalsIgnoreCase("HCP")) {
            HCPDriver.set(driver);
            drivers.put(appName, HCPDriver);
        }

        /**
         * Add your app condition here
         * Example:
         *
         * else if (appName.equalsIgnoreCase("app")) {
         *             app_driver.set(driver);
         *             drivers.put(appName, app_driver);
         *         }
         */
    }

    @Bean
    @Qualifier(value = "web")
    @Scope("prototype")
    public WebDriver webDriver() {
        if (drivers.containsKey("web"))
            return drivers.get("web").get();
        return null;
    }

    @Bean
    @Qualifier(value = "HH")
    @Scope("prototype")
    public WebDriver hhDriver() {
        if (drivers.containsKey("HH"))
            return drivers.get("HH").get();
        return null;
    }

    @Bean
    @Qualifier(value = "HCP")
    @Scope("prototype")
    public WebDriver hcpDriver() {
        if (drivers.containsKey("HCP"))
            return drivers.get("HCP").get();
        return null;
    }

    /**
     *
     * Add you app bean here with unique Qualifier
     * Note : appName field is case sensitive.
     *   @Bean
     *     @Qualifier(value = "appName")
     *     @Scope("prototype")
     *     public WebDriver hhDriver() {
     *         if (drivers.containsKey("appName"))
     *             return drivers.get("appName").get();
     *         return null;
     *     }
     */

}
