package com.biofourmis.biovitals.configs;

import com.biofourmis.biovitals.constants.configuration.Clinician;
import com.biofourmis.biovitals.constants.configuration.Config;
import com.biofourmis.biovitals.constants.configuration.TestConfig;
import com.biofourmis.biovitals.constants.scripts.ClinicInfo;
import com.biofourmis.biovitals.impl.BioImpl;
import com.biofourmis.biovitals.model.Authentication;
import com.biofourmis.biovitals.model.User;
import com.biofourmis.biovitals.repository.ExcelRepo;
import com.biofourmis.biovitals.utilities.CustomUtils;
import com.biofourmis.biovitals.utilities.RestUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.cucumber.java.Scenario;
import io.restassured.response.Response;
import lombok.Data;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.*;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.test.context.ContextConfiguration;

import java.util.*;

@Log4j
@Data
@Configuration
@PropertySource(value = "file:./src/test/resources/properties/custom.properties")
@ContextConfiguration(locations = "./src/test/resources/testNG.xml")
public class CustomConfig implements ApplicationContextAware {

    private static ApplicationContext context;
    private Scenario scenario;

    @Autowired
    private Environment env;
    @Autowired
    private AppConfig appConfig;

    @Value("${data.file}")
    private String dataFile;

    @Value("${app.supported.environments}")
    private String[] supportedEnv;
    @Value("${app.supported.clinics}")
    private String[] supportedClinics;
    @Value("${app.supported.applications}")
    private String[] apps;

    @Value("${slack.report.channel}")
    private String reportChannel;
    @Value("${slack.bot.token}")
    private String token;
    @Value("${slack.channel.debug}")
    private String debugChannel;
    @Value("${slack.channel.server}")
    private String serverChannel;


    private String user;
    private String mail;
    private String password;
    private String role;
    private TestConfig testConfig;
    private String accessToken;
    private User userConfig;

    @Lazy
    @Bean
    @Scope("prototype")
    public Scenario scenario() {
        return this.scenario;
    }

    public String getUrl() {
        String url = env.getProperty("app.url").replace("env",
                testConfig.getEnvironment().toLowerCase(Locale.ROOT).strip());

        if (url == null)
            throw new RuntimeException("URL is not specified, please include app.{clinicName}.{server}.url " +
                    "property in the custom.properties");
        return url;
    }

    @Lazy
    @Bean
    public User setAPIData(String arg0) throws Throwable {
        RestUtils restUtils = new RestUtils();
        Map<String, String> login = new LinkedHashMap<>();
        String email;
        String password;

        if (arg0.equalsIgnoreCase("nurse")) {
            email = testConfig.getNurseMail();
            password = testConfig.getNursePass();
        } else {
            email = testConfig.getClinicianMail();
            password = testConfig.getClinicianPass();
        }

        login.put("url", "https://api-qa-hospitalathome.biofourmis.com/api/v1/auth/login");
        login.put("reqMethod", "POST");
        login.put("reqBody", new ObjectMapper()
                .writeValueAsString(
                        Authentication
                                .builder()
                                .email(email)
                                .password(password)
                                .build()));

        Response response = restUtils.call(login);
        accessToken = response.getBody().jsonPath().get("data.accessToken").toString();
        userConfig = new ObjectMapper().convertValue(response.getBody().jsonPath().get("data.user"), User.class);
        return userConfig;
    }

    @Bean
    @Order(value = 3)
    public void setupReportData() throws Exception {
        Map<String, String> configData = new HashMap<>();
        String channel = reportChannel;
        if (channel.equalsIgnoreCase("server"))
            channel = serverChannel;
        else if (channel.equalsIgnoreCase("off"))
            channel = reportChannel;
        else
            channel = debugChannel;

        configData.put("Channel", channel);
        configData.put("Token", token);
        configData.put("Environment", testConfig.getEnvironment());
        configData.put("Clinic", testConfig.getClinicName());

        CustomUtils.reportData.putAll(configData);

        log.info("Test Config Data: " + testConfig);
        log.info("Reporting channel: " + reportChannel);
    }

    @Bean
    @Order(value = 2)
    public ClinicInfo loadClinicInfo() throws Exception {
        ExcelRepo<ClinicInfo> clinicInfoBio = new BioImpl<>(appConfig.getCurrentWorkingDir() + dataFile, ClinicInfo.class);
        Map<String, String> condition = new LinkedHashMap<>();
        condition.put("Name", testConfig.getClinicName());
        condition.put("Server", testConfig.getEnvironment());
        ClinicInfo clinicInfo = clinicInfoBio.findByClause(condition).stream().findFirst().get();
        return clinicInfo;
    }

    @Bean
    @Order(value = 1)
    public void loadCreds() throws Exception {
        String path = System.getProperty("user.dir") + dataFile;
        ExcelRepo<Clinician> objectExcelRepo = null;
        if (testConfig.getClinicianName().equalsIgnoreCase("null")) {
            this.mail = this.user = testConfig.getClinicianMail();

            objectExcelRepo = new BioImpl<>(path, Clinician.class);
            Clinician dataSet = objectExcelRepo.findOne("Mail", this.mail);

            this.user = dataSet.getUser();
            this.mail = dataSet.getMail();
            this.password = dataSet.getPassword();
            this.role = dataSet.getRole();
            this.testConfig.setClinicianMail(this.mail);
            this.testConfig.setClinicianPass(this.password);
            this.testConfig.setClinicianName(this.user);
            this.testConfig.setClinicianRole(this.role);

        } else {
            this.user = testConfig.getClinicianName();
            this.mail = testConfig.getClinicianMail();
            this.password = testConfig.getClinicianPass();
            this.role = "Physician";
        }

        if (testConfig.getNurseName().equalsIgnoreCase("null")) {
            String mail = this.user = testConfig.getNurseMail();
            if (Objects.isNull(objectExcelRepo))
                objectExcelRepo = new BioImpl<>(path, Clinician.class);
            Clinician dataSet = objectExcelRepo.findOne("Mail", mail);

            this.testConfig.setNurseName(dataSet.getUser());
            this.testConfig.setNurseMail(dataSet.getMail());
            this.testConfig.setNursePass(dataSet.getPassword());
            this.testConfig.setNurseRole("Nurse");
        }
    }

    @Bean
    @Order(value = 0)
    public TestConfig setupTestNGExecutionFile() throws Exception {
        Map<String, String> configData = new HashMap<>();
        String path = System.getProperty("user.dir") + dataFile;
        List<String> suiteParams = Arrays.asList("browser", "HH", "HCP",
                "clinicName", "env", "clinicianName", "clinicianMail", "clinicianPass",
                "nurseName", "nurseMail", "nursePass",
                "primaryPatientMRN", "primaryPatientName", "secondaryPatientMRN", "secondaryPatientName",
                "unassignedPatientMRN", "unassignedPatientName");

        ExcelRepo<Config> objectExcelRepo = new BioImpl<>(path, Config.class);
        List<Config> dataSet = objectExcelRepo.findAll();

        for (Config config : dataSet) {
            if (config.getProperty().contains("Clinic Name"))
                configData.put("clinicName", config.getValue());
            else if (config.getProperty().contains("Environment"))
                configData.put("env", config.getValue());
            else if (config.getProperty().contains("Browser"))
                configData.put("browser", config.getValue());
            else if (config.getProperty().contains("Patient App"))
                configData.put("HH", config.getValue());
            else if (config.getProperty().contains("HCP App"))
                configData.put("HCP", config.getValue());
            else if (config.getProperty().contains("Clinician"))
                configData.put("clinicianMail", config.getValue());
            else if (config.getProperty().contains("Nurse"))
                configData.put("nurseMail", config.getValue());
            else if (config.getProperty().contains("Primary patient"))
                configData.put("primaryPatientMRN", config.getValue());
            else if (config.getProperty().contains("Secondary patient"))
                configData.put("secondaryPatientMRN", config.getValue());
            else if (config.getProperty().contains("Unassigned patient"))
                configData.put("unassignedPatientMRN", config.getValue());
        }

        ExcelRepo<com.biofourmis.biovitals.constants.configuration.Patient> patientExcelRepo =
                new BioImpl<>(path, com.biofourmis.biovitals.constants.configuration.Patient.class);
        com.biofourmis.biovitals.constants.configuration.Patient patient =
                patientExcelRepo.findOne("MRN", configData.get("primaryPatientMRN"));

        configData.put("primaryPatientName", patient.getPatientName());
        patient = patientExcelRepo.findOne("MRN", configData.get("secondaryPatientMRN"));
        configData.put("secondaryPatientName", patient.getPatientName());
        patient = patientExcelRepo.findOne("MRN", configData.get("unassignedPatientMRN"));
        configData.put("unassignedPatientName", patient.getPatientName());

        for (String name : suiteParams) {
            String value;
            if ((value = System.getProperty(name)) != null)
                configData.put(name, value);
            else if (!configData.containsKey(name))
                configData.put(name, "null");
        }

        testConfig = new ObjectMapper().convertValue(configData, TestConfig.class);
        return testConfig;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        CustomConfig.context = applicationContext;
    }

    public static <T extends Object> T getBean(Class<T> beanClass) {
        return context.getBean(beanClass);
    }

}
