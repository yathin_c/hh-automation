package com.biofourmis.biovitals.constants.scripts;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;

@Data
public class ClinicInfo {
    @JsonAlias({"clinicName","Name"})
    private String clinicName;
    @JsonAlias({"menu","LHN Menu"})
    private String menu;
    @JsonAlias({"activities","Activities"})
    private String activities;
    @JsonAlias({"activityTimeline","Activity Timeline"})
    private Integer activityTimeline;
    @JsonAlias({"defaultThreshold","Default Thresholds"})
    private String defaultThreshold;
    @JsonAlias({"alerts","Alerts"})
    private String alerts;
    @JsonAlias({"server","Server"})
    private String server;
    @JsonAlias({"addPatientSteps","Add Patient Steps"})
    private String addPatientSteps;

}
