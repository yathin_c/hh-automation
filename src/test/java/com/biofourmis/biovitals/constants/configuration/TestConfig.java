package com.biofourmis.biovitals.constants.configuration;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TestConfig {

    private String clinicName;
    @JsonAlias({"environment", "env" })
    private String environment;
    private String browser;
    @JsonAlias({"patientAppPlatform", "HH" })
    private String patientAppPlatform;
    @JsonAlias({"hcpAppPlatform", "HCP" })
    private String hcpAppPlatform;
    private String primaryPatientMRN;
    private String primaryPatientName;
    private String secondaryPatientMRN;
    private String secondaryPatientName;
    private String unassignedPatientMRN;
    private String unassignedPatientName;
    private String clinicianMail;
    private String clinicianName;
    private String clinicianPass;
    private String clinicianRole;
    private String nurseMail;
    private String nurseName;
    private String nursePass;
    private String nurseRole;

}
