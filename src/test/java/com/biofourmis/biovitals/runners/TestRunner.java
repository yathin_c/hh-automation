package com.biofourmis.biovitals.runners;

import com.biofourmis.biovitals.drivers.AppiumDriverBuilder;
import com.biofourmis.biovitals.utilities.CustomUtils;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.testng.annotations.*;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

@CucumberOptions(
        features = {"src/test/resources/features/"},
        plugin = {"json:target/cucumber/cucumber.json",
                "com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"},
        glue = {"com.biofourmis.biovitals.stepdefinitions"},
        tags = "@Workflow"
)
@Log4j
public class TestRunner extends AbstractTestNGCucumberTests {

    @Override
    @DataProvider(parallel = true)
    public Object[][] scenarios() {
        return super.scenarios();
    }

    @AfterTest
    public void afterTest() {
        AppiumDriverBuilder builder = AppiumDriverBuilder.getBean(AppiumDriverBuilder.class);
        if (builder != null)
            builder.stopServer();
    }

    @AfterSuite
    public void afterSuite() throws IOException {
        CustomUtils customUtils = new CustomUtils(null);
        customUtils.sendReport();
    }
}
