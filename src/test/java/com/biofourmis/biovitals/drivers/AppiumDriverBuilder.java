package com.biofourmis.biovitals.drivers;

import com.biofourmis.biovitals.configs.AppConfig;
import com.biofourmis.biovitals.configs.CustomConfig;
import com.biofourmis.biovitals.constants.DeviceConstants;
import com.github.javafaker.Faker;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.IOSMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;
import java.util.Locale;

/**
 * This class is read only do not modify anything here
 */
@Component
@RequiredArgsConstructor
@Log4j
public class AppiumDriverBuilder implements ApplicationContextAware {

    private final AppConfig appConfig;
    private final CustomConfig customConfig;
    private final Faker faker;
    protected AppiumDriverLocalService appium = null;
    private static final Object lock = new Object();

    public WebDriver getAppiumDriver(DeviceConstants deviceConstants, String appName, boolean noReset) throws IOException {
        try {
            appConfig.setAppCapabilities(appName);
            DesiredCapabilities desiredCapabilities = setDesiredCapabilities(deviceConstants, noReset);
            URL url = null;
            if (appConfig.isAppiumAutoStart()) {
                startAppiumServer();
                url = new URL(appium.getUrl().toString());
            } else
                url = new URL(String.format("http://%s:%d/wd/hub", appConfig.getAppiumIP(), appConfig.getAppiumPort()));

            String platform =deviceConstants.getDevicePlatform();
            if (platform.equalsIgnoreCase("android") || platform.equalsIgnoreCase("tab"))
                return new AndroidDriver<MobileElement>(url, desiredCapabilities);
            else if (platform.equalsIgnoreCase("ios") || platform.equalsIgnoreCase("ipad"))
                return new IOSDriver<MobileElement>(url, desiredCapabilities);
            else
                throw new RuntimeException("This platform not supported yet.[" + platform + "]");

        } catch (Exception e) {
            log.error("Failed while setup appium driver for the device." + e.getLocalizedMessage());
            if (!appConfig.isAppiumAutoStart())
                log.warn("According to properties file, an appium server should be running on port : " + appConfig.getAppiumPort());
            throw e;
        }
    }


    private DesiredCapabilities setDesiredCapabilities(DeviceConstants deviceConstants, boolean noReset) {
        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setCapability(MobileCapabilityType.UDID, deviceConstants.getUdid());
        desiredCapabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, deviceConstants.getProductVersion());
        desiredCapabilities.setCapability(MobileCapabilityType.NO_RESET, noReset);
        desiredCapabilities.setCapability(MobileCapabilityType.DEVICE_NAME, deviceConstants.getModelNumber());
        desiredCapabilities.setCapability("systemPort", faker.number().numberBetween(8200, 8299));
        desiredCapabilities.setCapability("newCommandTimeout", 10000);
        String platform = deviceConstants.getDevicePlatform();
        if (platform.toLowerCase(Locale.ROOT).contains("ios") || platform.toLowerCase(Locale.ROOT).contains("ipad")) {
            desiredCapabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "ios");
            desiredCapabilities.setCapability("xcodeOrgId", "3LKN6PPU74");
            desiredCapabilities.setCapability("xcodeSigningId", "iPhone Developer");
            desiredCapabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCuiTest");
            desiredCapabilities.setCapability(IOSMobileCapabilityType.BUNDLE_ID, appConfig.getAppPackage());
            desiredCapabilities.setCapability("useNewWDA", false);
        } else if (platform.equalsIgnoreCase("android") || platform.toLowerCase(Locale.ROOT).contains("tab")) {
            desiredCapabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "android");
            desiredCapabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "UIAutomator2");
            desiredCapabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, appConfig.getAppPackage());
            desiredCapabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, appConfig.getAppActivity());
        } else {
            throw new RuntimeException(deviceConstants.getDevicePlatform() + " is not supported yet.");
        }
        return desiredCapabilities;
    }

    private AppiumDriverLocalService startAppiumServer() {
        synchronized (lock) {
            if (appium != null)
                return appium;
            try {
                AppiumServiceBuilder builder = new AppiumServiceBuilder().withArgument(GeneralServerFlag.SESSION_OVERRIDE);
                appium = builder.usingAnyFreePort().build();
                if (!appConfig.isAppiumServerLogsEnable())
                    appium.clearOutPutStreams();
                appium.start();
                log.info("Appium server has been started");
                return appium;
            } catch (Exception exception) {
                log.error("Failed to start Appium server. " + exception.fillInStackTrace().getCause());
                throw exception;
            }
        }
    }

    public void stopServer() {
        try {
            if (appConfig.isAppiumAutoStart()) {
                if (appium != null)
                    appium.stop();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static ApplicationContext context;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        AppiumDriverBuilder.context = applicationContext;
    }

    public static <T extends Object> T getBean(Class<T> beanClass) {
        if (context != null)
            return context.getBean(beanClass);
        return null;
    }
}
