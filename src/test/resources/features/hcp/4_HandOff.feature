Feature: Hand off patient feature HCP App

  Background:
    Given Launch the browser
    Given Login into dashboard
    Given Add a new patient
    Given Launch the "HCP" App with reset
    Then Login into HCP app

  Scenario: To verify Create and Edit a HandOff in HCP app
    Then Search patient in HCP APP and open Handoff patient
    Then Verify add Hand Off with "Stable" Illness severity in HCP app
    Then Search patient in HCP APP and open Handoff patient
    And Verify edit Hand Off with "Watcher" Illness severity in HCP app
    Then Search patient in HCP APP and open Handoff patient
    Then Verify edit Hand Off with "NoChange" Illness severity in HCP app