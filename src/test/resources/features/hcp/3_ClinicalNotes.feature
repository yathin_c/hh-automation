Feature: Clinical Notes feature HCP App

  Background:
    Given Launch the browser
    Given Login into dashboard
    Given Add a new patient
    Given Launch the "HCP" App with reset
    Then Login into HCP app

  Scenario: To verify Add, Update and Delete a Clinical note and verify same in HCP App
    Then Navigate to patient under my patients
    And Verify Add a note
    And Search patient in HCP APP and open Clinical notes
    Then Verify added note displayed in HCP App
    Then Verify update a note
    Then Verify updated note displayed in HCP App
    And Verify Search a note
    Then Verify Delete a note
    Then Verify deleted note not displayed in HCP App