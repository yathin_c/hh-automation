Feature: HCP Authentication

  Background:
    Given Launch the "HCP" App with reset

  Scenario: Verify HCP APP login button enable / disable
    Then Verify Login button is disable without filling email and password
    And Verify Login button is disable after filling email only
    And Verify Login button is disable after filling password only
    Then Verify Login button is enable after filling email and password

  Scenario: Verify Login with invalid credentials
    Then Enter invalid email and password
    And Verify invalid user password message

  Scenario: Verify Login with valid credentials
    Then Enter valid email and password
    And Verify login successfully


