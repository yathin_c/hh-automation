Feature: Tasks HCP App


  Background:
    Given Launch the "HCP" App with noReset

  Scenario: Add
    Given Launch the browser
    And Login into dashboard
    Then Add a new patient

  Scenario: To verify Add, Edit, Comment and Complete a task with required fields on patient details page
    Then Search patient name under tasks
    And Verify create new task in HCP app with Required fields
    Then Verify comment on created task in HCP app
    And Verify edit a task in HCP app with Required fields
    Then Verify complete a task in HCP app
    And Verify comment on completed task in HCP app
    Then Verify re-open completed task in HCP app
    And Verify comment on re-opened task in HCP app
    Then Verify edit re-open task in HCP app with All fields
    Then Verify complete a re-open task in HCP app

  Scenario: To verify Add, Edit, Comment and Complete a task with all fields on patient details page
    Then Search patient name under tasks
    And Verify create new task in HCP app with All fields
    Then Verify comment on created task in HCP app
    And Verify edit a task in HCP app with All fields
    Then Verify complete a task in HCP app
    And Verify comment on completed task in HCP app
    Then Verify re-open completed task in HCP app
    And Verify comment on re-opened task in HCP app
    Then Verify edit re-open task in HCP app with Required fields
    Then Verify complete a re-open task in HCP app


