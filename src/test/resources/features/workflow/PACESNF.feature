@PACESNF
Feature: PACESNF workflow test cases execution

  Background:
    Given Launch the browser

   ## Login
  Scenario: Verify texts displayed on Login page
    Then Verify login screen texts

  Scenario: Verify the functionality of Login button when valid email and password is entered.
    Then Verify caregiver login successfully
    And Logout user

  Scenario: Verify login when user enters email in uppercase
    Then Verify caregiver login successfully when the username in uppercase
    And Logout user

  Scenario: Verify if login button is enabled when valid email and password fields are entered
    When Enter the username and password
    Then Verify the login button should be enabled

  Scenario: Verify if login button is disabled
    Then Verify disabled login button

  Scenario: Verify invalid format of email address and error message
    Then Verify invalid username format
      | random    |
      | random@   |
      | 123       |
      | 123@      |
      | @mail.com |

  Scenario: Verify invalid format of password and error message
    Then Verify invalid password format
      | 12        |
      | Car       |
      | C@        |
      | Care@care |
      | care@1234 |
      | Care12345 |

  Scenario: Verify email required message
    Then Verify "email" required i.e. "Email address is required"

  Scenario: Verify password required message
    Then Verify "password" required i.e. "Password is required"

  Scenario: Verify if the entered password characters are masked when mask password button (or eye button) is selected
    When Enter the username and password
    Then Verify the password should be in masked form

  Scenario: Verify if the entered password characters are displayed when unmask password button is selected
    When Enter the username and password
    And Click on the eye button of password and verify the password

  Scenario: Verify the login functionality by entering invalid email and password
    When Enter the username as "Invalid@mail.com" and password as "Invalid@Done"
    And Click on the login button
    Then Verify the error message after invalid email and password

    # LHN Menu
  Scenario: To verify navigation on LHN menus and respected pages
    And Login into dashboard
    Then Verify LHN menu options displayed
      | My patients  |
      | All patients |
      | Discharged   |
      | Tasks        |
      | Inventory    |
    And Verify each menu is clickable and should navigate to respected page

    ## Add Patient
  Scenario: To verify Add Patient button and wizard
    And Login into dashboard
    Then Verify Add Patient button and wizard

  Scenario: To verify screens for Add Patient flow and Mandatory fields error message for invalid values
    And Login into dashboard
    Then Verify mandatory fields message

  Scenario: To verify Cancel option without filling any field
    And Login into dashboard
    Then Verify flow should terminate with cancel info pop-up

  Scenario: To verify Cancel option after filling details till [Personal information] step
    And Login into dashboard
    Then Verify after accepting cancel patient wizard, patient should not be created when details filled till Personal information

  Scenario: To verify Cancel option after filling details till [Medical information] step
    And Login into dashboard
    Then Verify after accepting cancel patient wizard, patient should not be created when details filled till Medical information

  Scenario: To verify Cancel option after filling details till [Care team] step
    And Login into dashboard
    Then Verify after accepting cancel patient wizard, patient should not be created when details filled till Care team

  Scenario: To verify Cancel option after filling details till [Equipment information] step
    And Login into dashboard
    Then Verify after accepting cancel patient wizard, patient should not be created when details filled till Equipment information

  Scenario: To verify Cancel option after filling mandatory details only
    And Login into dashboard
    And Fill patient mandatory details
    Then Verify after accepting cancel patient wizard, patient should not be created

  Scenario: To verify Add Patient with accept Cancel option on review page
    And Login into dashboard
    And Open Add Patient wizard
    Then Fill patient details of section Personal information
    And Click on Cancel and decline the info pop-up
    Then Fill patient details of section Medical information
    And Click on Cancel and decline the info pop-up
    Then Fill patient details of section Care team
    And Click on Cancel and decline the info pop-up
    Then Fill patient details of section Equipment information
    And Click on Cancel and decline the info pop-up
    Then Fill patient details of section Review
    And Click on Cancel and accept the info pop-up
    And Verify patient should not be created with given MNR Number

  Scenario: To verify Add Patient with decline Cancel option and save patient
    And Login into dashboard
    And Open Add Patient wizard
    Then Fill patient details of section Personal information
    And Click on Cancel and decline the info pop-up
    Then Fill patient details of section Medical information
    And Click on Cancel and decline the info pop-up
    Then Fill patient details of section Care team
    And Click on Cancel and decline the info pop-up
    Then Fill patient details of section Equipment information
    And Click on Cancel and decline the info pop-up
    Then Fill patient details of section Review
    And Click on Cancel and decline the info pop-up
    Then Verify all field details on review page
    And Save patient
    Then Verify patient created successfully

  Scenario: To verify Add Patient with Mandatory fields only
    And Login into dashboard
    And Add patient mandatory details
    Then Verify mandatory field details on review page
    And Save patient
    Then Verify patient created successfully

  ## Add Patient and login
  Scenario: Verify Add Patient with all details
    And Login into dashboard
    And Open Add Patient wizard
    Then Fill patient details of section Personal information
    Then Fill patient details of section Medical information
    Then Fill patient details of section Care team
    Then Fill patient details of section Equipment information
    Then Fill patient details of section Review
    Then Verify all field details on review page
    And Save patient
    Then Verify new patient tutorial before login

 ## Config ##
  Scenario: Add Patient for workflow further execution
    Then Launch the "HH" App with reset
    And Login into dashboard
    Then Add a new patient
    And Scan QR and login into app

    ## Clinical notes ##
  @TestCaseKey=HOHP-T982 @TestCaseKey=HOHP-T983
  Scenario: To verify Clinical Notes page and mandatory fields error messages
    And Navigate to patient under my patients
    Then Verify clinical notes page
    And Verify empty fields error message

  @TestCaseKey=HOHP-T983 @TestCaseKey=HOHP-T984 @TestCaseKey=HOHP-T985
  Scenario: To verify Add, Update, Delete and Search a Clinical note
    Then Navigate to patient under my patients
    And Verify Add a note
    Then Verify update a note
    Then Verify Delete a note

  @TestCaseKey=HOHP-T986 @TestCaseKey=HOHP-T987 @TestCaseKey=HOHP-T988 @TestCaseKey=HOHP-T999 @TestCaseKey=HOHP-T990
  Scenario:Verify Search, Calendar and Caregiver filter options
    Then Navigate to patient under my patients
    And Verify Add a note
    Then Verify Search filter input field
    And Verify Calendar filter option
    And Verify Caregiver filter option
    And Verify Calendar and Caregiver filter options together and Clear all

  @TestCaseKey=HOHP-T991
  Scenario: To verify Add, Update, Delete and Search a Clinical note
    Then Navigate to patient under all patients
    And Verify Add a note
    Then Verify update a note
    Then Verify Delete a note

  @TestCaseKey=HOHP-T992
  Scenario: Verify that caregiver is not able to edit/delete clinical notes of the patient which have been created by other caregivers
    Then Navigate to patient under my patients
    And Verify Add a note
    Then Logout user
    And Login nurse into dashboard
    Then Navigate to patient under my patients
    And Verify edit and delete option not visible

    # Care Plan
  Scenario: To verify Add and delete a Care Plan from HCP web
    And Login into dashboard
    Then Navigate to patient under my patients
    Then Verify Add a Care plan
    And Verify Delete a Care plan

  Scenario: To verify Add and Delete Care Plan on Patient app
    And Launch the "HH" App with noReset
    Then Login into dashboard
    Then Navigate to patient under my patients
    Then Verify Add a Care plan
    Then Verify Added Care plan displayed in Patient App
    And Verify Delete a Care plan
    Then Verify Deleted Care plan displayed in Patient App

  Scenario: To verify Activity Timeline and filter options under my patients
    And Login into dashboard
    Then Navigate to patient under my patients
    Then Verify Activity Page details of clinic

  Scenario: To verify Add, Update and Delete an Activity
    And Launch the "HH" App with noReset
    And Login into dashboard
    Then Navigate to patient under my patients
    Then Verify Add an Activity "Drink water"
      | No of Glass |
      | 2           |
    And Verify added activity "Drink water" in patient app
    Then Verify Update an Activity "Drink water"
    And Verify updated activity "Drink water" in patient app
    Then Verify Delete an Activity "Drink water"
    And Verify deleted activity "Drink water" in patient app

    ## Diary Note
  Scenario: To verify diary screen, add and update note with required fields
    And Launch the "HH" App with noReset
    And Login into dashboard
    Then Navigate to Diary and verify components in patient app
    And Verify add a new diary note with required fields in patient app
    Then Verify update diary note with required fields in patient app

  Scenario: To verify diary screen, add and update note with all fields
    And Launch the "HH" App with noReset
    And Login into dashboard
    Then Navigate to Diary and verify components in patient app
    And Verify add a new diary note with all fields in patient app
    Then Verify update diary note with all fields in patient app

  Scenario: To verify diary screen, add and update note with required fields in HCP web
    And Launch the "HH" App with noReset
    And Login into dashboard
    Then Navigate to Diary and verify components in patient app
    Then Verify add a new diary note with required fields in patient app
    Then Navigate to patient under my patients
    Then Verify added diary note in with required fields hcp web
    Then Verify update diary note with required fields in patient app
    And Verify updated diary note in with required fields hcp web

  Scenario: To verify diary screen, add and update note with all fields in HCP web
    And Launch the "HH" App with noReset
    And Login into dashboard
    Then Navigate to Diary and verify components in patient app
    Then Verify add a new diary note with all fields in patient app
    Then Navigate to patient under my patients
    And Verify added diary note in with all fields hcp web
    Then Verify update diary note with all fields in patient app
    And Verify updated diary note in with all fields hcp web

  Scenario: To verify respond a Diary Note from HCP web
    And Launch the "HH" App with noReset
    And Login into dashboard
    Then Navigate to Diary and verify components in patient app
    Then Verify add a new diary note with required fields in patient app
    Then Navigate to patient under my patients
    Then Verify respond diary note from hcp web
    And Verify response on diary note in patient app

# Contact
  Scenario: Verify Send message to patient
    And Launch the "HH" App with noReset
    And Login into dashboard
    Then Search patient under my patients
    And Verify navigate to chat section by clicking on chat icon on patient home screen
    Then Verify sending 1 text message from HCP web dashboard
    And Verify text message pop-up displayed in patient
    Then Verify chat count badge in patient app
    And Verify received text message from HCP clinician

  Scenario: Verify send multiple text message to patient form HCP web
    And Launch the "HH" App with noReset
    And Login into dashboard
    Then Search patient under my patients
    And Verify navigate to chat section by clicking on chat icon on patient home screen
    Then Verify sending 2 text message from HCP web dashboard
    And Verify text message pop-up displayed in patient
    Then Verify chat count badge in patient app
    And Verify received text message from HCP clinician

  Scenario: Verify Send message to patient
    And Launch the "HH" App with noReset
    And Login into dashboard
    Then Verify sending 1 text message from Patient app to clinician
    Then Search patient under my patients
    And Verify badge count and chat on HCP web dashboard

  Scenario: Verify Send multiple message to patient
    And Launch the "HH" App with noReset
    And Login into dashboard
    Then Verify sending 2 text message from Patient app to clinician
    Then Search patient under my patients
    And Verify badge count and chat on HCP web dashboard

  Scenario: Verify make audio call from patient app and reject on HCP web dashboard
    And Launch the "HH" App with noReset
    And Login into dashboard
    Then Navigate to clinician contact
    And Start Audio call from patient app
    Then Receive Audio call on HCP web dashboard
    And Verify Reject call from HCP web dashboard
    Then Verify call Rejected successfully in patient app

  Scenario: Verify make audio call from patient app and receive on HCP web dashboard
    And Launch the "HH" App with noReset
    And Login into dashboard
    Then Navigate to clinician contact
    And Start Audio call from patient app
    Then Receive Audio call on HCP web dashboard
    And Verify Accept call from HCP web dashboard
    Then Verify call Accepted successfully in patient app
    Then Verify End call from HCP Web dashboard
    And Verify Call ended successfully in patient app

  Scenario: Verify make video call from patient app and reject on HCP web dashboard
    And Launch the "HH" App with noReset
    And Login into dashboard
    Then Navigate to clinician contact
    And Start Video call from patient app
    Then Receive Video call on HCP web dashboard
    And Verify Reject call from HCP web dashboard
    Then Verify call Rejected successfully in patient app

  Scenario: Verify make video call from patient app and receive on HCP web dashboard
    And Launch the "HH" App with noReset
    And Login into dashboard
    Then Navigate to clinician contact
    And Start Video call from patient app
    Then Receive Video call on HCP web dashboard
    And Verify Accept call from HCP web dashboard
    Then Verify call Accepted successfully in patient app
    Then Verify End call from HCP Web dashboard
    And Verify Call ended successfully in patient app

  Scenario: Verify make audio call from HCP web dashboard and reject on patient app
    And Launch the "HH" App with noReset
    And Login into dashboard
    Then Navigate to patient under my patients
    And Verify start Audio call on patient details screen
    Then Receive Audio call on patient app
    And Verify Reject call from patient app
    Then Verify call Rejected successfully in HCP web dashboard

  Scenario: Verify make audio call from HCP web dashboard and receive on patient app
    And Launch the "HH" App with noReset
    And Login into dashboard
    Then Navigate to patient under my patients
    And Verify start Audio call on patient details screen
    Then Receive Audio call on patient app
    And Verify Accept call from patient app
    Then Verify call Accepted successfully in HCP web dashboard
    Then Verify End call from patient app
    And Verify Call ended successfully in HCP Web dashboard

  Scenario: Verify make video call from HCP web dashboard and reject on patient app
    And Launch the "HH" App with noReset
    And Login into dashboard
    Then Navigate to patient under my patients
    And Verify start Video call on patient details screen
    Then Receive Video call on patient app
    And Verify Reject call from patient app
    Then Verify call Rejected successfully in HCP web dashboard

  Scenario: Verify make video call from HCP web dashboard and receive on patient app
    And Launch the "HH" App with noReset
    And Login into dashboard
    Then Navigate to patient under my patients
    And Verify start Video call on patient details screen
    Then Receive Video call on patient app
    And Verify Accept call from patient app
    Then Verify call Accepted successfully in HCP web dashboard
    Then Verify End call from patient app
    And Verify Call ended successfully in HCP Web dashboard

    ## Hand off ##
  @TestCaseKey=HOHP-T676 @TestCaseKey=HOHP-T677 @TestCaseKey=HOHP-T678 @TestCasekey=HOHP-T679 @TestCasekey=HOHP-T680 @TestCaseKey=HOHP-T684 @TestCaseKey=HOHP-T685 @TestCasekey=HOHP-T686
  Scenario: Check all section is mandatory for handoff submission
    Then Search patient under my patients
    And Verify handoff container details and mandatory sections
    Then Verify adding HandOff with "Stable" Illness severity
    And Navigate to patient under my patients
    Then Verify HandOff details after successfully HandOff patient
    And Logout user
    Then Login nurse into dashboard
    And Navigate to patient under my patients
    Then Verify HandOff details after successfully HandOff patient
    And Search patient under my patients
    Then Remove this patient from my patients
    And Navigate to removed patient under all patients and Verify HandOff details should be read only mode
    And Logout user
    Then Login into dashboard
    And Navigate to patient under my patients
    Then Update HandOff details with "Unstable" Illness severity
    And Verify decline reset No change priory visit
    Then Verify HandOff details after successfully HandOff patient
    Then Update HandOff details with "Watcher" Illness severity
    And Verify accept reset No change priory visit
    Then Verify Nothing should change after decline reset

  @TestCasekey=HOHP-T681 @TestCaseKey=HOHP-T682 @TestCaseKey=HOHP-T683
  Scenario: To verify multiple and customize of awareness
    And Navigate to patient under my patients
    And Verify all Illness severity are selectable and highlighted after selected
    Then Verify multi select awareness options
    And Verify customize option of awareness

    ## Tasks
  Scenario: To verify task container and cancel options for creation of task
    And Login into dashboard
    Then Navigate to patient under my patients
    And Verify closing task container
    Then Verify Cancel button functionality

  Scenario: To verify Add, Edit, Comment and Complete a task with required fields on patient details page
    And Login into dashboard
    Then Navigate to patient under my patients
    And Verify create new task with Required fields
    Then Verify comment on created task
    And Verify edit a task with Required fields
    Then Verify complete a task
    And Verify comment on completed task
    Then Verify re-open completed task
    And Verify comment on re-opened task
    Then Verify edit re-open task with All fields
    Then Verify complete a re-open task

  Scenario: To verify Add, Edit, Comment and Complete a task with all fields on patient details page
    And Login into dashboard
    Then Navigate to patient under my patients
    And Verify create new task with All fields
    Then Verify comment on created task
    And Verify edit a task and remove optional fields
    Then Verify complete a task
    And Verify comment on completed task
    Then Verify re-open completed task
    And Verify comment on re-opened task
    Then Verify edit re-open task with All fields
    Then Verify complete a re-open task

  Scenario: To verify Add, Edit, Comment and Complete a task with required/all fields from tasks LHN menu
    And Login into dashboard
    Then Navigate to Tasks tab on HCP web dashboard
    And Verify create new task with Required fields on tasks tab
    Then Verify comment on created task on tasks tab
    And Verify edit a task with Required fields on tasks tab
    Then Verify complete a task on task tab
    And Verify comment on completed task on tasks tab
    Then Verify re-open completed task on tasks tab
    And Verify comment on re-opened task on tasks tab
    Then Verify edit re-open task with All fields on tasks tab
    Then Verify complete a re-open task on task tab

    ##Setting theres hold values
  Scenario: Verify setting theres hold values for patient
    And Login into dashboard
    Then Navigate to patient under my patients
    And Verify setting therese hold values
    Then Verify reset therese hold values

    ##Inventory
  Scenario: To verify Add a device in inventory
    And Login into dashboard
    Then Navigate to Inventory tab on HCP web dashboard
    And Add new device "AED" in inventory
    And Assign device to caregiver
    And Delete device

    ##Patient app
  Scenario: Validate settings screen and Logout into HH Patient app
    And Launch the "HH" App with noReset
    Then Navigate to Hidden settings screen
    And Verify Hidden setting screen elements

   ## Patient app
  Scenario: Login/Logout into HH Patient app
    And Launch the "HH" App with noReset
    Then Logout patient

   ## Add Remove Patient
  Scenario: To verify Add/Remove patient from my patients
    And Login into dashboard
    Then Verify add and remove patient form my patients