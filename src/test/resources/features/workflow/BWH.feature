@BWH
Feature:Validate BWH workflow

  Background:
    Given Launch the browser
    And Launch the "HH" App with reset

  Scenario: Verify texts displayed on Login page
    Then Verify login screen texts

  Scenario: Verify the functionality of Login button when valid email and password is entered.
    Then Verify caregiver login successfully
    And Logout user

  Scenario: Verify the login functionality by entering invalid email and password
    When Enter the username as "Invalid@mail.com" and password as "Invalid@Care"
    And Click on the login button
    Then Verify the error message after invalid email and password

  Scenario: Verify available LHN menu and theirs respected pages
    And Login into dashboard
    Then Verify LHN menu options displayed
      | My patients  |
      | All patients |
      | Discharged   |
      | Tasks        |
      | Inventory    |
    And Verify each menu is clickable and should navigate to respected page

  Scenario: Verify Add Patient with Mandatory fields only
    And Login into dashboard
    And Add patient mandatory details
    Then Verify mandatory field details on review page
    And Save patient

  Scenario: Verify Add Patient with all details
    And Login into dashboard
    And Open Add Patient wizard
    Then Fill patient details of section Personal information
    Then Fill patient details of section Medical information
    Then Fill patient details of section Care team
    Then Fill patient details of section Equipment information
    Then Fill patient details of section Review
    Then Verify all field details on review page
    And Save patient
    Then Scan Created patient QR and login into app
    Then Verify new patient tutorial after login

  Scenario: Verify Update and Delete Clinical note
    Then Login into dashboard
    And Navigate to patient "MRN1000" under my patients
    Then Verify Add a note
    Then Verify update a note
    And Verify Search a note
    Then Verify Delete a note

  Scenario: Verify Add and Delete Care Plan
    Then Login into dashboard
    Then Open QR of Patient "MRN1000"
    And Scan QR and login into app
    And Navigate to patient "MRN1000" under my patients
    Then Verify Add a Care plan
    Then Verify Added Care plan displayed in Patient App
    And Verify Delete a Care plan
    Then Verify Deleted Care plan displayed in Patient App

  Scenario: Verify Activity Page details on Activities under my patients
    Then Login into dashboard
    And Navigate to patient "MRN1000" under my patients
    Then Verify Activity Page details of clinic "bwh"
      | Use inhaler                |
      | Use nebulizer              |
      | Inject insulin             |
      | Check blood pressure       |
      | Check weight               |
      | Check temporal temperature |
      | Drink water                |
      | Elevate legs               |
      | Elevate arm                |
      | Home hospital team visit   |
      | Physical therapy visit     |
      | Place hot pack             |
      | Place cold pack            |
      | Walk                       |
      | Check blood glucose        |

  Scenario: Verify Add, Update and Delete an Activity
    And Login into dashboard
    Then Open QR of Patient "MRN1000"
    And Scan QR and login into app
    And Navigate to patient "MRN1000" under my patients
    Then Verify Add an Activity "Drink water"
      | No of Glass |
      | 2           |
    And Verify added activity "Drink water" in patient app
    Then Verify Update an Activity "Drink water"
    And Verify updated activity "Drink water" in patient app
    Then Verify Delete an Activity "Drink water"
    And Verify deleted activity "Drink water" in patient app

  Scenario: Login/Logout into HH Patient app
    Then Open QR of Patient "MRN1000"
    And Scan QR and login into app
    Then Logout patient

  Scenario: Verify Add and Update a Diary Note with required fields
    And Login into dashboard
    Then Open QR of Patient "MRN1000"
    And Scan QR and login into app
    Then Navigate to Diary and verify components in patient app
    Then Verify add a new diary note with required fields in patient app
    And Navigate to patient "MRN1000" under my patients
    Then Verify added diary note in with required fields hcp web
    Then Verify update diary note with required fields in patient app
    And Verify updated diary note in with required fields hcp web

  Scenario: Verify Add and Update a Diary Note with all fields
    And Login into dashboard
    Then Open QR of Patient "MRN1000"
    And Scan QR and login into app
    Then Navigate to Diary and verify components in patient app
    Then Verify add a new diary note with all fields in patient app
    And Navigate to patient "MRN1000" under my patients
    And Verify added diary note in with all fields hcp web
    Then Verify update diary note with all fields in patient app
    And Verify updated diary note in with all fields hcp web

  Scenario: Verify respond a Diary Note from HCP web
    And Login into dashboard
    Then Open QR of Patient "MRN1000"
    And Scan QR and login into app
    Then Navigate to Diary and verify components in patient app
    Then Verify add a new diary note with required fields in patient app
    And Navigate to patient "MRN1000" under my patients
    Then Verify respond diary note from hcp web
    And Verify response on diary note in patient app

  Scenario: Validate settings screen
    And Login into dashboard
    Then Open QR of Patient "MRN1000"
    And Scan QR and login into app
    Then Navigate to Hidden settings screen
    And Verify Hidden setting screen elements

  Scenario: Verify make audio call from patient app and reject on HCP web dashboard
    And Login into dashboard
    Then Open QR of Patient "MRN1000"
    And Scan QR and login into app
    Then Navigate to "Himanshu" clinician contact
    And Start Audio call from patient app
    Then Receive Audio call on HCP web dashboard
    And Verify Reject call from HCP web dashboard
    Then Verify call Rejected successfully in patient app

  Scenario: Verify make audio call from patient app and receive on HCP web dashboard
    And Login into dashboard
    Then Open QR of Patient "MRN1000"
    And Scan QR and login into app
    Then Navigate to "Himanshu" clinician contact
    And Start Audio call from patient app
    Then Receive Audio call on HCP web dashboard
    And Verify Accept call from HCP web dashboard
    Then Verify call Accepted successfully in patient app
    Then Verify End call from HCP Web dashboard
    And Verify Call ended successfully in patient app

  Scenario: Verify make video call from patient app and reject on HCP web dashboard
    And Login into dashboard
    Then Open QR of Patient "MRN1000"
    And Scan QR and login into app
    Then Navigate to "Himanshu" clinician contact
    And Start Video call from patient app
    Then Receive Video call on HCP web dashboard
    And Verify Reject call from HCP web dashboard
    Then Verify call Rejected successfully in patient app

  Scenario: Verify make video call from patient app and receive on HCP web dashboard
    And Login into dashboard
    Then Open QR of Patient "MRN1000"
    And Scan QR and login into app
    Then Navigate to "Himanshu" clinician contact
    And Start Video call from patient app
    Then Receive Video call on HCP web dashboard
    And Verify Accept call from HCP web dashboard
    Then Verify call Accepted successfully in patient app
    Then Verify End call from HCP Web dashboard
    And Verify Call ended successfully in patient app

  Scenario: Verify make audio call from HCP web dashboard and reject on patient app
    And Login into dashboard
    Then Open QR of Patient "MRN1000"
    And Scan QR and login into app
    Then Navigate to patient "MRN1000" under my patients
    And Verify start Audio call on patient details screen
    Then Receive Audio call on patient app
    And Verify Reject call from patient app
    Then Verify call Rejected successfully in HCP web dashboard

  Scenario: Verify make audio call from HCP web dashboard and receive on patient app
    And Login into dashboard
    Then Open QR of Patient "MRN1000"
    And Scan QR and login into app
    Then Navigate to patient "MRN1000" under my patients
    And Verify start Audio call on patient details screen
    Then Receive Audio call on patient app
    And Verify Accept call from patient app
    Then Verify call Accepted successfully in HCP web dashboard
    Then Verify End call from patient app
    And Verify Call ended successfully in HCP Web dashboard

  Scenario: Verify make video call from HCP web dashboard and reject on patient app
    And Login into dashboard
    Then Open QR of Patient "MRN1000"
    And Scan QR and login into app
    Then Navigate to patient "MRN1000" under my patients
    And Verify start Video call on patient details screen
    Then Receive Video call on patient app
    And Verify Reject call from patient app
    Then Verify call Rejected successfully in HCP web dashboard

  Scenario: Verify make video call from HCP web dashboard and receive on patient app
    And Login into dashboard
    Then Open QR of Patient "MRN1000"
    And Scan QR and login into app
    Then Navigate to patient "MRN1000" under my patients
    And Verify start Video call on patient details screen
    Then Receive Video call on patient app
    And Verify Accept call from patient app
    Then Verify call Accepted successfully in HCP web dashboard
    Then Verify End call from patient app
    And Verify Call ended successfully in HCP Web dashboard

  Scenario: Verify Send message to patient
    And Login into dashboard
    Then Open QR of Patient "MRN1000"
    And Scan QR and login into app
    Then Search patient "MRN1000" under my patients
    And Verify navigate to chat section by clicking on chat icon on patient home screen
    Then Verify sending 1 text message from HCP web dashboard
    And Verify text message pop-up displayed in patient
    Then Verify chat count badge in patient app
    And Verify received text message from HCP clinician "Himanshu"

  Scenario: Verify Send message to patient
    And Login into dashboard
    Then Open QR of Patient "MRN1000"
    And Scan QR and login into app
    Then Search patient "MRN1000" under my patients
    And Verify navigate to chat section by clicking on chat icon on patient home screen
    Then Verify sending 2 text message from HCP web dashboard
    And Verify text message pop-up displayed in patient
    Then Verify chat count badge in patient app
    And Verify received text message from HCP clinician "Himanshu"

  Scenario: Verify Send message to patient
    And Login into dashboard
    Then Open QR of Patient "MRN1000"
    And Scan QR and login into app
    Then Verify sending 1 text message from Patient app to clinician "Himanshu"
    Then Search patient "MRN1000" under my patients
    And Verify badge count and chat on HCP web dashboard

  Scenario: Verify Send message to patient
    And Login into dashboard
    Then Open QR of Patient "MRN1000"
    And Scan QR and login into app
    Then Verify sending 2 text message from Patient app to clinician "Himanshu"
    Then Search patient "MRN1000" under my patients
    And Verify badge count and chat on HCP web dashboard

  Scenario: Verify create a HandOff from HCP web dashboard
    Then Login into dashboard
    And Add a new patient
    And Verify add Hand Off with "Stable" Illness severity
    And Verify edit Hand Off with "Watcher" Illness severity

  Scenario: Verify Add a task from task menu on patient page
    Then Login into dashboard
    Then Navigate to patient "MRN1000" under my patients
    And Verify closing task container
    Then Verify Cancel button functionality

  Scenario: Verify Add a task from task menu on patient page
    And Login into dashboard
    Then Navigate to patient "MRN1000" under my patients
    And Verify create new task with Required fields
    Then Verify comment on created task
    And Verify edit a task with Required fields
    Then Verify complete a task
    And Verify comment on completed task
    Then Verify re-open completed task
    And Verify comment on re-opened task
    Then Verify edit re-open task with All fields
    Then Verify complete a re-open task

  Scenario: Verify Add a task from task menu on tasks tab
    And Login into dashboard
    Then Navigate to patient "MRN1000" under my patients
    And Verify create new task with All fields
    Then Verify comment on created task
    And Verify edit a task and remove optional fields
    Then Verify complete a task
    And Verify comment on completed task
    Then Verify re-open completed task
    And Verify comment on re-opened task
    Then Verify edit re-open task with All fields
    Then Verify complete a re-open task

  Scenario: Verify Add a task from task menu on patient page
    And Login into dashboard
    Then Navigate to Tasks tab on HCP web dashboard
    And Verify create new task with Required fields on tasks tab
    Then Verify comment on created task on tasks tab
    And Verify edit a task with Required fields on tasks tab
    Then Verify complete a task
    And Verify comment on completed task on tasks tab
    Then Verify re-open completed task on tasks tab
    And Verify comment on re-opened task on tasks tab
    Then Verify edit re-open task with All fields on tasks tab
    Then Verify complete a re-open task

  Scenario: Verify Add/Remove patient from my patients
    And Login into dashboard
    Then Verify "MRN1001" add and remove patient form my patients
