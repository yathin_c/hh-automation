@Workflow
Feature: Automation workflow test cases execution

  Background:
    Given Launch the browser

        ## Prerequisite ##
  Scenario: Prerequisite
    When Login into dashboard
    Then Add a new patient
    Then Launch the "HH" App with reset
    And Scan QR and login into app

    ## Login Web ##
  Scenario: Verify texts displayed on Login page
    Then Verify login screen texts

  @TestCaseKey=HOHP-T216
  Scenario: Verify the functionality of Login button when valid email and password is entered.
    Then Verify caregiver login successfully
    And Logout user

  Scenario: Verify login when user enters email in uppercase
    Then Verify caregiver login successfully when the username in uppercase
    And Logout user

  Scenario: Verify if login button is enabled when valid email and password fields are entered
    When Enter the username and password
    Then Verify the login button should be enabled

  @TestCaseKey=HOHP-T220
  Scenario: Verify if login button is disabled
    Then Verify disabled login button

  Scenario: Verify invalid format of email address and error message
    Then Verify invalid username format
      | random    |
      | random@   |
      | 123       |
      | 123@      |
      | @mail.com |

  Scenario: Verify invalid format of password and error message
    Then Verify invalid password format
      | 12        |
      | Car       |
      | C@        |
      | Care@care |
      | care@1234 |
      | Care12345 |

  @TestCaseKey=HOHP-T221
  Scenario: Verify email required message
    Then Verify "email" required i.e. "Email address is required"

  @TestCaseKey=HOHP-T221
  Scenario: Verify password required message
    Then Verify "password" required i.e. "Password is required"

  Scenario: Verify if the entered password characters are masked when mask password button (or eye button) is selected
    When Enter the username and password
    Then Verify the password should be in masked form

  Scenario: Verify if the entered password characters are displayed when unmask password button is selected
    When Enter the username and password
    And Click on the eye button of password and verify the password

  @TestCaseKey=HOHP-T230
  Scenario: Verify the login functionality when user enters invalid mail id and password then error message should be displayed saying "your email id and password is not valid, you have 2 login attempts remaining"
    When Enter the username as "testdata@mail.com" and password as "Care@1235"
    And After enter username and password click 6 time on the login button to see error message

  @TestCaseKey=HOHP-T218 @TestCaseKey=HOHP-T237
  Scenario: Verify the login functionality by entering invalid email and password
    When Enter the username as "Invalid@mail.com" and password as "Invalid@Care"
    And Click on the login button
    Then Verify the error message after invalid email and password

  @TestCaseKey=HOHP-T217
  Scenario:Verify that clicking on browser back button after successful login should not take User to logout screen
    Then Verify caregiver login successfully
    And Verify User is not logged out after clicking on Back button

  @TestCaseKey=HOHP-T227
  Scenario:Verify that User is Logged out from old window when User logs intoNew window
    And Login into dashboard
    Then Verify User is logged out from old window after logging into new Window

       ## LHN Menu ##
  Scenario: To verify navigation on LHN menus and respected pages
    When Login into dashboard
    Then Verify LHN menu options displayed
      | My patients  |
      | All patients |
      | Discharged   |
      | Tasks        |
      | Inventory    |
    And Verify each menu is clickable and should navigate to respected page

        ## Add Patient ##
  Scenario: To verify Add Patient button and wizard
    When Login into dashboard
    Then Verify Add Patient button and wizard

  Scenario: To verify screens for Add Patient flow and Mandatory fields error message for invalid values
    When Login into dashboard
    Then Verify mandatory fields message

  Scenario: To verify Cancel option without filling any field
    When Login into dashboard
    Then Verify flow should terminate with cancel info pop-up

  Scenario: To verify Add Patient with Mandatory fields only
    When Login into dashboard
    And Add patient mandatory details
    Then Verify mandatory field details on review page
    And Save patient
    Then Verify patient created successfully

#  Scenario: To verify Add Patient with All fields only
#    When Login into dashboard
#    And Add patient mandatory details
#    Then Verify all field details on review page
#    And Save patient
#    Then Verify patient created successfully

    ## Clinical notes ##
  @TestCaseKey=HOHP-T982 @TestCaseKey=HOHP-T983
  Scenario: To verify Clinical Notes page and mandatory fields error messages
    When Login into dashboard
    And Navigate to patient under my patients
    Then Verify clinical notes page
    And Verify empty fields error message

  @TestCaseKey=HOHP-T983 @TestCaseKey=HOHP-T984 @TestCaseKey=HOHP-T985
  Scenario: To verify Add, Update, Delete and Search a Clinical note
    When Login into dashboard
    Then Navigate to patient under my patients
    And Verify Add a note
    Then Verify update a note
    Then Verify Delete a note

  @TestCaseKey=HOHP-T986 @TestCaseKey=HOHP-T987 @TestCaseKey=HOHP-T988 @TestCaseKey=HOHP-T999 @TestCaseKey=HOHP-T990
  Scenario:Verify Search, Calendar and Caregiver filter options
    When Login into dashboard
    Then Navigate to patient under my patients
    And Verify Add a note
    Then Verify Search filter input field
    And Verify Calendar filter option
    And Verify Caregiver filter option
    And Verify Calendar and Caregiver filter options together and Clear all

  @TestCaseKey=HOHP-T991
  Scenario: To verify Add, Update, Delete and Search a Clinical note
    When Login into dashboard
    Then Navigate to patient under all patients
    And Verify Add a note
    Then Verify update a note
    Then Verify Delete a note

  @TestCaseKey=HOHP-T992
  Scenario: Verify that caregiver is not able to edit/delete clinical notes of the patient which have been created by other caregivers
    When Login into dashboard
    Then Navigate to patient under my patients
    And Verify Add a note
    Then Logout user
    And Login nurse into dashboard
    Then Navigate to patient under my patients
    And Verify edit and delete option not visible

      ## Activities and Care plan ##
  @TestCaseKey=HOHP-T294
  Scenario: To verify Care Plan container under Activities and Care plan tab
    When Login into dashboard
    And Navigate to patient under my patients
    Then Verify Care plan page

  Scenario: To verify Add and delete a Care Plan from HCP web
    When Login into dashboard
    And Navigate to patient under my patients
    Then Verify Add a Care plan
    And Verify Delete a Care plan

  @TestCaseKey=HOHP-T295 @TestCaseKey=HOHP-T296
  Scenario: To verify Add and Delete Care Plan on Patient app
    When Login into dashboard
    And Launch the "HH" App with noReset
    Then Navigate to patient under my patients
    Then Verify Add a Care plan
    Then Verify Added Care plan displayed in Patient App
    And Verify Delete a Care plan
    Then Verify Deleted Care plan displayed in Patient App

  @TestCaseKey=HOHP-T298
  Scenario: Try to add care plan for other patients
    When Login into dashboard
    Then Navigate to patient under all patients
    And Adding a care plan should not be applicable for other patients

  @TestCaseKey=HOHP-T279
  Scenario: To verify Activity Page details under my patients
    When Login into dashboard
    And Navigate to patient under my patients
    Then Verify Activity Page details of clinic

  @TestCaseKey=HOHP-T279
  Scenario: To verify Activity Timeline and filter options under my patients
    When Login into dashboard
    Then Navigate to patient under my patients
    Then Verify Activity Page details of clinic

  @TestCaseKey=HOHP-T281 @TestCaseKey=HOHP-T287 @TestCaseKey=HOHP-T288
  Scenario: To verify Add, Update and Delete an Activity
    When Login into dashboard
    And Launch the "HH" App with noReset
    Then Navigate to patient under my patients
    Then Verify Add an Activity "Drink water"
      | No of Glass |
      | 2           |
    And Verify added activity "Drink water" in patient app
    Then Verify Update an Activity "Drink water"
    And Verify updated activity "Drink water" in patient app
    Then Verify Delete an Activity "Drink water"
    And Verify deleted activity "Drink water" in patient app

    ## Diary Note ##
  @TestCaseKey=HOHP-T154 @TestCaseKey=HOHP-T343 @TestCaseKey=HOHP-T354
  Scenario: To verify app and web diary screens when no note available
    When Login into dashboard
    And Launch the "HH" App with noReset
    And Navigate to patient under my patients
    Then Verify HCP web patient diary content when no note available
    And Navigate to patient under all patients
    Then Verify HCP web patient diary content when no note available
    And Navigate to Diary and verify components in patient app

  @TestCaseKey=HOHP-T158 @TestCaseKey=HOHP-T157
  Scenario: To verify add note by missing required field and cancel
    When Login into dashboard
    And Launch the "HH" App with noReset
    Then Verify mandatory fields while add new note
    And Verify canceling note after adding details

  @TestCaseKey=HOHP-T156 @TestCaseKey=HOHP-T344
  Scenario: Verify adding a new note in Diary screen by selecting all, excluding optional fields
    When Login into dashboard
    And Launch the "HH" App with noReset
    Then Verify add a new diary note with required fields in patient app
    Then Navigate to patient under my patients
    Then Verify added diary note in with required fields hcp web
    Then Verify update diary note with required fields in patient app
    And Verify updated diary note in with required fields hcp web

  @TestCaseKey=HOHP-T155 @TestCaseKey=HOHP-T344 @TestCaseKey=HOHP-T347
  Scenario: Verify adding a new note in Diary screen by selecting all, excluding optional fields
    When Login into dashboard
    And Launch the "HH" App with noReset
    Then Verify add a new diary note with all fields in patient app
    Then Navigate to patient under my patients
    Then Verify added diary note in with all fields hcp web
    Then Verify update diary note with all fields in patient app
    And Verify updated diary note in with all fields hcp web

  @TestCaseKey=HOHP-T352 @TestCaseKey=HOHP-T353
  Scenario: To verify respond a Diary Note from HCP web
    When Login into dashboard
    And Launch the "HH" App with noReset
    Then Verify add a new diary note with required fields in patient app
    Then Navigate to patient under my patients
    Then Verify respond diary note from hcp web
    And Verify response on diary note in patient app
    Then Verify patient can't edit responded diary notes
    And Verify add a new diary note with required fields in patient app
    Then Navigate to patient under my patients
    When Unacknowledged only option selected
    Then Only Unacknowledged notes should be visible

  @TestCaseKey=HOHP-T348 @TestCaseKey=HOHP-T349 @TestCaseKey=HOHP-T350 @TestCaseKey=HOHP-T351
  Scenario: To Verify calendar functionality of Diary Note in HCP web
    When Login into dashboard
    And Launch the "HH" App with noReset
    Then Verify add a new diary note with all fields in patient app
    And Navigate to patient under my patients
    Then Verify calendar functionality
    Then Verify filter functionality

    ## Hand Off ##
  @TestCaseKey=HOHP-T676 @TestCaseKey=HOHP-T677 @TestCaseKey=HOHP-T678 @TestCasekey=HOHP-T679 @TestCasekey=HOHP-T680 @TestCaseKey=HOHP-T684 @TestCaseKey=HOHP-T685 @TestCasekey=HOHP-T686
  Scenario: Check all section is mandatory for handoff submission
    When Login into dashboard
    Then Search patient under my patients
    And Verify handoff container details and mandatory sections
    Then Verify adding HandOff with "Stable" Illness severity
    And Navigate to patient under my patients
    Then Verify HandOff details after successfully HandOff patient
    And Logout user
    Then Login nurse into dashboard
    And Navigate to patient under my patients
    Then Verify HandOff details after successfully HandOff patient
    And Search patient under my patients
    Then Remove this patient from my patients
    And Navigate to removed patient under all patients and Verify HandOff details should be read only mode
    And Logout user
    Then Login into dashboard
    And Navigate to patient under my patients
    Then Update HandOff details with "Unstable" Illness severity
    And Verify decline reset No change priory visit
    Then Verify HandOff details after successfully HandOff patient
    Then Update HandOff details with "Watcher" Illness severity
    And Verify accept reset No change priory visit
    Then Verify Nothing should change after decline reset

  @TestCasekey=HOHP-T681 @TestCaseKey=HOHP-T682 @TestCaseKey=HOHP-T683
  Scenario: To verify multiple and customize of awareness
    When Login into dashboard
    And Navigate to patient under my patients
    And Verify all Illness severity are selectable and highlighted after selected
    Then Verify multi select awareness options
    And Verify customize option of awareness

     ## Setting theres hold values ##
  @TestCaseKey=HOHP-T304 @TestCaseKey=HOHP-T307 @TestCaseKey=HOHP-T308 @TestCaseKey=HOHP-T309
  Scenario:Verify set therese hold value container, default values, set and reset values
    When Login into dashboard
    And Navigate to patient under my patients
    Then verify therese hold option and container
    And verify default therese hold values
    Then verify closing therese hold container

  @TestCaseKey=HOHP-T305 @TestCaseKey=HOHP-T565 @TestCaseKey=HOHP-T306
  Scenario: Verify the confirmation message while resetting the threshold limit
    When Login into dashboard
    And Navigate to patient under my patients
    And Verify setting therese hold values
    Then Verify reset therese hold values

  @TestCaseKey=HOHP-T311 @TestCaseKey=HOHP-T312 @TestCaseKey=HOHP-T313 @TestCaseKey=HOHP-T314
  Scenario:Verify open/close the alert popup, badge count and empty screen
    When Login into dashboard
    And Navigate to patient under my patients
    Then verify alert option on individual patient page
    And verify badge count when no alert

  @TestCaseKey=HOHP-T306 @TestCasekey=HOHP-T315 @TestCasekey=HOHP-T316
  Scenario:Verify that the Event is triggering or not on set threshold values
    When Login into dashboard
    And Navigate to patient under my patients
    Then verify alert after set therese hold values for vital "High HR" as 100 and sync for 30 minutes with "Everion"
    And verify resolving an alert and status after resolved

  @TestCaseKey=HOHP-T1297 @TestCasekey=HOHP-T1298 @TestCaseKey=HOHP-T1300
  Scenario: Verify global event alert, badge count and resolve it
    When Login into dashboard
    And Navigate to patient under my patients
    Then verify alert badge count and triggered event should be visible in global alert
    And Navigate to patient under all patients
    Then verify alert badge count and triggered event should not be visible in global alert

  @TestCasekey=HOHP-T310 @TestCaseKey=HOHP-T317 @TestCaseKey=HOHP-T1299
  Scenario: Try to set the threshold and resolve alert for other patient in All patient list
    When Login into dashboard
    And Navigate to patient under all patients
    Then verify set therese hold values for un-assigned patient
    And verify alert for vital "High HR" with "Everion"

      ## Tasks ##
  Scenario: To verify task container and cancel options for creation of task
    When Login into dashboard
    Then Navigate to patient under my patients
    And Verify closing task container
    Then Verify Cancel button functionality

  Scenario: To verify Add, Edit, Comment and Complete a task with required fields on patient details page
    When Login into dashboard
    Then Navigate to patient under my patients
    And Verify create new task with Required fields
    Then Verify comment on created task
    And Verify edit a task with Required fields
    Then Verify complete a task
    And Verify comment on completed task
    Then Verify re-open completed task
    And Verify comment on re-opened task
    Then Verify edit re-open task with All fields
    Then Verify complete a re-open task

  Scenario: To verify Add, Edit, Comment and Complete a task with all fields on patient details page
    When Login into dashboard
    Then Navigate to patient under my patients
    And Verify create new task with All fields
    Then Verify comment on created task
    And Verify edit a task and remove optional fields
    Then Verify complete a task
    And Verify comment on completed task
    Then Verify re-open completed task
    And Verify comment on re-opened task
    Then Verify edit re-open task with All fields
    Then Verify complete a re-open task

  Scenario: To verify Add, Edit, Comment and Complete a task with required/all fields from tasks LHN menu
    When Login into dashboard
    Then Navigate to Tasks tab on HCP web dashboard
    And Verify create new task with Required fields on tasks tab
    Then Verify comment on created task on tasks tab
    And Verify edit a task with Required fields on tasks tab
    Then Verify complete a task on task tab
    And Verify comment on completed task on tasks tab
    Then Verify re-open completed task on tasks tab
    And Verify comment on re-opened task on tasks tab
    Then Verify edit re-open task with All fields on tasks tab
    Then Verify complete a re-open task on task tab

        ## Inventory
  Scenario: To verify Add a device in inventory
    When Login into dashboard
    Then Navigate to Inventory tab on HCP web dashboard
    And Add new device "AED" in inventory
    And Assign device to caregiver
    And Delete device

    ## Chat ##
  @TestCaseKey=HOHP-T319
  Scenario: To verify send single text message to patient
    When Login into dashboard
    Then Search patient under my patients
    And Verify navigate to chat section by clicking on chat icon on patient home screen
    Then Verify sending 1 text message from HCP web dashboard
    Then Navigate to patient under my patients
    And Verify navigate to chat section by clicking on chat icon on patient details screen
    Then Verify sending 2 text message from HCP web dashboard

  @TestCaseKey=HOHP-T330
  Scenario: To verify send single text message to patient
    When Login into dashboard
    Then Search patient under my patients
    And Verify start video call on patient home screen and cut
    Then Navigate to patient under my patients
    And Verify start voice call on patient details screen and cut

  @TestCaseKey=HOHP-T326
  Scenario: Minimize and maximize the video/audio screen
    When Login into dashboard
    Then Search patient under my patients
    And Start voice call
    Then Verify minimize and maximize chat container
    Then Navigate to patient under my patients
    And Start voice call
    Then Verify minimize and maximize chat container

  @TestCaseKey=HOHP-T327
  Scenario: Try to drag the call popup in vital screen
    When Login into dashboard
    Then Search patient under my patients
    And Start voice call
    Then Verify perform action without minimize the call container
    And Minimize the container and drag on other location
    Then Perform action after minimize the call container

  @TestCaseKey=HOHP-T328 @TestCaseKey=HOHP-T329
  Scenario: Navigate to web screens while doing video/audio call
    When Login into dashboard
    Then Search patient under my patients
    And Start voice call
    And Minimize the call container
    Then Verify Video and voice call icons are disable while clinician in call
    And Verify sending text message from HCP web dashboard

  @TestCaseKey=HOHP-T328 @TestCaseKey=HOHP-T329
  Scenario: Navigate to web screens while doing video/audio call
    When Login into dashboard
    Then Search patient under my patients
    And Start voice call
    And Minimize the call container
    And Navigate to patient under all patients
    Then Verify video, voice call and chat icons are disable for unassigned patients while clinician in call

  @TestCaseKey=HOHP-T331 @TestCaseKey=HOHP-T332
  Scenario: Verify the chat, audio and video call option in patient list screen for other patient
    When Login into dashboard
    Then Navigate to All patient screen
    And Search patient under all patients without refresh
    Then Verify video, voice call and chat icons are invisible for unassigned patients while clinician in call
    And Navigate to patient under all patients
    Then Verify video, voice call and chat icons are disable for unassigned patients while clinician in call

#  @TestCaseKey=HOHP-T325
#  Scenario: Try to do video call
#    When Login into dashboard
#    Given Launch the "HH" App with noReset
#    Then Navigate to patient under my patients
#    And Verify start Video call on patient details screen
#    Then Receive Video call on patient app
#    And Verify Accept call from patient app
#    Then Verify call Accepted successfully in HCP web dashboard
#    And Verify video call container details in HCP web dashboard

  @TestCaseKey=HOHP-T322
  Scenario: Try to send chat
    When Login into dashboard
    Then Launch the "HH" App with noReset
    And Search patient under my patients
    And Verify navigate to chat section by clicking on chat icon on patient home screen
    Then Verify sending 1 text message from HCP web dashboard
    And Verify text message pop-up displayed in patient and click Later
    And Verify chat count badge in patient app
    And Verify received text message from HCP clinician

  @TestCaseKey=HOHP-T321 @TestCaseKey=HOHP-T320
  Scenario: Try to read the received chat
    When Login into dashboard
    Then Launch the "HH" App with noReset
    And Verify sending 2 text message from Patient app to clinician
    Then Search patient under my patients
    And Verify badge count and chat on HCP web dashboard
    Then Navigate to patient under my patients
    Then Again send 1 text message from Patient app to clinician
    And Verify badge count and chat on HCP web dashboard

  @TestCaseKey=HOHP-T333
  Scenario: Try to receive the call
    When Login into dashboard
    Then Launch the "HH" App with noReset
    And Navigate to clinician contact
    And Start Audio call from patient app
    Then Receive Audio call on HCP web dashboard
    And Verify Accept call from HCP web dashboard

  @TestCaseKey=HOHP-T334
  Scenario: Try to decline the call
    When Login into dashboard
    Then Launch the "HH" App with noReset
    And Navigate to clinician contact
    And Start Video call from patient app
    Then Receive Video call on HCP web dashboard
    And Verify Reject call from HCP web dashboard
    Then Verify call Rejected successfully in patient app

  @TestCaseKey=HOHP-T335
  Scenario: Verify the call connectivity by staying minimum 1 min in call
    When Login into dashboard
    Then Launch the "HH" App with noReset
    And Navigate to clinician contact
    And Start Audio call from patient app
    Then Receive Audio call on HCP web dashboard
    And Verify Accept call from HCP web dashboard
    Then Verify call connectivity more than 1 min

     ## Patient app
  Scenario: Validate settings screen and Logout into HH Patient app
    And Launch the "HH" App with noReset
    Then Navigate to Hidden settings screen
    And Verify Hidden setting screen elements

   ## Patient app
  Scenario: Login/Logout into HH Patient app
    And Launch the "HH" App with noReset
    Then Logout patient

   ## Add Remove Patient
  Scenario: To verify Add/Remove patient from my patients
    When Login into dashboard
    Then Verify add and remove patient form my patients