Feature: HCP web [Clinical Notes] flow verification

  Background:
    Given Launch the browser
    Then Login into dashboard

  Scenario: Prerequisite for automation
    And Add a new patient

    @TestCaseKey=HOHP-T982 @TestCaseKey=HOHP-T983
    Scenario: To verify Clinical Notes page and mandatory fields error messages
      And Navigate to patient under my patients
      Then Verify clinical notes page
      And Verify empty fields error message

    @TestCaseKey=HOHP-T983 @TestCaseKey=HOHP-T984 @TestCaseKey=HOHP-T985
    Scenario: To verify Add, Update, Delete and Search a Clinical note
      Then Navigate to patient under my patients
      And Verify Add a note
      Then Verify update a note
      Then Verify Delete a note

    @TestCaseKey=HOHP-T986 @TestCaseKey=HOHP-T987 @TestCaseKey=HOHP-T988 @TestCaseKey=HOHP-T999 @TestCaseKey=HOHP-T990
    Scenario:Verify Search, Calendar and Caregiver filter options
      Then Navigate to patient under my patients
      And Verify Add a note
      Then Verify Search filter input field
      And Verify Calendar filter option
      And Verify Caregiver filter option
      And Verify Calendar and Caregiver filter options together and Clear all

    @TestCaseKey=HOHP-T991
    Scenario: To verify Add, Update, Delete and Search a Clinical note
      Then Navigate to patient under all patients
      And Verify Add a note
      Then Verify update a note
      Then Verify Delete a note

    @TestCaseKey=HOHP-T992
    Scenario: Verify that caregiver is not able to edit/delete clinical notes of the patient which have been created by other caregivers
      Then Navigate to patient under my patients
      And Verify Add a note
      Then Logout user
      And Login nurse into dashboard
      Then Navigate to patient under my patients
      And Verify edit and delete option not visible