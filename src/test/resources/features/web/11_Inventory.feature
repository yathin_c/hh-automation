Feature: HCP web patient [Inventory] functionality verification

  Background:
    Given Launch the browser
    And Login into dashboard

  Scenario: To verify Add a device in inventory
    Then Navigate to Inventory tab on HCP web dashboard
    And Add new device "AED - [Care Team]" in inventory
    And Assign device to caregiver
    And Delete device