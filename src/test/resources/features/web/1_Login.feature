Feature: HCP web authentication flow to verify the validation error messages while caregiver login

  Background:
    Given Launch the browser

  Scenario: Verify texts displayed on Login page
    Then Verify login screen texts

  @TestCaseKey=HOHP-T216
  Scenario: Verify the functionality of Login button when valid email and password is entered.
    Then Verify caregiver login successfully
    And Logout user

  Scenario: Verify login when user enters email in uppercase
    Then Verify caregiver login successfully when the username in uppercase
    And Logout user

  Scenario: Verify if login button is enabled when valid email and password fields are entered
    When Enter the username and password
    Then Verify the login button should be enabled

  @TestCaseKey=HOHP-T220
  Scenario: Verify if login button is disabled
    Then Verify disabled login button

  Scenario: Verify invalid format of email address and error message
    Then Verify invalid username format
      | random    |
      | random@   |
      | 123       |
      | 123@      |
      | @mail.com |

  Scenario: Verify invalid format of password and error message
    Then Verify invalid password format
      | 12        |
      | Car       |
      | C@        |
      | Care@care |
      | care@1234 |
      | Care12345 |

  @TestCaseKey=HOHP-T221
  Scenario: Verify email required message
    Then Verify "email" required i.e. "Email address is required"

  @TestCaseKey=HOHP-T221
  Scenario: Verify password required message
    Then Verify "password" required i.e. "Password is required"

  Scenario: Verify if the entered password characters are masked when mask password button (or eye button) is selected
    When Enter the username and password
    Then Verify the password should be in masked form

  Scenario: Verify if the entered password characters are displayed when unmask password button is selected
    When Enter the username and password
    And Click on the eye button of password and verify the password

  @TestCaseKey=HOHP-T230
  Scenario: Verify the login functionality when user enters invalid mail id and password then error message should be displayed saying "your email id and password is not valid, you have 2 login attempts remaining"
    When Enter the username as "testdata@mail.com" and password as "Care@1235"
    And After enter username and password click 6 time on the login button to see error message

  @TestCaseKey=HOHP-T218 @TestCaseKey=HOHP-T237
  Scenario: Verify the login functionality by entering invalid email and password
    When Enter the username as "Invalid@mail.com" and password as "Invalid@Care"
    And Click on the login button
    Then Verify the error message after invalid email and password

  @TestCaseKey=HOHP-T217
  Scenario:Verify that clicking on browser back button after successful login should not take User to logout screen
  Then Verify caregiver login successfully
  And Verify User is not logged out after clicking on Back button

  @TestCaseKey=HOHP-T227
  Scenario:Verify that User is Logged out from old window when User logs intoNew window
  And Login into dashboard
  Then Verify User is logged out from old window after logging into new Window
