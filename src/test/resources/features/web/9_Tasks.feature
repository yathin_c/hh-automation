Feature: HCP web [Tasks] functionality verification

  Background:
    Given Launch the browser
    And Login into dashboard

  Scenario: To verify task container and cancel options for creation of task
    Then Navigate to patient "MRN1000" under my patients
    And Verify closing task container
    Then Verify Cancel button functionality

  Scenario: To verify Add, Edit, Comment and Complete a task with required fields on patient details page
    Then Navigate to patient "MRN1000" under my patients
    And Verify create new task with Required fields
    Then Verify comment on created task
    And Verify edit a task with Required fields
    Then Verify complete a task
    And Verify comment on completed task
    Then Verify re-open completed task
    And Verify comment on re-opened task
    Then Verify edit re-open task with All fields
    Then Verify complete a re-open task

  Scenario: To verify Add, Edit, Comment and Complete a task with all fields on patient details page
    Then Navigate to patient "MRN1000" under my patients
    And Verify create new task with All fields
    Then Verify comment on created task
    And Verify edit a task and remove optional fields
    Then Verify complete a task
    And Verify comment on completed task
    Then Verify re-open completed task
    And Verify comment on re-opened task
    Then Verify edit re-open task with All fields
    Then Verify complete a re-open task

  Scenario: To verify Add, Edit, Comment and Complete a task with required/all fields from tasks LHN menu
    Then Navigate to Tasks tab on HCP web dashboard
    And Verify create new task with Required fields on tasks tab
    Then Verify comment on created task on tasks tab
    And Verify edit a task with Required fields on tasks tab
    Then Verify complete a task
    And Verify comment on completed task on tasks tab
    Then Verify re-open completed task on tasks tab
    And Verify comment on re-opened task on tasks tab
    Then Verify edit re-open task with All fields on tasks tab
    Then Verify complete a re-open task