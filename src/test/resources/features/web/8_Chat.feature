Feature: HCP web video, voice and chat verification

  Background:
    Given Launch the browser
    And  Login into dashboard

  Scenario: Prerequisite
    When Add a new patient
    Then Launch the "HH" App with reset
    And Scan QR and login into app

  @TestCaseKey=HOHP-T319
  Scenario: To verify send single text message to patient
    Then Search patient under my patients
    And Verify navigate to chat section by clicking on chat icon on patient home screen
    Then Verify sending 1 text message from HCP web dashboard
    Then Navigate to patient under my patients
    And Verify navigate to chat section by clicking on chat icon on patient details screen
    Then Verify sending 2 text message from HCP web dashboard

  @TestCaseKey=HOHP-T330
  Scenario: To verify send single text message to patient
    Then Search patient under my patients
    And Verify start video call on patient home screen and cut
    Then Navigate to patient under my patients
    And Verify start voice call on patient details screen and cut

  @TestCaseKey=HOHP-T326
  Scenario: Minimize and maximize the video/audio screen
    Then Search patient under my patients
    And Start voice call
    Then Verify minimize and maximize chat container
    Then Navigate to patient under my patients
    And Start voice call
    Then Verify minimize and maximize chat container

  @TestCaseKey=HOHP-T327
  Scenario: Try to drag the call popup in vital screen
    Then Search patient under my patients
    And Start voice call
    Then Verify perform action without minimize the call container
    And Minimize the container and drag on other location
    Then Perform action after minimize the call container

  @TestCaseKey=HOHP-T328 @TestCaseKey=HOHP-T329
  Scenario: Navigate to web screens while doing video/audio call
    Then Search patient under my patients
    And Start voice call
    And Minimize the call container
    Then Verify Video and voice call icons are disable while clinician in call
    And Verify sending text message from HCP web dashboard

  @TestCaseKey=HOHP-T328 @TestCaseKey=HOHP-T329
  Scenario: Navigate to web screens while doing video/audio call
    Then Search patient under my patients
    And Start voice call
    And Minimize the call container
    And Navigate to patient under all patients
    Then Verify video, voice call and chat icons are disable for unassigned patients while clinician in call

  @TestCaseKey=HOHP-T331 @TestCaseKey=HOHP-T332
  Scenario: Verify the chat, audio and video call option in patient list screen for other patient
    Then Navigate to All patient screen
    And Search patient under all patients without refresh
    Then Verify video, voice call and chat icons are invisible for unassigned patients while clinician in call
    And Navigate to patient under all patients
    Then Verify video, voice call and chat icons are disable for unassigned patients while clinician in call

  @TestCaseKey=HOHP-T325
  Scenario: Try to do video call
    Given Launch the "HH" App with noReset
    Then Navigate to patient under my patients
    And Verify start Video call on patient details screen
    Then Receive Video call on patient app
    And Verify Accept call from patient app
    Then Verify call Accepted successfully in HCP web dashboard
    And Verify video call container details in HCP web dashboard

  @TestCaseKey=HOHP-T322
  Scenario: Try to send chat
    Given Launch the "HH" App with noReset
    Then Search patient under my patients
    And Verify navigate to chat section by clicking on chat icon on patient home screen
    Then Verify sending 1 text message from HCP web dashboard
    And Verify text message pop-up displayed in patient and click Later
    And Verify chat count badge in patient app
    And Verify received text message from HCP clinician

  @TestCaseKey=HOHP-T321 @TestCaseKey=HOHP-T320
  Scenario: Try to read the received chat
    Given Launch the "HH" App with noReset
    Then Verify sending 2 text message from Patient app to clinician
    Then Search patient under my patients
    And Verify badge count and chat on HCP web dashboard
    Then Navigate to patient under my patients
    Then Again send 1 text message from Patient app to clinician
    And Verify badge count and chat on HCP web dashboard

  @TestCaseKey=HOHP-T333
  Scenario: Try to receive the call
    Given Launch the "HH" App with noReset
    Then Navigate to clinician contact
    And Start Audio call from patient app
    Then Receive Audio call on HCP web dashboard
    And Verify Accept call from HCP web dashboard

  @TestCaseKey=HOHP-T334
  Scenario: Try to decline the call
    Given Launch the "HH" App with noReset
    Then Navigate to clinician contact
    And Start Video call from patient app
    Then Receive Video call on HCP web dashboard
    And Verify Reject call from HCP web dashboard
    Then Verify call Rejected successfully in patient app

  @TestCaseKey=HOHP-T335
  Scenario: Verify the call connectivity by staying minimum 1 min in call
    Given Launch the "HH" App with noReset
    Then Navigate to clinician contact
    And Start Audio call from patient app
    Then Receive Audio call on HCP web dashboard
    And Verify Accept call from HCP web dashboard
    Then Verify call connectivity more than 1 min