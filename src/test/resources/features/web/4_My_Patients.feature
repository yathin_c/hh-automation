Feature: HCP web [My Patient] features verification flow

  Background:
    Given Launch the browser
    And Login into dashboard

  @TestCaseKey=HOHP-T248
  Scenario: Home screen should show 'My patient' list by default
    Then Verify My patient list displayed

  @TestCaseKey=HOHP-T264 @TestCaseKey=HOHP-T265
  Scenario: Verify the search input field
    Then Verify search input field is clickable, accept input and clear by clicking on cross(X) icon
    And Check the input data for search component

  @TestCaseKey=HOHP-T271  @TestCaseKey=HOHP-T275
  Scenario: Navigate to view QR code screen from patient list
    Then Verify QR code displayed by navigating through overflow menu
    And Try to close the QR code popup page

  @TestCaseKey=HOHP-T266
  Scenario: Check for 'All Patient' list
    Then User should navigate successfully to All patient screen
    And All patient should show list of My patient and Other patient