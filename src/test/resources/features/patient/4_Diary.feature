Feature: Patient app [Diary notes] feature validation

  Background:
    Given Launch the "HH" App with noReset
    And Launch the browser
    And Login into dashboard

  Scenario: Prerequisite for automation
    Then Launch the "HH" App with reset
    Then Add a new patient
    And Scan QR and login into app

    # //The previous QR code has expired. Please generate a new code to proceed.
    #        // Generate a new code

  @TestCaseKey=HOHP-T154 @TestCaseKey=HOHP-T343 @TestCaseKey=HOHP-T354
  Scenario: To verify app and web diary screens when no note available
    And Navigate to patient under my patients
    Then Verify HCP web patient diary content when no note available
    And Navigate to patient under all patients
    Then Verify HCP web patient diary content when no note available
    And Navigate to Diary and verify components in patient app

  @TestCaseKey=HOHP-T158 @TestCaseKey=HOHP-T157
  Scenario: To verify add note by missing required field and cancel
    Then Verify mandatory fields while add new note
    And Verify canceling note after adding details

  @TestCaseKey=HOHP-T156 @TestCaseKey=HOHP-T344
  Scenario: Verify adding a new note in Diary screen by selecting all, excluding optional fields
    Then Verify add a new diary note with required fields in patient app
    Then Navigate to patient under my patients
    Then Verify added diary note in with required fields hcp web
    Then Verify update diary note with required fields in patient app
    And Verify updated diary note in with required fields hcp web

  @TestCaseKey=HOHP-T155 @TestCaseKey=HOHP-T344 @TestCaseKey=HOHP-T347
  Scenario: Verify adding a new note in Diary screen by selecting all, excluding optional fields
    Then Verify add a new diary note with all fields in patient app
    Then Navigate to patient under my patients
    Then Verify added diary note in with all fields hcp web
    Then Verify update diary note with all fields in patient app
    And Verify updated diary note in with all fields hcp web

  @TestCaseKey=HOHP-T352 @TestCaseKey=HOHP-T353
  Scenario: To verify respond a Diary Note from HCP web
    Then Verify add a new diary note with required fields in patient app
    Then Navigate to patient under my patients
    Then Verify respond diary note from hcp web
    And Verify response on diary note in patient app
    Then Verify patient can't edit responded diary notes
    And Verify add a new diary note with required fields in patient app
    Then Navigate to patient under my patients
    When Unacknowledged only option selected
    Then Only Unacknowledged notes should be visible

  @TestCaseKey=HOHP-T348 @TestCaseKey=HOHP-T349 @TestCaseKey=HOHP-T350 @TestCaseKey=HOHP-T351
  Scenario: To Verify calendar functionality of Diary Note in HCP web
    And Navigate to patient under my patients
    Then Verify calendar functionality
    Then Verify filter functionality
